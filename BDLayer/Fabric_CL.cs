﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace BDLayer
{
  public  class Fabric_CL
    {
        string _Conn;
        public Fabric_CL()
        {
            _Conn = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        }
        public void Save(Fabric  y)
        {
            
            DBAccessDataContext db = new DBAccessDataContext();
             
            db.Connection.ConnectionString = _Conn;
            db.Insert_Fabric_SP(y.FID,y.GID, y.Fabric1, y.GM,y.GSM, y.EntryID);
                   
            db.SubmitChanges();
                    

        }
        public void SaveProduction(FabricProduction p,List<EYarnConsumption> liwi)
        {

            string res = "";
            string strXML = DataSerialization.SerializeObject<List<EYarnConsumption>>(liwi);
           

            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Insert_FabricProduction_SP(p.SL, p.FID, p.MCNo,p.OrderNo, p.PDate, p.OperatorID, p.Niddle, p.Shift, p.ProductionQty, p.EntryID,strXML,ref res);

            


            db.SubmitChanges();
            if (res == "1")
            {

            }


        }
        public void SaveDelivery(FabricDelivery d)
        {

            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Insert_FabricDelivery_SP(d.SL, d.FID, d.DQty, d.PCardId,d.MCNo,d.RollNo, d.DDate, d.EntryID);
            //db.Insert_YarnPurchase_SP(y.SL, y.YID, y.PurchaseType, y.Qty, y.PDate, y.PurchaseFrom, y.Challan, y.StockTo, y.EntryID);
            

            db.SubmitChanges();


        }
        public IQueryable GetActiveFabric()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Fabrics
                     from q in db.FabricGroups
                     where p.EMode == "E" && p.GID==q.SL orderby p.FID descending
                     //select p;
                     select new
                     {
                         
                         p.FID,
                         p.GID,
                         p.Fabric1,
                         p.GM,
                         p.GSM,
                         p.EMode,
                         p.EntryDate,
                         p.EntryID,
                         q.FGroupName

                     };

            return pr;
        }
        public IQueryable GetActiveProduction()
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;
      
            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.FabricProductions
                     from f in db.Fabrics
                     where p.EMode == "E" && f.FID == p.FID
                     orderby p.SL descending
                     //select p;
                     select new {
                      p.SL,p.FID,p.ProductionQty,p.Shift,p.Niddle,p.PDate,
                      p.EMode,p.EntryDate,p.EntryID,p.OperatorID,p.MCNo,
                      f.Fabric1,f.GM,f.GSM,p.OrderNo
                         
                     };

            return pr.Take(200);
        }
        public IQueryable GetActiveDelivery()
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.FabricDeliveries
                     from f in db.Fabrics
                     where p.EMode == "E" && f.FID == p.FID
                     orderby p.SL descending
                     select new
                     {
                      p.SL,p.FID,p.DQty,p.PCardId,p.EMode,p.EntryDate,p.EntryID,
                      f.Fabric1,
                      f.GM,
                      f.GSM,p.RollNo
                     };
                     

            return pr.Take(200);
        }

        public IQueryable GetAllFabricGroup()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.FabricGroups
                     
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetDeletedFabrics()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Fabrics
                     where p.EMode == "D"
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetDeletedProduction()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.FabricProductions
                     from f in db.Fabrics
                     where p.EMode == "D" && f.FID == p.FID
                     //select p;
                     select new
                     {
                         p.SL,
                         p.FID,
                         p.ProductionQty,
                         p.Shift,
                         p.Niddle,
                         p.PDate,
                         p.EMode,
                         p.EntryDate,
                         p.EntryID,
                         p.OperatorID,
                         p.MCNo,
                         f.Fabric1,
                         f.GM,
                         f.GSM

                     };

            return pr.Take(200);
        }
        public IQueryable GetDeletedDelivery()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.FabricDeliveries
                     from f in db.Fabrics
                     where p.EMode == "D" && f.FID == p.FID
                     orderby p.SL descending
                     select new
                     {
                         p.SL,
                         p.FID,
                         p.DQty,
                         p.PCardId,
                         p.EMode,
                         p.EntryDate,
                         p.EntryID,
                         f.Fabric1,
                         f.GM,
                         f.GSM
                     };

            return pr.Take(200);
        }
        public void DeleteActiveFabric(long custid,string mo,string eid)
        {
           
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "FABRIC");
            db.SubmitChanges();


        }
        public void ProductionDeleteActive(long custid, string mo, string eid)
        {
            
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "FABRIC_PRODUCTION");
            db.SubmitChanges();


        }
        public void DeliveryDeleteActive(long custid, string mo, string eid)
        {
          
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "FABRIC_DELIVERY");
            db.SubmitChanges();


        }
        public void IssueToDeyingDeleteActive(long custid, string mo, string eid)
        {

            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.DELETE_GREY_ISSUE_TO_DYEING(custid,eid);
            db.SubmitChanges();


        }
        public IQueryable GetAlFabric()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Fabrics
                     where p.EMode == "E" orderby p.Fabric1
                     //select p;
                     select new 
                     {
                         
                      p.FID    ,
                      fab = p.Fabric1 
                     };

            return pr;
        }
        public IQueryable GetAlFabricbyGRoup(Int32 gid)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Fabrics
                     where p.EMode == "E" && p.GID==gid
                     orderby p.Fabric1
                     //select p;
                     select new
                     {

                         p.FID,
                         fab = p.Fabric1 ,
                         p.Fabric1
                     };

            return pr;
        }
        public IQueryable GetAlOperator()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Operators
                     where p.EMode == "E"
                     select p;
                    

            return pr;
        }
    }
  public class EYarnConsumption
  {

      public Int32 YID { get; set; }


      public double QtyPer { get; set; }
      public double QtyKG { get; set; }

  }
}
