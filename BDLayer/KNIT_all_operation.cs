﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Collections;

namespace BDLayer
{
   public class KNIT_all_operation
    {
        string _Conn;
        public KNIT_all_operation()
        {
            _Conn = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        }

        public ProcessCard GetSinglePCardInfo(string ProcessCardNo )
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            return (from p in db.ProcessCards

                     where p.Emode == "E" && p.PCardNo == ProcessCardNo

                     select p).SingleOrDefault();

          

        }

        public List<KNIT_GREY_ROLL_RCV_From_SubCon_GETResult> KNIT_PROD_ROLL_SubContact_GET(int assignID)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            return (from p in db.KNIT_GREY_ROLL_RCV_From_SubCon_GET(assignID)

                    select p).ToList<KNIT_GREY_ROLL_RCV_From_SubCon_GETResult>();



        }

        public bool KNIT_PROD_ROLL_SubContact_INS(KNIT_GREY_ROLL_RCV_From_SubCon_GETResult ob)
        {
           

            try
            {
                DBAccessDataContext db = new DBAccessDataContext();


                db.Connection.ConnectionString = _Conn;
                db.KNIT_GREY_ROLL_RCV_From_SubCon_INS(ob.AssignMachineID,ob.RollNo,ob.RollQty,ob.PDate,ob.SubContactName,ob.Notes,ob.Challan);

                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }



        }
        public bool KNIT_PROD_ROLL_SubContact_Delete(int ID)
        {


            try
            {
                DBAccessDataContext db = new DBAccessDataContext();


                db.Connection.ConnectionString = _Conn;
                db.KNIT_GREY_ROLL_RCV_From_SubCon_Delete(ID);

                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }



        }
       public double GetOrderProductBalance(Int32 OPID)
        {
            
            try
            {
                DBAccessDataContext db = new DBAccessDataContext();
                
                db.Connection.ConnectionString = _Conn;
                var pr = (from p in db.KNIT_getOrderProductBalance(OPID)

                         select p).SingleOrDefault();

                return Convert.ToDouble( pr.BookingQty);

                
            }
            catch
            {
                return 0;
            }

            

        }
       public List<KNIT_Prod_AssignMachine> GetListOfMCByRefNo(string ReqNO)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           return (from p in db.KNIT_Prod_Setups
                    from q in db.KNIT_Prod_AssignMachines
                    where p.ID==q.KnitSetupID && p.SetupCode==ReqNO  
                     select q).ToList < KNIT_Prod_AssignMachine>();

       }

       public Fabric GetFabircDetailsByReqNo(string ReqNO)
       {


           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var k= (from p in db.KNIT_Prod_Setups
                   from f in db.Fabrics
                   where p.FabricID == f.FID && p.SetupCode == ReqNO
                   select new {p.FabricID,f.Fabric1,p.GM,p.GSM }
                   
                   );

          
           Fabric ob = new Fabric();
           foreach(var val in k)
           {
               
               ob.Fabric1 = val.Fabric1;
               ob.FID = val.FabricID??0;
               ob.GM = val.GM;
               ob.GSM = val.GSM;
              
           }
           return ob;
       }
       public List<ERequisition> GetReqNoByColor(int OPDID)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.ProcessCards

                    where p.OPDID == OPDID
                    select new {  p.ReqNo };



           List<ERequisition> liStr = new List<ERequisition>();
           ERequisition ob = new ERequisition();
           ob.ReqNo = "---Select Req No----";
           liStr.Add(ob);
           foreach (var temp in pr.GroupBy(x => x.ReqNo).Select(g => g.First()))
           {
               if (temp.ReqNo != null)
               {
                   ob = new ERequisition();
                   ob.ReqNo = temp.ReqNo;
                   liStr.Add(ob);
                   

               }
            
           }


           return liStr;

       }
       public Fabric getFabricByReqNo(string ReqNo)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_Prod_Setups
                    from f in db.Fabrics 
                    where p.FabricID==f.FID && p.SetupCode==ReqNo
                    select f;

           Fabric obj = new Fabric();
           foreach (var a in pr)
           {
               obj = a;
           }
           return obj;

       }
       public List<EKnitingAvailableMCList> GetMCList(Int64 ID)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_GET_AVAILABLE_MC_LIST(ID)
                   
                    orderby p.MCNo,p.AssignDate
                        select p;


           List<EKnitingAvailableMCList> li = new List<EKnitingAvailableMCList>();

           foreach (var v in pr)
           {
               EKnitingAvailableMCList ob = new EKnitingAvailableMCList();
               ob.ID =Convert.ToInt32( v.ID);
               ob.Machine = v.MCNo;
               ob.AssignQty =Convert.ToDouble( v.AssignQTY);
              
               if (v.IsRunning == 0)
               {
                   ob.IsRunning = false;
                   ob.Status = "Available";

               }
               else
               {
                   ob.IsRunning = true;
                   ob.Status = "Running";
               }
               li.Add(ob);
           }
           return li;
       }

       public List<EDyedFabricProcess> GetReturnFabricList(Int64 ID)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.DyedFabricProcess_Get(ID)

                   
                    select p;


           List<EDyedFabricProcess> li = new List<EDyedFabricProcess>();

           foreach (var v in pr)
           {
               EDyedFabricProcess ob = new EDyedFabricProcess();
               ob.SL = Convert.ToInt32(v.SL);
               ob.Select = false;
               ob.ProcessCardNo = v.PCardNo;
               ob.RollNo = v.RollNo.ToString();
               ob.RollQty = v.Qty.ToString();
               ob.status = "Available";
               li.Add(ob);
           }
           return li;
       }


       public List<EKnitingRunningMCList> GetRunningMCList()
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_GET_RUNNING_MC_LIST()
             
                    orderby p.MCNo, p.AssignDate
                    select p;


           List<EKnitingRunningMCList> li = new List<EKnitingRunningMCList>();

           foreach (var v in pr)
           {
               EKnitingRunningMCList ob = new EKnitingRunningMCList();
               ob.ID = Convert.ToInt64(v.ID);
               ob.Machine = v.MCNo;

               ob.AssignDate =Convert.ToDateTime(v.AssignDate).ToString("dd-MM-yyyy");
              
               ob.Fabric = v.Fabric;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
              
               

     
               li.Add(ob);
           }
           return li;
       }



       public List<EKNITDailyReport> KnitDailyProductionReport(string MCNo, DateTime? frm, DateTime? To,string Sht)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_DAILY_PRODUCTION_REPORT(MCNo, frm, To)
                    where (Sht == "All Shift" || p.Shift == Sht)
                    select p;


           List<EKNITDailyReport> li = new List<EKNITDailyReport>();

           foreach (var v in pr)
           {
               EKNITDailyReport ob = new EKNITDailyReport();
               
               ob.MCNo = v.MCNo;
               ob.Fabric = v.Fabric;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
               ob.Roll_No = v.RollNo??0;
               ob.RollQty =Convert.ToDouble( Convert.ToDouble(v.RollQty ).ToString("#.0"));
               ob.KnitDateTime =Convert.ToDateTime( v.PDate);
               ob.FabricGroup = v.FGroupName;
               ob.Shift = v.Shift;
               li.Add(ob);
           }
           return li;
       }
       public List<EKNITDailyReport> KnitDailyProductionSummaryReport(string MCNo, DateTime? frm, DateTime? To, string Sht)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_DAILY_PRODUCTION_REPORT_SUMMARY(MCNo, frm, To,Sht)
                    
                 

                    select p;


           List<EKNITDailyReport> li = new List<EKNITDailyReport>();

           foreach (var v in pr)
           {
               EKNITDailyReport ob = new EKNITDailyReport();
               ob.FabricGroup = v.FGroupName;
               ob.MCNo = v.MCNo;
               ob.Fabric = v.Fabric;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
               ob.Roll_No = v.TotalRoll ?? 0;
               ob.RollQty = Convert.ToDouble(Convert.ToDouble(v.TotalQty).ToString("#.0")); 

               ob.KnitDateTime = Convert.ToDateTime(v.LastDate);

               ob.Shift = Sht;
               li.Add(ob);
           }
           return li;
       }

       public IQueryable GetKNITBalanceSheet1(DateTime dt)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_REPORT_KNIT_BALANCE_SHEET(dt)

                    select p;

           return (IQueryable)pr;

       }
       public List<REPORT_KNIT_PROD_GREY_STOCKResult> GetGreyReport(DateTime dt, string MCNo)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           return (from p in db.REPORT_KNIT_PROD_GREY_STOCK(dt, MCNo)
                   select p).ToList<REPORT_KNIT_PROD_GREY_STOCKResult>();

       }
       public List<EKnitingBalanceSheet> GetKNITBalanceSheet(DateTime dt,string MCNo)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_REPORT_KNIT_BALANCE_SHEET(dt)
                    where (MCNo == "All MC" || p.MCNo == MCNo)
                    select p;


           List<EKnitingBalanceSheet> li = new List<EKnitingBalanceSheet>();

           foreach (var v in pr)
           {
               EKnitingBalanceSheet ob = new EKnitingBalanceSheet();
               ob.ID = Convert.ToInt64(v.ID);
               ob.MCNo = v.MCNo;
               ob.FabricGroup = v.FGroupName;
               ob.Fabric = v.Fabric;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
               ob.ReqNo = v.ReqNo;
               ob.Location = v.Location;
               //ob.ProductionQty = Convert.ToDouble(v.ProductionQty);
               ob.OpeningBalance = Convert.ToDouble((v.PrevBal??0 ).ToString("0.0"));
               ob.Receive = Convert.ToDouble((v.TodayBal ?? 0).ToString("0.0"));
               ob.GreyIssueToLab = Convert.ToDouble((v.IssueQty??0).ToString("0.0"));
               ob.GreyIssueToDeying = Convert.ToDouble((v.GreyIssueQty??0).ToString("0.0"));

               ob.IssueBalanceByLab = Convert.ToDouble((ob.OpeningBalance + ob.Receive - ob.GreyIssueToLab).ToString("0.0"));
               ob.ActualBalance = Convert.ToDouble((ob.OpeningBalance + ob.Receive - ob.GreyIssueToDeying).ToString("0.0"));


               if (v.AssignDate != null)
               {
                   ob.StartDate = Convert.ToDateTime(v.AssignDate).ToString("dd-MM-yyyy hh:mm tt");
               }
               else
               {
                   ob.StartDate = "";
               }
               if (v.StopDate != null)
               {
                   ob.StopDate = Convert.ToDateTime(v.StopDate).ToString("dd-MM-yyyy hh:mm tt");
               }
               else
               {
                   ob.StopDate = "";
               }

              



               li.Add(ob);
           }
           return li;
       }

       public List<EKnitingBalanceSheet> GetKNITBalanceSheetSubCon(DateTime dt, string MCNo)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_REPORT_KNIT_BALANCE_SHEET_SubCon(dt)
                    where (MCNo == "All MC" || p.MCNo == MCNo)
                    select p;


           List<EKnitingBalanceSheet> li = new List<EKnitingBalanceSheet>();

           foreach (var v in pr)
           {
               EKnitingBalanceSheet ob = new EKnitingBalanceSheet();
          
               ob.MCNo = v.MCNo;
               ob.FabricGroup = v.FGroupName;
               ob.Fabric = v.Fabric;
               ob.GM = v.GM;
               ob.GSM = v.GSM;

               ob.Location = v.Location;
               //ob.ProductionQty = Convert.ToDouble(v.ProductionQty);
               ob.OpeningBalance = Convert.ToDouble((v.PrevBal ?? 0).ToString("0.0"));
               ob.Receive = Convert.ToDouble((v.TodayBal ?? 0).ToString("0.0"));
               ob.GreyIssueToLab = Convert.ToDouble((v.IssueQty ?? 0).ToString("0.0"));
               ob.GreyIssueToDeying = Convert.ToDouble((v.GreyIssueQty ?? 0).ToString("0.0"));

               ob.IssueBalanceByLab = Convert.ToDouble((ob.OpeningBalance + ob.Receive - ob.GreyIssueToLab).ToString("0.0"));
               ob.ActualBalance = Convert.ToDouble((ob.OpeningBalance + ob.Receive - ob.GreyIssueToDeying).ToString("0.0"));


               if (v.AssignDate != null)
               {
                   ob.StartDate = Convert.ToDateTime(v.AssignDate).ToString("dd-MM-yyyy hh:mm tt");
               }
               else
               {
                   ob.StartDate = "";
               }
               if (v.StopDate != null)
               {
                   ob.StopDate = Convert.ToDateTime(v.StopDate).ToString("dd-MM-yyyy hh:mm tt");
               }
               else
               {
                   ob.StopDate = "";
               }





               li.Add(ob);
           }
           return li;
       }

       public List<EGreyOpeningBalance> GetGreyOpeningBalance(Int64 ID)
       {
           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_GREY_OPENING_BALANCE(ID)

                    select p;


           List<EGreyOpeningBalance> li = new List<EGreyOpeningBalance>();

           foreach (var v in pr)
           {
               EGreyOpeningBalance ob = new EGreyOpeningBalance();
               ob.ID = Convert.ToInt64(v.AssignMCID);
               ob.MCNo = v.MCNo;

               ob.Fabric = v.Fabric;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
                li.Add(ob);
           }
           return li;
       }

       public List<EYarnList> GetYarnListOfSetup(Int64 ID)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_Prod_YarnConsumtions
                    from q in db.Yarns
                    where p.YarnID==q.YID && p.KnitSetupID==ID
                    select new { p.ID, p.YarnID, q.YarndGroupID, p.PerQty, p.Qty };


           List<EYarnList> li = new List<EYarnList>();

           foreach (var v in pr)
           {
               EYarnList ob = new EYarnList();
               ob.ID = Convert.ToInt64(v.ID);
               ob.YarnID = v.YarnID??0;

               ob.YarnGroupID = v.YarndGroupID??0;
               ob.QtyPer =Convert.ToDouble( v.PerQty);
               ob.Qty = Convert.ToDouble(v.Qty);






               li.Add(ob);
           }
           return li;
       }



       public List<EFabcricList> GetFabricListByMCno(string MCNo)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_GET_FABRIC_LIST(MCNo)

                    select p;


           List<EFabcricList> li = new List<EFabcricList>();

           foreach (var v in pr)
           {
               EFabcricList ob = new EFabcricList();
               ob.FID = v.FID.ToString()+ "_"+ v.gm;
               ob.Fabric = v.Fabric + "  GM:" + v.gm ;

              
               li.Add(ob);
           }
           return li;
       }
       public string GetKnitFInishQty(Int32 ID)
       {
           string tot="0";
            DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           db.KNIT_SETUP_GET_FINISH_QTY(ID,ref tot );
           return tot;
        
       }
       public List<EKnitSetupOrderList> GetKnitSetupOrder(Int32 ID)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_SETUP_ORDER_GET(ID)

                    select p;


           List<EKnitSetupOrderList> li = new List<EKnitSetupOrderList>();

           foreach (var v in pr)
           {
               EKnitSetupOrderList ob = new EKnitSetupOrderList();
               ob.OPID = v.OPID??0;
               ob.Description = v.Description;
              
               ob.OrderNo = v.OrderNo;
               ob.Qty =Convert.ToDouble( v.TotalQty);

              
               li.Add(ob);
           }
           return li;
       }
       

       public List<ERptKnitRunningMC> GetRPT_KnitRunningMC(DateTime dt)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_REPORT_RUNNING_MC(dt)
                   
                    select p;


           List<ERptKnitRunningMC> li = new List<ERptKnitRunningMC>();
           int i = 1;
           foreach (var v in pr)
           {
               ERptKnitRunningMC ob = new ERptKnitRunningMC();

               ob.ReqNo = v.SetupCode;
               ob.ID = Convert.ToInt64(v.ID);
               ob.SL = i;
               ob.Grey_Construction = v.Fabric;
               ob.Orders_No = v.Order_List;
               ob.YarnConstruction = v.Yarn_list;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
               
               //ob.ProductionQty = Convert.ToDouble(v.ProductionQty);
               ob.MC_Prod_Qty = Convert.ToDouble(v.AssignQty);
               ob.Running_MC = v.MC_list;
               ob.TotalProductionQty = Convert.ToDouble(v.Balance.ToString("#.0"));
               ob.BalanceQty =Convert.ToDouble( (ob.MC_Prod_Qty - ob.TotalProductionQty).ToString("#.0"));

               ob.Notes = v.Notes;
               ob.FabricGroup = v.FGroupName;
               i = i + 1;
               li.Add(ob);
           }
           return li;
       }
       public List<ERptGreyIssueToLab> GetRPT_GreyIssueToLab(string dt,string pino,string MCNo)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_REPORT_GREY_ISSUE_TO_LAB(dt, pino, MCNo)
                                        select p;


           List<ERptGreyIssueToLab> li = new List<ERptGreyIssueToLab>();
           int i = 1;
           foreach (var v in pr)
           {
               ERptGreyIssueToLab ob = new ERptGreyIssueToLab();

      
               ob.SL = i;
               ob.Customer = v.CName;
               ob.OrderNo = v.PINO;

               ob.ItemConstruction = v.Fabric;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
               ob.MCNo = v.MCNo;
               ob.Color =v.Color;
               ob.LapDip = v.LapDip;
               //ob.ProductionQty = Convert.ToDouble(v.ProductionQty);
               ob.GreyIssueToLAB = Convert.ToDouble(v.IssueQty.Value.ToString("#.0"));
               if (v.EntryDate != null)
               {
                   ob.PCardMakeDate =Convert.ToDateTime(v.EntryDate).ToString("dd-MM-yyyy hh:mm tt");
               }
               ob.GreyIssueDate = Convert.ToDateTime(v.IssueDate).ToString("dd-MM-yyyy hh:mm tt");

               i++;

               li.Add(ob);
           }
           return li;
       }

       public List<ERptGreyIssueToDeying> GetRPT_GreyIssueToDeying(string dt, string pino)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_REPORT_GREY_ISSUE_TO_DEYING(dt, pino)

                    select p;


           List<ERptGreyIssueToDeying> li = new List<ERptGreyIssueToDeying>();
           int i = 1;
           foreach (var v in pr)
           {
               ERptGreyIssueToDeying ob = new ERptGreyIssueToDeying();


               ob.SL = i;
               ob.Customer = v.CName;
               ob.Color = v.Color;
               ob.OrderNo = v.PINO;
               ob.ProcessCard = v.PCardNo;
               ob.ItemConstruction = v.Fabric;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
               ob.MCNo = v.MCNo;
               ob.LotNo = v.LotNo;
               //ob.ProductionQty = Convert.ToDouble(v.ProductionQty);
               ob.GreyIssueToDeying = Convert.ToDouble(v.IssueQty.Value.ToString("#.0"));

               ob.GreyIssueDate = Convert.ToDateTime(v.IssueDate).ToString("dd-MM-yyyy hh:mm tt");

               i++;

               li.Add(ob);
           }
           return li;
       }
       public List<EKnitingProductionSetupView> GetKNITProductionGet(Int64 ID,string MCNo)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_ProductionSetup_GET(MCNo)
                    where (@ID==0 || p.ID==ID)
                    select p;


           List<EKnitingProductionSetupView> li = new List<EKnitingProductionSetupView>();

           foreach (var v in pr)
           {
               EKnitingProductionSetupView ob = new EKnitingProductionSetupView();
               ob.ID = Convert.ToInt64(v.ID);
               ob.FabricID = v.FabricID??0;
               ob.Notes = v.Notes;
               ob.Fabric = v.Fabric;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
               ob.GID = v.GID??0;
               ob.ChallanNo = v.challanno;
               ob.MCNo = v.submcNo;
               
               ob.Location = v.Location;
               ob.ProductionQty =Convert.ToDouble(v.ProductionQty);
               ob.SubContact= v.SubcontactName;
               ob.Date = Convert.ToDateTime(v.SetupDate).ToString("dd-MM-yyyy");
               



               li.Add(ob);
           }
           return li;
       }

       public List<EKnitingProductionSetupView> GetKNITProductionGet( string MCNo)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_ProductionSetupView_GET(MCNo)

                    select p;


           List<EKnitingProductionSetupView> li = new List<EKnitingProductionSetupView>();

           foreach (var v in pr)
           {
               EKnitingProductionSetupView ob = new EKnitingProductionSetupView();
               ob.ID = Convert.ToInt64(v.ID);
               ob.FabricID = v.FabricID ?? 0;
               ob.Notes = v.Notes;
               ob.Fabric = v.Fabric;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
               ob.GID = v.GID ?? 0;
               ob.ChallanNo = v.challanno;
               ob.MCNo = v.submcNo;
               ob.LossPer = Convert.ToDouble( v.LossPer);
               ob.Location = v.Location;
               ob.ProductionQty = Convert.ToDouble(v.ProductionQty);
               ob.SubContact = v.SubcontactName;
               ob.Date = Convert.ToDateTime(v.SetupDate).ToString("MM/dd/yyyy");
               ob.AssignMCID = v.AssignMCID;
               ob.IsSubContact = v.IsSubContact??false;


               li.Add(ob);
           }
           return li;
       }

       public string SaveKnitSetup(EKnitingProductionSetup p, List<EKnitingAvailableMCList> liMC, List<EYarnConsumptionList> liYarn, List<EKnitSetupOrderList> liO)
       {
           string res = "";
           string rcode = "";
           DBAccessDataContext db = new DBAccessDataContext();
           string strXMLMC = "";
           if (liMC != null)
           {
               strXMLMC = DataSerialization.SerializeObject<List<EKnitingAvailableMCList>>(liMC);
           }
           string strXMLYarn = DataSerialization.SerializeObject<List<EYarnConsumptionList>>(liYarn);
           string strXMLOrder = DataSerialization.SerializeObject<List<EKnitSetupOrderList>>(liO);

           db.Connection.ConnectionString = _Conn;
           db.KNIT_Prod_Setup_INS_UPD(p.ID, Convert.ToDecimal(p.ProductionQty),Convert.ToDecimal(p.ExtraQty), p.FabricID, p.GM, p.GSM, p.SetupDate, p.Notes, p.SubContact, p.EntryUserID, p.IsActive, strXMLYarn, strXMLMC, strXMLOrder, p.MCNO, p.ChallanNo, p.Location,p.IsSubContact, ref res,ref rcode);

           db.SubmitChanges();

           return res+ "|" +rcode;
       }

       public string SaveKnitDyedFabricProcesssSetup(EDyedFabricProcessSetup p, List<EDyedFabricProcess> liMC)
       {
           try
           {
               string res = "";
               DBAccessDataContext db = new DBAccessDataContext();
               string strXMLMC = "";
               if (liMC != null)
               {
                   strXMLMC = DataSerialization.SerializeObject<List<EDyedFabricProcess>>(liMC);
               }

               db.Connection.ConnectionString = _Conn;
               db.Insert_DyedFabricProcess(p.ID, p.ProcessCardNo, p.RollQty, p.RollNo, p.EntryID, p.DDate,p.ReprocessSection,p.ReprocessDescription, strXMLMC, p.PCardReason, ref res);

               db.SubmitChanges();

               return res;
           }
           catch (Exception ex)
           {
               return "Error :"+ ex.Message.ToString();
           }
       }


       public bool DeleteKnitDyedFabricProcesssSetup(Int32 ID,string Pcard)
       {
           try
           {
               
               DBAccessDataContext db = new DBAccessDataContext();
               
               db.Connection.ConnectionString = _Conn;
               db.DeleteDyedFabricProcess(ID,Pcard);

               db.SubmitChanges();

               return true;
           }
           catch
           {
               return false;
           }
       }

       public List<EDyedFabricProcessView> GetKnitDyedFabricProcesss(string PINO)
       {



           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.GetDyedFabricProcess(PINO)
                    
                    select p;


           List<EDyedFabricProcessView> li = new List<EDyedFabricProcessView>();

           foreach (var v in pr)
           {
               EDyedFabricProcessView ob = new EDyedFabricProcessView();
               ob.ID = Convert.ToInt32(v.SL);
               ob.Product = v.Description;
               ob.Color = v.Color;
               ob.PCardNo = v.PCardNo;
               ob.RollNo =Convert.ToInt32( v.NoOfRoll);
               ob.RollQty =Convert.ToDecimal( v.PQty);
               
              
               ob.DDate = Convert.ToDateTime(v.EntryDate);




               li.Add(ob);
           }
           return li;
       }
       public string SaveKnitIssueToPCard(EGreyFabricIssueToPCard p)
       {
           string res = "";
           DBAccessDataContext db = new DBAccessDataContext();

         
           db.Connection.ConnectionString = _Conn;
           db.KNIT_GreyIssue_INS(p.ID,p.FabricID,p.GM,p.GSM,p.MCNo,p.PCardNO,Convert.ToDecimal(p.IssueQty),p.RollNo,p.IssueDate,p.Section, p.EntryUserID,p.ReqNo,ref res);

           db.SubmitChanges();

           return res;
       }
       public bool SaveGreyOpeningStock(List<EGreyOpeningBalance> lio)
       {
           
           DBAccessDataContext db = new DBAccessDataContext();

           try
           {
               db.Connection.ConnectionString = _Conn;
               foreach (EGreyOpeningBalance o in lio)
               {
                   db.KNIT_GREY_ROLL_RCV_INS(o.ID, 0, Convert.ToDecimal(o.Qty));

                   db.SubmitChanges();
               }
               return true;
           }
           catch
           {
               return false;
           }
          
       }
       public List<EGreyFabricIssueToPCard> GetKNITGreyIssue(string Pcard)
       {

           try
           {

               DBAccessDataContext db = new DBAccessDataContext();

               db.Connection.ConnectionString = _Conn;
               var pr = from p in db.KNIT_GreyIssue_GET_ByPCard(Pcard)

                        select p;

                       

               List<EGreyFabricIssueToPCard> li = new List<EGreyFabricIssueToPCard>();

               foreach (var v in pr)
               {
                   EGreyFabricIssueToPCard ob = new EGreyFabricIssueToPCard();
                   ob.ID = Convert.ToInt64(v.ID);
                   ob.MCNo = v.MCNo;
                   ob.GID = v.GID ?? 0;
                   ob.IssueDate = Convert.ToDateTime(v.IssueDate);
                   ob.PCardNO = v.PCardNo;
                   ob.FabricID = v.FID ?? 0;
                   ob.GM = v.GM;
                   ob.GSM = v.GSM;
                   ob.Fabric = v.Fabric;
                   ob.IssueQty = Convert.ToDouble(v.IssueQty);
                   ob.RollNo = v.RollNo ?? 0;


                   li.Add(ob);
               }
               return li;

           }
           catch
           {
               return null;
           }
       }
       public bool StopRunningMC( Int64 ID,string MC,bool IsStop)
       {
           try
           {
               string res = "";
               DBAccessDataContext db = new DBAccessDataContext();

               db.Connection.ConnectionString = _Conn;
               db.KNIT_UPDATE_RUNNING_MC(ID,MC, IsStop);

               db.SubmitChanges();

               return true;
           }
           catch
           {
               return false;
           }
       }

       public bool SaveKnitMachineSetup(EKnitingMachine obj)
       {
           try
           {
              
               DBAccessDataContext db = new DBAccessDataContext();

               db.Connection.ConnectionString = _Conn;
               db.KNIT_Machine_INS_UPD(obj.MCNo,Convert.ToDecimal( obj.Capacity), obj.Dia, obj.GG, obj.MFloor);

               db.SubmitChanges();

               return true;
           }
           catch
           {
               return false;
           }
       }
       public IQueryable GetAlMCNo()
       {
           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_Machines
                   
                    orderby p.MCNo
                    select p
                   ;

           return pr;
       }
       public IQueryable GetAlMCNoByReqNo(string ReqNo)
       {
           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_Prod_AssignMachines
                    from q in db.KNIT_Prod_Setups 

                    where p.KnitSetupID==q.ID && q.SetupCode==ReqNo

                    orderby p.MCNo
                    select  p.MCNo
                   ;

           return pr;
       }
       public string SaveGreyBooking(EGreyFabricIssue p)
       {
           string res = "";
           DBAccessDataContext db = new DBAccessDataContext();

           
           db.Connection.ConnectionString = _Conn;
           db.KNIT_GreyBooking_INS_UPD(p.ID,p.FabricID,p.GM,p.GSM,p.MCNo,p.PINO,p.OPDID,Convert.ToDecimal( p.IssueQty),p.IssueDate,p.EntryUserID,p.IsActive,p.ReqNo,ref res);

           db.SubmitChanges();

           return res;
       }
       public bool DeleteGreyBooking(Int64 ID,string UserID)
       {
          
           DBAccessDataContext db = new DBAccessDataContext();


           db.Connection.ConnectionString = _Conn;
           db.KNIT_GreyFabricIssueDelete(ID,UserID);

           db.SubmitChanges();

           return true;
       }
       public List<EGreyFabricIssueGet> GetGreyIssue(string MCNo,DateTime? dt)
       {

           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_GreyFabricIssueGET(MCNo, dt)

                    orderby p.ID descending
                    select p;


           List<EGreyFabricIssueGet> li = new List<EGreyFabricIssueGet>();
             
           foreach (var v in pr)
           {
               EGreyFabricIssueGet ob = new EGreyFabricIssueGet();
               ob.ID = Convert.ToInt64(v.ID);
               ob.Color = v.Color;
               ob.Fabric = v.Fabric;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
               ob.MCNo = v.MCNo;
               ob.ReqNo = v.reqNo;
               ob.IssueQty =Convert.ToDouble( v.IssueQty);
               ob.IssueDate =Convert.ToDateTime(v.IssueDate).ToString("dd-MMM-yyyy");
               li.Add(ob);
           }
           return li;
       }


       public List<EKNIT_Setup> GetSetupList(string dt)
       {

           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_GET_Setup_List(dt)

                  orderby p.SetupCode descending
                    select p;


           List<EKNIT_Setup> li = new List<EKNIT_Setup>();

           foreach (var v in pr)
           {
               EKNIT_Setup ob = new EKNIT_Setup();
              
               ob.SetupCode = v.SetupCode;
               li.Add(ob);
           }
           return li;
       }



       public List<EGreyFabricIssue> GetGreyIssueBooking( Int64 ID)
       {




           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_GreyBookings
                    from q in db.OrderProducts
                    from s in db.OrderProductDetails 
                    from r in db.Fabrics
                    where p.ID==ID  && q.OPID==s.OPID && s.OPDID==p.OPDID && r.FID==p.FabricID
                    orderby p.ID descending
                    select new {p.ID,p.OPDID,q.OPID,r.GID,r.FID ,p.GM,p.GSM,p.MCNo,p.IssueQty,p.IssueDate,p.PINO};


           List<EGreyFabricIssue> li = new List<EGreyFabricIssue>();

           foreach (var v in pr)
           {
               EGreyFabricIssue ob = new EGreyFabricIssue();
               ob.ID = Convert.ToInt64(v.ID);
               ob.OPDID = v.OPDID??0;
               ob.OPID = v.OPID;
               ob.PINO = v.PINO;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
               ob.MCNo = v.MCNo;
               ob.FabricGID = v.GID??0;
               ob.FabricID = v.FID;
               ob.IssueQty = Convert.ToDouble(v.IssueQty);
               ob.IssueDate = Convert.ToDateTime(v.IssueDate);
               li.Add(ob);
           }
           return li;
       }

       public List<EKnitingSubContactReport> GetKnitingSubContactReport(DateTime From ,DateTime To)
       {




           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_ProductionSetup_GET("")
                   
                    where  p.SubcontactName !=null && p.SubcontactName.Length>2  && p.SetupDate>= From.Date && p.SetupDate<=To.Date
                    select p ;


           List<EKnitingSubContactReport> li = new List<EKnitingSubContactReport>();

           foreach (var v in pr)
           {
               EKnitingSubContactReport ob = new EKnitingSubContactReport();
               ob.ID = v.ID;
               ob.SubContactCompanyName = v.SubcontactName;
               ob.FabricConstruction = v.Fabric;
               ob.GM = v.GM;
               ob.GSM = v.GSM;
               ob.MCNo = v.submcNo??"";
               ob.ChallanNo = v.challanno;
               ob.Notes = v.Notes;
               ob.ReceiveDate =Convert.ToDateTime( v.SetupDate).ToString("dd-MM-yyyy");
               ob.ReceiveQty = v.ProductionQty.ToString();
             
               li.Add(ob);
           }
           return li;
       }
       public List<EProuctionSetup> GetProuctionSetup(string MCNo,DateTime From,DateTime ToDate)
       {
           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_GET_Finalized_MachineSetup(MCNo,From,ToDate)

                    select p;


           List<EProuctionSetup> li = new List<EProuctionSetup>();

           foreach (var v in pr)
           {
               EProuctionSetup ob = new EProuctionSetup();
               ob.ID = Convert.ToInt64(v.ID);
               ob.Fabric = v.Fabric;
               ob.MCNo = v.MCNo;
               ob.StartDate = v.AssignDate.Value.ToString("dd-MMM-yyyy");
               if (v.IsRunning == false)
               {
                   if (ob.StopDate != null)
                   {
                       ob.StopDate = v.StopDate.Value.ToString("dd-MMM-yyyy");
                   }
                   ob.Status = "Running";
               }
               else
               {
                   ob.Status = "Finished";
               }
             
               ob.GM = v.GM;
               ob.GSM = v.GSM;
               ob.ProductionQty = Convert.ToDouble(v.ProductionQty??0);
               ob.TotalRoll = Convert.ToDouble(v.totalRoll);
               ob.TotalKG = Convert.ToDouble((v.totalQty??0).ToString("#.0"));
               
               li.Add(ob);
           }
           return li;
       }

       

       public List<EProuctionRoll> GetProuctionRoll(Int64 ID)
       {
           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.KNIT_GET_Finalized_Roll(ID)

                    select p;


           List<EProuctionRoll> li = new List<EProuctionRoll>();

           foreach (var v in pr)
           {
               EProuctionRoll ob = new EProuctionRoll();
             
               ob.MCNo = v.MCNo;


               ob.RollNo = v.RollNo??0;
               ob.RollQty =Convert.ToDouble( v.RollQty);
               ob.Date = Convert.ToDateTime(v.PDate);
               li.Add(ob);
           }
           return li;
       }

       public bool FinalizedSetup(Int64 ID)
       {

           try
           {
               DBAccessDataContext db = new DBAccessDataContext();


               db.Connection.ConnectionString = _Conn;
               db.KNIT_SET_Finalized_MachineSetup(ID);

               db.SubmitChanges();
               return true;
           }
           catch
           {
               return false;
           }
           
       }
       public List<EWareHousePacking> GetWareHouseStock(string WHType, DateTime? dt, string PINO)
       {
           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.rpt_packing_Stock_By_WarehouseType(WHType,dt,PINO)

                    select p;


           List<EWareHousePacking> li = new List<EWareHousePacking>();
           int count = 1;
           foreach (var v in pr)
           {
               EWareHousePacking ob = new EWareHousePacking();
               ob.SL = count ;
               ob.Merchandiser = v.EntryID;
               ob.Date =Convert.ToDateTime( v.pdat);
               ob.OrderNo = v.PINO;
               ob.Customer = v.Customer;
               ob.ItemDescription = v.Description;
               ob.GSM = v.GSM.ToString();
               ob.Width = v.WidthText;
               ob.Color = v.Color;
               ob.Lotno = v.LotNo;
               ob.ProcessCard = v.PCardNo;
               ob.RollQty =Convert.ToDouble( v.TotalRollStockQTY);
               ob.StockQty = Convert.ToDouble( v.TotalPackingStockQTY);
               count++;
               li.Add(ob);
           }
           return li;
       }

       public List<EWareHouseRCVPacking> GetWareHouseReceive(string WHType, DateTime? dt, DateTime? dtt, string PINO)
       {
           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.rpt_packing_Receive_By_WarehouseType(WHType, dt, dtt, PINO)

                    select p;


           List<EWareHouseRCVPacking> li = new List<EWareHouseRCVPacking>();
           int count = 1;
           foreach (var v in pr)
           {
               EWareHouseRCVPacking ob = new EWareHouseRCVPacking();
               ob.SL = count;
               ob.Merchandiser = v.EntryID;
                ob.OrderNo = v.PINO;
               ob.Customer = v.Customer;
               ob.ItemDescription = v.Description;
               ob.GSM = v.GSM.ToString();
               ob.Width = v.WidthText;
               ob.Color = v.Color;
               ob.Lotno = v.LotNo;
               ob.ProcessCard = v.PCardNo;
               ob.ReceiveDate =Convert.ToDateTime( v.pdat);
               ob.RcvRollQty = Convert.ToDouble(v.TotalRollStockQTY);
               ob.RcvQty = Convert.ToDouble(v.TotalPackingStockQTY);
               count++;
               li.Add(ob);
           }
           return li;
       }

       public List<rpt_packing_Delivery_summary_byColorResult> GetWareHouseDeliveryByColor( string PINO)
       {
           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = (from p in db.rpt_packing_Delivery_summary_byColor( PINO)

                    select p).ToList();

           return pr;
       }
       public List<EWareHouseDelivery> GetWareHouseDelivery(DateTime? dt, DateTime? tdt, string PINO)
       {
           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.rpt_packing_Delivery( dt,tdt, PINO)

                    select p;


           List<EWareHouseDelivery> li = new List<EWareHouseDelivery>();
           int count = 1;
           foreach (var v in pr)
           {
               EWareHouseDelivery ob = new EWareHouseDelivery();
               ob.SL = count;
               ob.Merchandiser = v.EntryID;
               ob.DeliveryDate = Convert.ToDateTime(v.pdat).ToString("dd-MMM-yyyy");
               ob.OrderNo = v.PINO;
               ob.Customer = v.Customer;
               ob.ItemDescription = v.Description;
               ob.Notes = v.Notes;
               
               ob.GSM = v.GSM.ToString();
               ob.Width = v.WidthText;
               ob.Color = v.Color;
               ob.Lotno = v.LotNo;
               ob.ProcessCard = v.PCardNo;
               ob.Challan = v.ChallanNo;
               ob.GetPassNo = v.GatePass;
               ob.VehicleNo = v.VehicleNo;
               ob.DeliveredRollQty = Convert.ToDouble(v.TotalRollDeliveredQTY);
               ob.DeliveredQtyKGS = Convert.ToDouble(v.TotalPackingDeliveredQTY);
               ob.DeliveredQtyByUnit = Convert.ToDouble(v.TotalPackingDeliveredQTYYDS);
               ob.Unit = v.Unit;
               ob.DeliveryStatus = v.DeliveryStatus;
               ob.AssignFrom = v.AssignFrom;
               ob.DeliveryNotes = v.DeliveryNotes;
               count++;
               li.Add(ob);
           }
           return li;
       }


       public List<EWareHouseGeneralDelivery> GetWareHouseGeneralDelivery(DateTime? dt, DateTime? tdt)
       {
           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.GatePass_DeliveryReport(dt, tdt)

                    select p;


           List<EWareHouseGeneralDelivery> li = new List<EWareHouseGeneralDelivery>();
           int count = 1;
           foreach (var v in pr)
           {
       //        public Int64 SL { get; set; }
       //public string DeliveryDate { get; set; }
       //public string RefName { get; set; }
       //public string RefNo { get; set; }
       //public string PartyName { get; set; }
       //public string Type { get; set; }
       //public string ItemGroup { get; set; }
       //public string ItemName { get; set; }
       //public string Description { get; set; }
       //public string Challan { get; set; }

       //public string GatePassNo { get; set; }
       //public string VehicleNo { get; set; }

       //public double CTN_BAG_PCS_ROLL_QTY { get; set; }
       //public double DeliverQTY { get; set; }
       //public string Unit { get; set; }

       //public string Note { get; set; }
               EWareHouseGeneralDelivery ob = new EWareHouseGeneralDelivery();
               ob.SL = count;
               ob.RefName = v.RefName;
               ob.DeliveryDate = Convert.ToDateTime(v.GatePassDate).ToString("dd-MMM-yyyy");
               ob.RefNo = v.RefNo;
               ob.PartyName = v.GivenCustomer;
               ob.Type = v.TypeName;
               ob.ItemGroup = v.GroupName;

               ob.ItemName = v.ItemName.ToString();
               ob.Description = v.Descriptons;
               ob.Challan = v.ChallanNo;
               ob.GatePassNo = v.GatePassNo;
               ob.VehicleNo = v.VehicleNo;
               ob.Challan = v.ChallanNo;
               
               ob.CTN_BAG_PCS_ROLL_QTY = Convert.ToDouble(v.CTNQty);
               ob.DeliverQTY = Convert.ToDouble(v.TotalQty);
              
               ob.Unit = v.Unit;
               ob.Note = v.Notes;
               count++;
               li.Add(ob);
           }
           return li;
       }

       public List<EWareHouseDeliverySummary> GetWareHouseDeliverySummary(DateTime? dt, DateTime? tdt, string PINO)
       {
           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.rpt_packing_Delivery_summary(dt, tdt, PINO)

                    select p;


           List<EWareHouseDeliverySummary> li = new List<EWareHouseDeliverySummary>();
           int count = 1;
           foreach (var v in pr)
           {
               EWareHouseDeliverySummary ob = new EWareHouseDeliverySummary();
               ob.SL = count;
               ob.Merchandiser = v.EntryID??"";
               ob.LastDelivery = Convert.ToDateTime(v.LastDelivery);
               ob.OrderNo = v.PINO;
               ob.Customer = v.Customer;
               //ob.Unit = v.uni;

               ob.BookingKG = Convert.ToDouble(Convert.ToDouble(v.BookingKGS).ToString("0.00"));
               ob.BookingYDS = Convert.ToDouble(Convert.ToDouble(v.BookingYDS).ToString("0.00"));
               ob.BookingMETER = Convert.ToDouble(Convert.ToDouble(v.BookingMETER).ToString("0.00"));

               ob.DeliveryKG = Convert.ToDouble(Convert.ToDouble(v.TotalKGS).ToString("0.00"));
               ob.DeliveryYDS = Convert.ToDouble(Convert.ToDouble(v.TotalYDS).ToString("0.00"));
               ob.DeliveryMETER = Convert.ToDouble(Convert.ToDouble(v.TotalMETER).ToString("0.00"));


              
               count++;
               li.Add(ob);
           }
           return li;
       }
       public List<EWareHouseReturn> GetWareHouseReturn(string WHType, DateTime? dt,DateTime? tdt, string PINO)
       {
           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.rpt_packing_Return(dt,tdt, PINO,WHType)

                    select p;


           List<EWareHouseReturn> li = new List<EWareHouseReturn>();
           int count = 1;
           foreach (var v in pr)
           {
               EWareHouseReturn ob = new EWareHouseReturn();
               ob.SL = count;
               ob.Merchandiser = v.EntryID;
               ob.ReturnDate = Convert.ToDateTime(v.pdat);
               ob.OrderNo = v.PINO;
               ob.Customer = v.Customer;
               ob.ItemDescription = v.Description;
               ob.GSM = v.GSM.ToString();
               ob.Width = v.WidthText;
               ob.Color = v.Color;
               ob.Lotno = v.LotNo;
               ob.ProcessCard = v.PCardNo;
               ob.ReturnRollQty = Convert.ToDouble(v.TotalRollReturnQTY);
               ob.ReturnQty = Convert.ToDouble(v.TotalPackingReturnQTY);
               count++;
               li.Add(ob);
           }
           return li;
       }
    }

   public class ERequisition
   {
       public string ReqNo { get; set; }
   }
  
}

