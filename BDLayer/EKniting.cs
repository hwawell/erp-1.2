﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BDLayer
{
   public  class EKniting
    {
    }
   public class EProuctionRoll
   {
       public string MCNo { get; set; }
       public int RollNo { get; set; }
       public double RollQty { get; set; }
       public DateTime Date { get; set; }


   }

   public class EProuctionSetup
   {
       public Int64 ID { get; set; }
       public string MCNo { get; set; }
       public string Fabric { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public string StartDate { get; set; }
       public string StopDate { get; set; }
       public string Status { get; set; }

       public double ProductionQty { get; set; }
       public double TotalRoll { get; set; }
       public double TotalKG { get; set; }
     


   }

   public class EKnitingProductionSetup
   {
       public Int64 ID { get; set; }
       public string OrderNo { get; set; }
       public string Location { get; set; }
       public Int32 OrderProductID  { get; set; }
       public double LossPer { get; set; }
       public double ProductionQty { get; set; }
       public double ExtraQty { get; set; }
       public Int32 FabricID { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public DateTime SetupDate  { get; set; }
       public string EntryUserID { get; set; }
       public string Notes { get; set; }
       public string MCNO { get; set; }
       public string ChallanNo { get; set; }
       public string SubContact { get; set; }
       public bool @IsActive { get; set; }
       public bool IsSubContact { get; set; }

      
   }
   public class EKnitingMachine
   {
      
       public string MCNo { get; set; }
       public string Dia { get; set; }
       public string GG { get; set; }
       public string MFloor { get; set; }
       public double Capacity { get; set; }
     


   }
   public class EKnitingSubContactReport
   {
       public Int64 ID { get; set; }
       public string SubContactCompanyName { get; set; }
       public string FabricConstruction { get; set; }
       public string MCNo { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public string ChallanNo { get; set; }
       public string ReceiveDate { get; set; }
       public string ReceiveQty { get; set; }
       public string Notes { get; set; }



   }
   public class EKnitingBalanceSheet
   {
       

       public Int64 ID { get; set; }

       public string ReqNo { get; set; }
       public string FabricGroup { get; set; }
       public string Fabric { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public string MCNo { get; set; }
       public string StartDate { get; set; }
       public string StopDate { get; set; }
    
       public string Location { get; set; }

       public double OpeningBalance { get; set; }
       public double Receive { get; set; }
       public double GreyIssueToLab { get; set; }
       public double IssueBalanceByLab { get; set; }
       public double GreyIssueToDeying { get; set; }
       public double ActualBalance { get; set; }
      


   }
   public class ERptKnitRunningMC
   {


       public Int64 ID { get; set; }
       public Int32 SL { get; set; }
       public string ReqNo { get; set; }
       public string Orders_No { get; set; }
       
       public string FabricGroup { get; set; }
       public string Grey_Construction { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public string YarnConstruction { get; set; }
      
       public double MC_Prod_Qty { get; set; }
       public string Running_MC { get; set; }
       public double TotalProductionQty { get; set; }
       public double BalanceQty { get; set; }
       public string Notes { get; set; }
       



   }
   public class ERptGreyIssueToLab
   {


       public int SL { get; set; }



       public string OrderNo { get; set; }
       public string Customer { get; set; }
       public string ItemConstruction { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public string MCNo { get; set; }
       public string Color { get; set; }
       public string LapDip { get; set; }
       public double GreyIssueToLAB { get; set; }
       public string GreyIssueDate { get; set; }

       public string PCardMakeDate { get; set; }
      



   }
   public class ERptGreyIssueToDeying
   {


       public int SL { get; set; }



       public string OrderNo { get; set; }
       public string Customer { get; set; }
       public string ItemConstruction { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public string LotNo { get; set; }
       public string MCNo { get; set; }
       public string Color { get; set; }
       public string ProcessCard { get; set; }

       public double GreyIssueToDeying { get; set; }
       public string GreyIssueDate { get; set; }




   }
   public class EGreyOpeningBalance
   {
       public Int64 ID { get; set; }
       public string MCNo { get; set; }
       public string Fabric { get; set; }
  
       public string GM { get; set; }
       public string GSM { get; set; }
     
      
       public double Qty { get; set; }
       public DateTime OpeningDate { get; set; }

   }
   public class EYarnList
   {
       public Int64 ID { get; set; }
       public Int64 YarnID { get; set; }
       public Int64 YarnGroupID { get; set; }

       public double QtyPer { get; set; }
       public double Qty { get; set; }
      
   }
   public class EKnitingProductionSetupView
   {
       public Int64 ID { get; set; }
       public bool IsSubContact { get; set; }
       public Int32 OrderProductID { get; set; }
       public Int32 FabricID { get; set; }

       public string OrderNo { get; set; }
       public string OrderProduct { get; set; }
       public string Fabric { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public string Notes { get; set; }
       public string MCNo { get; set; }
       public string ChallanNo { get; set; }
       public string SubContact { get; set; }
       public double LossPer { get; set; }
       public double ProductionQty { get; set; }
       public string Date { get; set; }
       public string Location { get; set; }
       public Int64 GID { get; set; }
       public Int64 AssignMCID { get; set; }

   }
[Serializable]
   public class EKnitingAvailableMCList
   {
      public Int32 ID { get; set; }
      public string Machine { get; set; }
      public double AssignQty { get; set; }
      public string Status { get; set; }
      public string AssignDate { get; set; }
      public bool IsRunning { get; set; }

   }
   public class EDyedFabricProcessSetup
   {
       public Int32 ID { get; set; }
      
       public string ProcessCardNo { get; set; }
       public int RollNo { get; set; }
       public decimal RollQty { get; set; }
       public DateTime DDate { get; set; }
       public string EntryID { get; set; }
       public string ReprocessSection { get; set; }
       public string ReprocessDescription { get; set; }
       public string PCardReason { get; set; }

   }
   public class EDyedFabricProcessView
   {
       public Int32 ID { get; set; }

       public string Product { get; set; }
       public string Color { get; set; }
       public string PCardNo { get; set; }
      
       public int RollNo { get; set; }
       public decimal RollQty { get; set; }
       public DateTime DDate { get; set; }
       


   }
   public class EDyedFabricProcess
   {
       public Int32 SL { get; set; }
       public bool Select { get; set; }
       public string ProcessCardNo { get; set; }
       public string RollNo { get; set; }
       public string RollQty { get; set; }
       public string status { get; set; }
      

   }

   public class EKnitingRunningMCList
   {
       public Int64 ID { get; set; }
       public string Machine { get; set; }
      
       public string AssignDate { get; set; }
       public string OrderNo { get; set; }
       public string Fabric { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public string OrderProduct { get; set; }
       


   }
   public class EYarnConsumptionList
   {
       public Int64 ID { get; set; }
       public Int32 YarnID { get; set; }
       public string Yarn { get; set; }
       public decimal PerQty { get; set; }
       public decimal Qty { get; set; }
   }

   public class EGreyFabricIssue
   {
  

       public Int64 ID { get; set; }
       public Int64 FabricID { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public string MCNo { get; set; }
       public string PINO { get; set; }
       public string ReqNo { get; set; }
       public Int64 OPDID { get; set; }
       public double IssueQty { get; set; }

       public DateTime IssueDate { get; set; }
       public string EntryUserID { get; set; }
       public bool IsActive { get; set; }
       public Int64 OPID { get; set; }
       public Int64 FabricGID { get; set; }
   }
   public class EKNIT_Setup
   {

       public Int32 ID { get; set; }
       public string SetupCode { get; set; }
   }
   public class EGreyFabricIssueGet
   {


       public Int64 ID { get; set; }
      
       public string Color { get; set; }
       public string Fabric { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public string MCNo { get; set; }
       public string ReqNo { get; set; }
       public double IssueQty { get; set; }

       public string IssueDate { get; set; }
      
   }
   public class EGreyFabricIssueToPCard
   {



       public Int64 ID { get; set; }
       public Int64 FabricID { get; set; }
       public Int64 GID { get; set; }
       public string Fabric { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public string ReqNo { get; set; }
       public string MCNo { get; set; }
       public string PCardNO { get; set; }

       public double IssueQty { get; set; }
       public int RollNo { get; set; }
       public DateTime IssueDate { get; set; }
       public string Section { get; set; }
       public string EntryUserID { get; set; }
       public bool IsActive { get; set; }
   }
}
