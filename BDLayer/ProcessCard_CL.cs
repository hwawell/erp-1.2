﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Data.Linq.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace BDLayer
{
    public class ProcessCard_CL
    {
        string _Conn;
        public ProcessCard_CL()
        {
            _Conn = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        }
        public string GenearteProcessCardNo()
        {
            DBAccessDataContext db = new DBAccessDataContext();
            string s = "";
            db.Connection.ConnectionString = _Conn;
            db.GenerateProcessCardNo(ref s);


            return s;

        }

        public string Save(ProcessCard y)
        {


            DBAccessDataContext db = new DBAccessDataContext();
            String result = "Save Failed!!";
            try
            {
                db.Connection.ConnectionString = _Conn;

                db.Insert_ProcessCard_SP_NEW(y.OPDID, y.PINO, y.PCardNo,y.PCardType, y.width, y.Weight, y.MCCapacity, y.YardCalc, y.LotNo, y.Unit, y.GSM,y.RequiredQty, y.ProductionQty,y.ExtraPer, y.MCNo, y.Description, y.Notes, y.FID, y.YDS_CAL, y.OrderNoSub, y.PackingStyle, y.EntryDate, y.EntryID,y.ReqNo, ref result);
                db.SubmitChanges();
            }
            catch
            {
            }
            return result;

        }
        public IQueryable GetActivePCard()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ProcessCards
                     from q in db.OrderProductDetails
                     //from w in db.OrderProducts
                     
                     where p.Emode == "E"  && p.OPDID==q.OPDID && p.Status=="DEYING"
                     orderby p.SL descending
                     //select p;
                     select new
                     {
                      p.SL,
                      p.PCardNo,
                      p.PINO,
                      q.Color,
                      p.Weight,
                      p.width,
                      p.LotNo,
                      p.ProductionQty
                      ,p.RequiredQty
                      ,p.ReqNo
                         
                     };

            return pr.Take(50);
        }


        public List<EPCardList> GetPCardList()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ProcessCards

                     //from w in db.OrderProducts

                     where p.Emode == "E"
                     select p;

            List<EPCardList> li = new List<EPCardList>();

            foreach (var k in pr)
            {
                EPCardList o = new EPCardList();
                o.PCardNo = k.PCardNo;
                li.Add(o);
            }

            return li;
        }
        public DataSet GetActiveSinglePCard(string pi)
        {
            DataSet ds = new DataSet();
            try
            {
                string str = "SELECT p.SL,p.PCardNo,p.PINO,q.Color ,op.Weight,op.width, p.LotNo, p.ProductionQty from ProcessCard p,OrderProductDetails q,OrderProduct op  where op.opid=q.opid and  p.Emode ='E' and  p.OPDID = q.OPDID and p.Status = 'DEYING' and PCardNo like '%" + pi + "%'";

                ds = SqlHelper.ExecuteDataset(this._Conn, CommandType.Text, str);
                return ds;
            }
            catch
            {
                return ds;
            }

        }
        ////public IQueryable GetActiveSinglePCard(string Pcard)
        ////{
           
        ////    DBAccessDataContext db = new DBAccessDataContext();

        ////    db.Connection.ConnectionString = _Conn;
        ////    var pr = from p in db.ProcessCards
        ////             from q in db.OrderProductDetails
        ////             //from w in db.OrderProducts

        ////             where p.Emode == "E" && p.OPDID == q.OPDID && p.Status == "DEYING"


        ////                  && SqlMethods.Like(p.PCardNo, Pcard)

        ////             orderby p.PCardNo descending
        ////             //select p;
        ////             select new
        ////             {
        ////                 p.SL,
        ////                 p.PCardNo,
        ////                 p.PINO,
        ////                 q.Color,
        ////                 p.Weight,
        ////                 p.width,
        ////                 p.LotNo,
        ////                 p.ProductionQty


        ////             };

        ////    return pr.Take(50);
        ////}
        public EPcardInfo GetSinglePCardInfo(string pi)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ProcessCards
                     from q in db.OrderProductDetails
                     from r in db.OrderProducts
                     from f in db.Fabrics
                     where p.Emode == "E" && p.PCardNo == pi
                     && p.OPDID == q.OPDID && q.OPID == r.OPID && f.FID==p.FID
                     select new
                     {
                        p.SL, p.PINO,p.PCardNo,p.ExtraPer ,r.Width,p.Weight,p.LotNo,p.MCCapacity,p.PCardType, p.ProductionQty,p.GSM,
                         p.Description,p.Notes,p.YardCalc,p.YDS_CAL,p.PackingStyle,
                         p.RequiredQty,
                         q.Color,q.OPDID,q.OPID,DESC=r.Description ,DESNOTES=r.Notes,p.FID,p.EntryDate,f.GID
                         ,p.ReqNo
                     };
            List<EPcardInfo> liPcard = new List<EPcardInfo>();
            EPcardInfo obj = new EPcardInfo();

            var temp = pr.Single();
            obj.ID = temp.SL;
            obj.PINO = temp.PINO;
            obj.PCardNo = temp.PCardNo;
            obj.Width = temp.Width.ToString();
            obj.Weight = temp.Weight.ToString();
            obj.LotNo = temp.LotNo;
            obj.MCCapacity = temp.MCCapacity.ToString();
            obj.PQTY = temp.ProductionQty.ToString();
            obj.GSM = temp.GSM.ToString();
            obj.Description = temp.Description;
            obj.Notes = temp.Notes;
            obj.txtYardCal = temp.YardCalc;
            obj.Ptype = temp.PCardType;
            obj.Date = temp.EntryDate.ToString();
            obj.PackingStyle = temp.PackingStyle;
            obj.txtCalculation = temp.YDS_CAL.ToString();
            obj.OrDesID =Convert.ToInt32(temp.OPID);
            obj.ColorID = Convert.ToInt32(temp.OPDID);
            obj.RQTY = temp.RequiredQty.ToString();
            if (temp.ExtraPer != null)
            {
                obj.ExtraQty = temp.ExtraPer.ToString();
            }
           
            obj.FGID =Convert.ToInt32( temp.GID);
            obj.FID = Convert.ToInt32(temp.FID);

            obj.ReqNo = temp.ReqNo;



            return obj;
        }
        public IQueryable GetPCardForOrder(string orderNo)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ProcessCards
                                      

                     where p.Emode == "E" && p.PINO==orderNo
                     orderby p.SL descending
                     //select p;
                     select new
                     {
                         
                          Name = string.Concat(p.PCardNo,"-->" , p.LotNo ),


                          p.PCardNo,
                          p.LotNo


                     };

            return pr;
        }
       
        public IQueryable GetDeletedPCard()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ProcessCards
                     from q in db.OrderProductDetails
                     from w in db.OrderProducts

                     where p.Emode == "D" && p.OPDID == q.OPDID && q.OPID == w.OPID
                     orderby p.SL descending
                     //select p;
                     select new
                     {
                         p.SL,
                         p.PCardNo,
                         w.PINO,
                         q.Color,
                         p.Weight,
                         p.width,
                         p.LotNo,



                     };

            return pr.Take(50);
        }
        public void DeleteActivePCard(long custid, string mo, string eid)
        {

            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "PROCESS_CARD");
            db.SubmitChanges();


        }
        public IQueryable GetAlOrder()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Orders
                     where p.EMode=="E" && p.IsApproved==true
                     orderby p.PINo.Substring(0,3), p.EntryDate descending
                     select p;


            return pr;
        }
        public bool CheckProcessCard(string pq)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ProcessCards
                     where p.PCardNo == pq && p.Emode=="E"
                    
                     select p;

            if (pr.Count() > 0)
                return true;
            else

                return false;
        }
        public bool CheckProcessCardWithProduct(string pq,Int32 OPID,Int32 OPDID)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ProcessCards
                     from q in db.OrderProductDetails
                     where p.OPDID==q.OPDID && p.PCardNo == pq && p.Emode == "E"
                    && (OPDID == 0 || p.OPDID == OPDID)
                     && (OPID == 0 || q.OPID==OPID)

                     select p;

            if (pr.Count() > 0)
                return true;
            else

                return false;
        }

        public IEnumerable GetProcessCard(string pq,int i)
        {
            DBAccessDataContext db = new DBAccessDataContext();
           
           
                db.Connection.ConnectionString = _Conn;
                var pr = from p in db.GetAllPCard(pq,i)

                         select p;
                return pr;
            
            
            
         
            
        }
        public string  GetCustomer(string pq)
        {
            DBAccessDataContext db = new DBAccessDataContext();
            string s = "";
            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Orders
                     from q in db.Customers 
                     where p.CustID==q.CustID && p.PINo==pq
                     select q;
            foreach (Customer k in pr)
            {
                s = k.CustID + "|" + k.CName + "|" + k.Address;
            }
            return s;

        }
        public EOrderItemBalance GetOrderProductBalance(int OPDID)
        {
            EOrderItemBalance obj = new EOrderItemBalance();
            try
            {
                DBAccessDataContext db = new DBAccessDataContext();
                string s = "";
                db.Connection.ConnectionString = _Conn;
                var pr = from p in db.getOrderProductBalance(OPDID)

                         select p;

                var temp = pr.Single();

                obj.ProductionQty = Convert.ToDouble(temp.ProductionComplete);
                obj.Booking = Convert.ToDouble(temp.BookingQTY);
            }
            catch
            {

            }

            return obj;

        }
        public EOrderPcard GetOrderProductByPcard(string Pcard)
        {
            EOrderPcard obj = new EOrderPcard();
            try
            {
                DBAccessDataContext db = new DBAccessDataContext();
                string s = "";
                db.Connection.ConnectionString = _Conn;
                var pr = from p in db.GET_ORDER_PRODUCT_BY_PCARD(Pcard)

                         select p;

                var temp = pr.Single();

                obj.SL = Convert.ToInt32(temp.SL);
                obj.Color = Convert.ToString(temp.Color);
                obj.OrderProduct = Convert.ToString(temp.Descrip);
                obj.Weight = Convert.ToString(temp.Weight);
                obj.Width = Convert.ToString(temp.width);
                obj.GSM = Convert.ToString(temp.GSM);
                obj.PINO = Convert.ToString(temp.PINO);
                obj.IssuedQty = Convert.ToDouble(temp.MCCapacity);

            }
            catch
            {

            }

            return obj;

        }
        public List<EOrderRollQty> GetPackingProductByPcard(string Pcard)
        {
            List<EOrderRollQty> objli = new List<EOrderRollQty>();
            try
            {
                
                DBAccessDataContext db = new DBAccessDataContext();
                string s = "";
                db.Connection.ConnectionString = _Conn;
                var pr = from p in db.GET_PACKING_PRODUCT_BY_PCARD(Pcard)

                         select p;

                EOrderRollQty obj;
                foreach (var a in pr)
                {
                    obj = new EOrderRollQty();
                    obj.SL = Convert.ToInt32(a.SL);
                    obj.Qty = Convert.ToDouble(a.Qty);
                    obj.RollNo = Convert.ToString(a.RollNo);
                    objli.Add(obj);
                }
              

               
               

            }
            catch
            {

            }

            return objli;

        }
        public bool INS_UPD_REPROCESS_PCARD(string pcard,string oldPcard,string Process,string ProcessDes,double qty,string lotno,string notes, List<EOrderRollQty> li)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            try
            {

                db.INSERT_PACKING_PRODUCT_BY_PCARD(pcard, oldPcard, Convert.ToDecimal(qty), lotno, notes, Process, ProcessDes);


                foreach (EOrderRollQty ob in li)
                {
                    db.UPDATE_PACKING_PRODUCT_BY_PCARD(Convert.ToInt32(ob.SL), pcard);
                }
                return true;
            }
            catch
            {
                db.DELETE_PACKING_PRODUCT_BY_PCARD(pcard);
                return false;
            }
        }

    }
    public class EPcardInfo
    {
        public Int64 ID { get; set; }

        public string PCardNo { get; set; }
        public string PINO { get; set; }

        public string Width { get; set; }
        public string ReqNo { get; set; }
        public string Weight { get; set; }
        public string LotNo { get; set; }
        public string MCCapacity { get; set; }
        public string RQTY { get; set; }
        public string PQTY { get; set; }
        public string ExtraQty { get; set; }
        public string GSM { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
        public string txtCalculation { get; set; }
        public string Date { get; set; }
        public string PackingStyle { get; set; }
        public string txtYardCal { get; set; }
        public string Ptype { get; set; }

        public Int32 OrDesID { get; set; }
        public Int32 ColorID { get; set; }
        public Int32 FGID { get; set; }
        public Int32 FID { get; set; }




       

    }
    public class EOrderItemBalance
    {
        public Int64 OPID { get; set; }
        public double Booking { get; set; }
        public double ProductionQty { get; set; }

       
    }
}
