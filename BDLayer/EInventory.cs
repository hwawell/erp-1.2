﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BDLayer
{
   public class EInventory
    {
        
        



    }

    public class EINV_Item
    {

        public Int32  ItemID { get; set; }
        public Int64  GID { get; set; }
        public string GroupName { get; set; }
        public string ItemName { get; set; }
        public Int32  ParentItemID { get; set; }
        public string ParentItem { get; set; }
        public string EntryMode { get; set; }
        public string EntryID { get; set; }
        public Int32 INVTypeID { get; set; }
        public double StockBalance { get; set; }

        public override string ToString()
        {
            return ItemName;
        }
    }

    public class EINV_Receive
    {
        public  Int64 ID { get; set; }
        public Int64 SL { get; set; }
        public  Int32 ItemID { get; set; }
        public  Int32 ReceiveTypeID { get; set; }
        public  Int32 LanderID { get; set; }
        public Int32 INVTypeID { get; set; }
        public Int32 StoreID { get; set; }
        public string ItemName { get; set; }
        public string ReceiveType { get; set; }
        public string LoanCompany { get; set; }
        
        public  string BoxUNIT { get; set; }
        public  double BoxQty { get; set; }
        public  double TotalQty { get; set; }
        public  string Unit { get; set; }

        public double UnitPrice { get; set; }
        public double TotalAmount { get; set; }
        public string CashMemo { get; set; }

        public string Purchaser { get; set; }

        public  string LCNo { get; set; }
        public  string InvoiceNo { get; set; }
        public  string GatePassNo { get; set; }
        public  DateTime? RDate { get; set; }
        public  string Supplier { get; set; }
        public  string Country { get; set; }
        public  string Notes { get; set; }
        public string EMode { get; set; }
        public  string EntryID { get; set; }
        public string GroupName { get; set; }
        public string ParentItem { get; set; }


                       
                        
                          

    }
    public class EINV_Issue
    {
        public  Int64 ID { get; set; }
        public Int32 SL { get; set; }
        public  Int32 ItemID { get; set; }
        public  Int32 IssueTypeID { get; set; }
        public  Int32 LanderID { get; set; }
        public Int32 StoreID { get; set; }

        public string ItemName { get; set; }
        public string IssueType { get; set; }
        public string LoanCompany { get; set; }
        
        public  string BoxUnit { get; set; }
        public  double BoxQty { get; set; }
        public  double TotalQty { get; set; }
        public  string Unit { get; set; }
        public  string InvoiceNo { get; set; }
        public  string GatePassNo { get; set; }
        public  string Supplier { get; set; }
        public  string Country { get; set; }
        public  DateTime? Idate { get; set; }
        public  string Notes { get; set; }
        public  string EntryID { get; set; }
        public  string EMode { get; set; }
        public  Int32 INVTypeID { get; set; }

        public string GroupName { get; set; }
        public string ParentName { get; set; }
    }
    public class EINV_Transfer
    {
        public Int64 ID { get; set; }
        public Int32 ItemID { get; set; }
      
        public Int32 FromStoreID { get; set; }
        public Int32 ToStoreID { get; set; }
        public Int64 IsssueID { get; set; }
        public Int64 ReceiveID { get; set; }

        public string ItemName { get; set; }
        public string TransferStore { get; set; }
       
        public double TransferQty { get; set; }
        public string Unit { get; set; }
       
        public DateTime? Tdate { get; set; }
        public string Notes { get; set; }
        public string EntryID { get; set; }
        public string RefCode { get; set; }
        public Int32 INVTypeID { get; set; }
    }
    public class EINV_TransferReport
    {



        public Int64 ID { get; set; }
    
        public string ItemName { get; set; }
        public string GroupName { get; set; }
        public DateTime? TransferDate { get; set; }

        public double TransferQty { get; set; }
        public string Unit { get; set; }

       
        public string Notes { get; set; }
        public string ReqNo { get; set; }
        public string ToStore { get; set; }
        public string FromStore { get; set; }
    }
    public class EINV_Store
    {
        public Int64 ID { get; set; }
        public string StoreName { get; set; }

       

    }
    public class EINV_StoreReport
    {
        public Int64 ID { get; set; }
        public Int32 ItemID { get; set; }

        public Int32 INVStoreID { get; set; }
     

        public string ItemName { get; set; }
        public string ParentName { get; set; }

       
        public string GroupName { get; set; }

        public double OpenBal { get; set; }

        public string IssTotStr { get; set; }
        public string RcvTotStr { get; set; }
        public string StockStr { get; set; }
        public double IssTot { get; set; }
        public double RcvTot { get; set; }
        public double Balance { get; set; }


      
    }
    public class EINV_StoreReportDetails
    {
        public Int64 ID { get; set; }
        public Int64 ItemID { get; set; }
        public string ItemName { get; set; }
        public string ParentName { get; set; }


        public string GroupName { get; set; }

        public double OpenBal { get; set; }

        public double[] MainStoreRCVList { get; set; }
        public double[] MainStoreIssList { get; set; }
        public double[] MainStoreBalList { get; set; }

        public double MainStoreRCV { get; set; }
        public double SubStore1RCV { get; set; }
        public double SubStore2RCV { get; set; }
        public double SubStore3RCV { get; set; }

        public double MainStoreISS { get; set; }
        public double SubStore1ISS { get; set; }
        public double SubStore2ISS { get; set; }
        public double SubStore3ISS { get; set; }


        public double MainStoreBAL { get; set; }
        public double SubStore1BAL { get; set; }
        public double SubStore2BAL { get; set; }
        public double SubStore3BAL { get; set; }


        
        public double Balance { get; set; }



    }
}
