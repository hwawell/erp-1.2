﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
namespace BDLayer
{
    public class Yarn_DL
    {
     string _Conn;
        public Yarn_DL()
        {
            _Conn = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        }

        public void SaveYarnGroup(YarnGroup y)
        {

            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.YarnGroup_INS_UPD(y.ID, y.YarnGroupName);

            db.SubmitChanges();


        }
        public IQueryable GetActiveYarnGroup()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.YarnGroups
                   
                     //select p;
                     select p;

            return pr;
        }
        public void Save(Yarn  y)
        {
            
            DBAccessDataContext db = new DBAccessDataContext();
             
            db.Connection.ConnectionString = _Conn;
            db.INSERT_YARN_SP(y.YID, y.YarndGroupID, y.YarnName, y.YarnDesc, y.EntryID);
                   
            db.SubmitChanges();
                    

        }
      
        public bool SavePurchase(YarnPurchase y)
        {
             try
            {
         
                DBAccessDataContext db = new DBAccessDataContext();

                db.Connection.ConnectionString = _Conn;

                db.Insert_YarnPurchase_SP(y.SL, y.YID, y.PurchaseType, y.UNIT, y.CTNQty, y.TotalQty, y.LCNo, y.LotNo, y.InvoiceNo, y.PDate, y.Supplier, y.Country, y.Notes, y.Status, y.YarnLCBookingID, y.FromBarcodeID, y.ToBarcodeID, y.EntryID);

                db.SubmitChanges();
                return true;
            }
             catch
             {
                 return false;
             }



        }
        public bool SaveIssue(YarnIssue y)
        {

            try
            {
                DBAccessDataContext db = new DBAccessDataContext();

                db.Connection.ConnectionString = _Conn;
                //db.Insert_YarnPurchase_SP(y.SL, y.YID, y.PurchaseType, y.Qty, y.PDate, y.PurchaseFrom, y.Challan, y.StockTo, y.EntryID);

                db.INSERT_YARN_ISSUE_SP(y.SL, y.YID, y.IssueType, y.Unit, y.CTNQTy, y.Qty, y.Idate, y.IssueTo,y.LotNo,y.InvoiceNo,y.IssueDesc,y.Supplier,y.Country, y.EntryID);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }
        public IQueryable GetActiveRecord()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Yarns
                     where p.EMode == "E" 
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetYarnByYarnGroup(Int64 yGID)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Yarns
                     where p.EMode == "E" && p.YarndGroupID==yGID
                     //select p;
                     select p;

            return pr;
        }

        public IQueryable GetYarnCountry()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.YarnSuppliers
                     where p.SType=="COUNTRY"

                     orderby p.CountryOrSupplier
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetYarnSupplier()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.YarnSuppliers
                     where p.SType == "SUPPLIER"
                     orderby p.CountryOrSupplier
                     //select p;
                     select p;

            return pr;
        }
        public bool SaveYarnCountrySupplier(YarnSupplier ob)
        {

            try
            {
                DBAccessDataContext db = new DBAccessDataContext();

                db.Connection.ConnectionString = _Conn;
                db.YarnCountrySupplier_INS_UPD(ob.ID, ob.CountryOrSupplier, ob.SType);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }

           
        }
        public IQueryable GetActivePurchaseRecord(int start, int end)
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;
      
            db.Connection.ConnectionString = _Conn;

            var pr = from p in db.YarnPurchases
                     from q in db.Yarns
                     where p.EMode == "E" && p.YID==q.YID orderby p.PDate descending
                     //select p;
                     select new
                     {
                         p.SL,
                         p.YID,
                         q.YarnName,
                         p.TotalQty,
                         p.Status,
                         p.PDate,
                         p.Supplier,p.Country
                        

                     };

            return pr.Skip(start - 1).Take(end);
        }
        public IQueryable GetActivePurchaseRecordBYLCNO(Int64 ID)
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;

            db.Connection.ConnectionString = _Conn;

            var pr = from p in db.YarnPurchases
                     
                     where p.EMode == "E" && p.YarnLCBookingID==ID
                     orderby p.SL descending 
                     //select p;
                     select new
                     {
                         p.SL,
                         p.UNIT,
                         p.LotNo,
                         p.InvoiceNo,
                         p.PDate,
                         p.CTNQty,

                         p.Notes,
                         p.TotalQty
                         


                     };

            return pr;
        }
        public IQueryable GetActiveIssueRecord(int start, int end)
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.YarnIssues
                     from q in db.Yarns
                     where p.EMode == "E" && p.YID==q.YID orderby p.Idate descending
                     //select p;
                     select new
                     {
                         p.SL,
                         p.YID,
                         q.YarnName,
                         p.Qty,
                         p.IssueTo,
                         p.Idate,
                         p.Country,p.Supplier

                     };

            return pr.Skip(start - 1).Take(end);
        }
        public IQueryable GetDeletedRecord()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Yarns
                     where p.EMode == "D"
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetDeletedPurchaseRecord()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.YarnPurchases
                     where p.EMode == "D"
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetDeletedIssuedRecord()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.YarnIssues
                     where p.EMode == "D"
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetAllUnits()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Units
                       //select p;
                     select p;

            return pr;
        }
       
        public void DeleteActive(long custid,string mo,string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid,"YARN");
            db.SubmitChanges();


        }
        public void DeleteYarnGroup(long custid, string mo, string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "YARN");
            db.SubmitChanges();


        }
        public void DeleteYarnSupplier(long custid, string mo, string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "YARN_SUPPLIER");
            db.SubmitChanges();


        }
        public void PurchaseDeleteActive(long custid, string mo, string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "YARN_PURCHASE");
            db.SubmitChanges();


        }
        public void IssuedDeleteActive(long custid, string mo, string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "YARN_ISSUE");
            db.SubmitChanges();


        }
        public List<EYarnRequistion> GetYarnRequisition(DateTime From, DateTime To)
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;
            int i=0;
            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.NEW_YARN_REQUISITION_LIST(From,To)
                    
                     
                     select p;

            List<EYarnRequistion> li = new List<EYarnRequistion>();
            foreach (var k in pr)
            {
                EYarnRequistion obj = new EYarnRequistion();
                i++;
                obj.ID = i;
                obj.ReqNo = k.SetupCode;
                obj.ReqDate =k.SetupDate.Value.Date.ToString("dd-MMM-yyyy");
                obj.GreyDescription = k.Fabric;
                obj.GM = k.GM;
                obj.GSM = k.GSM;
              
                obj.YARN = k.YarnName;
                obj.ReqQty =Convert.ToDouble( k.Qty);
                obj.IssuedQty = Convert.ToDouble(k.Issued);
                obj.BalanceQty = Convert.ToDouble(obj.ReqQty-obj.IssuedQty);

                obj.OrderNo = k.orderList;
                obj.KNIT_MC = k.MC;

                li.Add(obj);
            }

            return li;
        }

        public List<EYarnStockReport> GetYarnStockReport(DateTime From, Int64 GID)
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;
            int i = 0;
            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.NEW_YARN_STOCK_REPORT(From, GID)


                     select p;

            List<EYarnStockReport> li = new List<EYarnStockReport>();
            foreach (var k in pr)
            {
                EYarnStockReport obj = new EYarnStockReport();
                i++;
                obj.ID = i;

                obj.YarnGroup = k.YarnGroupName;
                obj.YarnName = k.YarnName;
                obj.YarnCompany = k.Supplier;

                obj.YarnCountry = k.Country;
                obj.Unit = k.UNIT;
                obj.AvailableCTN =(k.TotalCTN-k.TotalIssuedBOX).ToString();

                obj.LotNo = k.LotNo;
                obj.OpeningBalance =Convert.ToDouble(k.OPBalRCV);



                obj.OpeningBalance = Convert.ToDouble(k.OPBalRCV)-Convert.ToDouble(k.ISS_OPBal);
                obj.Purchase_Receive = Convert.ToDouble(k.RECEIVE_QTY);
                obj.Return_Receive = Convert.ToDouble(k.RETURN_QTY);
                obj.Loan_Receive = Convert.ToDouble(k.LOAN_QTY);
                obj.SubCOn_Receive = Convert.ToDouble(k.SC_QTY);
                obj.Production_Issue = Convert.ToDouble(k.ISS_PROD_QTY);
                obj.Return_Issue = Convert.ToDouble(k.ISS_RETURN_QTY);
                obj.Loan_Issue = Convert.ToDouble(k.ISS_LOAN_QTY);
                obj.SubCont_Issue = Convert.ToDouble(k.ISS_SC_QTY);
                obj.ClosingBalanceQty = obj.OpeningBalance +(obj.Purchase_Receive+obj.Return_Receive+obj.Loan_Receive+obj.SubCOn_Receive) ;
                obj.ClosingBalanceQty  = obj.ClosingBalanceQty - (obj.Production_Issue + obj.Return_Issue + obj.Loan_Issue + obj.SubCont_Issue);

                if (obj.ClosingBalanceQty >= 1)
                {
                    li.Add(obj);
                }
      
       
            }

            return li;
        }
        public List<EYarnPurchase> GetYarnReceiveReport(DateTime From, DateTime To, string Type)
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;
            int i = 0;
            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Yarn_PURCHASE_rpt_sp(From, To,Type)


                     select p;

            List<EYarnPurchase> li = new List<EYarnPurchase>();
            foreach (var k in pr)
            {
                EYarnPurchase obj = new EYarnPurchase();
                i++;
                obj.ID = i;
                obj.DDate = k.PDate.Value.Date.ToString("dd-MM-yyyy");
                obj.YarnCompany = k.Supplier;
                obj.YarnCountry = k.country;
                obj.LCNumber = k.LCNo;
                obj.InvoiceNo = k.InvoiceNo;
                obj.YarnGroup = k.YarnGroupName;
                obj.YarnName = k.YarnName;
                obj.Lot = k.LotNo;
                obj.Unit = k.UNIT;
                obj.UnitQty =Convert.ToDouble( k.CTNQty);
                obj.TotalQty =Convert.ToDouble( k.TotalQty);
                obj.ReceiveType = k.PurchaseType;

                obj.Note = k.Notes;
                li.Add(obj);
              
            }

            return li;
        }

        public List<EYarnIssue> GetYarnIssueReport(DateTime From, DateTime To, string Type)
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;
            int i = 0;
            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Yarn_Issue_rpt_sp(From, To, Type)


                     select p;

            List<EYarnIssue> li = new List<EYarnIssue>();
            foreach (var k in pr)
            {
                EYarnIssue obj = new EYarnIssue();
                i++;
                obj.ID = i;
                obj.DDate = k.Idate.Value.Date.ToString("dd-MM-yyyy");
                obj.YarnCompany = k.Supplier;
                obj.YarnCountry = k.country;
                obj.IssueTo = k.IssueTo;
                obj.InvoiceNo = k.InvoiceNo;
                obj.YarnGroup = k.YarnGroupName;
                obj.YarnName = k.YarnName;
                obj.MC_List = k.MCList;
                obj.Lot = k.LotNo;
                obj.Unit = k.Unit;
                obj.UnitQty = Convert.ToDouble(k.CTNQTy);
                obj.TotalQty = Convert.ToDouble(k.Qty);
                obj.IssueType = k.IssueType;

                obj.Note = k.IssueDesc;

                li.Add(obj);
     
            }

            return li;
        }


        public string SaveYarnLCBooking(YarnLCBooking y)
        {

            try
            {
                DBAccessDataContext db = new DBAccessDataContext();
                string str="";
                db.Connection.ConnectionString = _Conn;
                //db.Insert_YarnPurchase_SP(y.SL, y.YID, y.PurchaseType, y.Qty, y.PDate, y.PurchaseFrom, y.Challan, y.StockTo, y.EntryID);

                db.YarnLCBooking_INS_UPD(y.ID,y.YID,y.Supplier,y.CountryOfOrigin,y.LCQty,y.ODate,y.LCNo,y.Notes,y.IsAllReceived,y.EMode,y.EntryID,y.ImportPINO,y.TotalLCValue,ref str);
                db.SubmitChanges();
                return str;
            }
            catch
            {
                return "0-Database Connection Failed";
            }

        }
        public bool IsDuplicateBarcode(Int32 frmID, Int32 toID)
        {

            try
            {
                DBAccessDataContext db = new DBAccessDataContext();
                string str = "";
                db.Connection.ConnectionString = _Conn;
                //db.Insert_YarnPurchase_SP(y.SL, y.YID, y.PurchaseType, y.Qty, y.PDate, y.PurchaseFrom, y.Challan, y.StockTo, y.EntryID);

                db.YARN_BarcodeCheaking(frmID, toID,ref str );
                db.SubmitChanges();

                if (str=="1")
                  return true;
                else
                    return false;
            }
            catch
            {
                return true;
            }

        }
        public bool DeleteYarnLCBooking(Int64 ID,string EntryID)
        {

            try
            {
                DBAccessDataContext db = new DBAccessDataContext();
                string str = "";
                db.Connection.ConnectionString = _Conn;
                //db.Insert_YarnPurchase_SP(y.SL, y.YID, y.PurchaseType, y.Qty, y.PDate, y.PurchaseFrom, y.Challan, y.StockTo, y.EntryID);

                db.YarnLCBooking_Delete(ID,EntryID);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public List<EYarnLCBooking> GetYarnLCBooking(int Start,int To,string LCNO)
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;
            int i = 0;
            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.YarnLCBooking_Get(LCNO)


                     select p;

            List<EYarnLCBooking> li = new List<EYarnLCBooking>();
            foreach (var k in pr.Skip(Start - 1).Take(To))
            {
                EYarnLCBooking obj = new EYarnLCBooking();
               
                obj.ID = k.ID;
                obj.YID =Convert.ToInt32( k.YID);
                obj.YGID = Convert.ToInt32(k.YarndGroupID);
                obj.Yarn = k.YarnName;
                obj.Supplier = k.Supplier;
                obj.Country = k.CountryOfOrigin;
                obj.LCNo = k.LCNo;
                obj.LCQty = k.LCQty.ToString();
                obj.Notes = k.Notes;
                obj.ReceiveQty = k.RcvQty.ToString();
                obj.Date = k.ODate.Value.ToString("MM/dd/yyyy");
                obj.YarnLCValue =Convert.ToDouble( k.TotalLCValue??0);
                obj.YarnTotalLCValue = Convert.ToDouble(Convert.ToDouble( k.LCQty) * obj.YarnLCValue);
                li.Add(obj);
            }

            return li;
        }


    }
}
