﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace BDLayer
{
    public class Customer_DL
    {
        string _Conn;
        public Customer_DL()
        {
            _Conn = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        }
        public void Save(Customer  p)
        {
            string res="";
            DBAccessDataContext db = new DBAccessDataContext();
             
            db.Connection.ConnectionString = _Conn;
            db.INSERT_CUSTOMER_SP(p.CustID, p.CName, p.Address, p.Tel, p.ConPerson, p.EntryID);
                        
            db.SubmitChanges();
                    

        }
        public IQueryable GetActiveCustomers()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Customers
                     where p.EMode == "E" 
                     orderby p.CName

                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetDeletedCustomers()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Customers
                     where p.EMode == "D"
                     //select p;
                     select p;

            return pr;
        }
        public void DeleteActive(long custid,string mo,string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Customer_Mode(custid, mo, eid);
            db.SubmitChanges();


        }
    }
}
