﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BDLayer
{
   public class Entity
    {
      
    }
   [Serializable]
   public class EKnitSetupOrderList
   {

       public string OrderNo { get; set; }
       public int OPID { get; set; }
       public string Description { get; set; }
       public double Qty { get; set; }

   }
   public class EOrderRollQty
   {




       public bool Select { get; set; }
       public Int64 SL { get; set; }
       public string RollNo { get; set; }
       public double Qty { get; set; }

   }
   public class EOrderPcard
   {




       public Int64 SL { get; set; }
       public string PINO { get; set; }
       public string OrderProduct { get; set; }
       public string Color { get; set; }
       public string Width { get; set; }
       public string GSM { get; set; }

       public string Weight { get; set; }

       public double IssuedQty { get; set; }

   }
   public class EDeydFab_Receive
   {
       public Int32 ID { get; set; }
       public Int32 OPID { get; set; }
       public string OrderNo { get; set; }
       public string Product { get; set; }
       public DateTime? RcvDate { get; set; }
       public double Qty { get; set; }
       public int Roll { get; set; }
       public string Invoice { get; set; }
       public string TypeOfProduction { get; set; }
       public string Notes { get; set; }
       public string EntryID { get; set; }

   }
   public class EDeydFab_Issue
   {
       public Int32 ID { get; set; }
       public Int32 OPID { get; set; }
       public string OrderNo { get; set; }
       public string PCardNo { get; set; }
       public DateTime? IssueDate { get; set; }
       public double Qty { get; set; }
       public int Roll { get; set; }

       public string TypeOfProduction { get; set; }
       public string TypeOfWork { get; set; }
       public string Notes { get; set; }
       public string EntryID { get; set; }

   }
   public class EAttendence
   {

      
       public string PCardNo { get; set; }



       public string PINO { get; set; }
       public Int64 Empid { get; set; }

       public DateTime width { get; set; }

       public string Weight { get; set; }

       public string MCCapacity { get; set; }

       public double LotNo { get; set; }
       public string GSM { get; set; }
       public int ProductionQty { get; set; }

   }

   public class EOrderProductDetails
   {


      

       public string Color { get; set; }
          public string ColorNo { get; set; }
          public string Idesc { get; set; }
       public Int64 OPDID { get; set; }
       public Int64 OPID { get; set; }
       public double Qty { get; set; }

   }
   public class EWareHousePacking
   {




       public Int64 SL { get; set; }
       public string Merchandiser { get; set; }
       public DateTime Date { get; set; }
       public string OrderNo { get; set; }
       public string Customer { get; set; }
       public string ItemDescription { get; set; }
       public string GSM { get; set; }

       public string Width { get; set; }

       public string Color { get; set; }
       public string Lotno { get; set; }
       public string ProcessCard { get; set; }
       public double RollQty { get; set; }
       public double StockQty { get; set; }

   }
   public class EWareHouseRCVPacking
   {




       public Int64 SL { get; set; }
       public string Merchandiser { get; set; }
       public string OrderNo { get; set; }
       public string Customer { get; set; }
       public string ItemDescription { get; set; }
       public string GSM { get; set; }

       public string Width { get; set; }

       public string Color { get; set; }
       public string Lotno { get; set; }
       public string ProcessCard { get; set; }
       public DateTime ReceiveDate { get; set; }
       public double RcvRollQty { get; set; }
       public double RcvQty { get; set; }

   }
   public class EWareHouseDeliverySummary
   {




       public Int64 SL { get; set; }
       
       
       public string OrderNo { get; set; }
       public string Customer { get; set; }
       public string Merchandiser { get; set; }
       public DateTime LastDelivery { get; set; }


       public double BookingKG { get; set; }
       public double BookingYDS { get; set; }
       public double BookingMETER { get; set; }

       public string Unit { get; set; }

       public double DeliveryKG { get; set; }
       public double DeliveryYDS { get; set; }
       public double DeliveryMETER { get; set; }


      

      

   }
   public class EWareHouseDelivery
   {




       public Int64 SL { get; set; }
       public string DeliveryDate { get; set; }
       public string Merchandiser { get; set; }
       public string OrderNo { get; set; }
       public string Customer { get; set; }
       public string ItemDescription { get; set; }
       public string Notes { get; set; }
       public string GSM { get; set; }
       public string Width { get; set; }
       public string Color { get; set; }
      
       public string Lotno { get; set; }
       public string ProcessCard { get; set; }

       public string Challan { get; set; }
       public string GetPassNo { get; set; }
       public string VehicleNo { get; set; }
       public double DeliveredRollQty { get; set; }
       public double DeliveredQtyKGS { get; set; }
       public string Unit { get; set; }
       public double DeliveredQtyByUnit { get; set; }

       public string DeliveryStatus { get; set; }
       public string AssignFrom { get; set; }
       public string DeliveryNotes { get; set; }

   }
   public class EWareHouseGeneralDelivery
   {




       public Int64 SL { get; set; }
       public string DeliveryDate { get; set; }
       public string RefName { get; set; }
       public string RefNo { get; set; }
       public string PartyName { get; set; }
       public string Type { get; set; }
       public string ItemGroup { get; set; }
       public string ItemName { get; set; }
       public string Description { get; set; }
       public string Challan { get; set; }

       public string GatePassNo { get; set; }
       public string VehicleNo { get; set; }

       public double CTN_BAG_PCS_ROLL_QTY { get; set; }
       public double DeliverQTY { get; set; }
       public string Unit { get; set; }

       public string Note { get; set; }

   }
   public class EWareHouseReturn
   {




       public Int64 SL { get; set; }
       public DateTime ReturnDate { get; set; }
       public string Merchandiser { get; set; }
       public string OrderNo { get; set; }
       public string Customer { get; set; }
       public string ItemDescription { get; set; }
       public string GSM { get; set; }

       public string Width { get; set; }

       public string Color { get; set; }
       public string Lotno { get; set; }
       public string ProcessCard { get; set; }
       public double ReturnRollQty { get; set; }
       public double ReturnQty { get; set; }

   }

   public class EKNITDailyReport
   {




      
       public string MCNo { get; set; }
       public string FabricGroup { get; set; }
       public string Fabric { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public int Roll_No { get; set; }

       public double RollQty { get; set; }

       public DateTime KnitDateTime { get; set; }
       public String Shift { get; set; }
      

   }
}
