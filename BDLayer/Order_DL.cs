﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.Linq;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Collections;


namespace BDLayer
{
   public class Order_DL
    {
        string _Conn;
        public Order_DL()
        {
            _Conn = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        }
        public void Save(Order  o)
        {
            string res="";
            DBAccessDataContext db = new DBAccessDataContext();
             
            db.Connection.ConnectionString = _Conn;
            db.INSERT_ORDER_SP(o.PINo, o.CustID, o.PDate, o.RcvDate, o.ShpDate, o.LCNo, o.EntryID);
                   
            db.SubmitChanges();
                    

        }
        public List<KNIT_Prod_Setup> GetReq(string OrderNo)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            return ( from p in db.KNIT_Prod_Setups
                     from q in db.KNIT_Prod_Orders
                     where p.ID == q.KNIT_SETUP_ID && q.OrderNo == OrderNo
                     orderby p.SetupCode
                     select p).ToList < KNIT_Prod_Setup>();
                    

            
        }
        public List<ProcessCard> GetReqByPCardNo(string PcarNo)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            return (from p in db.ProcessCards
                    where p.PCardNo == PcarNo
                    orderby p.ReqNo
                    select p).ToList<ProcessCard>();



        }
        public void SaveOP(OrderProduct o)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            
            db.INSERT_OrderProduct_SP(o.OPID, o.PINO, o.Description, o.Notes, o.Weight, o.Width, o.Unit, 0,o.PerUnitKG, o.EntryID);
            db.SubmitChanges();


        }
        public void SaveOPDetails(OrderProductDetail o)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.INSERT_OrderProductDetails_SP(o.OPDID, o.OPID, o.Color, o.ColorNo, o.Idesc, o.Qty, o.EntryID);
            db.SubmitChanges();


        }
        public void SaveOrderReceive(string PINO,Int32 CustID,string EntryID)
        {

            try
            {
                DBAccessDataContext db = new DBAccessDataContext();

                db.Connection.ConnectionString = _Conn;
                db.NEW_ORDER_RECEIVE(PINO, CustID, EntryID);
                db.SubmitChanges();

            }
            catch
            {

            }


        }
        public IQueryable GetActiveRecord()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Orders
                     where p.EMode == "E"
                     orderby p.PINo.Substring(0, 3), p.EntryDate descending
                     //select p;
                     select p;

            return pr;
        }
        public List<EOrderList> GetActiveOrderList()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            //var pr = from p in db.NEW_GET_PINO(0)
            //         select p;

            var pr = from p in db.Orders
                     where p.EMode == "E"
                     
                     select p;

            List<EOrderList> li =new  List<EOrderList>();
            EOrderList obj = new EOrderList();
            
             foreach(var a in pr)
             {
                 obj = new EOrderList();
                 obj.OrderNo = a.PINo;
                
                 li.Add(obj);
             }

             return li;
        }

        public bool HasOrderNoExist(string PINO)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            //var pr = from p in db.NEW_GET_PINO(0)
            //         select p;

            var pr = from p in db.Orders
                     where p.EMode == "E" && p.PINo==PINO

                     select p;

            if (pr.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

            
        }
       
       
        public List<OrderQ> GetNewOrderQueque(string CName,string PINO)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.NEW_getOrderQ()
                     where p.IsNew==true && p.IsReceived==false
                     && (CName == "" || p.CustomerName.ToUpper().Contains(CName.ToUpper()))
                      && (PINO == "" || p.PINo.ToUpper().Contains(PINO.ToUpper())) 
                     orderby p.PINo
                     //select p;
                     select p;
             List<OrderQ> li =new  List<OrderQ>();

             foreach(var a in pr)
             {
                 OrderQ obj = new OrderQ();
                 obj.PINo = a.PINo;
                 obj.ProductionType = a.ProductionType;
                 obj.CustID = a.CustID;
                 obj.CustomerName = a.CustomerName;
                 obj.ApprovalDate = a.ApprovalDate;
                 obj.OrderTypeName = a.OrderTypeName;
                 obj.EntryID = a.EntryID;
                 li.Add(obj);
             }

             return li;
        }

        public List<OrderQ> GetRevisedOrderQueque(string CName, string PINO)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.NEW_getOrderQ()
                     where p.IsNew == false && p.IsReceived == false
                      && (CName == "" || p.CustomerName.ToUpper().Contains(CName.ToUpper()))
                      && (PINO == "" || p.PINo.ToUpper().Contains(PINO.ToUpper())) 
                     orderby p.PINo
                     //select p;
                     select p;
            List<OrderQ> li = new List<OrderQ>();

            foreach (var a in pr)
            {
                OrderQ obj = new OrderQ();
                obj.PINo = a.PINo;
                obj.ProductionType = a.ProductionType;
                obj.CustID = a.CustID;
                obj.CustomerName = a.CustomerName;
                obj.ApprovalDate = a.ApprovalDate;
                obj.OrderTypeName = a.OrderTypeName;
                obj.EntryID = a.EntryID;
                li.Add(obj);
            }

            return li;
        }
        public List<EProductionReport> GetAllProductionSummary(DateTime From, DateTime To, string PINo, long CustID, string Merchand, int Operation)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.NEW_RPT_PRODUCTION_Summ(From, To, PINo, CustID, Merchand, Operation)

                     select p;
            List<EProductionReport> li = new List<EProductionReport>();

            foreach (var a in pr)
            {
                EProductionReport obj = new EProductionReport();
                obj.OPID = a.OPID;
                obj.OrderNo = a.PINO;
                obj.Product = a.Description;
                obj.Merchandiser = a.EntryID;
                obj.Customer = a.CName;
                obj.GSM = a.GSM.ToString();
                obj.Width = a.WidthText;
                obj.BookingQty =Convert.ToDouble( Convert.ToDouble(a.BQty).ToString("0.00"));
                obj.Qty = Convert.ToDouble(a.TotQty);
                obj.Balance =  Convert.ToDouble( (obj.BookingQty - obj.Qty).ToString("0.00"));
                li.Add(obj);
            }

            return li;
        }
        public List<EProductionReport> GetAllProductionSummaryAll( string PINo, long CustID, string Merchand, int Operation)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.NEW_RPT_PRODUCTION_Summ_All(PINo, CustID, Merchand, Operation)

                     select p;
            List<EProductionReport> li = new List<EProductionReport>();

            try
            {
                foreach (var a in pr)
                {
                    EProductionReport obj = new EProductionReport();
                    obj.OPID = a.OPID;
                    obj.OrderNo = a.PINO ?? "";
                    obj.Product = a.Description ?? "";
                    obj.Merchandiser = a.EntryID ??"";
                    obj.Customer = a.CName ?? "";
                    obj.GSM = a.GSM.ToString() ?? "";
                    obj.Width = a.WidthText ?? "";
                    obj.BookingQty = Convert.ToDouble(Convert.ToDouble(a.BQty).ToString("0.00"));
                    obj.Qty = Convert.ToDouble(a.TotQty);
                    obj.Balance = Convert.ToDouble((obj.BookingQty - obj.Qty).ToString("0.00"));
                    li.Add(obj);
                }
            }
            catch
            {

            }

            return li;
        }
       

        public List<OrderQ> GetReceivedOrder(string CName, string PINO)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.NEW_getOrderQ()
                     where  p.IsReceived == true
                      && (CName == "" || p.CustomerName.ToUpper().Contains(CName.ToUpper()))
                      && (PINO == "" || p.PINo.ToUpper().Contains(PINO.ToUpper()))
                     orderby p.PINo
                     //select p;
                     select p;
            List<OrderQ> li = new List<OrderQ>();

            foreach (var a in pr)
            {
                OrderQ obj = new OrderQ();
                obj.PINo = a.PINo;
                obj.ProductionType = a.ProductionType;
                obj.CustID = a.CustID;
                obj.CustomerName = a.CustomerName;
                obj.ApprovalDate = a.ApprovalDate;
                obj.OrderTypeName = a.OrderTypeName;
                obj.EntryID = a.EntryID;
                li.Add(obj);
            }

            return li;
        }
        public DataSet GetOrderData(string pi)
        {
            DataSet ds = new DataSet();
            try
            {
                string str = "SELECT CustID,CONVERT(nvarchar(11), Pdate, 101) PDate,CONVERT(nvarchar(11), RcvDate, 101) RcvDate,CONVERT(nvarchar(11), ShpDate, 101) ShpDate,lCNO from [order] where  EMode='E' and PINO='" + pi + "'";
               
                ds = SqlHelper.ExecuteDataset(this._Conn, CommandType.Text, str);
                return ds;
            }
            catch
            {
                return ds;
            }
           
        }
        public DataSet GetOrderDataQ(string pi)
        {
            DataSet ds = new DataSet();
            try
            {
                string str = "SELECT * from [orderQ] where PINO='" + pi + "'";

                ds = SqlHelper.ExecuteDataset(this._Conn, CommandType.Text, str);
                return ds;
            }
            catch
            {
                return ds;
            }

        }
        public DataSet GetOrderDataQ(bool IsNew,bool IsRevised)
        {
            DataSet ds = new DataSet();
            try
            {
                string str = "SELECT  from [orderQ] where IsApproved=1 and PINO='" + IsNew + "'";

                ds = SqlHelper.ExecuteDataset(this._Conn, CommandType.Text, str);
                return ds;
            }
            catch
            {
                return ds;
            }

        }
        public IEnumerable  GetOrderProduct(string pi)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var q = from p in db.getOrder(pi)
                    select p;
            return q;
        }
        public IEnumerable GetOrderProductQ(string pi)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var q = from p in db.NEW_getOrderProductQ(pi)
                    select p;
            return q;
        }
        //public IEnumerable GetOrderProduct(string pi)
        //{
        //    DBAccessDataContext db = new DBAccessDataContext();

        //    db.Connection.ConnectionString = _Conn;
        //    var q = from p in db.getOrder(pi)
        //            select p;
        //    return q;
        //}
        public IEnumerable GetOrderProductDesc(string pi)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var q = from p in db.OrderProducts
                    where p.EMode == "E" && p.PINO == pi
                    select new
                    {
                        p.Description,p.OPID, 
                          p.Notes ,
                        DESC = string.Format("{0}>{1}>{2}>{3}", p.Description, p.Notes,p.Weight,p.Width)



                    };
            return q;
        }

        public List<EOrderProductDetails> GetOrderProductColorList(long pi)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var q = from p in db.NEW_getOrderProduct_DETAILS(pi)
                    
                    select p;
            List<EOrderProductDetails> li = new List<EOrderProductDetails>();
            EOrderProductDetails obj;
            foreach (var v in q)
            {
                obj = new EOrderProductDetails();
                obj.Color = v.Color;
                obj.ColorNo = v.ColorNo;
                obj.OPDID = v.OPDID;
                obj.OPID = v.OPID ?? 0;
                obj.Qty = Convert.ToDouble(v.Qty);
                obj.Idesc = v.Idesc;

                li.Add(obj);
            }
            return li;
        }

        public IEnumerable GetOrderProductColor(long pi)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var q = from p in db.OrderProductDetails
                    where p.EMode == "E" && p.OPID == pi
                    select p;

           
            return q;
        }
        public IEnumerable GetOrderStatus( string pi)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var q = from p in db.GetCurrentStatus(pi)
                    select p;
            return q;
        }
        public IEnumerable GetOrderProductDetails(long pi)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var q = from p in db.getOrderProduct(pi)
                   
                    select p ;
            return q;
        }
        public IEnumerable GetOrderProductDetailsQ(long pi)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var q = from p in db.NEW_getOrderProductDetails(pi)

                    select p;
            return q;
        }
        public IEnumerable GetDes()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var q = from p in db.Descriptions
                    where p.EntryMode=="E"  
                    select p;
            return q;
        }
        public DataSet GetOrderProduct1(string pi)
        {

            string strSql = "SELECT     PINO,OPID, [Description],  Weight, Width, Unit, PerUnitPrice FROM  OrderProduct WHERE EMODE='E' and    (PINO ='" + pi + "') UNION ALL SELECT     '" + pi + "',0 , '' ,'','','',0 FROM         OrderProduct  WHERE     (PINO = '" + pi + "') having count(*)=0 ";
            return SqlHelper.ExecuteDataset(this._Conn, CommandType.Text, strSql);


        }
        public void DeleteOrderProduct(long op,string eid)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(op, "D", eid, "ORDER_PRODUCT");
            db.SubmitChanges();
            

        }
        public void DeleteOrderProductDetails(long op, string eid)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(op, "D", eid, "OrderProductDetails");
            db.SubmitChanges();


        }
        public Boolean OrderUpdate(string PI,string Mode,string EID)
        {

            try
            {
                string str = "Update [Order] set EMode='"+ Mode +"',EntryID='"+ EID + "' where PINO='" + PI + "'";
                SqlHelper.ExecuteNonQuery(this._Conn, CommandType.Text, str);

                return true;
            }
            catch
            {
                return false;
            }
        }
        public Boolean OrderProductUpdate(long OPI, string Mode, string EID)
        {

            try
            {
                string str = "Update [OrderProduct] set EMode='" + Mode + "',EntryID='" + EID + "' where OPID='" + OPI + "'";
                SqlHelper.ExecuteNonQuery(this._Conn, CommandType.Text, str);
               
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable GetColor()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var q = from p in db.Colors
                    where p.EntryMode=="E"
                    select p;
            return q;
        }
        public List<EProductionLoss> GetProductLoss(Int64 OPDID,EProductionLoss objLoss)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.NEW_ORDER_LOSS_REPORT(OPDID)
                     
                     
                     select p;
            List<EProductionLoss> li = new List<EProductionLoss>();

            foreach (var a in pr)
            {
                EProductionLoss obj = new EProductionLoss();
                obj.Order = objLoss.Order;
                obj.Customer = objLoss.Customer;
                obj.Product = objLoss.Product;
                obj.Color = objLoss.Color;
                obj.ProcessCard = a.PCardNo;
                obj.Customer = a.CName;
                obj.LotNo = a.LotNo;
                obj.BookingQty = Convert.ToDouble(a.BookingQty);
                obj.GreyIssueQty = Convert.ToDouble(a.DeyingQty);
                obj.PackingQty = Convert.ToDouble(a.PackingQty);
                obj.LossKG =Convert.ToDouble((obj.GreyIssueQty - obj.PackingQty).ToString("0.00"));

                if (obj.PackingQty > 0)
                {
                    obj.LossPer = Convert.ToDouble(((100 * obj.LossKG) / obj.GreyIssueQty).ToString("0"));
                }

                li.Add(obj);
            }

            return li;
        }

        //public bool AuthorAccountStatus(AccountStatus aStatus)
        //{
        //    try
        //    {
        //        SqlParameter StatusCode = new SqlParameter("@StatusCode", aStatus.AccountStatusValue);
        //        SqlParameter AUTHOR_PROCESS = new SqlParameter("@AUTHOR_PROCESS", "Active");
        //        SqlParameter AUTHOR_MODE = new SqlParameter("@AUTHOR_MODE", "T");
        //        SqlParameter AUTHOR_USER_ID = new SqlParameter("@AUTHOR_USER_ID", USession.SUserID);
        //        SqlHelper.ExecuteNonQuery(this.conn, CommandType.StoredProcedure, "Authorize_AccountStatus",
        //            StatusCode, AUTHOR_PROCESS, AUTHOR_MODE, AUTHOR_USER_ID);
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        //public List<AccountStatus> GetAllAccountStatus()
        //{
        //    List<AccountStatus> aStatusList = new List<AccountStatus>();
        //    string strSql = "SELECT * FROM  [AccountStatus] where ENTRY_MODE='E' order by serialid";
        //    SqlDataReader reader = SqlHelper.ExecuteReader(this.conn, CommandType.Text, strSql);
        //    while (reader.Read())
        //    {
        //        AccountStatus aStatus = new AccountStatus();
        //        aStatus.AccountStatusValue = reader["StatusCode"].ToString();
        //        aStatus.AccountStatusDescription = reader["Description"].ToString();
        //        aStatusList.Add(aStatus);
        //    }
        //    return aStatusList;
        //}
       
    }
}
