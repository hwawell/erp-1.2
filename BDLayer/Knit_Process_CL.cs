﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Collections;
namespace BDLayer
{
    //Notes:
    //Section= DEYING ,DRY ,RAISING,SHEARING,ANTIPILLING,SETTING,PACKING
    
  public  class Knit_Process_CL
    {
         string _Conn;
         public Knit_Process_CL()
        {
            _Conn = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        }
        public void Save(KnitProcessing  k)
        {
            
            DBAccessDataContext db = new DBAccessDataContext();
             
            db.Connection.ConnectionString = _Conn;

           
            db.Insert_KNIT_Processing_SP(k.SL, k.PCardNo,k.CCardNo, k.PQty, k.MCNo, k.Notes, k.Section, k.Status, k.EntryID,k.EntryDate,k.SHIFT);

           
                   
            db.SubmitChanges();
                    

        }
        public bool IsExits(string pc)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.KnitProcessings
                     where p.EMode == "E" && p.Section == "DEYING" && p.Status == "P"  && p.PCardNo==pc
                      select p;

            if (pr.Count() > 0)
                return true;
            else
                return false;


           
        }
        
        public void updateSave(KnitProcessing k)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;

            db.Delete_KNIT_Processing_SP(k.SL);
            
            db.SubmitChanges();
                    

        }
        public long getReturnSL(string pcard,string Sec)
        {
            DBAccessDataContext db = new DBAccessDataContext();
            long sl=-1;
            db.Connection.ConnectionString = _Conn;
            var q = from p in db.KnitProcessings
                    where p.PCardNo == pcard && p.Section == Sec
                    select p;
            foreach(KnitProcessing k in q)
            {
                sl = k.SL;
            }

            return sl;
        }
        public void deleteData(KnitProcessing k)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Delete_KNIT_Processing_SP(k.SL);
            db.SubmitChanges();

        }
        public DataSet GetActiveDataDeying(string sec)
        {

            DataSet ds;
            string strSql = "SELECT k.SL,(k.PCardNo + (Case  isnull(CCardNo,'') when ''  then '' else  '-' + CCardNo END)) PALL,k.PCardNo,[CCardNo],[PQty],k.[MCNo],k.[Notes],[Section],k.[Status],[SHIFT],FID,k.EntryID,CONVERT(nvarchar(11), k.EntryDate, 101) as EntryDate FROM [KnitProcessing] k,ProcessCard p Where k.PCardNo=p.PCardNo and k.EMODE='E' and k.Status!='F' and k.Section='DEYING'   order by k.SL Desc";
            ds = SqlHelper.ExecuteDataset(this._Conn, CommandType.Text, strSql);

            return ds;


            
        }
        public DataSet GetActiveDataDeyingbyPCard(string sec,string Pcard)
        {

            DataSet ds;
            string strSql = "SELECT k.SL,(k.PCardNo + (Case  isnull(CCardNo,'') when ''  then '' else  '-' + CCardNo END)) PALL,k.PCardNo,[CCardNo],[PQty],k.[MCNo],k.[Notes],[Section],k.[Status],[SHIFT],FID,k.EntryID,CONVERT(nvarchar(11), k.EntryDate, 101) as EntryDate FROM [KnitProcessing] k,ProcessCard p Where k.PCardNo=p.PCardNo and k.EMODE='E' and k.Status!='F' and k.Section='DEYING' and   K.PCardNo='"+ Pcard +"'  order by k.SL Desc ";
            ds = SqlHelper.ExecuteDataset(this._Conn, CommandType.Text, strSql);

            return ds;



        }
        public IQueryable GetActiveData(string sec)
        {

           

            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.KnitProcessings
                     where p.EMode == "E" && p.Section == sec.Trim() && p.Status != "F"
                     orderby p.SL descending
                     select new
                     {
                         p.SL,
                         p.PCardNo,
                         p.CCardNo,
                         p.PQty,
                         p.MCNo,
                         p.Notes,
                         p.EntryDate,
                         p.EntryID,
                         PALL = p.PCardNo + (p.CCardNo == null ? "" : "-"+ p.CCardNo)
                       
                     };

                     
            return pr;
        }
        public IQueryable GetActiveData(string sec,string Pcard)
        {



            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.KnitProcessings
                     where p.EMode == "E" && p.Section == sec.Trim() && p.Status != "F"
                     && (Pcard=="*" || p.PCardNo==Pcard)
                     orderby p.SL descending
                     select new
                     {
                         p.SL,
                         p.PCardNo,
                         p.CCardNo,
                         p.PQty,
                         p.MCNo,
                         p.Notes,
                         p.EntryDate,
                         p.EntryID,
                         PALL = p.PCardNo + (p.CCardNo == null ? "" : "-" + p.CCardNo)

                     };


            return pr;
        }
        public IQueryable GetDeletedData(string sec)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.KnitProcessings
                     where p.EMode == "D" && p.Section==sec
                     select p;
                    
            return pr;
        }
        public void DeleteActiveData(long custid, string mo, string eid)
        {

            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "KNITT_PROCESS");
            db.SubmitChanges();


        }

        public void DeleteFabricDelivery(string pCard)
        {

            try
            {
                string str = "Update FabricDelivery set EMode='D' where PCardId='" + pCard + "'";
                SqlHelper.ExecuteNonQuery(this._Conn, CommandType.Text, str);

                // return true;
            }
            catch
            {
                //return false;
            }


        }
       
        public IQueryable GetAllDeyingList()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ProcessCards
                     where p.Emode== "E" && p.Status=="START" 
                     orderby p.PCardNo
                     //select p;
                     select new
                     {

                         p.PCardNo,
                         fab = p.PCardNo+ "--->Lot:" + p.LotNo 
                     };

            return pr;
        }
        public void AllSummaryReport(DateTime f, DateTime t)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;

            db.ProcessYarnBalanceReport(f, t);
            db.SubmitChanges();

            db.KnittingBalanceReport(f, t);
            db.SubmitChanges();

            db.GreayBalanceReport(f, t);
            db.SubmitChanges();

            db.ProcessALLBalanceReport(f, t);
            db.SubmitChanges();
          

        }
        public long GetMax()
        {

                DataSet ds;
                string strSql = "  Select isnull(max(wid),0)+1 from Delivery ";
               ds= SqlHelper.ExecuteDataset(this._Conn, CommandType.Text, strSql);

               return Convert.ToInt32(ds.Tables[0].Rows[0][0]);

           
        }
        public void UpdateDelivery(long w, long sl)
        {

            try
            {
                string str = "Update Packing set wid=" + w + " where sl=" + sl + "";
                SqlHelper.ExecuteNonQuery(this._Conn, CommandType.Text, str);

               // return true;
            }
            catch
            {
                //return false;
            }
        }
        public void UpdateProcessCard(string Pcard, double  wet)
        {

            try
            {
                string str = "Update ProcessCard set Weight=" + wet + " where PCardNo='" + Pcard + "'";
                SqlHelper.ExecuteNonQuery(this._Conn, CommandType.Text, str);

                // return true;
            }
            catch
            {
                //return false;
            }
        }
        public void SaveDelivery(Delivery d)
        {

            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;


            db.INSERT_DELIVERY_FINAL_SP(d.WID, d.CID, d.PID, d.DDate, d.ChallanNo, d.Description, d.TotQty, d.TotalRoll);




            db.SubmitChanges();

        }
    }
}
