﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace BDLayer
{
    public class Chemical_DL
    {
         string _Conn;
         public Chemical_DL()
        {
            _Conn = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        }
        public void Save(ChemicalList  y)
        {
            
            DBAccessDataContext db = new DBAccessDataContext();
             
            db.Connection.ConnectionString = _Conn;
            db.INSERT_CHEMICAL_SP (y.SL,y.GID,y.ChemicalName,y.EntryID);
                   
            db.SubmitChanges();
                    

        }
        public void SaveInventory(ChemicalInventory y)
        {
         
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.INSERT_CHEMICAL_INVENTORY_SP(y.SL, y.CID, y.StockDate, y.Received, y.Consumed, y.EntryUserID);



            db.SubmitChanges();


        }

        public bool SavePURCHASE(ChemicalPurchase y)
        {

            try
            {
                DBAccessDataContext db = new DBAccessDataContext();

                db.Connection.ConnectionString = _Conn;
                db.INSERT_CHEMICAL_PURCHASE_SP(y.SL, y.CID, y.UNIT, y.CTNQty, y.TotalQty, y.LCNo, y.InvoiceNo, y.PDate, y.PurchaseFrom, y.Notes, y.Status, y.EntryID);



                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }
        public bool SaveISSUE(ChemicalIssue y)
        {
            try
            {
                DBAccessDataContext db = new DBAccessDataContext();

                db.Connection.ConnectionString = _Conn;
                db.INSERT_CHEMICAL_ISSUE_SP(y.SL, y.CID, y.Unit, y.CTNQTy, y.Qty, y.Idate, y.IssueDesc, y.EntryID);



                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
         


        }
        
        public IQueryable GetActiveRecord(Int64 gid)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ChemicalLists
                     where p.EntryMode == "E" && p.GID==gid orderby p.ChemicalName 
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetActiveInventory()
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;
      
            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ChemicalInventories
                     from q in db.ChemicalLists
                     where p.EntryMode == "E" && q.SL == p.CID
                     orderby p.EntryDateTime descending
                     //select p;
                     select new
                     {
                         p.SL,
                         p.CID,
                         q.ChemicalName,
                         p.StockDate,
                         p.Received,
                         p.Consumed,
                         p.EntryUserID ,
                         p.EntryDateTime

                     };

            return pr.Skip(0).Take(100);
        }
        public IQueryable GetActivePURCHASE(int start,int end)
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ChemicalPurchases
                     from q in db.ChemicalLists
                     where p.EMode == "E" && q.SL == p.CID
                     orderby p.PDate descending
                     //select p;
                     select new
                     {
                         p.SL,
                         p.CID,
                         q.ChemicalName,
                         p.UNIT,
                         p.CTNQty,
                         p.TotalQty,
                         p.LCNo,
                         p.InvoiceNo,
                         p.PDate,
                         p.Status

                     };

            return pr.Skip(start - 1).Take(end);
        }

        public IQueryable GetActiveIssue(int start, int end)
        {
            DBAccessDataContext db = new DBAccessDataContext();
            DateTime dp = DateTime.Today;

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ChemicalIssues
                     from q in db.ChemicalLists
                     where p.EMode == "E" && q.SL == p.CID
                     orderby p.Idate descending
                     
                     //select p;
                     select new
                     {
                         p.SL,
                         p.CID,
                         q.ChemicalName,
                         p.Unit,
                         p.CTNQTy,
                         p.Qty,
                        
                         p.Idate
                        

                     };

            return pr.Skip(start - 1).Take(end);
        }
       
       
        public IQueryable GetDeletedRecord()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ChemicalLists
                     where p.EntryMode == "D"
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetDeletedInventory()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ChemicalInventories
                     where p.EntryMode == "D"
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetDeletedIssuedRecord()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.YarnIssues
                     where p.EMode == "D"
                     //select p;
                     select p;

            return pr;
        }
        public void DeleteActive(long custid,string mo,string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "ChemicalList");
            db.SubmitChanges();


        }
        public void DeletePurchase(long custid, string mo, string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "ChemicalPurchase");
            db.SubmitChanges();


        }
        public void DeleteIssue(long custid, string mo, string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "ChemicalIssue");
            db.SubmitChanges();


        }
        public void InventoryDeleteActive(long custid, string mo, string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "ChemicalInventory");
            db.SubmitChanges();


        }
        public IQueryable GetChemicalGroup()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.ChemicalGroups
                     where p.EntryMode == "E" orderby p.GroupName 
                     //select p;
                     select p;

            return pr;
        }

        public IQueryable GetUnit()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Units
                     
                     orderby p.UNIT1
                     //select p;
                     select p;

            return pr;
        }
      
    }
}
