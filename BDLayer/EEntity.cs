﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BDLayer
{
   public class EEntity
    {
    }
   public class EOrderList
   {
       
       public string OrderNo { get; set; }
       

   }
   public class EPCardList
   {

       public string PCardNo { get; set; }


   }
   public class EFabcricList
   {
       public string FID { get; set; }
       public string Fabric { get; set; }
      
   }
   public class EProductionReport
   {
       public Int64 OPID { get; set; }
       public string OrderNo { get; set; }
       public string Merchandiser { get; set; }
       public string Customer { get; set; }
       public string Product { get; set; }
       public string GSM { get; set; }
       public string Width { get; set; }
       public double BookingQty { get; set; }
       public double Qty { get; set; }
       public double Balance { get; set; }
       
   }
   public class EProductionLoss
   {
      
       public string Order { get; set; }
       public string Customer { get; set; }
       public string Product { get; set; }
       public string Color { get; set; }
       public string ProcessCard { get; set; }
       public string LotNo { get; set; }
       public double BookingQty { get; set; }
       public double GreyIssueQty { get; set; }
       public double PackingQty { get; set; }
       public double LossKG { get; set; }
       public double LossPer { get; set; }

   }
   public class EYarnIssue
   {
       public Int32 ID { get; set; }
       public string DDate { get; set; }
       public string YarnCompany { get; set; }
       public string YarnCountry { get; set; }
       public string YarnGroup { get; set; }
       public string YarnName { get; set; }
       public string Lot { get; set; }
       public string MC_List { get; set; }
       public string Unit { get; set; }
       public double UnitQty { get; set; }
       public double TotalQty { get; set; }
       public string IssueTo { get; set; }
       public string InvoiceNo { get; set; }
       public string IssueType { get; set; }
       public string Note { get; set; }


   }

   public class EYarnPurchase
   {
       public Int32 ID { get; set; }
       public string DDate { get; set; }
       public string YarnCompany { get; set; }
       public string YarnCountry { get; set; }
       public string LCNumber { get; set; }
       public string InvoiceNo { get; set; }
       public string YarnGroup { get; set; }
       public string YarnName { get; set; }
       public string Lot { get; set; }
       public string Unit { get; set; }
       public double UnitQty { get; set; }
       public double TotalQty { get; set; }
      
      
       public string ReceiveType { get; set; }
       public string Note { get; set; }


   }

   public class EYarnLCBooking
   {
       public Int64 ID { get; set; }
       public Int32 YID { get; set; }
       public Int32 YGID { get; set; }
       public string Yarn { get; set; }
       public string Supplier { get; set; }
       public string Country { get; set; }
       public string LCNo { get; set; }
       public string Date { get; set; }
       public string LCQty { get; set; }
       public string ReceiveQty { get; set; }
       public string Notes { get; set; }
       public string ImportPINo { get; set; }
       public double YarnLCValue { get; set; }
       public double YarnTotalLCValue { get; set; }
   }
   public class EYarnRequistion
   {
       public Int32 ID { get; set; }
       public string ReqNo { get; set; }
       public string ReqDate { get; set; }
       public string GreyDescription { get; set; }
       public string GM { get; set; }
       public string GSM { get; set; }
       public string KNIT_MC { get; set; }
       public string YARN { get; set; }
       public double ReqQty { get; set; }
       public double IssuedQty { get; set; }
       public double BalanceQty { get; set; }
       public string OrderNo { get; set; }
      

   }
   public class EYarnStockReport
   {
       public Int32 ID { get; set; }
       public string YarnGroup { get; set; }
       public string YarnName { get; set; }
       public string YarnCompany { get; set; }
       public string YarnCountry { get; set; }
       public string Unit { get; set; }
       

       public string LotNo { get; set; }
       

       public double OpeningBalance { get; set; }
       public double Purchase_Receive { get; set; }
       public double Return_Receive { get; set; }
       public double Loan_Receive { get; set; }
       public double SubCOn_Receive { get; set; }
       public double Production_Issue { get; set; }
       public double Return_Issue { get; set; }
       public double Loan_Issue { get; set; }
       public double SubCont_Issue { get; set; }
       public double ClosingBalanceQty { get; set; }
       public string AvailableCTN { get; set; }


   }
}
