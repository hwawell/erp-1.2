﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace BDLayer
{
   public class Utilities_DL
    {
         string _Conn;
         public Utilities_DL()
        {
            _Conn = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        }

         public static DataSet GetTheDataSet(string[,] List, string SpName)
         {

             // string sqlCon = "Data Source=192.168.101.17;Initial Catalog=HRMS;User ID=sa;Password=sa1234";// ConfigurationManager.AppSettings["DBConnectionString"].ToString();
             string sqlCon = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
             SqlConnection Con = new SqlConnection(sqlCon);
             SqlCommand cmd = new SqlCommand();
             DataSet ds = null;
             SqlDataAdapter adapter;
             try
             {
                 Con.Open();
                 cmd.CommandText = SpName;
                 cmd.CommandType = CommandType.StoredProcedure;



                 for (int i = 0; i < List.GetLength(0); i++)
                 {
                     cmd.Parameters.Add(new SqlParameter(List[i, 0], List[i, 1]));

                 }


                 cmd.Connection = Con;
                 ds = new DataSet();
                 adapter = new SqlDataAdapter(cmd);
                 adapter.Fill(ds, "Users");
             }
             catch (Exception ex)
             {
                 throw new Exception(ex.Message);
             }
             finally
             {
                 cmd.Dispose();
                 if (Con.State != ConnectionState.Closed)
                     Con.Close();
             }
             return ds;
         }


         public bool IsExist(string UserID, string Password)
         {
             DBAccessDataContext db = new DBAccessDataContext();
             db.Connection.ConnectionString = _Conn;
             SEC_USER su = db.SEC_USERs.SingleOrDefault(a => a.USERS_ID == UserID);
             if (su != null)
             {
                
                 if (su.USER_PASS == Password)
                     return true;
                 else
                     return false;
             }
             else
                 return false;
             return false;
         }
         public bool HasChangePassword(string UserID, string Password,string NewPass)
         {
             DBAccessDataContext db = new DBAccessDataContext();
             string str = "";
             db.Connection.ConnectionString = _Conn;
             db.Change_PASSWORD(UserID,Password,NewPass,ref str);
             if (str == "1")
             {
                 return true;
             }
             else
             {
                 return false;
             }
         }
        public void SaveColor(Color  y)
        {
            
            DBAccessDataContext db = new DBAccessDataContext();
             
            db.Connection.ConnectionString = _Conn;
            db.Insert_Color_SP(y.SL,y.ColorName);
                   
            db.SubmitChanges();
                    

        }
        public void SaveDesc(Description y)
        {

            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Insert_Description_sp(y.SL,y.Descriptions,y.Others);

            db.SubmitChanges();


        }
        public void SaveOperator(Operator y)
        {

            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Insert_Operator_sp(y.SL, y.OperatorName, y.Section);

            db.SubmitChanges();


        }
        public IQueryable GetActiveColor()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Colors
                     where p.EntryMode == "E"
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetActiveDescription()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Descriptions
                     where p.EntryMode == "E"
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetActiveOperator()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Operators
                     where p.EMode == "E"
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetDeletedColor()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Colors
                     where p.EntryMode == "D"
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetDeletedOperator()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Operators
                     where p.EMode == "D"
                     //select p;
                     select p;

            return pr;
        }
        public IQueryable GetDeletedDesc()
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.Descriptions
                     where p.EntryMode == "D"
                     //select p;
                     select p;

            return pr;
        }
        public void DeleteActiveColor(long custid, string mo, string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "Color");
            db.SubmitChanges();


        }
        public void DeleteActiveDesc(long custid, string mo, string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "Description");
            db.SubmitChanges();


        }
        public void DeleteActiveOperator(long custid, string mo, string eid)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.Update_Table_Mode(custid, mo, eid, "OPERATOR");
            db.SubmitChanges();


        }
        public bool CheckSecurity(string sid,string uid)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = from p in db.SEC_USER_RIGHTs
                     where p.SECURITY_ID==sid && p.USERS_ID==uid && p.USER_RIGHT_ACTIVE=='T' && p.USER_RIGHT_AUTHORIZED=='T'
                     //select p;
                     select p;
            if (pr.Count() > 0)
                return true;
            else
                return false;
           

        }
    }
}
