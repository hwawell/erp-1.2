﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace BDLayer
{
   public class GreyFabric_DL
    {

        string _Conn;
        public GreyFabric_DL()
        {
            _Conn = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        }
        public string SaveIssue(EDeydFab_Issue y)
        {

            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.KNIT_DYED_FABRIC_ISSUE_INS_UPD(y.ID,y.OrderNo, y.OPID,y.PCardNo,y.Roll, Convert.ToDecimal(y.Qty),  y.TypeOfProduction,y.TypeOfWork,y.IssueDate,y.EntryID,y.Notes, ref res);

            db.SubmitChanges();
            return res ;
                    

        }
        public string SaveReceive(EDeydFab_Receive y)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.KNIT_DYED_FABRIC_RCV_INS_UPD(y.ID, y.OrderNo, y.OPID, Convert.ToDecimal(y.Qty),y.Roll, y.Invoice, y.TypeOfProduction, y.Notes, y.RcvDate, y.EntryID, ref res);

            db.SubmitChanges();
            return res ;

        }

        public string DeyedFabricDelete(Int32? ID, bool? isIssue)
        {
            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.KNIT_DYED_FABRIC_DELETE(isIssue,ID,ref res);

            db.SubmitChanges();
            return res;

        }

        public List<KNIT_DYED_FABRIC_RECEIVE_GETResult> GetReceive(string OrderNo)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = (from p in db.KNIT_DYED_FABRIC_RECEIVE_GET(OrderNo)

                     //select p;
                     select p).ToList();
                     

            return pr;
        }
        public List<KNIT_DYED_FABRIC_Issue_GETResult> GetIssue(string OrderNo)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = (from p in db.KNIT_DYED_FABRIC_Issue_GET(OrderNo)

                      //select p;
                      select p).ToList();


            return pr;
        }

        public List<rpt_DYED_FABRIC_IssueResult> rpt_GreyFabric_Issue(string OrderNo,string fromD,string to)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = (from p in db.rpt_DYED_FABRIC_Issue(fromD, to, OrderNo)

                      //select p;
                      select p).ToList();


            return pr;
        }

        public List<rpt_DYED_FABRIC_ReceiveResult> rpt_GreyFabric_Receive(string OrderNo, string fromD, string to)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = (from p in db.rpt_DYED_FABRIC_Receive(fromD, to, OrderNo)

                      //select p;
                      select p).ToList();


            return pr;
        }


        public List<RPT_KNIT_PROCESS_DETAILSResult> RPT_GETPROCESS_DETAILS( string frm, string to, string section)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = (from p in db.RPT_KNIT_PROCESS_DETAILS(section,frm,to)

                      //select p;
                      select p).ToList();


            return pr;
        }
        public List<RPT_KNIT_PROCESS_DETAILS_BY_ORDERResult> RPT_GETPROCESS_DETAILS_ByOrder(string OrderNo,  string section)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = (from p in db.RPT_KNIT_PROCESS_DETAILS_BY_ORDER(section,  OrderNo)

                      //select p;
                      select p).ToList();


            return pr;
        }
        public List<RPT_KNIT_PACKING_DETAILSResult> RPT_PACKING_DETAILS(string OrderNo, string frm, string to)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = (from p in db.RPT_KNIT_PACKING_DETAILS( frm, to)

                      //select p;
                      select p).ToList();


            return pr;
        }
        public List<RPT_KNIT_PACKING_DETAILS_BY_ORDERResult> RPT_PACKING_DETAILSByOrder(string OrderNo)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = (from p in db.RPT_KNIT_PACKING_DETAILS_BY_ORDER(OrderNo)

                      //select p;
                      select p).ToList();


            return pr;
        }

        public List<RPT_KNIT_PACKING_SUMMARYResult> RPT_PACKING_Summary(string OrderNo)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = (from p in db.RPT_KNIT_PACKING_SUMMARY( OrderNo)

                      //select p;
                      select p).ToList();


            return pr;
        }

        public List<RPT_KNIT_PROCESS_SUMMARYResult> RPT_Process_Summary(string OrderNo, string section)
        {
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            var pr = (from p in db.RPT_KNIT_PROCESS_SUMMARY(section,OrderNo)

                      //select p;
                      select p).ToList();


            return pr;
        }



        public string GetBalanceGreyFabric(string OrderNo)
        {

            string res = "";
            DBAccessDataContext db = new DBAccessDataContext();

            db.Connection.ConnectionString = _Conn;
            db.KNIT_DYED_FABRIC_Balance_GET(OrderNo, ref res);

            db.SubmitChanges();
            return res ;


            
        }
    }
}
