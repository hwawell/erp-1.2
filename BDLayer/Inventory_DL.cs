﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace BDLayer
{
   public class Inventory_DL
    {

       string _Conn;
       public Inventory_DL()
       {
           _Conn = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
       }
       public IQueryable GetUnit(Int32 iTypeID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_Units
                    where p.INVTypeID == iTypeID 
                     orderby p.UnitName
                    select p;
           return pr;
       }
       public string GetInventoryType(Int32 Id)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_Types
                    where p.ID==Id
                    orderby p.InventoryType
                    select p;

           return pr.FirstOrDefault().InventoryType;
           
       }

       public IQueryable GetSupplier(Int32 iTypeID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_Suppliers
                    where p.SType=="SUPPLIER"  && p.INVTypeID==iTypeID  
                    orderby p.CountryOrSupplier
                    select p;
           return pr;
       }
       public IQueryable GetCountry(Int32 iTypeID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_Suppliers
                    where p.SType == "Country"  && p.INVTypeID==iTypeID 
                    orderby p.CountryOrSupplier
                    select p;
           return pr;
       }

       public IQueryable GetReceiveType(Int32 typeID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_ReceiveIssueTypes
                    where p.IsReceive == true && p.INVTypeID == typeID
                    orderby p.TypeName
                    select p;
           return pr;
       }
       public IQueryable GetIssueType(Int32 typeID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_ReceiveIssueTypes
                    where p.IsReceive == false && p.INVTypeID == typeID 
                    orderby p.TypeName
                    select p;
           return pr;
       }

       public IQueryable GetLander(Int32 typeID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_Landers
                    where p.INVTypeID==typeID
                    orderby p.LName
                    select p;
           return pr;
       }

       public IQueryable GetStore(Int32 typeID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_Stores
                    where p.INVTypeID == typeID
                    orderby p.StoreName
                    select p;
           return pr;
       }
       public IQueryable GetItemGroup(Int32 typeID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_Groups
                    where p.INVTypeID==typeID && p.EntryMode=="E"
                    orderby p.GroupName
                    select p;
           return pr;
       }
       public List<EINV_Store> GetStoreList(Int32 typeID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_Stores
                    where p.INVTypeID == typeID
                    orderby p.StoreName
                    select p;

           List<EINV_Store> li = new List<EINV_Store>();
           foreach (var k in pr)
           {
               EINV_Store ob = new EINV_Store();
               ob.ID = k.ID;
               ob.StoreName = k.StoreName;
               li.Add(ob);
           }

           return li;
       }
       public List<EINV_Item> GetInventoryItem(Int32 typeID, Int32 PItemID, Int32 GID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_ITEM_GET(typeID, PItemID, GID)

                    select p;

           List<EINV_Item> li = new List<EINV_Item>();
           foreach (var k in pr)
           {
               EINV_Item ob = new EINV_Item();
               ob.ItemID = k.ItemID;
               ob.ItemName = k.ItemName;
               ob.GID = k.GID??0;
               ob.GroupName = k.GroupName;
               ob.ParentItemID = k.ParentItemID??0;
               ob.ParentItem = k.ParentItem;
               li.Add(ob);

           }
           return li;
       }


       public List<EINV_StoreReport> GetInventoryStockReport(Int32 typeID, Int32 GID, string frm, string to,string ItemName)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_RPT_Stock_GET(Convert.ToInt32( typeID),Convert.ToInt32(  GID),frm,to)
                  
                    select p;

           List<EINV_StoreReport> li = new List<EINV_StoreReport>();
           foreach (var k in pr)
           {
               EINV_StoreReport ob = new EINV_StoreReport();
               ob.ItemID = k.ItemID;
               ob.ItemName = k.ItemName;
               
               ob.GroupName = k.GroupName;
               ob.ParentName = k.ParentNames;

               ob.StockStr = k.TotStock;
               ob.OpenBal = Convert.ToDouble(k.OpenBal);
               ob.IssTotStr = k.TotIssue;
               ob.RcvTotStr = k.TotRcv;

               string[] issval = ob.IssTotStr.Split(';');
               for (int i = 0; i < issval.Count(); i++)
               {
                   ob.IssTot = ob.IssTot + Convert.ToDouble(issval[i].Substring(issval[i].IndexOf('=') + 1));
               }
               string[] rcvval = ob.RcvTotStr.Split(';');
               for (int i = 0; i < issval.Count(); i++)
               {
                   ob.RcvTot = ob.RcvTot + Convert.ToDouble(rcvval[i].Substring(rcvval[i].IndexOf('=') + 1));
               }
               ob.Balance = ob.OpenBal - ob.IssTot + ob.RcvTot;
             
               li.Add(ob);

           }
           return li;
       }
       public List<EINV_Item> GetInventoryItemBasic(Int32 typeID,  Int32 GID,Int32 storeID)
       {
           try
           {
               DBAccessDataContext db = new DBAccessDataContext();
               db.Connection.ConnectionString = _Conn;
               var pr = from p in db.INV_ItemBasicInfo_GET(typeID, GID, storeID)

                        select p;

               List<EINV_Item> li = new List<EINV_Item>();
               foreach (var k in pr)
               {
                   EINV_Item ob = new EINV_Item();
                   ob.ItemID = k.ItemID;
                   ob.ItemName = k.ItemName;
                   
                   ob.GID = k.GID ?? 0;
                   ob.StockBalance = Convert.ToDouble(k.TotBalance);

                   li.Add(ob);

               }
               return li;
           }

           catch
           {
               return null;
           }
       }

       public EINV_Item GetInventoryItemBasicByItem(Int32 typeID, Int32 GID,Int64 ItemID,Int32 storeID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_ItemBasicInfo_GET(typeID, GID, storeID)
                    where p.ItemID==ItemID
                    select p;

           EINV_Item ob = new EINV_Item();
           foreach (var k in pr)
           {
               
               ob.ItemID = k.ItemID;
               ob.ItemName = k.ItemName;
               ob.GID = k.GID ?? 0;
               ob.StockBalance = Convert.ToDouble(k.TotBalance);
                          

           }
           return ob;
       }

       public List<EINV_Receive> GetInventoryReceive(Int32 typeID, Int32 GID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_Receive_GET(typeID, GID)
                    orderby p.RDate descending
                    select p;

           List<EINV_Receive> li = new List<EINV_Receive>();
           foreach (var a in pr)
           {
               EINV_Receive obj = new EINV_Receive();

               obj.ID = a.ID;
               obj.ItemID = a.ItemID??0;
               obj.ItemName = a.ItemName;
               obj.ReceiveTypeID = a.ReceiveTypeID??0;
               obj.ReceiveType = a.TypeName;
               obj.LanderID = a.LanderID??0;
               obj.BoxUNIT = a.BoxUNIT;
               obj.BoxQty =Convert.ToDouble( a.BoxQty);
               obj.TotalQty =Convert.ToDouble(  a.TotalQty);
               obj.Unit = a.Unit;
               obj.LCNo = a.LCNo;
               obj.StoreID = a.INVStoreID??0;
               obj.InvoiceNo = a.InvoiceNo;
               obj.GatePassNo = a.GatePassNo;
               obj.RDate = a.RDate;
               obj.Supplier = a.Supplier;
               obj.Country = a.Country;
               obj.Notes = a.Notes;
               obj.TotalAmount = Convert.ToDouble(a.TotalAmount);
               obj.UnitPrice = Convert.ToDouble((obj.TotalAmount / obj.TotalQty).ToString("0.00"));
               obj.CashMemo = a.CashMemo;
               obj.Purchaser = a.PurchaserName;
               
               li.Add(obj);

           }
           return li;
       }

       public List<EINV_Issue> GetInventoryIssue(Int32 typeID, Int32 GID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_Issue_GET(typeID, GID)

                    select p;

           List<EINV_Issue> li = new List<EINV_Issue>();
           foreach (var a in pr)
           {
               EINV_Issue obj = new EINV_Issue();

               obj.ID = a.ID;
               obj.ItemID =Convert.ToInt32( a.ItemID);
               obj.ItemName = a.ItemName;
               obj.IssueType = a.TypeName;
               obj.IssueTypeID = a.IssueTypeID??0;
               obj.LanderID = a.LanderID??0;
               obj.StoreID = a.INVStoreID??0;
               //obj.BoxUnit = a.BoxUnit;
               //obj.BoxQty =Convert.ToDouble( a.BoxQty);
               obj.TotalQty =Convert.ToDouble( a.TotalQty);
               obj.Unit = a.Unit;
               obj.InvoiceNo = a.InvoiceNo;
               obj.GatePassNo = a.GatePassNo;
              // obj.Supplier = a.Supplier;
               //obj.Country = a.Country;
               obj.Idate = a.Idate;
               obj.Notes = a.Notes;
               
               

               li.Add(obj);

           }
           return li;
       }

       public List<EINV_Transfer> GetInventoryTransfer(Int32 typeID, Int32 GID, Int32 storeID)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_Transfer_GET(typeID, GID, storeID)

                    select p;

           List<EINV_Transfer> li = new List<EINV_Transfer>();
           foreach (var a in pr)
           {
               EINV_Transfer obj = new EINV_Transfer();

               obj.ID = a.ID;
               obj.ItemID = Convert.ToInt32(a.ItemID);
               obj.ItemName = a.ItemName;
               obj.TransferStore = a.StoreName;
               obj.FromStoreID = a.FromStoreID ?? 0;
               obj.ToStoreID = a.ToStoreID ?? 0;
               obj.ReceiveID = a.INV_ReceiveID ?? 0;
               obj.IsssueID = a.INV_IssueID ?? 0;
               //obj.BoxUnit = a.BoxUnit;
               //obj.BoxQty =Convert.ToDouble( a.BoxQty);
               obj.TransferQty = Convert.ToDouble(a.TransferQty);
               obj.Unit = a.TransferUnit;

               obj.RefCode = a.ReqNo;
               obj.Tdate = a.TransferDate;
               obj.Notes = a.Notes;



               li.Add(obj);

           }
           return li;
       }

       public string SaveInventoryReceive(EINV_Receive p)
       {
           string res = "";
           try
           {
               DBAccessDataContext db = new DBAccessDataContext();


               db.Connection.ConnectionString = _Conn;
               db.INV_Receive_INS_UPD(p.ID, p.ItemID, p.ReceiveTypeID, p.LanderID, p.BoxUNIT, Convert.ToDecimal(p.BoxQty), Convert.ToDecimal(p.TotalQty), Convert.ToDecimal(p.TotalAmount), p.CashMemo, p.Purchaser, p.Unit, p.LCNo, p.InvoiceNo, p.GatePassNo, p.RDate, p.Supplier, p.Country, p.Notes, p.EMode, p.EntryID, p.INVTypeID, p.StoreID, ref res);

               db.SubmitChanges();
           }
           catch(Exception ex)
           {
               res ="0-Operation Failed Error :" + ex.Message.ToString();
           }
           return res;
       }
       public string SaveInventoryIssue(EINV_Issue p)
       {
           string res = "";
           try
           {
               DBAccessDataContext db = new DBAccessDataContext();


               db.Connection.ConnectionString = _Conn;
               db.INV_Issue_INS_UPD(p.ID, p.ItemID, p.IssueTypeID, p.LanderID, p.BoxUnit, Convert.ToDecimal(p.BoxQty), Convert.ToDecimal(p.TotalQty), p.Unit, p.InvoiceNo, p.GatePassNo, p.Supplier, p.Country,p.Idate ,p.Notes, p.EMode, p.EntryID, p.INVTypeID,p.StoreID, ref res);

               db.SubmitChanges();
           }
           catch (Exception ex)
           {
               res = "0-Operation Failed Error :" + ex.Message.ToString();
           }
           return res;
       }
       public string SaveInventoryTransfer(EINV_Transfer p)
       {
           string res = "";
           try
           {
               DBAccessDataContext db = new DBAccessDataContext();


               db.Connection.ConnectionString = _Conn;
               db.INV_Transfer_INS_UPD(p.ID, p.ItemID,p.FromStoreID,p.ToStoreID,p.Tdate,Convert.ToDecimal(p.TransferQty),p.Unit,p.IsssueID,p.ReceiveID,p.Notes,p.INVTypeID,p.EntryID,p.RefCode,ref res);

               db.SubmitChanges();
           }
           catch (Exception ex)
           {
               res = "0-Operation Failed Error :" + ex.Message.ToString();
           }
           return res;
       }
       public string SaveUnit(INV_Unit p)
       {
           string res = "";
           DBAccessDataContext db = new DBAccessDataContext();


           db.Connection.ConnectionString = _Conn;
           db.INV_Unit_INS_UPD(p.ID,p.UnitName,p.INVTypeID, ref res);

           db.SubmitChanges();

           return res;
       }
       public string SaveLander(INV_Lander p)
       {
           string res = "";
           DBAccessDataContext db = new DBAccessDataContext();


           db.Connection.ConnectionString = _Conn;
           db.INV_Lander_INS_UPD(p.ID, p.LName, p.INVTypeID, ref res);

           db.SubmitChanges();

           return res;
       }
       public string SaveSupplierCountry(INV_Supplier p)
       {
           string res = "";
           DBAccessDataContext db = new DBAccessDataContext();


           db.Connection.ConnectionString = _Conn;
           db.INV_Supplier_INS_UPD(p.ID,p.CountryOrSupplier,p.SType,p.INVTypeID,ref res);

           db.SubmitChanges();

           return res;
       }

       public string SaveIssueReceiveType(INV_ReceiveIssueType p)
       {
           string res = "";
           DBAccessDataContext db = new DBAccessDataContext();


           db.Connection.ConnectionString = _Conn;
           db.INV_ReceiveIssueType_INS_UPD(p.ID,p.TypeName,p.INVTypeID,p.IsReceive, ref res);

           db.SubmitChanges();

           return res;
       }


       public string SaveItemGroup(INV_Group p)
       {
           string res = "";
           DBAccessDataContext db = new DBAccessDataContext();


           db.Connection.ConnectionString = _Conn;
           db.INV_Group_INS_UPD(p.SL,p.GroupName,p.EntryMode,p.INVTypeID, ref res);

           db.SubmitChanges();

           return res;
       }

       public string SaveInventoryItem(EINV_Item p)
       {
           string res = "";
           DBAccessDataContext db = new DBAccessDataContext();


           db.Connection.ConnectionString = _Conn;
           db.INV_Item_INS_UPD(p.ItemID,p.GID,p.ItemName,p.ParentItemID,p.EntryMode,p.EntryID,System.DateTime.Now,p.INVTypeID, ref res);

           db.SubmitChanges();

           return res;
       }
       public void DeleteINVReceive(long id, string mo, string eid)
       {

           DBAccessDataContext db = new DBAccessDataContext();

           db.Connection.ConnectionString = _Conn;
           db.Update_Table_Mode(id, mo, eid, "INV_RECEIVE");
           db.SubmitChanges();


       }
       public List<EINV_Receive> GetInventoryReceiveReport(Int32 typeID, Int32 GID, Int32 rcvTypeID, string frm, string to)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_RPT_Receive_GET(typeID,rcvTypeID, GID,frm,to)
                  
                    orderby p.ID descending
                    select p;

           List<EINV_Receive> li = new List<EINV_Receive>();
           int i = 1;
           foreach (var a in pr)
           {
               EINV_Receive obj = new EINV_Receive();

               obj.ID = a.ID;
               obj.SL = i;
               i++;
               obj.ItemID = a.ItemID ?? 0;
               obj.ItemName = a.ItemName;
               obj.GroupName = a.GroupName;
               obj.ParentItem = a.GroupNames;
              
               obj.ReceiveTypeID = a.ReceiveTypeID ?? 0;
               obj.ReceiveType = a.TypeName;
               obj.LanderID = a.LanderID ?? 0;
               obj.BoxUNIT = a.BoxUNIT;
               obj.BoxQty = Convert.ToDouble(a.BoxQty);
               obj.TotalQty = Convert.ToDouble(a.TotalQty);
               obj.Unit = a.Unit;
               obj.TotalAmount =Convert.ToDouble( a.TotalAmount);
               obj.UnitPrice = Convert.ToDouble((obj.TotalAmount / obj.TotalQty).ToString("0.00"));
               obj.CashMemo = a.CashMemo;
               obj.Purchaser = a.PurchaserName;
               obj.LCNo = a.LCNo;
               obj.StoreID = a.INVStoreID ?? 0;
               obj.InvoiceNo = a.InvoiceNo;
               obj.GatePassNo = a.GatePassNo;
               obj.RDate = a.RDate;
               obj.Supplier = a.Supplier;
               obj.Country = a.Country;
               obj.Notes = a.Notes;


               li.Add(obj);

           }
           return li;
       }


       public List<EINV_Issue> GetInventoryIssueReport(Int32 typeID, Int32 IsstypeID, Int32 GID, string frm, string to)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_RPT_Issue_GET(typeID,IsstypeID, GID,frm,to)
                   
                    orderby p.ID descending
                    select p;
           int i = 1;
           List<EINV_Issue> li = new List<EINV_Issue>();
           foreach (var a in pr)
           {
               EINV_Issue obj = new EINV_Issue();

               obj.ID = a.ID;
               obj.SL = i;
               i++;
               obj.ItemID =(int) a.ItemID;
               obj.ItemName = a.ItemName;
               obj.IssueTypeID = a.IssueTypeID?? 0;
               obj.IssueType = a.TypeName;
               obj.LanderID = a.LanderID ?? 0;
               obj.BoxUnit = a.BoxUnit;
               obj.BoxQty = Convert.ToDouble(a.BoxQty);
               obj.TotalQty = Convert.ToDouble(a.TotalQty);
               obj.Unit = a.Unit;
               obj.GroupName = a.GroupName;
               obj.ParentName = a.GroupNames;
               obj.StoreID = a.INVStoreID ?? 0;
               obj.InvoiceNo = a.InvoiceNo;
               obj.GatePassNo = a.GatePassNo;
               obj.LoanCompany = a.GivenCustomer;
               obj.Idate = a.Idate;
               obj.Supplier = a.Supplier;
               obj.Country = a.Country;
               obj.Notes = a.Notes;


               li.Add(obj);

           }
           return li;
       }


       public List<EINV_TransferReport> GetInventoryTransferReport(Int32 typeID, Int32 GID, string frm, string to)
       {
           DBAccessDataContext db = new DBAccessDataContext();
           db.Connection.ConnectionString = _Conn;
           var pr = from p in db.INV_Transfer_Issue_GET(typeID,  GID, frm, to)

                    orderby p.ID descending
                    select p;
           int i = 1;
           List<EINV_TransferReport> li = new List<EINV_TransferReport>();
           foreach (var a in pr)
           {
               EINV_TransferReport obj = new EINV_TransferReport();

              
               obj.ID = i;
               i++;
             
               obj.ItemName = a.ItemName;
               obj.GroupName = a.GroupName;
               obj.TransferDate = a.TransferDate;
               obj.TransferQty =Convert.ToDouble( a.TransferQty);
               obj.Unit = a.TransferUnit;
               obj.Notes = a.Notes;
               obj.ReqNo = a.ReqNo;
               obj.ToStore = a.ToStore;
               obj.FromStore = a.FromStore;





               li.Add(obj);

           }
           return li;
       }

    }
}
