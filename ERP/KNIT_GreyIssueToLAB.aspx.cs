﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
namespace ERP
{
    public partial class KNIT_GreyIssue : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadOrder();
               
               
            }
        }
        private void loadOrder()
        {
           

        }
       
        private void LoadOrderProductDesc()
        {
            ddlOP.DataSource = new Order_DL().GetOrderProductDesc(tbAuto.Text);
            ddlOP.DataValueField = "OPID";
            ddlOP.DataTextField = "DESC";

            ddlOP.DataBind();
            ddlOP.Items.Add("--Select--");
            ddlOP.Text = "--Select--";

            ddlReq.DataSource = new Order_DL().GetReq(tbAuto.Text);
            ddlReq.DataValueField = "SetupCode";
            ddlReq.DataTextField = "SetupCode";

          
        }
       

        private void LoadMCGrid()
        {




            ddlMCNo.DataSource = new KNIT_all_operation().GetAlMCNo();
            ddlMCNo.DataValueField = "MCNo";
            ddlMCNo.DataTextField = "MCNo";
            ddlMCNo.DataBind();
        

        }
        private void LoadOrderproductDetails()
        {
            try
            {

                ddlColor.DataSource = new Order_DL().GetOrderProductColorList(Convert.ToInt32(ddlOP.SelectedValue.ToString()));
                ddlColor.DataValueField = "OPDID";
                ddlColor.DataTextField = "Color";
                ddlColor.DataBind();
                ddlColor.Items.Add("--Select--");
                ddlColor.Text = "--Select--";


               
            }
            catch
            {
            }
        }

        protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void ddlOP_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOP.SelectedItem.Text != "--Select--")
            {


                LoadOrderproductDetails();
            }
        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (new Order_DL().HasOrderNoExist(tbAuto.Text) == false)
            {
                lblStatus.Text = "Order No Not Valid";
                
            }
            else
            {

                string[] str = hdnFID.Value.ToString().Split('_');
                EGreyFabricIssue obj = new EGreyFabricIssue();
                obj.PINO = tbAuto.Text.ToString();
                obj.OPDID = Convert.ToInt32(ddlColor.SelectedValue.ToString());
                obj.ID = Common.GetNumber(hdnValue.Value.ToString());
                obj.IssueQty = Common.GetDouble(txtIssueQty.Text);
                obj.FabricID = Convert.ToInt64(str[0]);
                obj.GM = str[1];
                obj.GSM = str[2];
                obj.MCNo = ddlMCNo.Text;
          
                obj.ReqNo = ddlReq.SelectedItem.Value.ToString();
               
                obj.IssueDate = System.DateTime.Now;
                obj.EntryUserID = USession.SUserID;
                obj.IsActive = true;
               
                if (IsValidEntry() == true)
                {
                    txtIssueQty.Text = string.Empty;

                   
                    txtIssueQty.Text = string.Empty;
                    ddlOP.DataSource = null;
                    ddlOP.DataBind();
                    ddlColor.DataSource = null;
                    ddlColor.DataBind();

                   
                    lblStatus.Text = new KNIT_all_operation().SaveGreyBooking(obj);
                }

            }
       
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            Int64 ID = Common.GetNumber(row.Cells[2].Text);
            List<EGreyFabricIssue> li = new KNIT_all_operation().GetGreyIssueBooking(ID);
            if (li.Count > 0)
            {
                //if (new Order_DL().HasOrderNoExist(tbAuto.Text) == false)
                //{
                //    lblStatus.Text = "Order No Not Valid";

                //}
                //else
                //{

                    tbAuto.Text = li[0].PINO.ToString();

                    LoadOrderProductDesc();
                    ddlOP.SelectedValue = li[0].OPID.ToString();

                    LoadOrderproductDetails();
                    ddlColor.SelectedValue = li[0].OPDID.ToString();

                   // ddlFG.SelectedValue = li[0].FabricGID.ToString();
                   
                    
                   // txtGM.Text = li[0].GM.ToString();
                   // txtGSM.Text = li[0].GSM.ToString();
                    ddlMCNo.SelectedValue = li[0].MCNo.ToString();
                    loadFabric();
                   // ddlFabric.SelectedValue = li[0].FabricID.ToString()+"_"+ li[0].GM;
                    //txtGSM.Text = li[0].GSM;
                    txtIssueQty.Text = li[0].IssueQty.ToString();

                    hdnValue.Value = li[0].ID.ToString();
                //}
            }
            
            
        }
        private bool IsValidEntry()
        {
            bool res;

            res = true;

            if (ddlColor.Text == "--Select--" || Common.GetNumber(ddlColor.SelectedValue) < 1)
            {
                lblStatus.Text = "Select a Product Color !!!";

                res = false;
            }


           

          

            if (ddlMCNo.Text =="--Select--")
            {
                lblStatus.Text = "Select any Machine No !!!";

                res = false;
            }

            

            if ( Common.GetDouble(txtIssueQty.Text) ==0)
            {
                lblStatus.Text = "Issued Qty is not given !!!";

                res = false;
            }
            return res;
        }
        protected void btnOperation_Click(object sender, EventArgs e)
        {
             Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            Int64 ID =Common.GetNumber( row.Cells[1].Text);
            if (ID > 0)
            {
                new KNIT_all_operation().DeleteGreyBooking(ID, USession.SUserID);
                LoadGrid();
            }

           
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            LoadGrid();
        }
        private void LoadGrid()
        {

            
        gvData.DataSource = new KNIT_all_operation().GetGreyIssue(ddlMCNo.Text,OpenFunction.ConvertDate(txtFrom0.Text));
            gvData.DataBind();
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtIssueQty.Text = string.Empty;
            
           
            hdnValue.Value = "0";
            txtIssueQty.Text = string.Empty;
           
        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[1].Visible = false;
                // e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadOrderProductDesc();
        }

        protected void ddlMCNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadFabric();
        }

        private void loadFabric()
        {
            //ddlFabric.DataSource = new KNIT_all_operation().GetFabricListByMCno(ddlMCNo.Text);
            //ddlFabric.DataTextField = "Fabric";
            //ddlFabric.DataValueField = "FID";
            //ddlFabric.DataBind();
        }

        protected void ddlColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            int OPDID = 0;
            int.TryParse(ddlColor.SelectedItem.Value.ToString(), out OPDID);
            ddlReq.DataSource = new KNIT_all_operation().GetReqNoByColor(OPDID);
            ddlReq.DataTextField = "ReqNo";
            ddlReq.DataValueField = "ReqNo";
            ddlReq.DataBind();
            
        }

        protected void ddlReq_SelectedIndexChanged(object sender, EventArgs e)
        {
           KNIT_all_operation obj= new KNIT_all_operation();
            ddlMCNo.DataSource = obj.GetListOfMCByRefNo(ddlReq.SelectedItem.Text);
            ddlMCNo.DataTextField = "MCNo";
            ddlMCNo.DataValueField = "MCNo";
            ddlMCNo.DataBind();

           var val= obj.GetFabircDetailsByReqNo(ddlReq.SelectedItem.Text);
           if (val != null)
           {
               hdnFID.Value = val.FID.ToString() + "_" + val.GM + "_" + val.GSM;
               lblFabric.Text = val.Fabric1 + "  GM:" + val.GM + " GSM:" + val.GSM; 
           }
        }
    }
}
