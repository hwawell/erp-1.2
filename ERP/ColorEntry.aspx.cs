﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;

namespace ERP
{
    public partial class WebForm26 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                   if (new Utilities_DL().CheckSecurity("500", USession.SUserID) == true)
                {
                this.rblNormal.Items.FindByText("Active").Selected = true;
                loadGridData();
                }

                   else
                   {
                       Response.Redirect("Home.aspx");
                   }
            }
        }
        private void loadGridData()
        {


            gvProducts.Columns[2].Visible = true;
            gvProducts.Columns[3].Visible = true;
            gvProducts.Columns[4].Visible = false;
            gvProducts.DataSource = new Utilities_DL().GetActiveColor();
            gvProducts.DataBind();

        }
        private void loadDeletedData()
        {

            gvProducts.Columns[2].Visible = false;
            gvProducts.Columns[3].Visible = false;
            gvProducts.Columns[4].Visible = true;
            gvProducts.DataSource = new Utilities_DL().GetDeletedColor();
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            txtColor.Text = (row.Cells[1].Text);
           

            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Utilities_DL().DeleteActiveColor(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Utilities_DL().DeleteActiveColor(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            Color c = new Color();
            c.SL = Convert.ToInt64(hdnCode.Value);
            c.ColorName = txtColor.Text.ToString();
          

         
            if (txtColor.Text.Length > 0)
            {
                new Utilities_DL().SaveColor(c);
            }
            loadGridData();
            //loadGridData();
            updPanel.Update();
            mpeProduct.Hide();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Color c = new Color();
            c.SL = Convert.ToInt64(hdnCode.Value);
            c.ColorName = txtColor.Text.ToString();



            if (txtColor.Text.Length > 0)
            {
                new Utilities_DL().SaveColor(c);
            }
            //loadGridData();
            //loadGridData();
            txtColor.Text = "";
          
            updPanel.Update();
            mpeProduct.Show();
            txtColor.Focus();

        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            txtColor.Text = string.Empty;
          



            mpeProduct.Show();
            txtColor.Focus();
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblNormal.Items.FindByText("Active").Selected == true)
            {
                loadGridData();

            }
            else
            {
                loadDeletedData();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            loadGridData();
        }
    }
}
