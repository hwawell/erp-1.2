﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;

namespace ERP
{
    public partial class WebForm12 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

               ////// if (new Utilities_DL().CheckSecurity("505", USession.SUserID) == true)
               ////// {
               ////// ////this.rblNormal.Items.FindByText("Active").Selected = true;
               //////// loadGridData();
               ////// //loadPC();
               //////// loadFabric();
               ////// }
               ////// else
               ////// {
               //////     Response.Redirect("Home.aspx");
               ////// }
            }
        }
        private void loadPC()
        {
            //ddlPCardno.DataSource = new Knit_Process_CL().GetAllDeyingList();
            //ddlPCardno.DataValueField = "PCardNo";
            //ddlPCardno.DataTextField = "fab";
            //ddlPCardno.DataBind();
            //ddlPCardno.Items.Insert(0, "--Select Process Card--");
        }
        private void loadFabric()
        {
            ////ddlFabric.DataSource = new Fabric_CL().GetAlFabric();
            ////ddlFabric.DataValueField = "Fid";
            ////ddlFabric.DataTextField = "fab";
            ////ddlFabric.DataBind();
            ////ddlFabric.Items.Insert(0, "--Select Fabric--");
            //ddlFabric.Items.Insert(0, "Select--");
        }
        private bool isValidDate(string s)
        {
            DateTime d;
            try
            {
                d = Convert.ToDateTime(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        private void loadGridData()
        {

            gvProducts.Columns[8].Visible = true;
            gvProducts.Columns[9].Visible = false;
            gvProducts.Columns[10].Visible = false;
            gvProducts.Columns[11].Visible = false;
            gvProducts.DataSource = new Knit_Process_CL().GetActiveDataDeying("DEYING").Tables[0];
            gvProducts.DataBind();

        }
        private void loadDeletedData()
        {
            try
            {
                gvProducts.Columns[8].Visible = false;
                gvProducts.Columns[9].Visible = false;
                gvProducts.Columns[10].Visible = false;
                gvProducts.Columns[11].Visible = true;
                gvProducts.DataSource = new Knit_Process_CL().GetDeletedData("DEYING");
                gvProducts.DataBind();
            }
            catch (Exception)
            {
            }
            

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;

            lblProcessCard.Text = (row.Cells[1].Text.ToString().Trim());
            txtQty.Text = (row.Cells[2].Text.ToString().Trim());
            hdnQty.Value = row.Cells[2].Text.ToString();
            txtMCNo.Text = row.Cells[3].Text.Replace("&nbsp;", "").Trim();
            txtNotes.Text = row.Cells[4].Text.Replace("&nbsp;", "").Trim();
            txtDate.Text = row.Cells[5].Text.Replace("12:00:00 AM", "");
            txtQty.ReadOnly = true;
            txtMCNo.Focus();
            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Knit_Process_CL().DeleteActiveData(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            if (row.Cells[1].Text.Contains("-")==false)
            {
                new Knit_Process_CL().DeleteFabricDelivery(row.Cells[12].Text);

            }
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Knit_Process_CL().DeleteActiveData(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                if (e.Row.Cells.Count > 9)
                {
                    e.Row.Cells[10].Visible = false;
                    e.Row.Cells[11].Visible = false;
                }
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        //protected void btnSave_Click1(object sender, EventArgs e)
        //{
            
        //    KnitProcessing c = new KnitProcessing();
        //    c.SL = Convert.ToInt64(hdnCode.Value);
           
            
        //        if (checkDecimal(txtQty.Text) == false)
        //        {
        //            txtQty.Text = "0";
        //        }
        //        if (Convert.ToDecimal(txtQty.Text) > 0)
        //        {
        //            c.Section = "DEYING";
        //            c.PQty = Convert.ToDecimal(txtQty.Text);
        //            c.MCNo = txtMCNo.Text.ToString();
        //            c.Notes = txtNotes.Text.ToString();
        //            c.Status = "P";
        //            c.EntryID = USession.SUserID;
        //            if (isValidDate(txtDate.Text) == true)
        //                c.EntryDate = Convert.ToDateTime(txtDate.Text);
        //            else
        //            {
        //                c.EntryDate = DateTime.Now.Date;
        //                txtDate.Text = DateTime.Now.Date.ToShortDateString();
        //            }
        //            c.SHIFT = ddlShift.SelectedItem.Text.ToString();
        //            if (txtQty.Text.Length > 0)
        //            {
        //                new Knit_Process_CL().Save(c);
        //            }


                    
                
        //        loadGridData();
        //        //loadGridData();
        //        updPanel.Update();
        //        mpeProduct.Hide();
        //        }
            
        //}
        private bool checkDecimal(string s)
        {

            try
            {
                decimal v;
                v = Convert.ToDecimal(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

            Save();
            ////////KnitProcessing c = new KnitProcessing();
            ////////c.SL = Convert.ToInt64(hdnCode.Value);
            

           
            ////////    if (checkDecimal(txtQty.Text) == false)
            ////////    {
            ////////        txtQty.Text = "0";
            ////////    }
            ////////    c.Section = "DEYING";
            ////////    c.PQty = Convert.ToDecimal(txtQty.Text);
            ////////    c.MCNo = txtMCNo.Text.ToString();
            ////////    c.Notes = txtNotes.Text.ToString();
            ////////    c.Status = "P";
            ////////    c.EntryID = USession.SUserID;
            ////////    if (isValidDate(txtDate.Text) == true)
            ////////        c.EntryDate = Convert.ToDateTime(txtDate.Text);
            ////////    else
            ////////    {
            ////////        c.EntryDate = DateTime.Now.Date;
            ////////        txtDate.Text = DateTime.Now.Date.ToShortDateString();
            ////////    }
            ////////    c.SHIFT = ddlShift.SelectedItem.Text.ToString();
            ////////    if (txtQty.Text.Length > 0)
            ////////    {
            ////////        new Knit_Process_CL().Save(c);
            ////////    }

                
            ////////    loadGridData();
               
            ////////    txtQty.Text = "";
            ////////    txtMCNo.Text = "";
            ////////    txtNotes.Text = "";
            ////////    updPanel.Update();
            ////////    mpeProduct.Show();
               
                //}
            
        }
        private void Save()
        {
            decimal  qty;
            KnitProcessing c = new KnitProcessing();
            c.SL = Convert.ToInt64(hdnCode.Value);
            c.PCardNo = lblProcessCard.Text.Trim();
            //string cd = hdnCCard.Value.Trim().Replace("&nbsp;", "").ToString();
            //if (cd != "")
            //{
            c.CCardNo = "";
            //}

            if (checkDecimal(txtQty.Text) == false)

                qty = 0;
            else
                qty = Convert.ToDecimal(txtQty.Text);


            c.Section = "DEYING";
            c.PQty = qty;
            c.MCNo = txtMCNo.Text.ToString();
            c.Notes = txtNotes.Text.Trim();
            c.Status = "P";
            c.EntryID = USession.SUserID;
            if (isValidDate(txtDate.Text.ToString()) == true)
                c.EntryDate = Convert.ToDateTime(txtDate.Text);
            else
                c.EntryDate = DateTime.Now.Date;
            c.SHIFT = ddlShift.SelectedItem.Text.ToString();
            
                if (txtMCNo.Text.Length > 0)
                {
                    new Knit_Process_CL().Save(c);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('MC No Not Given .Try Again');</script>", false);
                }
                
            
           


            loadGridData();

            txtQty.Text = "";
            txtMCNo.Text = "";
            txtNotes.Text = "";


            updPanel.Update();
            mpeProduct.Hide();
        }
        private void SaveFabricData()
        {
            ////FabricDelivery c = new FabricDelivery();

           
            ////c.SL = 0;
            ////c.FID = Convert.ToInt64(ddlFabric.SelectedValue.ToString());
            ////c.PCardId = ddlPCardno.Text.ToString();
      
            ////c.DQty = Convert.ToDecimal(txtQty.Text);
            ////c.DDate = Convert.ToDateTime(txtProductionDate.Text);
            ////c.EntryID = USession.SUserID;
            ////if (ddlPCardno.Visible == true)
            ////{
            ////    if (c.DQty > 0)
            ////    {
            ////        new Fabric_CL().SaveDelivery(c);
            ////        new Knit_Process_CL().UpdateProcessCard(c.PCardId, Convert.ToDouble(txtQty.Text));

            ////    }
            ////}
        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            txtQty.Text = "";
            txtMCNo.Text = "";
            txtNotes.Text = "";
            
            txtQty.ReadOnly = false;
            
         


            mpeProduct.Show();
            
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (rblNormal.Items.FindByText("Active").Selected == true)
            //{
                loadGridData();

            //}
            //else
            //{
            //    loadDeletedData();
            //}
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            loadGridData();
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
        
            Button btnSent = sender as Button;
            GridViewRow row = (GridViewRow)btnSent.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;


            KnitProcessing c = new KnitProcessing();
            c.SL = 0;
            c.PCardNo = (row.Cells[1].Text);
            if (checkDecimal((row.Cells[2].Text)) == false)
            {
                (row.Cells[2].Text) = "0";
            }
            c.PQty = Convert.ToDecimal((row.Cells[2].Text));
            c.Section = "RAISING";
            c.Status = "P";
            c.MCNo = "";
            c.Notes = "";

            c.EntryID = USession.SUserID;
            if (isValidDate(row.Cells[5].Text.ToString()) == true)
                c.EntryDate = Convert.ToDateTime(row.Cells[5].Text);
            else
                c.EntryDate = DateTime.Now.Date;
            c.SHIFT = ddlShift.SelectedItem.Text.ToString();
            if (((row.Cells[3].Text.Replace("&nbsp;", "").Trim().Length) > 0) )
            {

                if (c.PQty > 0)
                {
                    new Knit_Process_CL().Save(c);

                    c.SL = Convert.ToInt64(row.Cells[0].Text);
                    c.Section = "DEYING";
                    c.Status = "F";
                    c.MCNo = row.Cells[3].Text.Trim();
                    c.Notes = row.Cells[4].Text.Replace("&nbsp;", "");
                    new Knit_Process_CL().Save(c);
                    loadGridData();
                }
            }

           
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            loadGridData();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            gvProducts.Columns[8].Visible = true;
            gvProducts.Columns[9].Visible = false;
            gvProducts.Columns[10].Visible = false;
            gvProducts.Columns[11].Visible = false;
            if (txtPCard.Text.Trim() == "*")
            {
                gvProducts.DataSource = new Knit_Process_CL().GetActiveDataDeying("DEYING").Tables[0];
            }
            else
            {
                gvProducts.DataSource = new Knit_Process_CL().GetActiveDataDeyingbyPCard("DEYING", txtPCard.Text).Tables[0];
            }
            gvProducts.DataBind();
            updPanel.Update();
        }
    }
}
