﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="ERP.WebForm33" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
     <div>
     <h3>Change Password</h3>
     </div>
     <div style="float: left; width: 30%; height:auto; margin-left:20%; display: inline;" 
         align="right">
      
       Old Password : 
      </div>
     <div style="float: left; width: 30%; height:auto;" align="left">
         <asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password" 
             MaxLength="12"></asp:TextBox>
     </div>
    
     <div style="float: left; width: 30%; height:auto; margin-left:20%; display: inline;" 
         align="right">
      New Password : 
     </div>
     <div style="float: left; width: 30%; height:auto;" align="left">
       <asp:TextBox ID="txtNew" runat="server" TextMode="Password" MaxLength="12"></asp:TextBox>
     </div>
    
     
     <div style="float: left;  width: 30%; height:auto; margin-left:20%; display: inline;" 
         align="right">
     Confirm Password :
     </div>
    
     <div style="float: left; width: 30%; height:auto;" align="left" >
     
    <asp:TextBox ID="txtConfirm" runat="server" TextMode="Password" MaxLength="12"></asp:TextBox>
     </div>
     
      <div style="float: left;  width: 30%; height:auto; margin-left:20%; display: inline;" 
         align="right">
     .
     </div>
    
     <div style="float: left; width: 30%; height:auto;" align="left" >
     
         <asp:Button ID="btnSave" runat="server" Text="Change Password" 
             onclick="btnSave_Click" />
     </div>
      <div style="float: left;  width: 30%; height:auto; margin-left:20%; display: inline;" 
         align="right">
     .
     </div>
    
     <div style="float: left; width: 30%; height:auto;" align="left" >
         <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
   
     </div>
     
</div>
</asp:Content>
