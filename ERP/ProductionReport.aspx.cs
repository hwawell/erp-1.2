﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDLayer;
using System.Configuration;
using Ext.Net;
using System.IO;
using System.Reflection;
using System.Data;
namespace ERP
{
    public partial class ProductionReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtFrom.Text = System.DateTime.Now.Date.ToString("MM/dd/yyyy");
                txtTo.Text = System.DateTime.Now.Date.ToString("MM/dd/yyyy");

                ddlCustomer.DataSource = new Customer_DL().GetActiveCustomers();
                ddlCustomer.DataTextField = "CName";
                ddlCustomer.DataValueField = "CustID";

                ddlCustomer.DataBind();
                ddlCustomer.Items.Add("All Customer");
                ddlCustomer.Text = "All Customer";

                


            }
        }


      
        protected void btnPacking_Click(object sender, EventArgs e)
        {
           

            List<EProductionReport> liProd = new List<EProductionReport>();

            if (rdoDate.Checked == true)
            {
                string PValue = rdoPList.SelectedValue.ToString();
                if (PValue == "DEYING")
                {
                    //this.Window1.Title = "Production Report Summary : Dyeing";
                    liProd = new Order_DL().GetAllProductionSummary(Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text), "", 0, "", 1);
                }
                else if (PValue == "PACKING")
                {
                   // this.Window1.Title = "Production Report Summary : Packing";
                    liProd = new Order_DL().GetAllProductionSummary(Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text), "", 0, "", 2);
                }
                else if (PValue == "DELIVERY")
                {
                   // this.Window1.Title = "Production Report Summary : Delivery";
                    liProd = new Order_DL().GetAllProductionSummary(Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text), "", 0, "", 3);
                }
            }
            else
            {
                string PValue = rdoPList.SelectedValue.ToString();
                string Order = "";
                string Merchandiser = "";
                Int64 CustID = 0;
                if (tbAuto.Text.Length==0)
                {
                    Order = "All Order";

                }
                else
                {
                    Order = tbAuto.Text;
                }
                if (ddlMerchandiser.Text == "All Merchandiser")
                {
                    Merchandiser = "";

                }
                else
                {
                    Merchandiser = ddlMerchandiser.Text;
                }
                if (ddlCustomer.Text == "All Customer")
                {
                    CustID = 0;

                }
                else
                {
                    CustID = Convert.ToInt64(ddlCustomer.SelectedValue);
                }
                if (Order == "" && Merchandiser == "" && CustID == 0)
                {

                }
                else
                {
                    if (PValue == "DEYING")
                    {
                       // this.Window1.Title = "Production Report Summary : Dyeing";
                        liProd = new Order_DL().GetAllProductionSummaryAll(Order, CustID, Merchandiser, 1);
                    }
                    else if (PValue == "PACKING")
                    {
                       // this.Window1.Title = "Production Report Summary : Packing";
                        liProd = new Order_DL().GetAllProductionSummaryAll(Order, CustID, Merchandiser, 2);
                    }
                    else if (PValue == "DELIVERY")
                    {
                       // this.Window1.Title = "Production Report Summary : Delivery";
                        liProd = new Order_DL().GetAllProductionSummaryAll(Order, CustID, Merchandiser, 3);
                    }
                }
            }
            //List<EProductionReport> liProd = new List<EProductionReport>();

            //string PValue = rdoPList.SelectedValue.ToString();
            //if (PValue == "DEYING")
            //{
            //    liProd = new Order_DL().GetAllProductionSummary(Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text), "", 0, "",1);
            //}
            //else if (PValue == "PACKING")
            //{
            //    liProd = new Order_DL().GetAllProductionSummary(Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text), "", 0, "",2);
            //}
            //else if (PValue == "DELIVERY")
            //{
            //    liProd = new Order_DL().GetAllProductionSummary(Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text), "", 0, "",3);
            //}

            Export("ProductionSummaryReport-"+rdoPList.SelectedValue.ToString(), liProd);
        }
        
       
        public void Export(string fileName, List<EProductionReport> empList)
        {

            //The Clear method erases any buffered HTML output.

            HttpContext.Current.Response.Clear();

            //The AddHeader method adds a new HTML header and value to the response sent to the client.

            HttpContext.Current.Response.AddHeader(

               "content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));

            //The ContentType property specifies the HTTP content type for the response.

            HttpContext.Current.Response.ContentType = "application/ms-excel";

            //Implements a TextWriter for writing information to a string. The information is stored in an underlying StringBuilder.

            using (StringWriter sw = new StringWriter())
            {

                //Writes markup characters and text to an ASP.NET server control output stream. This class provides formatting capabilities that ASP.NET server controls use when rendering markup to clients.

                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {

                    //  Create a form to contain the List

                    Table table = new Table();

                    TableRow row0 = new TableRow();

                    TableHeaderCell col10 = new TableHeaderCell();
                    col10.Text = "Production Report : " + rdoPList.SelectedValue.ToString();
                    TableHeaderCell col20 = new TableHeaderCell();
                    col20.Text = " From " + txtFrom.Text + " To " + txtTo.Text;

                    row0.Cells.Add(col10);
                    row0.Cells.Add(col20);

                    //  add each of the data item to the table
                    table.Rows.Add(row0);



                    TableRow row = new TableRow();

                    foreach (PropertyInfo proinfo in new EProductionReport().GetType().GetProperties())
                    {

                        TableHeaderCell hcell = new TableHeaderCell();

                        hcell.Text = proinfo.Name;

                        row.Cells.Add(hcell);

                    }

                    table.Rows.Add(row);

                   
                    foreach (EProductionReport emp in empList)
                    {
                       
                       
                        TableRow row1 = new TableRow();

                        TableCell col1 = new TableCell();
                        col1.Text = "" + emp.OrderNo;
                        TableCell col2 = new TableCell();
                        col2.Text = "" + emp.Merchandiser;
                        TableCell col3 = new TableCell();
                        col3.Text = "" + emp.Customer;
                        TableCell col4 = new TableCell();
                        col4.Text = "" + emp.Product;

                        TableCell col5 = new TableCell();
                        col5.Text = "" + emp.GSM;


                        TableCell col6 = new TableCell();
                        col6.Text = "" + emp.Width;
                        TableCell col7 = new TableCell();
                        col7.Text = "" + emp.BookingQty;
                        TableCell col8 = new TableCell();
                        col8.Text = "" + emp.Qty;
                        TableCell col9 = new TableCell();
                        col9.Text = "" + emp.Balance;

                        row1.Cells.Add(col1);
                        row1.Cells.Add(col2);
                        row1.Cells.Add(col3);
                        row1.Cells.Add(col4);
                        row1.Cells.Add(col5);
                        row1.Cells.Add(col6);
                        row1.Cells.Add(col7);
                        row1.Cells.Add(col8);
                        row1.Cells.Add(col9);
                        table.Rows.Add(row1);

                    }

                    //  render the table into the htmlwriter

                    table.RenderControl(htw);

                    //  render the htmlwriter into the response

                    HttpContext.Current.Response.Write(sw.ToString());

                    HttpContext.Current.Response.End();

                }

            }

        }
    }
}
