using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for USession
/// </summary>
public class USession
{	
    public static string SUserID
    {
        get
        {
            object obj = HttpContext.Current.Session["SUserID"];
            if (obj == null) return "";
            return obj as string;
        }
        set
        {
            HttpContext.Current.Session["SUserID"] = value;
        }
    }

    public static string SUserName
    {
        get
        {
            object obj = HttpContext.Current.Session["SUserName"];
            if (obj == null) return "";
            return obj as string;
        }
        set
        {
            HttpContext.Current.Session["SUserName"] = value;

        }
    }

    public static string SUserType
    {
        get
        {
            object obj = HttpContext.Current.Session["SUserType"];
            if (obj == null) return "";
            return obj as string;
        }
        set
        {
            HttpContext.Current.Session["SUserType"] = value;

        }
    }
}
