﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


    public class OpenFunction
    {
        public static bool IsNumeric(object expression)
        {
            bool isNum;
            double retNum;
            isNum = double.TryParse(expression.ToString(), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }
        public static bool IsChar(object expression)
        {
            bool isChar;
            char  retChar;
            isChar = char.TryParse(expression .ToString (),out retChar );
            return isChar;
        }

        public static DateTime? ConvertDate(string Dt)
        {
            try
            {
                return Convert.ToDateTime(Dt);

            }
            catch
            {
                return null;
            }
           
        }

    }
