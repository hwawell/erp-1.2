using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Diagnostics;


public class ErrorHandling_CL
{
    public void WriteErrorDescription(String ErrorString)
    {
        //' Create the source, if it does not already exist.
        if (!EventLog.SourceExists("OCM"))
            EventLog.CreateEventSource("OCM", "Online Collection Log");
        //' Create an EventLog instance and assign its source.
        EventLog myLog = new EventLog();
        myLog.Source = "OCM";
        // ' Write an informational entry to the event log.    
        myLog.WriteEntry(ErrorString);
    }
}
