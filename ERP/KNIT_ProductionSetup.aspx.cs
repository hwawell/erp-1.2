﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;


namespace ERP
{
    public partial class KNIT_ProductionSetup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                LoadFabricType();
                LoadYarnGroup();
                LoadMachine();
            }
        }
        
        private void LoadFabricType()
        {


            ddlFabricType.DataSource = new Fabric_CL().GetAllFabricGroup();
            ddlFabricType.DataValueField = "SL";
            ddlFabricType.DataTextField = "FGroupName";
            ddlFabricType.DataBind();

        }
        private void LoadMachine()
        {

            List<EKnitingAvailableMCList> li = new KNIT_all_operation().GetMCList(0);
            List<EKnitingAvailableMCList> liw = new List<EKnitingAvailableMCList>();
            if (ViewState["liMC"] != null)
            {
                liw = (List<EKnitingAvailableMCList>)ViewState["liMC"];

            }
            foreach (EKnitingAvailableMCList obj in liw)
            {
                if (li.Exists(o => o.Machine == obj.Machine) == false)
                {
                    li.Add(obj);
                }
            }

            
            ddlMCNo0.DataSource = li;
            ddlMCNo0.DataValueField = "Machine";
            ddlMCNo0.DataTextField = "Machine";
            ddlMCNo0.DataBind();

        }
        private void LoadYarn(Int64 ygID,DropDownList dt)
        {
            dt.DataSource = new Yarn_DL().GetYarnByYarnGroup(ygID);
            dt.DataValueField = "YID";
            dt.DataTextField = "YarnName";
            dt.DataBind();
            dt.Items.Add("--Select--");
            dt.Text = "--Select--";


        }
        private void LoadYarnGroup()
        {

            
      
            ddlYarnType.DataSource = new Yarn_DL().GetActiveYarnGroup();
            ddlYarnType.DataValueField = "ID";
            ddlYarnType.DataTextField = "YarnGroupName";
            ddlYarnType.DataBind();
            ddlYarnType.Items.Add("--Select--");
            ddlYarnType.Text = "--Select--";

            LoadAllYarnGroup(ddlYarnType, ddlYarnType0);
            LoadAllYarnGroup(ddlYarnType, ddlYarnType1);
            LoadAllYarnGroup(ddlYarnType, ddlYarnType2);
            LoadAllYarnGroup(ddlYarnType, ddlYarnType3);

           


        }
        private void LoadAllYarnGroup(DropDownList dtSource , DropDownList Assign )
        {
            Assign.DataSource = dtSource.DataSource;
            Assign.DataValueField = "ID";
            Assign.DataTextField = "YarnGroupName";
            Assign.DataBind();
            Assign.Items.Add("--Select--");
            Assign.Text = "--Select--";
        }
        private void LoadFabric()
        {
            ddlFabric.DataSource = new Fabric_CL().GetAlFabricbyGRoup(Convert.ToInt32(ddlFabricType.SelectedValue));
            ddlFabric.DataValueField = "Fid";
            ddlFabric.DataTextField = "fab";
            ddlFabric.DataBind();
        }
       
        private void LoadMCGrid(Int32  ID)
        {
            
            List<EKnitingAvailableMCList> li = new KNIT_all_operation().GetMCList(ID).FindAll(o=> o.ID>0 );
           

          
            gvProducts.AutoGenerateColumns = false;
            gvProducts.DataSource = li;
           
           // 
            //ViewState["liv"] = li;
            gvProducts.DataBind();
            ViewState["liMC"] = li;

            LoadMachine();
           
        }
       

      

        protected void ddlFabricType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadFabric();
        }

        protected void ddlYarnType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dt = (DropDownList)sender;
            Int64 GID = Convert.ToInt64(dt.SelectedValue);
            LoadYarn(GID, ddlYarn);
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;


               // e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }

            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    Button btn = (Button)e.Row.Cells[3].Controls[1];
            //    if (e.Row.Cells[2].Text == "Running")
            //    {
            //        btn.Text = "Stop";
            //        btn.BackColor = System.Drawing.Color.Red;
            //    }
            //    else
            //    {
            //        btn.Text = "Start";
            //        btn.BackColor = System.Drawing.Color.Green;
            //    }

            //}

        }

        protected void gvProducts1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }

           

        }


        protected void btnOperation_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            //LoadMCGrid();

            List<EKnitingAvailableMCList> li = new List<EKnitingAvailableMCList>();
            if (ViewState["liMC"] != null)
            {
                li = (List<EKnitingAvailableMCList>)ViewState["liMC"];

            }

           
            EKnitingAvailableMCList obj = new EKnitingAvailableMCList();
            obj.Machine = ddlMCNo0.Text;
            obj.AssignQty=Common.GetDouble( txtAssignQty.Text);
            obj.IsRunning = true;

            if (li.Exists(o => o.Machine == obj.Machine))
            {
                
                li.SingleOrDefault(o => o.Machine == obj.Machine).AssignQty = Common.GetDouble(txtAssignQty.Text);
                li.SingleOrDefault(o => o.Machine == obj.Machine).IsRunning = true;
                li.SingleOrDefault(o => o.Machine == obj.Machine).Status = "Running";
            }
            else
            {
                li.Add(obj);
            }
       
            ViewState["liMC"] = li;
            gvProducts.AutoGenerateColumns = false;
            gvProducts.DataSource = li;
            gvProducts.DataBind();


            double avail = (Common.GetDouble(txtNet.Text) - li.FindAll(o=> o.IsRunning==true).Sum(o => o.AssignQty));
            txtRemaining.Text = avail.ToString();
            txtAssignQty.Text = "";
        }

        protected void gvProducts_RowCreated(object sender, GridViewRowEventArgs e)
        {
             if (e.Row.RowType == DataControlRowType.DataRow )
             {
                 //Button btn= (Button)  e.Row.Cells[3].Controls[1];
                 //if (e.Row.Cells[2].Text=="Running")
                 //{
                 //    btn.Text = "Stop";
                 //    btn.BackColor = System.Drawing.Color.Red;
                 //}
                 //else
                 //{
                 //    btn.Text = "Start";
                 //    btn.BackColor = System.Drawing.Color.Green;
                 //}
                
             }
        
        }

        protected void btnOperation_Click(object sender, EventArgs e)
        {
            Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;

            List<EKnitingAvailableMCList> li = new List<EKnitingAvailableMCList>();
            if (ViewState["liMC"] != null)
            {
                li = (List<EKnitingAvailableMCList>)ViewState["liMC"];

                li.RemoveAll(o => o.Machine == row.Cells[1].Text.Trim());
                ViewState["liMC"] = li;
                gvProducts.AutoGenerateColumns = false;
                gvProducts.DataSource = li;
                gvProducts.DataBind();
            }
            Calculate();
            //if (btnEdit.Text == "Start")
            //{
            //    row.ForeColor = System.Drawing.Color.Green;
            //    row.Font.Bold = true;
            //    row.Cells[2].Text = "Running";
            //    btnEdit.Text = "Stop";
            //    btnEdit.BackColor = System.Drawing.Color.Red;
            //}
            //else
            //{
            //    row.Font.Bold = false;
            //    row.Cells[2].Text = "Available";
            //    row.ForeColor = System.Drawing.Color.Black;
            //    btnEdit.Text = "Start";
            //    btnEdit.BackColor = System.Drawing.Color.Green;
            //}
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            
            List<EKnitingAvailableMCList> mylist = (List<EKnitingAvailableMCList>)ViewState["li"];
        }

        protected void dgvYarn_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void ddlYarnType0_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dt = (DropDownList)sender;
            Int64 GID = Convert.ToInt64(dt.SelectedValue);
            LoadYarn(GID, ddlYarn0);
        }

        protected void ddlYarnType1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dt = (DropDownList)sender;
            Int64 GID = Convert.ToInt64(dt.SelectedValue);
            LoadYarn(GID, ddlYarn1);
        }

        protected void ddlYarnType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dt = (DropDownList)sender;
            Int64 GID = Convert.ToInt64(dt.SelectedValue);
            LoadYarn(GID, ddlYarn2);
        }

        protected void ddlYarnType3_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dt = (DropDownList)sender;
            Int64 GID = Convert.ToInt64(dt.SelectedValue);
            LoadYarn(GID, ddlYarn3);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                EKnitingProductionSetup obj = new EKnitingProductionSetup();


                obj.ID = Common.GetNumber(hdnValue.Value);
                obj.ProductionQty = Common.GetDouble(txtNet.Text);
                obj.ExtraQty = Common.GetDouble(txtExtra.Text);
                obj.FabricID = Convert.ToInt32(ddlFabric.SelectedValue.ToString());
                obj.GM = txtGM.Text;
                obj.OrderNo = txtOrderNo.Text;
                obj.Location = txtLocation.Text;
                obj.SubContact = "";
                obj.IsSubContact = chkSubContact.Checked;
                obj.GSM = txtGSM.Text;
                obj.SetupDate = Common.GetDate(txtDate.Text);
                obj.IsActive = true;
                obj.Notes = txtNotes.Text;
             
                List<EYarnConsumptionList> li = GetYarnList();
                //if (li.Sum(o => o.PerQty) <= 100)
                //{
                if (obj.FabricID > 0)
                {

                    List<EKnitSetupOrderList> liO = new List<EKnitSetupOrderList>();
                    if (ViewState["liOrder"] != null)
                    {
                        liO = (List<EKnitSetupOrderList>)ViewState["liOrder"];

                    }

                    List<EKnitingAvailableMCList> liMC = new List<EKnitingAvailableMCList>();
                    if (ViewState["liMC"] != null)
                    {
                        liMC = (List<EKnitingAvailableMCList>)ViewState["liMC"];

                    }



                    lblStatus.Text = new KNIT_all_operation().SaveKnitSetup(obj, liMC, li, liO);
                   
                    if (lblStatus.Text.Contains("Successfully") == true)
                    {
                        
                        Clear();
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                        
                    }
                }
                btnSave.Focus();
            }

            catch(Exception ex)
            {
                lblStatus.ForeColor = System.Drawing.Color.Red;
                lblStatus.Text = "Error :"+ ex.Message.ToString();
                btnSave.Focus();
            }
            //}
            //else
            //{
            //    lblStatus.Text = "Yarn consumption Qty is greater than Production Qty";
            //}
        }
        private List<EYarnConsumptionList> GetYarnList()
        {
            EYarnConsumptionList obj;
            List<EYarnConsumptionList> li = new List<EYarnConsumptionList>();
            if (Common.GetDecimal(txtUserPercent.Text) > 0)
            {
                obj = new EYarnConsumptionList();
                obj.YarnID = Common.GetNumber( ddlYarn.SelectedValue.ToString());
                if (obj.YarnID > 0)
                {
                    obj.PerQty = Common.GetDecimal(txtUserPercent.Text);
                    obj.Qty = (Common.GetDecimal(txtNet.Text) * obj.PerQty) / 100;
                    li.Add(obj);
                }
            }


            if (Common.GetDecimal(txtUserPercent0.Text) > 0)
            {
                obj = new EYarnConsumptionList();
                obj.YarnID = Common.GetNumber(ddlYarn0.SelectedValue.ToString());
                if (obj.YarnID > 0)
                {
                    obj.PerQty = Common.GetDecimal(txtUserPercent0.Text);
                    obj.Qty = (Common.GetDecimal(txtNet.Text) * obj.PerQty) / 100;
                    li.Add(obj);
                }
            }


            if (Common.GetDecimal(txtUserPercent1.Text) > 0)
            {
                obj = new EYarnConsumptionList();
                obj.YarnID = Common.GetNumber(ddlYarn1.SelectedValue.ToString());
                if (obj.YarnID > 0)
                {
                    obj.PerQty = Common.GetDecimal(txtUserPercent1.Text);
                    obj.Qty = (Common.GetDecimal(txtNet.Text) * obj.PerQty) / 100;
                    li.Add(obj);
                }
            }


            if (Common.GetDecimal(txtUserPercent2.Text) > 0)
            {
                obj = new EYarnConsumptionList();
                obj.YarnID = Common.GetNumber(ddlYarn2.SelectedValue.ToString());
                if (obj.YarnID > 0)
                {
                    obj.PerQty = Common.GetDecimal(txtUserPercent2.Text);
                    obj.Qty = (Common.GetDecimal(txtNet.Text) * obj.PerQty) / 100;
                    li.Add(obj);
                }
            }


            if (Common.GetDecimal(txtUserPercent3.Text) > 0)
            {
                obj = new EYarnConsumptionList();
                obj.YarnID = Common.GetNumber(ddlYarn3.SelectedValue.ToString());
                if (obj.YarnID > 0)
                {
                    obj.PerQty = Common.GetDecimal(txtUserPercent3.Text);
                    obj.Qty = (Common.GetDecimal(txtNet.Text) * obj.PerQty) / 100;
                    li.Add(obj);
                }
            }


            return li;
        }

        private List<EKnitingAvailableMCList> GetMCList()
        {
            EKnitingAvailableMCList obj;
            List<EKnitingAvailableMCList> li = new List<EKnitingAvailableMCList>();

            foreach (GridViewRow ob in gvProducts.Rows)
            {
                obj=new EKnitingAvailableMCList();
                if (ob.Cells[2].Text=="Running")
                {
                    obj.Machine=ob.Cells[1].Text;
                    obj.ID=Common.GetNumber( ob.Cells[0].Text);
                    obj.IsRunning=true;

                    li.Add(obj);

                }
               
            }
            return li;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
            btnCancel.Focus();
        }
        private void Clear()
        {
            txtRemaining0.Text = string.Empty;
            txtRemaining.Text = string.Empty;
            txtKG.Text = string.Empty;
            txtKG0.Text = string.Empty;
            txtKG1.Text = string.Empty;
            txtKG2.Text = string.Empty;
            txtKG3.Text = string.Empty;
            txtNotes.Text = string.Empty;
            txtUserPercent.Text = string.Empty;
            txtUserPercent0.Text = string.Empty;
            txtUserPercent1.Text = string.Empty;
            txtUserPercent2.Text = string.Empty;
            txtUserPercent3.Text = string.Empty;
            hdnValue.Value = "0";
            txtGM.Text = string.Empty;
            txtGSM.Text = string.Empty;
            txtNet.Text = string.Empty;
            txtExtra.Text = string.Empty;
            txtLocation.Text = string.Empty;
            txtNotes.Text=string.Empty;
            LoadMachine();
            txtTotQty.Text = string.Empty;
            ViewState["liOrder"] = null;
            ViewState["liMC"] = null;

            gvProducts.Enabled = true;
            gvProducts.DataSource = null;
            gvProducts.DataBind();

            gvProducts0.DataSource = null;
            gvProducts0.DataBind();
            upGrid.Update();
            upOrder.Update();
            upFabric.Update();
            
            lblStatus.ForeColor = System.Drawing.Color.Black;
        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {


        
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            //mpeProduct.Hide();
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            
        }
        
        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                // e.Row.Cells[1].Visible = false;
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            
            if (txtOrderNo.Text.Length > 5)
            {
                ddlOrderDesc.DataSource = new Order_DL().GetOrderProductDesc(txtOrderNo.Text);
                ddlOrderDesc.DataValueField = "OPID";
                ddlOrderDesc.DataTextField = "DESC";

                ddlOrderDesc.DataBind();
                ddlOrderDesc.Items.Add("--Select--");
                ddlOrderDesc.Text = "--Select--";
            }
        
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            try
            {
                List<EKnitSetupOrderList> li = new List<EKnitSetupOrderList>();
                if (ViewState["liOrder"] != null)
                {
                    li = (List<EKnitSetupOrderList>)ViewState["liOrder"];

                }
                if (li.Count < 1)
                {
                    EKnitSetupOrderList obj = new EKnitSetupOrderList();
                    obj.OrderNo = txtOrderNo.Text;
                    obj.Description = ddlOrderDesc.SelectedItem.Text;
                    obj.OPID = Common.GetNumber(ddlOrderDesc.SelectedValue.ToString());
                    obj.Qty = Convert.ToDouble(txtQtyUsed.Text);
                    li.Add(obj);
                    ViewState["liOrder"] = li;
                    gvProducts0.DataSource = li;
                    gvProducts0.DataBind();

                    Calculate();
                    txtBookingQty.Text = "";
                    txtQtyUsed.Text = "";
                }
            }
            catch
            {

            }
        }

        private void Calculate()
        {

            List<EKnitSetupOrderList> li = new List<EKnitSetupOrderList>();
            li = (List<EKnitSetupOrderList>)ViewState["liOrder"];
            double extra = Common.GetDouble(txtExtra.Text);

            double tot = li.Sum(o => o.Qty);
            txtTotQty.Text = tot.ToString();
            tot = tot + tot * extra / 100;

            txtNet.Text = tot.ToString("0");

             List<EKnitingAvailableMCList> liM = new List<EKnitingAvailableMCList>();
             liM=(List<EKnitingAvailableMCList>)ViewState["liMC"];
             double mcQty=liM.FindAll(o=> o.IsRunning==true).Sum(o => o.AssignQty);
             double avail = (Common.GetDouble(txtNet.Text) - mcQty);

             
             txtRemaining.Text = ( avail- Common.GetDouble( txtRemaining0.Text)) .ToString();

             upGrid.Update();
             upOrder.Update();

        }

        protected void ddlOrderDesc_SelectedIndexChanged(object sender, EventArgs e)
        {
            Int32 opID =Common.GetNumber( ddlOrderDesc.SelectedValue.ToString());
            if (opID > 0)
            {
                txtBookingQty.Text = new KNIT_all_operation().GetOrderProductBalance(opID).ToString();
            }
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            List<EKnitingAvailableMCList> li = new List<EKnitingAvailableMCList>();
            if (ViewState["liMC"] != null)
            {
                li = (List<EKnitingAvailableMCList>)ViewState["liMC"];

            }
            EKnitingAvailableMCList obj = new EKnitingAvailableMCList();
            obj.ID = 0;
            obj.Machine = ddlMCNo0.Text;
            obj.AssignQty = Common.GetDouble(txtAssignQty.Text);
            obj.IsRunning = true;

            li.Add(obj);
            ViewState["liMC"] = li;
         
            double avail = (Common.GetDouble(txtNet.Text) - li.Sum(o => o.AssignQty));
            txtRemaining.Text = avail.ToString();
            upGrid.Update();
        }

        protected void btnOperation0_Click(object sender, EventArgs e)
        {
            Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;

            List<EKnitSetupOrderList> li = new List<EKnitSetupOrderList>();
            if (ViewState["liOrder"] != null)
            {
                li = (List<EKnitSetupOrderList>)ViewState["liOrder"];

                li.RemoveAll(o => o.OPID.ToString() == row.Cells[0].Text.Trim() && o.OrderNo==row.Cells[1].Text.Trim());
                ViewState["liOrder"] = li;
              
                gvProducts0.DataSource = li;
                gvProducts0.DataBind();
                Calculate();
            }

          
        }

        protected void gvProducts0_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }

            if (e.Row.RowType != DataControlRowType.Pager)
            {
                 e.Row.Cells[0].Visible = false;
                //e.Row.Cells[1].Visible = false;
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            txtRemaining0.Text = "0";
            txtRemaining.Text = "0";
            List<EKnitingProductionSetupView> li = new KNIT_all_operation().GetKNITProductionGet(ddlReq.SelectedValue);
            ddlFabricType.SelectedValue = li[0].GID.ToString();
            LoadFabric();
            ddlFabric.SelectedValue = li[0].FabricID.ToString();
            txtGM.Text = li[0].GM.ToString();
            txtGSM.Text = li[0].GSM.ToString();
            txtNet.Text = li[0].ProductionQty.ToString();
            txtDate.Text = li[0].Date;
            txtOrderNo.Text = li[0].OrderNo;
            txtLocation.Text = li[0].Location;
            txtNotes.Text = li[0].Notes;
            txtExtra.Text = li[0].LossPer.ToString();
            hdnValue.Value = li[0].ID.ToString();
            LoadMCGrid(Convert.ToInt32( li[0].ID));
            chkSubContact.Checked = li[0].IsSubContact;
            LoadMachine();
            txtRemaining0.Text = new KNIT_all_operation().GetKnitFInishQty(Convert.ToInt32(li[0].ID));
            List<EKnitSetupOrderList> lio = new KNIT_all_operation().GetKnitSetupOrder( Convert.ToInt32( li[0].ID));
            ViewState["liOrder"] = lio;

            gvProducts0.DataSource = lio;
            gvProducts0.DataBind();

            //gvProducts.Enabled = false;
            List<EYarnList> liYarn = new KNIT_all_operation().GetYarnListOfSetup(li[0].ID);
            int tot = liYarn.Count;
            if (tot > 0)
            {
                ddlYarnType.SelectedValue = liYarn[0].YarnGroupID.ToString();
                LoadYarn(liYarn[0].YarnGroupID, ddlYarn);
                txtKG.Text = liYarn[0].Qty.ToString();
                txtUserPercent.Text = liYarn[0].QtyPer.ToString();
                ddlYarn.SelectedValue = liYarn[0].YarnID.ToString();
                if (tot > 1)
                {
                    ddlYarnType0.SelectedValue = liYarn[1].YarnGroupID.ToString();
                    LoadYarn(liYarn[1].YarnGroupID, ddlYarn0);
                    txtKG0.Text = liYarn[1].Qty.ToString();
                    ddlYarn0.SelectedValue = liYarn[1].YarnID.ToString();
                    txtUserPercent0.Text = liYarn[1].QtyPer.ToString();
                }
                if (tot > 2)
                {
                    ddlYarnType1.SelectedValue = liYarn[2].YarnGroupID.ToString();
                    LoadYarn(liYarn[2].YarnGroupID, ddlYarn1);
                    txtKG1.Text = liYarn[2].Qty.ToString();
                    ddlYarn1.SelectedValue = liYarn[2].YarnID.ToString();
                    txtUserPercent1.Text = liYarn[2].QtyPer.ToString();
                }
                if (tot > 3)
                {
                    ddlYarnType2.SelectedValue = liYarn[3].YarnGroupID.ToString();
                    LoadYarn(liYarn[3].YarnGroupID, ddlYarn2);
                    txtKG2.Text = liYarn[3].Qty.ToString();
                    ddlYarn2.SelectedValue = liYarn[3].YarnID.ToString();
                    txtUserPercent2.Text = liYarn[3].QtyPer.ToString();
                }
                if (tot > 4)
                {
                    ddlYarnType3.SelectedValue = liYarn[4].YarnGroupID.ToString();
                    LoadYarn(liYarn[4].YarnGroupID, ddlYarn3);
                    txtKG3.Text = liYarn[4].Qty.ToString();
                    ddlYarn3.SelectedValue = liYarn[4].YarnID.ToString();
                    txtUserPercent3.Text = liYarn[4].QtyPer.ToString();
                }


            }
            Calculate();
            //mpeProduct.Hide();
        }

        protected void Button4_Click1(object sender, EventArgs e)
        {
            double extra = Common.GetDouble(txtExtra.Text);

            double tot = Common.GetDouble(txtTotQty.Text);
            tot = tot + tot * extra / 100;

            txtNet.Text = tot.ToString("0");
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            string dt = "1-" + txtReqNo.Text;
            List<EKNIT_Setup> li = new KNIT_all_operation().GetSetupList(dt);
            ddlReq.DataSource = li;
            ddlReq.DataValueField = "SetupCode";
            ddlReq.DataTextField = "SetupCode";
            ddlReq.DataBind();
        }
    }
   
}
