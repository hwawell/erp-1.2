﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="YarnIssueReport.aspx.cs" Inherits="ERP.YarnIssueReport" Title="Yarn Issue Report" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();
            List<EYarnIssue> liProd = new List<EYarnIssue>();
            this.Window1.Title = "Yarn Issue Report";

            liProd = new Yarn_DL().GetYarnIssueReport(Convert.ToDateTime(txtFrom1.Text), Convert.ToDateTime(txtFrom0.Text), "ALL");
                    
            
            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
    
   
   
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> Yarn Issue Report</h2></div>
 <div align="left" style="padding: 10px; ">
                      From
                   <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                    </div>
                       
                    <asp:TextBox ID="txtFrom1" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom1_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom1">
                    </cc1:CalendarExtender>
                       
                      Date
                    <asp:TextBox ID="txtFrom0" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom0_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom0">
                    </cc1:CalendarExtender>
                       
                </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
           
            >
            <Reader>
            <ext:JsonReader IDProperty="ID">
                    <Fields>
                      <ext:RecordField Name="ID" />
                     
                          <ext:RecordField Name="ID" />
                          <ext:RecordField Name="DDate" />
                          <ext:RecordField Name="YarnCompany" />
                          <ext:RecordField Name="YarnCountry" />
                          <ext:RecordField Name="LCNumber" />
                          <ext:RecordField Name="InvoiceNo" />
                          <ext:RecordField Name="YarnGroup" />
                          <ext:RecordField Name="YarnName" />
                          <ext:RecordField Name="Lot" />
                          <ext:RecordField Name="Unit" />
                          <ext:RecordField Name="UnitQty" />
                          <ext:RecordField Name="TotalQty" />
                           <ext:RecordField Name="IssueTo" />
                           <ext:RecordField Name="MC_List" />
                          
                          <ext:RecordField Name="IssueType" />
                         
                          <ext:RecordField Name="Note" />
                          
                          
                           
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Yarn Issue Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            
                            <ext:Column ColumnID="ID" Header="SL"  DataIndex="ID" Sortable="true" />
                             <ext:Column ColumnID="DDate" Header="Date"  DataIndex="DDate" Sortable="true" />
                            <ext:Column ColumnID="YarnCompany" Header="Yarn Company"  DataIndex="YarnCompany" Sortable="true" />
                            <ext:Column ColumnID="YarnCountry" Header="Yarn Country"  DataIndex="YarnCountry" Sortable="true" />
                             <ext:Column ColumnID="YarnGroup" Header="Yarn Group"  DataIndex="YarnGroup" Sortable="true" />
                            <ext:Column ColumnID="YarnName" Header="Yarn Name"  DataIndex="YarnName" Sortable="true" />
                            <ext:Column ColumnID="Lot" Header="Lot"  DataIndex="Lot" Sortable="true" />
                              <ext:Column ColumnID="MC_List" Header="MC_List"  DataIndex="MC_List" Sortable="true" />
                               
                           
                           
                            
                            <ext:Column ColumnID="Unit" Header="Unit"  DataIndex="Unit" Sortable="true" />
                            <ext:Column ColumnID="UnitQty" Header="Unit Qty"  DataIndex="UnitQty" Sortable="true" />
                            <ext:Column ColumnID="TotalQty" Header="TotalQty"  DataIndex="TotalQty" Sortable="true" />
                              <ext:Column ColumnID="IssueTo" Header="IssueTo"  DataIndex="IssueTo" Sortable="true" />
                           
                             <ext:Column ColumnID="InvoiceNo" Header="Invoice No"  DataIndex="InvoiceNo" Sortable="true" />
                           
                            <ext:Column ColumnID="IssueType" Header="Issue Type"  DataIndex="IssueType" Sortable="true" />
                            <ext:Column ColumnID="Note" Header="Note"  DataIndex="Note" Sortable="true" />
                            
                          
                       </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="DDate" />
                                <ext:StringFilter DataIndex="YarnCompany" />
                                <ext:StringFilter DataIndex="YarnCountry" />
                                <ext:StringFilter DataIndex="LCNumber" />
                                <ext:StringFilter DataIndex="InvoiceNo" />
                                 <ext:NumericFilter DataIndex="YarnGroup" />
                                <ext:StringFilter DataIndex="YarnName" />
                                 <ext:StringFilter DataIndex="Lot" />
                                   <ext:StringFilter DataIndex="MC_List" />
                                 
                                <ext:StringFilter DataIndex="Unit" />
                                <ext:StringFilter DataIndex="UnitQty" />
                                <ext:StringFilter DataIndex="TotalQty" />
                                <ext:StringFilter DataIndex="IssueType" />
                                <ext:StringFilter DataIndex="IssueTo" />
                                 <ext:NumericFilter DataIndex="Note" />
                              
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>
