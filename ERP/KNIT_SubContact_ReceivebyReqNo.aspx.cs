﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
namespace ERP
{
    public partial class KNIT_SubContact_ReceivebyReqNo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

               
                LoadYarnGroup();
                LoadMachine();
            }
        }

       
        private void LoadMachine()
        {



        }
        private void LoadYarn(Int64 ygID, DropDownList dt)
        {
            dt.DataSource = new Yarn_DL().GetYarnByYarnGroup(ygID);
            dt.DataValueField = "YID";
            dt.DataTextField = "YarnName";
            dt.DataBind();
            dt.Items.Add("--Select--");
            dt.Text = "--Select--";


        }
        private void LoadYarnGroup()
        {



        




        }
        private void LoadAllYarnGroup(DropDownList dtSource, DropDownList Assign)
        {
            Assign.DataSource = dtSource.DataSource;
            Assign.DataValueField = "ID";
            Assign.DataTextField = "YarnGroupName";
            Assign.DataBind();
            Assign.Items.Add("--Select--");
            Assign.Text = "--Select--";
        }
      

       




        protected void ddlFabricType_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

       

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;


                // e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = (Button)e.Row.Cells[3].Controls[1];
                if (e.Row.Cells[2].Text == "Running")
                {
                    btn.Text = "Stop";
                    btn.BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    btn.Text = "Start";
                    btn.BackColor = System.Drawing.Color.Green;
                }

            }

        }



        protected void btnOperation_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
           

        }

        protected void gvProducts_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = (Button)e.Row.Cells[3].Controls[1];
                if (e.Row.Cells[2].Text == "Running")
                {
                    btn.Text = "Stop";
                    btn.BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    btn.Text = "Start";
                    btn.BackColor = System.Drawing.Color.Green;
                }

            }

        }

        protected void btnOperation_Click(object sender, EventArgs e)
        {
            Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            if (btnEdit.Text == "Start")
            {
                row.ForeColor = System.Drawing.Color.Green;
                row.Font.Bold = true;
                row.Cells[2].Text = "Running";
                btnEdit.Text = "Stop";
                btnEdit.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                row.Font.Bold = false;
                row.Cells[2].Text = "Available";
                row.ForeColor = System.Drawing.Color.Black;
                btnEdit.Text = "Start";
                btnEdit.BackColor = System.Drawing.Color.Green;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            List<EKnitingAvailableMCList> mylist = (List<EKnitingAvailableMCList>)ViewState["li"];
        }

        protected void dgvYarn_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSubContact.Text.Length > 3)
                {
                    KNIT_GREY_ROLL_RCV_From_SubCon_GETResult obj = new KNIT_GREY_ROLL_RCV_From_SubCon_GETResult();
                    obj.AssignMachineID = Common.GetNumber(hdnFID.Value);
                    obj.Challan = txtChallan.Text;
                    obj.Notes = txtNotes.Text;
                    obj.RollNo = GridView1.Rows.Count + 1;
                    obj.RollQty = Common.GetDecimal(txtTot.Text);
                    obj.SubContactName = txtSubContact.Text;
                    obj.PDate = Common.GetDate(txtDate.Text);
                  
                   
                  
                        
                     bool res=new KNIT_all_operation().KNIT_PROD_ROLL_SubContact_INS(obj);
                     if (res == true)
                     {
                         lblStatus.Text = "Data Saved Successfully";
                     }
                     else
                     {
                         lblStatus.Text = "Save Failed";
                     }
                        
                }
                else
                {
                    lblStatus.Text = "Sub Contact Name is not given";
                    txtSubContact.Focus();
                }
            }

            catch (Exception ex)
            {
                lblStatus.ForeColor = System.Drawing.Color.Red;
                lblStatus.Text = "Error :" + ex.Message.ToString();
                btnSave.Focus();
            }

        }
       
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
            btnCancel.Focus();
        }
        private void Clear()
        {
            
            txtGM.Text = string.Empty;
            txtGSM.Text = string.Empty;
            txtSubContact.Text = string.Empty;
            txtTot.Text = string.Empty;

           
            upFabric.Update();

            lblStatus.ForeColor = System.Drawing.Color.Black;
        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {



        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
          
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                // e.Row.Cells[1].Visible = false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            List<EKnitingProductionSetupView> li = new KNIT_all_operation().GetKNITProductionGet(txtReqNo.Text);
            if (li != null && li.Count > 0  && li[0].IsSubContact==true)
            {
                hdnFID.Value = li[0].AssignMCID.ToString();
                txtFabric.Text = li[0].Fabric;
                txtGM.Text = li[0].GM;
                txtGSM.Text = li[0].GSM;

                GridView1.DataSource = new KNIT_all_operation().KNIT_PROD_ROLL_SubContact_GET(Convert.ToInt32( li[0].AssignMCID));
                GridView1.DataBind();
            }
            else
            {
                hdnFID.Value = "0";
                txtFabric.Text = "";
                txtGM.Text = "";
                txtGSM.Text = "";
                txtSubContact.Text = "";

                GridView1.DataSource = null;
                GridView1.DataBind();
            }

        }

        protected void btnClose_Click1(object sender, EventArgs e)
        {
            Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            Int32 ID = Common.GetNumber(row.Cells[0].Text);

            bool res = new KNIT_all_operation().KNIT_PROD_ROLL_SubContact_Delete(ID);

            int FID = Common.GetNumber(hdnFID.Value.ToString());
            GridView1.DataSource = new KNIT_all_operation().KNIT_PROD_ROLL_SubContact_GET(FID);
            GridView1.DataBind();
        }
    }
}
