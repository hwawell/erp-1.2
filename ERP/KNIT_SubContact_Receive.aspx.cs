﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
namespace ERP
{
    public partial class KNIT_SubContact_Receive : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                LoadFabricType();
                LoadYarnGroup();
                LoadMachine();
            }
        }

        private void LoadFabricType()
        {


            ddlFabricType.DataSource = new Fabric_CL().GetAllFabricGroup();
            ddlFabricType.DataValueField = "SL";
            ddlFabricType.DataTextField = "FGroupName";
            ddlFabricType.DataBind();

        }
        private void LoadMachine()
        {


            ddlMCNo.DataSource = new KNIT_all_operation().GetAlMCNo();
            ddlMCNo.DataValueField = "MCNo";
            ddlMCNo.DataTextField = "MCNo";
            ddlMCNo.DataBind();

        }
        private void LoadYarn(Int64 ygID, DropDownList dt)
        {
            dt.DataSource = new Yarn_DL().GetYarnByYarnGroup(ygID);
            dt.DataValueField = "YID";
            dt.DataTextField = "YarnName";
            dt.DataBind();
            dt.Items.Add("--Select--");
            dt.Text = "--Select--";


        }
        private void LoadYarnGroup()
        {



            ddlYarnType.DataSource = new Yarn_DL().GetActiveYarnGroup();
            ddlYarnType.DataValueField = "ID";
            ddlYarnType.DataTextField = "YarnGroupName";
            ddlYarnType.DataBind();
            ddlYarnType.Items.Add("--Select--");
            ddlYarnType.Text = "--Select--";

            LoadAllYarnGroup(ddlYarnType, ddlYarnType0);
            LoadAllYarnGroup(ddlYarnType, ddlYarnType1);
            LoadAllYarnGroup(ddlYarnType, ddlYarnType2);
            LoadAllYarnGroup(ddlYarnType, ddlYarnType3);




        }
        private void LoadAllYarnGroup(DropDownList dtSource, DropDownList Assign)
        {
            Assign.DataSource = dtSource.DataSource;
            Assign.DataValueField = "ID";
            Assign.DataTextField = "YarnGroupName";
            Assign.DataBind();
            Assign.Items.Add("--Select--");
            Assign.Text = "--Select--";
        }
        private void LoadFabric()
        {
            ddlFabric.DataSource = new Fabric_CL().GetAlFabricbyGRoup(Convert.ToInt32(ddlFabricType.SelectedValue));
            ddlFabric.DataValueField = "Fid";
            ddlFabric.DataTextField = "fab";
            ddlFabric.DataBind();
        }

       




        protected void ddlFabricType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadFabric();
        }

        protected void ddlYarnType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dt = (DropDownList)sender;
            Int64 GID = Convert.ToInt64(dt.SelectedValue);
            LoadYarn(GID, ddlYarn);
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;


                // e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = (Button)e.Row.Cells[3].Controls[1];
                if (e.Row.Cells[2].Text == "Running")
                {
                    btn.Text = "Stop";
                    btn.BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    btn.Text = "Start";
                    btn.BackColor = System.Drawing.Color.Green;
                }

            }

        }



        protected void btnOperation_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
           

        }

        protected void gvProducts_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btn = (Button)e.Row.Cells[3].Controls[1];
                if (e.Row.Cells[2].Text == "Running")
                {
                    btn.Text = "Stop";
                    btn.BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    btn.Text = "Start";
                    btn.BackColor = System.Drawing.Color.Green;
                }

            }

        }

        protected void btnOperation_Click(object sender, EventArgs e)
        {
            Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            if (btnEdit.Text == "Start")
            {
                row.ForeColor = System.Drawing.Color.Green;
                row.Font.Bold = true;
                row.Cells[2].Text = "Running";
                btnEdit.Text = "Stop";
                btnEdit.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                row.Font.Bold = false;
                row.Cells[2].Text = "Available";
                row.ForeColor = System.Drawing.Color.Black;
                btnEdit.Text = "Start";
                btnEdit.BackColor = System.Drawing.Color.Green;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            List<EKnitingAvailableMCList> mylist = (List<EKnitingAvailableMCList>)ViewState["li"];
        }

        protected void dgvYarn_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlYarnType0_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dt = (DropDownList)sender;
            Int64 GID = Convert.ToInt64(dt.SelectedValue);
            LoadYarn(GID, ddlYarn0);
        }

        protected void ddlYarnType1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dt = (DropDownList)sender;
            Int64 GID = Convert.ToInt64(dt.SelectedValue);
            LoadYarn(GID, ddlYarn1);
        }

        protected void ddlYarnType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dt = (DropDownList)sender;
            Int64 GID = Convert.ToInt64(dt.SelectedValue);
            LoadYarn(GID, ddlYarn2);
        }

        protected void ddlYarnType3_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList dt = (DropDownList)sender;
            Int64 GID = Convert.ToInt64(dt.SelectedValue);
            LoadYarn(GID, ddlYarn3);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSubContact.Text.Length > 3)
                {
                    EKnitingProductionSetup obj = new EKnitingProductionSetup();


                    obj.ID = Common.GetNumber(hdnValue.Value);
                    obj.ProductionQty = Convert.ToDouble(txtTot.Text);
                    obj.FabricID = Convert.ToInt32(ddlFabric.SelectedValue.ToString());
                    obj.GM = txtGM.Text;
                    obj.SubContact = txtSubContact.Text;
                    obj.ChallanNo = txtChallan.Text;
                    obj.MCNO = ddlMCNo.Text;
                    obj.GSM = txtGSM.Text;
                    obj.SetupDate = Common.GetDate(txtDate.Text);
                    obj.IsActive = true;
                    obj.Notes = txtNotes.Text;
                    List<EYarnConsumptionList> li = GetYarnList();
                    //if (li.Sum(o => o.PerQty) <= 100)
                    //{
                    if (obj.FabricID > 0)
                    {

                        lblStatus.Text = new KNIT_all_operation().SaveKnitSetup(obj, null, li,null);

                        if (lblStatus.Text.Contains("Successfully") == true)
                        {

                            Clear();
                            lblStatus.ForeColor = System.Drawing.Color.Green;

                        }
                    }
                    btnSave.Focus();
                }
                else
                {
                    lblStatus.Text = "Sub Contact Name is not given";
                    txtSubContact.Focus();
                }
            }

            catch (Exception ex)
            {
                lblStatus.ForeColor = System.Drawing.Color.Red;
                lblStatus.Text = "Error :" + ex.Message.ToString();
                btnSave.Focus();
            }
            //}
            //else
            //{
            //    lblStatus.Text = "Yarn consumption Qty is greater than Production Qty";
            //}
        }
        private List<EYarnConsumptionList> GetYarnList()
        {
            EYarnConsumptionList obj;
            List<EYarnConsumptionList> li = new List<EYarnConsumptionList>();
            if (Common.GetDecimal(txtUserPercent.Text) > 0)
            {
                obj = new EYarnConsumptionList();
                obj.YarnID = Common.GetNumber(ddlYarn.SelectedValue.ToString());
                if (obj.YarnID > 0)
                {
                    obj.PerQty = Common.GetDecimal(txtUserPercent.Text);
                    obj.Qty = (Common.GetDecimal(txtTot.Text) * obj.PerQty) / 100;
                    li.Add(obj);
                }
            }


            if (Common.GetDecimal(txtUserPercent0.Text) > 0)
            {
                obj = new EYarnConsumptionList();
                obj.YarnID = Common.GetNumber(ddlYarn0.SelectedValue.ToString());
                if (obj.YarnID > 0)
                {
                    obj.PerQty = Common.GetDecimal(txtUserPercent0.Text);
                    obj.Qty = (Common.GetDecimal(txtTot.Text) * obj.PerQty) / 100;
                    li.Add(obj);
                }
            }


            if (Common.GetDecimal(txtUserPercent1.Text) > 0)
            {
                obj = new EYarnConsumptionList();
                obj.YarnID = Common.GetNumber(ddlYarn1.SelectedValue.ToString());
                if (obj.YarnID > 0)
                {
                    obj.PerQty = Common.GetDecimal(txtUserPercent1.Text);
                    obj.Qty = (Common.GetDecimal(txtTot.Text) * obj.PerQty) / 100;
                    li.Add(obj);
                }
            }


            if (Common.GetDecimal(txtUserPercent2.Text) > 0)
            {
                obj = new EYarnConsumptionList();
                obj.YarnID = Common.GetNumber(ddlYarn2.SelectedValue.ToString());
                if (obj.YarnID > 0)
                {
                    obj.PerQty = Common.GetDecimal(txtUserPercent2.Text);
                    obj.Qty = (Common.GetDecimal(txtTot.Text) * obj.PerQty) / 100;
                    li.Add(obj);
                }
            }


            if (Common.GetDecimal(txtUserPercent3.Text) > 0)
            {
                obj = new EYarnConsumptionList();
                obj.YarnID = Common.GetNumber(ddlYarn3.SelectedValue.ToString());
                if (obj.YarnID > 0)
                {
                    obj.PerQty = Common.GetDecimal(txtUserPercent3.Text);
                    obj.Qty = (Common.GetDecimal(txtTot.Text) * obj.PerQty) / 100;
                    li.Add(obj);
                }
            }


            return li;
        }

     

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
            btnCancel.Focus();
        }
        private void Clear()
        {
            txtKG.Text = string.Empty;
            txtKG0.Text = string.Empty;
            txtKG1.Text = string.Empty;
            txtKG2.Text = string.Empty;
            txtKG3.Text = string.Empty;
            txtNotes.Text = string.Empty;
            txtUserPercent.Text = string.Empty;
            txtUserPercent0.Text = string.Empty;
            txtUserPercent1.Text = string.Empty;
            txtUserPercent2.Text = string.Empty;
            txtUserPercent3.Text = string.Empty;
            hdnValue.Value = "0";
            txtGM.Text = string.Empty;
            txtGSM.Text = string.Empty;
            txtSubContact.Text = string.Empty;
            txtTot.Text = string.Empty;

           
            upFabric.Update();

            lblStatus.ForeColor = System.Drawing.Color.Black;
        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {


            gvView.DataSource = new KNIT_all_operation().GetKNITProductionGet(0,"").FindAll(o=> o.SubContact!=null && o.SubContact.Length>2);
            gvView.DataBind();


            mpeProduct.Show();

        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            mpeProduct.Hide();
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            Int64 ID = Common.GetNumber(row.Cells[0].Text);
            List<EKnitingProductionSetupView> li = new KNIT_all_operation().GetKNITProductionGet(ID,"");
            ddlFabricType.SelectedValue = li[0].GID.ToString();
            LoadFabric();
            ddlFabric.SelectedValue = li[0].FabricID.ToString();
            txtGM.Text = li[0].GM.ToString();
            txtGSM.Text = li[0].GSM.ToString();
            ddlMCNo.Text = li[0].MCNo;
            txtChallan.Text = li[0].ChallanNo;
            txtSubContact.Text = li[0].SubContact.ToString();
            txtTot.Text = li[0].ProductionQty.ToString();
            txtDate.Text = li[0].Date;
            txtNotes.Text = li[0].Notes;
            hdnValue.Value = ID.ToString();
           
            //gvProducts.Enabled = false;
            List<EYarnList> liYarn = new KNIT_all_operation().GetYarnListOfSetup(ID);
            int tot = liYarn.Count;
            if (tot > 0)
            {
                ddlYarnType.SelectedValue = liYarn[0].YarnGroupID.ToString();
                LoadYarn(liYarn[0].YarnGroupID, ddlYarn);
                txtKG.Text = liYarn[0].Qty.ToString();
                txtUserPercent.Text = liYarn[0].QtyPer.ToString();
                ddlYarn.SelectedValue = liYarn[0].YarnID.ToString();
                if (tot > 1)
                {
                    ddlYarnType0.SelectedValue = liYarn[1].YarnGroupID.ToString();
                    LoadYarn(liYarn[1].YarnGroupID, ddlYarn0);
                    txtKG0.Text = liYarn[1].Qty.ToString();
                    ddlYarn0.SelectedValue = liYarn[1].YarnID.ToString();
                    txtUserPercent0.Text = liYarn[1].QtyPer.ToString();
                }
                if (tot > 2)
                {
                    ddlYarnType1.SelectedValue = liYarn[2].YarnGroupID.ToString();
                    LoadYarn(liYarn[2].YarnGroupID, ddlYarn1);
                    txtKG1.Text = liYarn[2].Qty.ToString();
                    ddlYarn1.SelectedValue = liYarn[2].YarnID.ToString();
                    txtUserPercent1.Text = liYarn[2].QtyPer.ToString();
                }
                if (tot > 3)
                {
                    ddlYarnType2.SelectedValue = liYarn[3].YarnGroupID.ToString();
                    LoadYarn(liYarn[3].YarnGroupID, ddlYarn2);
                    txtKG2.Text = liYarn[3].Qty.ToString();
                    ddlYarn2.SelectedValue = liYarn[3].YarnID.ToString();
                    txtUserPercent2.Text = liYarn[3].QtyPer.ToString();
                }
                if (tot > 4)
                {
                    ddlYarnType3.SelectedValue = liYarn[4].YarnGroupID.ToString();
                    LoadYarn(liYarn[4].YarnGroupID, ddlYarn3);
                    txtKG3.Text = liYarn[4].Qty.ToString();
                    ddlYarn3.SelectedValue = liYarn[4].YarnID.ToString();
                    txtUserPercent3.Text = liYarn[4].QtyPer.ToString();
                }


            }
            mpeProduct.Hide();
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                // e.Row.Cells[1].Visible = false;
            }
        }
    }
}
