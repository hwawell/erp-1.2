<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Knit_Plan.aspx.cs" Inherits="ERP.Knit_Plan" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="AjaxControls" namespace="AjaxControls" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <style type="text/css">
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
                       
        </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
     <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
	<script type="text/javascript">
	    $(function() {
	        $(".tb").autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                url: "DataLoad.asmx/GetOrderList",
	                    data: "{ 'PINO': '" + request.term + "' }",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    dataFilter: function(data) { return data; },
	                    success: function(data) {
	                        response($.map(data.d, function(item) {
	                            return {
	                            value: item.OrderNo
	                            }
	                        }))
	                    },
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                        
	                    }
	                });
	            },
	            minLength: 2
	        });
	    });

	    $(document).ready(function() {
	       
	       
	    });
	    


	
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
    <div>
        Dyeing
        Production Plan
    </div>
     <div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <div class="grid" >
                    <div>
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                        AssociatedUpdatePanelID="UpEntry">
                        <ProgressTemplate>
                            <img alt="" src="Images/loading.gif" style="height: 15px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    </div>
                    <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> </center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid" 
                        style="overflow:auto;  height: auto; background-color:"
                        whitesmoke";">     
                 <div align="center">
                   
                            
                           <asp:UpdatePanel ID="UpEntry" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" >
                        <ContentTemplate>
                          <div>
                            <div >
                                 Plan Date From :   <asp:TextBox ID="TextBox5" runat="server" TabIndex="16"></asp:TextBox>
                                      <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                          Enabled="True" Format="MM/dd/yyyy" TargetControlID="txtDate">
                                      </cc1:CalendarExtender>
                                      
                                      To  Date :   <asp:TextBox ID="TextBox6" runat="server" TabIndex="16"></asp:TextBox>
                                      <asp:Button ID="btnSearch" runat="server" onclick="btnSearch_Click" 
                                     TabIndex="4" Text="Search" />
                                      <cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                                          Enabled="True" Format="MM/dd/yyyy" TargetControlID="txtDate">
                                      </cc1:CalendarExtender>
                                  
                            </div>
                            <hr />
                            <div align="left" > Order No:
                                <asp:DropDownList ID="ddlMC1" runat="server" AutoPostBack="True" 
                                    Height="28px" 
                                    TabIndex="7" Width="126px">
                                </asp:DropDownList>
                              
                                Product :<asp:DropDownList ID="ddlDesc" runat="server" AutoPostBack="True" 
                                    Height="46px" onselectedindexchanged="ddlDesc_SelectedIndexChanged" 
                                    TabIndex="5" Width="299px">
                                </asp:DropDownList>
                              
                            </div>
                            <div style="float:left;">
                                  Color :<asp:DropDownList ID="ddlColor" runat="server" AutoPostBack="True" 
                                    Height="22px" 
                                    TabIndex="6" Width="190px">
                                </asp:DropDownList>
                                Booking Qty: <asp:TextBox ID="TextBox1" Width="100px" runat="server"></asp:TextBox>
                                Dying MC: 
                                  <asp:DropDownList ID="ddlMC" runat="server" AutoPostBack="True" 
                                          Height="27px" 
                                          TabIndex="7" Width="108px">
                                      </asp:DropDownList>
                            </div>
                            <div style="overflow:auto;height:200px">
                                   <asp:GridView 
                                ID="GridView1" runat="server" 
                                 AutoGenerateColumns="False" 
                                AllowPaging="True" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                >
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="�" LastPageText="�" />      
                                <Columns>
                                     
                                    <asp:BoundField 
                                        HeaderText="Fabric" DataField="sl" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                   
                                    <asp:BoundField 
                                        HeaderText="GM" DataField="PALL" 
                                        SortExpression="PALL" 
                                    >
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="GSM" DataField="Pqty" 
                                        SortExpression="Pqty" >
                                    
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                                             
                                    
                                    
                                   
                                  
                                   
                                    
                                    
                                     <asp:BoundField 
                                        HeaderText="Balance" DataField="Balance" 
                                        SortExpression="CCardNo">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField 
                                        HeaderText="Planned" DataField="Balance" 
                                        SortExpression="CCardNo">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="AvailableForPlan" DataField="Balance" 
                                        SortExpression="CCardNo">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Select Plan Qty">
                                        <ItemTemplate>
                                             <asp:CheckBox ID="chkSkip"  runat="server" />
                                               <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                   
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                            </div>
                            <div style="float:left;">
                               Plan Date :   <asp:TextBox ID="txtDate" runat="server" TabIndex="16"></asp:TextBox>
                                      <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                                          Enabled="True" Format="MM/dd/yyyy" TargetControlID="txtDate">
                                      </cc1:CalendarExtender>
                                      Priority : 
                                <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" 
                                          Height="28px" 
                                          TabIndex="7" Width="109px">  </asp:DropDownList>
                                          
                                          Total Plan Qty
                                <asp:TextBox ID="TextBox4" runat="server" Width="100px"></asp:TextBox>
                                          
                                          <asp:Button runat="server" Text="Add Schedule" />
                            </div>
                            <div>  </div>
                            <div>
                               <h2>List of added Plan</h2>
                                    <asp:GridView 
                                ID="gvProducts" runat="server" 
                                 AutoGenerateColumns="False" 
                                AllowPaging="True" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                onpageindexchanging="gvProducts_PageIndexChanging" PageSize="8">
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="�" LastPageText="�" />      
                                <Columns>
                                    <asp:BoundField 
                                        HeaderText="sl" DataField="sl" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    <asp:BoundField 
                                        HeaderText="Order No" DataField="PALL" 
                                        SortExpression="PCardNo" 
                                    >
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField 
                                        HeaderText="Product" DataField="Pqty" 
                                        SortExpression="Pqty">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField 
                                        HeaderText="Plan Qty" DataField="MCNo" 
                                        SortExpression="MCNo">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="MC No" DataField="NOtes" 
                                        SortExpression="NOtes">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                     <asp:BoundField 
                                        HeaderText="Priority" DataField="NOtes" 
                                        SortExpression="NOtes">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                   
                                    
                                  
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/btn_edit.gif" 
                                                onclick="btnEdit_Click" Height="16px" 
                                                />
                                            
                                           
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" 
                                                ImageUrl="~/Images/btn_delete.gif" onclick="btnDelete_Click" 
                                                Height="16px"
                                            onclientclick="javascript:return confirm('Are you sure you want to delete this row?');" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>
                            </div>
                          </div>
                        
                        
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        
          
                 </div>
                 <div>
                  
                 </div>
                 
              </div></div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                
          </div>
</asp:Content>
