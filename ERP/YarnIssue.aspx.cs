﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;

namespace ERP
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                //if (new Utilities_DL().CheckSecurity("503", USession.SUserID) == true)
                //{


                    loadData();
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
        private void loadData()
        {
            ddlCountry.DataSource = new Yarn_DL().GetYarnCountry();
            ddlCountry.DataValueField = "CountryOrSupplier";
            ddlCountry.DataTextField = "CountryOrSupplier";
            ddlCountry.DataBind();

            ddlSupplier.DataSource = new Yarn_DL().GetYarnSupplier();
            ddlSupplier.DataValueField = "CountryOrSupplier";
            ddlSupplier.DataTextField = "CountryOrSupplier";
            ddlSupplier.DataBind();


            ddlUnit.DataSource = new Yarn_DL().GetAllUnits();
            ddlUnit.DataValueField = "UNIT1";
            ddlUnit.DataTextField = "UNIT1";
            ddlUnit.DataBind();
            ddlUnit.Items.Add("--Select--");
            ddlUnit.Text = "--Select--";


            LoadYarnGroup();


        }
        private void LoadYarn(Int64 ygID)
        {
            ddlYarn.DataSource = new Yarn_DL().GetYarnByYarnGroup(ygID);
            ddlYarn.DataValueField = "YID";
            ddlYarn.DataTextField = "YarnName";
            ddlYarn.DataBind();
            ddlYarn.Items.Add("--Select--");
            ddlYarn.Text = "--Select--";
        }
        private void LoadYarnGroup()
        {


            ddlYarnGroup.DataSource = new Yarn_DL().GetActiveYarnGroup();
            ddlYarnGroup.DataValueField = "ID";
            ddlYarnGroup.DataTextField = "YarnGroupName";
            ddlYarnGroup.DataBind();
            ddlYarnGroup.Items.Add("--Select--");
            ddlYarnGroup.Text = "--Select--";


        }
        private void loadGridData()
        {




            gvProducts.DataSource = new Yarn_DL().GetActiveIssueRecord(Common.GetNumber(txtFrom.Text), Common.GetNumber(txtTo.Text));
            gvProducts.DataBind();

        }
        private void loadDeletedData()
        {

            gvProducts.Columns[9].Visible = false;
            gvProducts.Columns[10].Visible = false;
            gvProducts.Columns[11].Visible = true;
            gvProducts.DataSource = new Yarn_DL().GetDeletedIssuedRecord();
            gvProducts.DataBind();

        }

       
        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Yarn_DL().IssuedDeleteActive(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

      

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        
        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (CheckEntry() == true)
            {
                try
                {
                    YarnIssue c = new YarnIssue();
                    c.SL = 0;
                    c.YID =Convert.ToInt64(ddlYarn.SelectedValue);
                    c.Qty = Common.GetDecimal(txtTotalQTY.Text);
                    c.Unit = ddlUnit.Text;
                    c.Idate = Convert.ToDateTime(txtDate.Text);
                    c.IssueDesc = txtNotes.Text;
                    c.IssueType = ddlIssueTo.Text;
                    c.Supplier = ddlSupplier.Text;
                    c.Country = ddlCountry.Text;
                    c.IssueTo = txtIssueTo.Text.Trim();
                    c.LotNo = txtLotNo.Text;
                    c.CTNQTy = Common.GetDecimal(txtBOXQTY.Text);
                    c.InvoiceNo = txtInvoiceNo.Text.Trim();
                    c.EntryID = USession.SUserID;
                    if (new Yarn_DL().SaveIssue(c) == true)
                    {
                        lblMsg.Text = "Saved Successfully";
                    }
                }
                catch
                {

                }
                

            }
          
           
            txtDate.Text = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year;
            Clear();
           
           
           

        }
        private bool CheckEntry()
        {
            if (ddlUnit.Text == "--Select--")
            {
                lblMsg.Text = "Select Unit";
                return false;
            }
            else if (ddlYarn.Text == "--Select--")
            {
                lblMsg.Text = "Select Yarn";
                return false;
            }
            else if (Common.GetDecimal(ddlYarn.SelectedValue) == 0)
            {
                lblMsg.Text = "Select Yarn";
                return false;
            }

            else if (Common.GetDecimal(txtTotalQTY.Text) == 0)
            {
                lblMsg.Text = "Give QTY";
                return false;
            }
            else
            {
                return true;
            }
        }
      

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
            lblMsg.Text = "";

        }
        private void Clear()
        {
            txtBOXQTY.Text = "";
            txtNotes.Text = "";
            txtTotalQTY.Text = "";
            ddlYarn.Text = "--Select--";
            ddlYarn.Focus();
            UpEntry.Update();
        }

        protected void btnload_Click(object sender, EventArgs e)
        {
            loadGridData();
        }

        protected void ddlYarnGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
            else
            {
                Int64 GID = Convert.ToInt64(ddlYarnGroup.SelectedValue);
                LoadYarn(GID);
                UpEntry.Update();
            }
        }
    }
}
