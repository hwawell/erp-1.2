﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class INV_SupplierCountryEntry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (new Utilities_DL().CheckSecurity("502", USession.SUserID) == true)
                //{
                this.rblNormal.Items.FindByText("Country").Selected = true;
                loadGridData();

                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
        private void loadGridData()
        {
            string Head = new Inventory_DL().GetInventoryType(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            this.Title = Head + " - Supplier/Country Setup";
            lblHeader.Text = "Supplier/Country Setup for " + Head;

            if (this.rblNormal.Items.FindByText("Country").Selected == true)
            {
                gvProducts.Columns[2].Visible = true;
                gvProducts.Columns[3].Visible = true;

                gvProducts.DataSource = new Inventory_DL().GetCountry(Convert.ToInt32(Request.QueryString["INVTypeID"]));
                gvProducts.DataBind();
            }
            else
            {
                gvProducts.Columns[2].Visible = true;
                gvProducts.Columns[3].Visible = true;

                gvProducts.DataSource = new Inventory_DL().GetSupplier(Convert.ToInt32(Request.QueryString["INVTypeID"]));
                gvProducts.DataBind();
            }

        }



        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;

            txtYarn.Text = (row.Cells[1].Text);

            if (this.rblNormal.Items.FindByText("Country").Selected == true)
            {
                lblHead.Text = "Yarn Country";
            }
            else
            {
                lblHead.Text = "Yarn Supplier";
            }

            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Yarn_DL().DeleteYarnSupplier(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ////ImageButton btnEdit = sender as ImageButton;
            ////GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            ////new Yarn_DL().DeleteActive(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            ////loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;

            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            INV_Supplier c = new INV_Supplier();
            c.ID = Convert.ToInt64(hdnCode.Value);
            c.CountryOrSupplier = txtYarn.Text.ToString();
            if (this.rblNormal.Items.FindByText("Country").Selected == true)
            {
                c.SType = "COUNTRY";
            }
            else
            {
                c.SType = "SUPPLIER";
            }
            c.INVTypeID = Convert.ToInt32(Request.QueryString["INVTypeID"]);
            if (c.CountryOrSupplier.Length > 0)
            {
                new Inventory_DL().SaveSupplierCountry(c);
            }

            txtYarn.Text = "";

            updPanel.Update();

            txtYarn.Focus();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            INV_Supplier c = new INV_Supplier();
            c.ID = Convert.ToInt64(hdnCode.Value);
            c.CountryOrSupplier = txtYarn.Text.ToString();
            if (this.rblNormal.Items.FindByText("Country").Selected == true)
            {
                c.SType = "COUNTRY";
            }
            else
            {
                c.SType = "SUPPLIER";
            }
            c.INVTypeID = Convert.ToInt32(Request.QueryString["INVTypeID"]);
            if (c.CountryOrSupplier.Length > 0)
            {
                new Inventory_DL().SaveSupplierCountry(c);
            }

            updPanel.Update();
            loadGridData();


        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            txtYarn.Text = string.Empty;

            if (this.rblNormal.Items.FindByText("Country").Selected == true)
            {
                lblHead.Text = "Yarn Country";
            }
            else
            {
                lblHead.Text = "Yarn Supplier";
            }


            mpeProduct.Show();
            txtYarn.Focus();
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadGridData();

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            loadGridData();
        }
    }
}
