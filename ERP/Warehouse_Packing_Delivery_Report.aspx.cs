﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using BDLayer;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace ERP
{
    public partial class Warehouse_Packing_Delivery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                Window1.Hide();
                Window2.Hide();
                Window3.Hide();
                
                GridPanel2.ColumnModel.Columns[3].Header = "test";
            }
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            List<EWareHouseDelivery> liProd = new List<EWareHouseDelivery>();
            this.Window1.Title = "Fabric Delivery Report";
            liProd = new KNIT_all_operation().GetWareHouseDelivery(OpenFunction.ConvertDate(txtFrom.Text), OpenFunction.ConvertDate(txtFrom0.Text), tbAuto.Text);



            Export("DeliveryReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, liProd);
        }
        protected void btnGEN_Click(object sender, EventArgs e)
        {
            List<EWareHouseGeneralDelivery> liProd = new List<EWareHouseGeneralDelivery>();
            this.Window1.Title = "Genetal Delivery Report";
            liProd = new KNIT_all_operation().GetWareHouseGeneralDelivery(OpenFunction.ConvertDate(txtFrom.Text), OpenFunction.ConvertDate(txtFrom0.Text));



            ExportGeneral("GeneralDeliveryReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, liProd);
        }

        private void ExportGeneral(string fileName, List<EWareHouseGeneralDelivery> empList)
        {
            try
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                if (txtFrom.Text.Length > 4)
                {
                    HttpContext.Current.Response.Write("<div> <h3>General Delivery Report on " + Convert.ToDateTime(txtFrom.Text).ToString("dd-MMM-yyyy") + "</h3>  </div>");
                }
                else
                {
                    HttpContext.Current.Response.Write("<div> <h3>Delivery Report  </h3>  </div>");
                }
                HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
                HttpContext.Current.Response.Write("<BR>");
                //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
                HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
                  "  <TR>");

                foreach (PropertyInfo proinfo in new EWareHouseGeneralDelivery().GetType().GetProperties())
                {

                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    HttpContext.Current.Response.Write(proinfo.Name);
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");


                }
                HttpContext.Current.Response.Write("</TR>");
                foreach (EWareHouseGeneralDelivery emp in empList)
                {
                    TableRow row1 = new TableRow();
                    HttpContext.Current.Response.Write("</TR>");
                    foreach (PropertyInfo proinfo in new EWareHouseGeneralDelivery().GetType().GetProperties())
                    {
                        HttpContext.Current.Response.Write("<Td>");
                        //Get column headers  and make it as bold in excel columns
                        HttpContext.Current.Response.Write("<B>");
                        object value;
                        value = proinfo.GetValue(emp, null);
                        if (value == null)
                        {
                            value = "";
                        }
                        else
                        {
                            value = proinfo.GetValue(emp, null);
                        }
                        HttpContext.Current.Response.Write(value.ToString());
                        HttpContext.Current.Response.Write("</B>");
                        HttpContext.Current.Response.Write("</Td>");

                    }

                }
                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("</font>");
                HttpContext.Current.Response.End();
            }
            catch
            {

            }
        }

        
        protected void btnExport1_Click(object sender, EventArgs e)
        {
            List<EWareHouseDeliverySummary> liProd = new List<EWareHouseDeliverySummary>();
            this.Window2.Title = "Delivery Report";
            liProd = new KNIT_all_operation().GetWareHouseDeliverySummary(OpenFunction.ConvertDate(txtFrom.Text), OpenFunction.ConvertDate(txtFrom0.Text), tbAuto.Text);



            ExportSummary("DeliverySummaryReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, liProd);
        }
        public void Export(string fileName, List<EWareHouseDelivery> empList)
        {

            try
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                if (txtFrom.Text.Length > 4)
                {
                    HttpContext.Current.Response.Write("<div> <h3>Delivery Report on " + Convert.ToDateTime(txtFrom.Text).ToString("dd-MMM-yyyy") + "</h3>  </div>");
                }
                else
                {
                    HttpContext.Current.Response.Write("<div> <h3>Delivery Report  </h3>  </div>");
                }
                HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
                HttpContext.Current.Response.Write("<BR>");
                //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
                HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
                  "  <TR>");

                foreach (PropertyInfo proinfo in new EWareHouseDelivery().GetType().GetProperties())
                {

                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    HttpContext.Current.Response.Write(proinfo.Name);
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");


                }
                HttpContext.Current.Response.Write("</TR>");
                foreach (EWareHouseDelivery emp in empList)
                {
                    TableRow row1 = new TableRow();
                    HttpContext.Current.Response.Write("</TR>");
                    foreach (PropertyInfo proinfo in new EWareHouseDelivery().GetType().GetProperties())
                    {
                        HttpContext.Current.Response.Write("<Td>");
                        //Get column headers  and make it as bold in excel columns
                        HttpContext.Current.Response.Write("<B>");
                        object value;
                        value = proinfo.GetValue(emp, null);
                        if (value == null)
                        {
                            value ="";
                        }
                        else
                        {
                            value = proinfo.GetValue(emp, null);
                        }
                        HttpContext.Current.Response.Write(value.ToString());
                        HttpContext.Current.Response.Write("</B>");
                        HttpContext.Current.Response.Write("</Td>");

                    }

                }
                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("</font>");
                HttpContext.Current.Response.End();
            }
            catch
            {

            }
        }

        public void ExportSummary(string fileName, List<EWareHouseDeliverySummary> empList)
        {

             bool isYds = false;
            bool isMETER = false;

            isYds = empList.Exists(o => o.BookingYDS > 0);
            isMETER = empList.Exists(o => o.BookingMETER > 0);
            

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            if (txtFrom.Text.Length > 4)
            {
                HttpContext.Current.Response.Write("<div> <h3>Delivery Summary Report on " + Convert.ToDateTime(txtFrom.Text).ToString("dd-MMM-yyyy") + "</h3>  </div>");
            }
            else
            {
                HttpContext.Current.Response.Write("<div> <h3>Delivery Summary Report </h3>  </div>");
            }
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");


            HttpContext.Current.Response.Write("<Td>");
            HttpContext.Current.Response.Write("<B>");
            HttpContext.Current.Response.Write("SL");
            HttpContext.Current.Response.Write("</B>");
            HttpContext.Current.Response.Write("</Td>");

            HttpContext.Current.Response.Write("<Td>");
            HttpContext.Current.Response.Write("<B>");
            HttpContext.Current.Response.Write("Order No");
            HttpContext.Current.Response.Write("</B>");
            HttpContext.Current.Response.Write("</Td>");

            HttpContext.Current.Response.Write("<Td>");
            HttpContext.Current.Response.Write("<B>");
            HttpContext.Current.Response.Write("Customer");
            HttpContext.Current.Response.Write("</B>");
            HttpContext.Current.Response.Write("</Td>");

           

            HttpContext.Current.Response.Write("<Td>");
            HttpContext.Current.Response.Write("<B>");
            HttpContext.Current.Response.Write("Booking (KGS)");
            HttpContext.Current.Response.Write("</B>");
            HttpContext.Current.Response.Write("</Td>");

            if (isYds == true)
            {
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write("Booking (YDS)");
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }

            if (isMETER == true)
            {
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write("Booking (METER)");
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
        
            HttpContext.Current.Response.Write("<Td>");
            HttpContext.Current.Response.Write("<B>");
            HttpContext.Current.Response.Write("Delivered (KGS)");
            HttpContext.Current.Response.Write("</B>");
            HttpContext.Current.Response.Write("</Td>");

            if (isYds == true)
            {
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write("Delivered (YDS)");
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }

            if (isMETER == true)
            {
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write("Delivered (METER)");
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }

        

            HttpContext.Current.Response.Write("<Td>");
            HttpContext.Current.Response.Write("<B>");
            HttpContext.Current.Response.Write("Merchandiser");
            HttpContext.Current.Response.Write("</B>");
            HttpContext.Current.Response.Write("</Td>");

            HttpContext.Current.Response.Write("<Td>");
            HttpContext.Current.Response.Write("<B>");
            HttpContext.Current.Response.Write("Last Delivery Date ");
            HttpContext.Current.Response.Write("</B>");
            HttpContext.Current.Response.Write("</Td>");

            
            HttpContext.Current.Response.Write("</TR>");
            foreach (EWareHouseDeliverySummary emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");

                HttpContext.Current.Response.Write("<Td>"); HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(emp.SL.ToString()); HttpContext.Current.Response.Write("</B>"); HttpContext.Current.Response.Write("</Td>");

                HttpContext.Current.Response.Write("<Td>"); HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(emp.OrderNo.ToString()); HttpContext.Current.Response.Write("</B>"); HttpContext.Current.Response.Write("</Td>");

                HttpContext.Current.Response.Write("<Td>"); HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(emp.Customer.ToString()); HttpContext.Current.Response.Write("</B>"); HttpContext.Current.Response.Write("</Td>");

             
                HttpContext.Current.Response.Write("<Td>"); HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(emp.BookingKG.ToString()); HttpContext.Current.Response.Write("</B>"); HttpContext.Current.Response.Write("</Td>");

                if (isYds == true)
                {
                    HttpContext.Current.Response.Write("<Td>"); HttpContext.Current.Response.Write("<B>");
                    HttpContext.Current.Response.Write(emp.BookingYDS.ToString()); HttpContext.Current.Response.Write("</B>"); HttpContext.Current.Response.Write("</Td>");
                }

                if (isMETER == true)
                {
                    HttpContext.Current.Response.Write("<Td>"); HttpContext.Current.Response.Write("<B>");
                    HttpContext.Current.Response.Write(emp.BookingMETER.ToString()); HttpContext.Current.Response.Write("</B>"); HttpContext.Current.Response.Write("</Td>");
                }

              
                HttpContext.Current.Response.Write("<Td>"); HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(emp.DeliveryKG.ToString()); HttpContext.Current.Response.Write("</B>"); HttpContext.Current.Response.Write("</Td>");

                if (isYds == true)
                {
                    HttpContext.Current.Response.Write("<Td>"); HttpContext.Current.Response.Write("<B>");
                    HttpContext.Current.Response.Write(emp.DeliveryYDS.ToString()); HttpContext.Current.Response.Write("</B>"); HttpContext.Current.Response.Write("</Td>");
                }

                if (isMETER == true)
                {
                    HttpContext.Current.Response.Write("<Td>"); HttpContext.Current.Response.Write("<B>");
                    HttpContext.Current.Response.Write(emp.DeliveryMETER.ToString()); HttpContext.Current.Response.Write("</B>"); HttpContext.Current.Response.Write("</Td>");
                }
            
                HttpContext.Current.Response.Write("<Td>"); HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(emp.Merchandiser.ToString()); HttpContext.Current.Response.Write("</B>"); HttpContext.Current.Response.Write("</Td>");

                HttpContext.Current.Response.Write("<Td>"); HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(emp.LastDelivery.ToString("dd-MMM-yyyy")); HttpContext.Current.Response.Write("</B>"); HttpContext.Current.Response.Write("</Td>");


            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            List<rpt_packing_Delivery_summary_byColorResult> liProd = new List<rpt_packing_Delivery_summary_byColorResult>();
            this.Window3.Title = "Delivery Report";
            liProd = new KNIT_all_operation().GetWareHouseDeliveryByColor( tbAuto.Text);



            ExportDeliverySummary("DeliverySummaryReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, liProd);
        }
        public void ExportDeliverySummary(string fileName, List<rpt_packing_Delivery_summary_byColorResult> empList)
        {

            try
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                if (txtFrom.Text.Length > 4)
                {
                    HttpContext.Current.Response.Write("<div> <h3>Delivery Report on " + Convert.ToDateTime(txtFrom.Text).ToString("dd-MMM-yyyy") + "</h3>  </div>");
                }
                else
                {
                    HttpContext.Current.Response.Write("<div> <h3>Delivery Report  </h3>  </div>");
                }
                HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
                HttpContext.Current.Response.Write("<BR>");
                //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
                HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
                  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                  "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
                  "  <TR>");

                foreach (PropertyInfo proinfo in new rpt_packing_Delivery_summary_byColorResult().GetType().GetProperties())
                {

                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    HttpContext.Current.Response.Write(proinfo.Name);
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");


                }
                HttpContext.Current.Response.Write("</TR>");
                foreach (rpt_packing_Delivery_summary_byColorResult emp in empList)
                {
                    TableRow row1 = new TableRow();
                    HttpContext.Current.Response.Write("</TR>");
                    foreach (PropertyInfo proinfo in new rpt_packing_Delivery_summary_byColorResult().GetType().GetProperties())
                    {
                        HttpContext.Current.Response.Write("<Td>");
                        //Get column headers  and make it as bold in excel columns
                        HttpContext.Current.Response.Write("<B>");
                        object value;
                        value = proinfo.GetValue(emp, null);
                        if (value == null)
                        {
                            value = "";
                        }
                        else
                        {
                            value = proinfo.GetValue(emp, null);
                        }
                        HttpContext.Current.Response.Write(value.ToString());
                        HttpContext.Current.Response.Write("</B>");
                        HttpContext.Current.Response.Write("</Td>");

                    }

                }
                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("</font>");
                HttpContext.Current.Response.End();
            }
            catch
            {

            }
        }

     
    }
}
