﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGroup();
            }
        }
        private void LoadGroup()
        {
            Inventory_DL objDL = new Inventory_DL();
            ddlGroup.DataSource = objDL.GetItemGroup(1);
            ddlGroup.DataTextField = "GroupName";
            ddlGroup.DataValueField = "SL";
            ddlGroup.DataBind();

        }
    }
}
