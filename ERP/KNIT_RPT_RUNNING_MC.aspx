﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_RPT_RUNNING_MC.aspx.cs" Inherits="ERP.KNIT_RPT_RUNNING_MC" Title="Grey Running MC List" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();
            List<ERptKnitRunningMC> liProd = new List<ERptKnitRunningMC>();
            this.Window1.Title = "Knit Production Balance Sheet";
            liProd = new KNIT_all_operation().GetRPT_KnitRunningMC(Convert.ToDateTime(txtFrom.Text));
                    
            
            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
    
   
   
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> KNITTING RUNNING MC PRODUCTION REPORT</h2></div>
 <div align="left" style="padding: 10px; ">
                      From
                    <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom">
                    </cc1:CalendarExtender>
                   <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                    </div>
                       
                </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
           
            >
            <Reader>
            <ext:JsonReader IDProperty="SL">
                    <Fields>
                      <ext:RecordField Name="SL" />
                     
   
                         <ext:RecordField Name="ReqNo" />
                          <ext:RecordField Name="Orders_No" />
                        <ext:RecordField Name="FabricGroup" />
                          <ext:RecordField Name="Grey_Construction" />
                         <ext:RecordField Name="GM" />
                         <ext:RecordField Name="GSM" />
                         <ext:RecordField Name="YarnConstruction" />
                         
                         <ext:RecordField Name="MC_Prod_Qty" />
                         <ext:RecordField Name="Running_MC" />
                          <ext:RecordField Name="TotalProductionQty" />
                          <ext:RecordField Name="BalanceQty" />
                          <ext:RecordField Name="Notes" />
                   
                          
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Production Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                          <ext:Column ColumnID="ReqNo" Header="ReqNo"  DataIndex="ReqNo" Sortable="true" />
                          <ext:Column ColumnID="Orders_No" Header="Order No"  DataIndex="Orders_No" Sortable="true" />
                             <ext:Column ColumnID="FabricGroup" Header="FabricGroup"  DataIndex="FabricGroup" Sortable="true" />
                            <ext:Column ColumnID="Grey_Construction" Header="Grey_Construction"  DataIndex="Grey_Construction" Sortable="true" />
                            <ext:Column ColumnID="GM" Header="GM"  DataIndex="GM" Sortable="true" />
                            <ext:Column ColumnID="GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                            <ext:Column ColumnID="YarnConstruction" Header="YarnConstruction"  DataIndex="YarnConstruction" Sortable="true" />
                            <ext:Column ColumnID="MC_Prod_Qty" Header="MC_Prod_Qty"  DataIndex="MC_Prod_Qty" Sortable="true" />
                            
                            <ext:Column ColumnID="Running_MC" Header="Running_MC"  DataIndex="Running_MC" Sortable="true" />
                            <ext:Column ColumnID="TotalProductionQty" Header="TotalProductionQty"  DataIndex="TotalProductionQty" Sortable="true" />
                            <ext:Column ColumnID="BalanceQty" Header="BalanceQty"  DataIndex="BalanceQty" Sortable="true" />
                            <ext:Column ColumnID="Notes" Header="Notes"  DataIndex="Notes" Sortable="true" />
                            
                        
                       </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                            <ext:StringFilter DataIndex="Orders_No" />
                                <ext:StringFilter DataIndex="ReqNo" />
                                <ext:StringFilter DataIndex="FabricGroup" />
                                <ext:StringFilter DataIndex="Grey_Construction" />
                                <ext:StringFilter DataIndex="GM" />
                                <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="YarnConstruction" />
                                 <ext:NumericFilter DataIndex="MC_Prod_Qty" />
                                <ext:StringFilter DataIndex="Running_MC" />
                                 <ext:NumericFilter DataIndex="TotalProductionQty" />
                                  <ext:NumericFilter DataIndex="BalanceQty" />
                                  <ext:StringFilter DataIndex="Notes" />
                               
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>
