﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;

namespace ERP
{
    public partial class WebForm11 : System.Web.UI.Page
    {
        List<EOrderProductDetails> liColorobj = new List<EOrderProductDetails>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //if (new Utilities_DL().CheckSecurity("502", USession.SUserID) == true)
                //{
                    this.rblNormal.Items.FindByText("Active").Selected = true;
                  //  loadGridData();
                    loadOrder();
                    loadFabricGroup();
                    GenerateProcessCardNo();
                //}
                //else
                //{
                //    this.rblNormal.Items.FindByText("Active").Selected = true;
                //    //  loadGridData();
                //    loadOrder();
                //    loadFabricGroup();

                //    //Response.Redirect("Home.aspx");
                //}
            }
        }
        private void loadFabricGroup()
        {


            //ddFGroup.DataSource = new Fabric_CL().GetAllFabricGroup();
            //ddFGroup.DataValueField = "SL";
            //ddFGroup.DataTextField = "FGroupName";
            //ddFGroup.DataBind();

        }
        private void loadFabric()
        {
            ////ddlFabric.DataSource = new Fabric_CL().GetAlFabric();
            ////ddlFabric.DataValueField = "Fid";
            ////ddlFabric.DataTextField = "fab";
            ////ddlFabric.DataBind();
            ////ddlFabric.Items.Insert(0, "--Select Fabric--");
            //ddlFabric.Items.Insert(0, "Select--");
        }
        private void loadOrder()
        {
            //ddlOrder.DataSource = new Order_DL().GetActiveOrderList();

            //ddlOrder.DataTextField = "OrderNo";
            //ddlOrder.DataValueField = "OrderNo";

            
            //ddlOrder.DataBind();
            //ddlOrder.Items.Insert(0, "--Select Order No--");
           
        }
        private void loadGridData()
        {


            gvProducts.Columns[7].Visible = true;
            gvProducts.Columns[8].Visible = false;

            if (txtLastPCard.Text.Length > 2)
            {
                gvProducts.DataSource = new ProcessCard_CL().GetActiveSinglePCard(txtLastPCard.Text.ToUpper());
            }
            else
            {
                gvProducts.DataSource = new ProcessCard_CL().GetActivePCard();
            }
           
            gvProducts.DataBind();

        }
        private void loadDeletedData()
        {

            gvProducts.Columns[7].Visible = false;
            gvProducts.Columns[8].Visible = true;
            gvProducts.DataSource = new ProcessCard_CL().GetDeletedPCard();
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            tbAuto.Text = (row.Cells[1].Text);
            txtProcessCard.Text = (row.Cells[2].Text);
            ddlColor.SelectedValue = (row.Cells[3].Text);
            //txtTotRoll.Text = (row.Cells[4].Text);
            txtLotNo.Text = (row.Cells[5].Text);
            //txtWeight.Text = (row.Cells[5].Text);
            
            //txtPerKG.Text = (row.Cells[8].Text);
            //txtUnit.Text = (row.Cells[9].Text);

           
          

            updPanel.Update();

            /////mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new ProcessCard_CL().DeleteActivePCard(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                //e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            ProcessCard c = new ProcessCard();

            if (new ProcessCard_CL().CheckProcessCard(txtProcessCard.Text.Trim()) == false && (CheckEntryData() == true))
            {
                ////if (checkDecimal(txtTotRoll.Text) == false)
                ////{
                ////  //  txtTotRoll.Text = "0";
                ////}
                c.SL = Convert.ToInt64(hdnCode.Value);
                c.OPDID = Convert.ToInt32(ddlColor.SelectedValue.ToString());
                c.PCardNo = txtProcessCard.Text.Trim();

                //c.TotalRoll = Convert.ToInt16(txtTotRoll.Text);
                c.LotNo = txtLotNo.Text.Trim();

               


                c.EntryID = USession.SUserID;
                if (c.PCardNo.Length > 2)
                {
                    new ProcessCard_CL().Save(c);
                }
                loadGridData();
            }
            else
                lblMsg.Text = "Process Card is already Exist";
               // ScriptManager.RegisterClientScriptBlock(Page,Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Process Card is already Exists');</script>",false);

            //loadGridData();
           // updPanel.Update();
           //// mpeProduct.Hide();

        }
        private bool CheckEntryData()
        {
            string str="";
            bool check = true;
            if (new Order_DL().HasOrderNoExist(tbAuto.Text)==false)
            {
                str = "Order No Not Valid";
                check = false;
            }
            else if (ddlDesc.Text == "--Select--")
            {
                str = "Select Order Item";
                check = false;
            }
            else if (ddlColor.Text == "--Select--")
            {
                str = "Select Color";
                check = false;
            }
            else if (OpenFunction.IsNumeric(txtGSM.Text)==false)
            {
                str = "Please give GSM";
                check = false;
            }

            else if (OpenFunction.IsNumeric(txtCalculation.Text) == false)
            {
                str = "Please give YDS Calculation value";
                check = false;
            }

            else if (ddlColor.Text == "--Select--")
            {
                str = "Select Color";
                check = false;
            }
            if (check == false)
            {
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('"+ str +"');</script>", false);
                lblMsg.Text = str;

            }
            return check;
        }
        private bool checkDecimal(string s)
        {

            try
            {
                decimal v;
                v = Convert.ToDecimal(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private void SaveData()
        {
          

            try
            {
                lblMsg.ForeColor = System.Drawing.Color.Red;
                if (CheckEntryData() == true)
                {
                    if (Convert.ToInt32(hdnCode.Value) > 0 || new ProcessCard_CL().CheckProcessCard(txtProcessCard.Text.Trim()) == false)
                    {
                        //if (checkDecimal(txtTotRoll.Text) == false)
                        //{
                        //    txtTotRoll.Text = "0";
                        //}
                        ProcessCard c = new ProcessCard();
                        //if (Convert.ToInt32(hdnCode.Value) > 0)
                        //{
                        //    c.SL = Convert.ToInt32(hdnCode.Value);
                        //}
                        c.OPDID = Convert.ToInt32(ddlColor.SelectedValue.ToString());
                        c.PCardNo = txtProcessCard.Text.Trim();
                        c.PINO = tbAuto.Text;
                        c.PCardType = ddlPType.Text;
                        c.ReqNo = txtReqNo.Text;
                        c.width = Common.GetDecimal(txtwidth.Text);
                        c.Weight = Common.GetDecimal(txtWeight.Text);
                        c.MCCapacity = Common.GetDecimal(txtMCCapacity.Text);
                        c.LotNo = txtLotNo.Text;
                        c.YardCalc = txtYardCal.Text.ToString();
                        if (OpenFunction.IsNumeric(txtCalculation.Text) == false)
                        {
                            c.YDS_CAL = 0;
                        }
                        else
                        {
                            c.YDS_CAL = Convert.ToInt16(Convert.ToDouble(txtCalculation.Text));
                        }
                        if (OpenFunction.IsNumeric(txtGSM.Text) == false)
                        {
                            c.GSM = 0;
                        }
                        else
                        {
                            c.GSM = Convert.ToInt16(Convert.ToDouble(txtGSM.Text)); 
                           
                        }
                        if (OpenFunction.IsNumeric(txtExtraPer.Text) == false)
                        {
                            c.ExtraPer = 0;
                        }
                        else
                        {
                            c.ExtraPer = Convert.ToDecimal(txtExtraPer.Text); 
                           
                        }

                        c.PackingStyle = txtPackingStyle.Text;

                        c.RequiredQty = Common.GetDecimal(txtRequired.Text);
                        c.ProductionQty = Common.GetDecimal(txtProductionQty.Text);
                        c.Description = txtDescription.Text.Trim();
                        c.Notes = txtNotes.Text.Trim();
                        c.EntryDate = Convert.ToDateTime(txtDate.Text);
                        Fabric obj = new KNIT_all_operation().getFabricByReqNo(txtReqNo.Text);
                        int FID = 0;
                        if (obj != null)
                        {
                            FID =(Int32) obj.FID;
                           
                        }

                       
                       

                        c.FID = FID;
                        c.EntryID = USession.SUserID;
                      
                        if (c.PCardNo.Length > 2)
                        {

                           string result= new ProcessCard_CL().Save(c);
                           if (result.Contains("Successfully")==true)
                           {
                               txtLastPCard.Text = txtProcessCard.Text;

                               ClearALl();
                               //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Data Saved successfully !!!');</script>", false);
                           }
                           lblMsg.ForeColor = System.Drawing.Color.Green;
                            lblMsg.Text = result;
                            tbAuto.Focus();

                        }
                        else
                            lblMsg.Text = "Give Process Card No";

                    }
                    else
                        lblMsg.Text = "Process Card is already Exist";

                }

                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Process Card is already Exists');</script>", false);
                GenerateProcessCardNo();
                UpEntry.Update();

            }
            catch (Exception)
            {
                lblMsg.Text = "Process Card Not saved";
                UpEntry.Update();
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {




            SaveData();



            /////mpeProduct.Show();
           

        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            // txtYarn.Text = string.Empty;
            // txtYarnDesc.Text = string.Empty;

            txtProcessCard.Text = "";
        
           
            txtLotNo.Text = "";
        
           
         


           //// mpeProduct.Show();
            tbAuto.Focus();
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

            ClearALl();
           
        }

        private void ClearALl()
        {


            try
            {
                GenerateProcessCardNo();
                txtLotNo.Text = "";
                txtExtraPer.Text = "";
                txtWeight.Text = "";
                txtwidth.Text = "";
                txtGSM.Text = "";
                txtLotNo.Text = "";
                txtMCCapacity.Text = "";
                txtRequired.Text = "";
                txtProductionQty.Text = "";
                txtCalculation.Text = "60";
                txtDescription.Text = "";
                txtNotes.Text = "";
                txtPackingStyle.Text = "";

                txtReqNo.Text = "";
                ddlColor.Text = "--Select--";
                ddlDesc.Text = "--Select--";
                txtBalanceQty.Text = "0";
                txtAvailableQty.Text = "0";
                txtBookingQty.Text = "0";
                txtExtraPer.Text = "0";

                txtYardCal.Text = "";
                hdnCode.Value = "0";


                GenerateProcessCardNo();
                UpEntry.Update();
            }
            catch
            {

            }
        }
        protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        private void LoadOrderProductDesc()
        {
            if (tbAuto.Text.Length > 5)
            {
                ddlDesc.DataSource = new Order_DL().GetOrderProductDesc(tbAuto.Text);
                ddlDesc.DataValueField = "OPID";
                ddlDesc.DataTextField = "DESC";


             
                ddlDesc.DataBind();
                ddlDesc.Items.Add("--Select--");
                ddlDesc.Text = "--Select--";


            }
        }
        protected void ddlDesc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDesc.SelectedItem.Text != "--Select--")
            {
                

                LoadOrderproductDetails();
            }
        }
        private void LoadOrderproductDetails()
        {
            try
            {
                liColorobj = new Order_DL().GetOrderProductColorList(Convert.ToInt32(ddlDesc.SelectedValue.ToString()));
                ddlColor.DataSource = liColorobj;
            ddlColor.DataValueField = "OPDID";
            ddlColor.DataTextField = "Color";
            ddlColor.DataBind();
            ddlColor.Items.Add("--Select--");
            ddlColor.Text = "--Select--";

           
                string s = ddlDesc.SelectedItem.Text.Replace(',', ' ');
                string[] desc = s.Split('>');
                if (desc.Length > 1)
                {
                    txtDescNotes.Text = desc[1];
                    txtGSM.Text = desc[desc.Length - 2];
                    txtwidth.Text = desc[desc.Length-1];
                    txtWeight.Text = ((Convert.ToDouble(txtGSM.Text) * 1.4 * Convert.ToDouble(txtwidth.Text)) / 60).ToString();
                    UpEntry.Update();
                    upWid.Update();
                }
            }
            catch
            {
            }
        }
        protected void btnload_Click(object sender, EventArgs e)
        {
            if (rblNormal.Items.FindByText("Active").Selected == true)
            {
                loadGridData();

            }
            else
            {
                loadDeletedData();
            }
        }

        protected void txtCalculation1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           

            string jscript = "";

            string ReportName = "";
            string pr = "PROCESS_CARD";

            ReportName = "rptProcesssCardNew";


            string Fdate = tbAuto.Text;


            string pCard = txtLastPCard.Text;
            string RTYpe = "Process Card Print"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&PCARD=" + pCard + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";
            //string script = "var windowObject = window.self; windowObject.opener = window.self; windowObject.close();";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Close Window", script, true);
            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm11), "ScriptFunction", jscript);
        }

        protected void gvProducts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddFGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
              //  LoadFabric();
                
            }
        }
        private void LoadFabric()
        {

            Fabric obj = new KNIT_all_operation().getFabricByReqNo(txtReqNo.Text);
            if (obj != null)
            {
                hdnFID.Value = obj.FID.ToString();
                txtReqNo.Text = obj.Fabric1;
            }
            //try
            //{
            //    ddlFabric.DataSource = new Fabric_CL().GetAlFabricbyGRoup(Convert.ToInt16(ddFGroup.SelectedValue));
            //    ddlFabric.DataValueField = "Fid";
            //    ddlFabric.DataTextField = "fab";
            //    ddlFabric.DataBind();
            //    upFa.Update();
            //}
            //catch
            //{
            //}
        }
        protected void txtGSM_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnEditPCard_Click(object sender, EventArgs e)
        {
            EPcardInfo obj = new ProcessCard_CL().GetSinglePCardInfo(txtLastPCard.Text);
            if (obj != null)
            {
                hdnCode.Value = obj.ID.ToString();
                txtProcessCard.Text = obj.PCardNo;




                tbAuto.Text= obj.PINO;
                LoadOrderProductDesc();
                ddlDesc.SelectedValue = obj.OrDesID.ToString();
                LoadOrderproductDetails();
                ddlColor.SelectedValue = obj.ColorID.ToString();
                txtReqNo.Text = obj.ReqNo;
                Fabric objFab = new KNIT_all_operation().getFabricByReqNo(txtReqNo.Text);
                if (objFab != null)
                {
                    hdnFID.Value = objFab.FID.ToString();
                    txtFabric.Text = objFab.Fabric1;
                }

               
             
                txtPackingStyle.Text = obj.ID.ToString();
                txtLotNo.Text = obj.LotNo;
                txtExtraPer.Text = obj.ExtraQty;
                txtWeight.Text = obj.Weight;
                txtwidth.Text = obj.Width;
                txtGSM.Text = obj.GSM;
                txtLotNo.Text = obj.LotNo;
                txtRequired.Text = obj.RQTY;
                txtMCCapacity.Text = obj.MCCapacity;
                txtProductionQty.Text = obj.PQTY;
                txtCalculation.Text = obj.txtCalculation;
                txtDescription.Text = obj.Description;
                txtNotes.Text = obj.Notes;
                txtPackingStyle.Text = obj.PackingStyle;
                txtExtraPer.Text = obj.ExtraQty;
                txtYardCal.Text = obj.txtYardCal;
                txtDate.Text = obj.Date;
                ddlPType.Text = obj.Ptype;
                EOrderItemBalance obj1 = new ProcessCard_CL().GetOrderProductBalance(obj.ColorID);
                if (obj != null)
                {
                    double Actual = (ConvertToDoucble(txtMCCapacity.Text) * 100) / (100 + ConvertToDoucble(txtExtraPer.Text));
                    txtAvailableQty.Text = (obj1.Booking - obj1.ProductionQty + Actual).ToString("0.00");
                    txtBookingQty.Text = obj1.Booking.ToString();
                    txtBalanceQty.Text = (obj1.Booking - obj1.ProductionQty ).ToString();
                }
                else
                {
                    txtAvailableQty.Text = "0";
                    txtBookingQty.Text = "0";
                }

                UpEntry.Update();
                
            }
        }

        protected void ddlColor_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlColor.SelectedItem.Text != "--Select--")
            {
                EOrderItemBalance obj = new ProcessCard_CL().GetOrderProductBalance(Convert.ToInt32(ddlColor.SelectedValue));
                if (obj != null)
                {
                    txtAvailableQty.Text =(obj.Booking-obj.ProductionQty).ToString();
                    txtBookingQty.Text = obj.Booking.ToString();
                }
                else
                {
                    txtAvailableQty.Text = "0";
                    txtBookingQty.Text = "0";
                }
                txtExtraPer.Focus();
            }

          

        }
        private void BalanceQty()
        {
           // txtBalanceQty.Text = (obj.Booking - obj.ProductionQty).ToString();
            double bal;
            double per;
            double MCQty;
            per=ConvertToDoucble(txtExtraPer.Text);
            MCQty=ConvertToDoucble(txtMCCapacity.Text);
            bal = Convert.ToDouble(txtAvailableQty.Text) - ((MCQty * 100) / (100 + per));
            txtBalanceQty.Text = bal.ToString("0.00");
            
        }
        private double ConvertToDoucble(string val)
        {
            double res = 0;
            try
            {
                res = Convert.ToDouble(val);
            }
            catch
            {
                res = 0;
            }
            return res;
        }
        private void GenerateProcessCardNo()
        {
            txtProcessCard.Text = new ProcessCard_CL().GenearteProcessCardNo();

        }
        protected void txtMCCapacity_TextChanged(object sender, EventArgs e)
        {
            
            BalanceQty();
            txtwidth.Focus();
           
        }

        protected void txtExtraPer_TextChanged(object sender, EventArgs e)
        {

            LoadProductionQty();
            txtMCCapacity.Focus();

        }
        protected void txtRequired_TextChanged(object sender, EventArgs e)
        {

            LoadProductionQty();
            txtExtraPer.Focus();

        }
        private void LoadProductionQty()
        {
            BalanceQty();
            txtProductionQty.Text = (ConvertToDoucble(txtRequired.Text) * (1 + (ConvertToDoucble(txtExtraPer.Text) / 100))).ToString("0");
            upProduction.Update();
        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            string jscript = "";

            string ReportName = "";
            string pr = "PROCESS_CARD";

            ReportName = "rptProcesssCardNew";


            string Fdate = tbAuto.Text;


            string pCard = txtLastPCard.Text;
            string RTYpe = "Process Card Print"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&PCARD=" + pCard + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";
            //string script = "var windowObject = window.self; windowObject.opener = window.self; windowObject.close();";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Close Window", script, true);
            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm11), "ScriptFunction", jscript);
        }

        protected void btnPrintNew_Click(object sender, EventArgs e)
        {

            string jscript = "";

            string ReportName = "";
            string pr = "PROCESS_CARD";

            ReportName = "rptProcesssCardNew";


            string Fdate = tbAuto.Text;


            string pCard = txtLastPCard.Text;
            string RTYpe = "Process Card Print"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&PCARD=" + pCard + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";
            //string script = "var windowObject = window.self; windowObject.opener = window.self; windowObject.close();";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Close Window", script, true);
            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm11), "ScriptFunction", jscript);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string jscript = "";

            string ReportName = "";
            string pr = "PROCESS_CARD";

            ReportName = "rptProcesssCardNewOnPaper2012";


            string Fdate = tbAuto.Text;


            string pCard = txtLastPCard.Text;
            string RTYpe = "Process Card Print"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&PCARD=" + pCard + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";
            //string script = "var windowObject = window.self; windowObject.opener = window.self; windowObject.close();";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Close Window", script, true);
            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm11), "ScriptFunction", jscript);
        }

        protected void txtNotes_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadOrderProductDesc();
        }

        protected void btnSearch0_Click(object sender, EventArgs e)
        {
            Fabric obj = new KNIT_all_operation().getFabricByReqNo(txtReqNo.Text);
            if (obj != null)
            {
                hdnFID.Value = obj.FID.ToString();
                txtFabric.Text = obj.Fabric1;
                UpEntry.Update();
            }
        }
    }
}
