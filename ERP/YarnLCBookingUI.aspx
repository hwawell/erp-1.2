﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="YarnLCBookingUI.aspx.cs" Inherits="ERP.YarnLCBookingUI" Title="Yarn LC Booking" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="AjaxControls" namespace="AjaxControls" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
                       
        </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <div class="grid" >
            <div class="rounded"  >
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>Yarn Booking By LC</h2></center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid" 
                        style="overflow:auto; width: 850px; height: 699px; background-color:"
                        whitesmoke";"> 
                        
                        <asp:UpdatePanel ID="upALl" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                        
                        
                          
                <div style="height:25px;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                        AssociatedUpdatePanelID="updPanel">
                        <ProgressTemplate>
                            <img alt="" src="Images/loading.gif" style="height: 15px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    </div>
                      <div style="height: 40px; border-bottom-style: none; border-bottom-color: #003399;">
                    <div style="float: left">
                </div>
                <div align="center" style="height: 30px">
                
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LCNo<%-- </ContentTemplate>
                    </asp:UpdatePanel>--%> <asp:TextBox ID="txtSearchLC" runat="server" Width="87px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                      Record From<asp:TextBox ID="txtFrom" runat="server" Width="37px">1</asp:TextBox>to<asp:TextBox ID="txtTo" runat="server" Width="37px">10</asp:TextBox><asp:Button ID="btnload" runat="server" onclick="btnload_Click" Text="Load" 
                          Width="86px" />
                                    <%--      <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>--%>
                                    
                    </div>
                
                </div>
                 
                  <asp:Panel ID="pnlDataEntry" runat="server"     
                        BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                        Width="700px" BorderWidth="2px" Height="170px">
                        <center>
                            <asp:Panel ID="ProductCaption" runat="server"   
                                Style="margin-bottom: 10px; cursor: hand;" 
                                BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                Yarn Booking (By LC) Entry Form</asp:Panel></center>
                            
                           <asp:UpdatePanel ID="UpEntry" runat="server" UpdateMode="Conditional" 
                            ChildrenAsTriggers="False" >
                        <ContentTemplate>
                        <asp:HiddenField ID="hdnValue" runat="server" />
                        <div>
                           <div style="float:left;width:20%" align="right"> Yarn Supplier</div> <div style="float:left;width:30%" align="left">    <asp:DropDownList ID="ddlSupplier" runat="server" Height="24px" TabIndex="1" Width="200px">  </asp:DropDownList>  </div>
                                        
                                    
                            
                           <div style="float:left;width:50%; " align="right"> Yarn Country <asp:DropDownList ID="ddlCountry" runat="server" Height="24px" TabIndex="1" 
                                       Width="150px" >
                                    </asp:DropDownList></div>
                                    
                            <div style="float:left;width:20%" align="right"> LC No:</div> <div style="float:left;width:30%" align="left">   <asp:TextBox ID="txtLCNO" runat="server" Width="150px" TabIndex="9"></asp:TextBox> </div>
                            <div style="float:left;width:50%" align="right"> Import PI No: <asp:TextBox ID="txtImportPINo" runat="server" Width="150px" TabIndex="9"></asp:TextBox>  </div>
                            
                            <div style="float:left;width:20%" align="right"> Date:</div>  <div style="float:left;width:20%" align="left"> <asp:TextBox ID="txtPDate" runat="server" Width="70px" TabIndex="6"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txtPDate_CalendarExtender" runat="server" 
                                        Enabled="True" TargetControlID="txtPDate">
                                    </cc1:CalendarExtender></div>
                            <div style="float:left;width:60%" align="right"> Notes: <asp:TextBox ID="txtNotes" runat="server" Width="300px" TabIndex="9"></asp:TextBox>  </div>
                        </div>
                        <div>
                         
                              <div style="float:left;width:100%; padding-left:10%; padding-top:10px " align="left">Yarn Group <asp:DropDownList ID="ddlYarnGroup" runat="server" AutoPostBack="True" 
                                        Height="22px" onselectedindexchanged="ddlYarnGroup_SelectedIndexChanged" 
                                        TabIndex="1" Width="180px">
                                    </asp:DropDownList>
                                    Yarn: <asp:DropDownList ID="ddlYarn" runat="server" Height="22px" TabIndex="1" 
                                         Width="250px" >
                                     </asp:DropDownList> 
                                     <div style="padding-top:3px">
                                    
                                     Qty: <asp:TextBox ID="txtTotQty" runat="server" Width="100px" Height="20px" TabIndex="5"></asp:TextBox>
                                      Unit Price: <asp:TextBox ID="txtTotPrice" runat="server" Width="120px" Height="20px" TabIndex="5"></asp:TextBox>
                                     <asp:Button ID="btnAdd" runat="server" Height="22px"  Text="Add" 
                                        Width="60px" onclick="btnAdd_Click" Font-Names="Georgia" TabIndex="6" />
                                        
                                     <asp:Button ID="Button1" runat="server" Height="22px"  Text="Clear" 
                                        Width="60px"  Font-Names="Georgia" TabIndex="6" onclick="Button1_Click" />
                                        
                                      </div>
                                    
                                    </div> 
                            
                        </div>
                        <div>
                               
                             <asp:Label ID="lblMsg" runat="server" Font-Bold="True" Font-Size="Smaller" 
                                         ForeColor="#CC3300"></asp:Label>
                              <asp:HiddenField ID="HiddenField1" runat="server" Value="-1" />
                        </div>
                        <div>
                                        
                                    <asp:UpdateProgress ID="UpdateProgress3" runat="server" 
                                        AssociatedUpdatePanelID="updPanel" DisplayAfter="10">
                                    <ProgressTemplate>
                                      <img id="dd" alt="Loading.." src="Images/loading.gif" />
                                    </ProgressTemplate>
                                    </asp:UpdateProgress>
                        </div>
                       
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        
                    </asp:Panel>
              
                <div style="height: 8px; border-bottom-style: groove; border-bottom-color: #003399;">
                
                </div>
                    <!-- Content Goes Here! -->
                    <asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        Total LC Value :<asp:Label ID="lblTotal" runat="server" Text=""></asp:Label><asp:GridView 
                                ID="gvProducts" runat="server" 
                                 AutoGenerateColumns="False" 
                                AllowPaging="True" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                onpageindexchanging="gvProducts_PageIndexChanging" PageSize="6">
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="«" LastPageText="»" />      
                                <Columns>
                                   
       
     
                                    <asp:BoundField 
                                        HeaderText="ID" DataField="ID" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="YID" HeaderText="YID" />
                                    <asp:BoundField DataField="YGID" HeaderText="YGID" />
                                    <asp:BoundField 
                                        HeaderText="Yarn Name" DataField="Yarn" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                     <asp:BoundField DataField="Supplier" HeaderText="Supplier" >
                                     <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>   
                                      
                                    <asp:BoundField DataField="Country" HeaderText="Country" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                      
                                  
                                     
                                           <asp:BoundField DataField="LCNo" HeaderText="LCNo" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                      
                                      <asp:BoundField DataField="LCQty" HeaderText="LCQty" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                     
                                      
                                    
                                     <asp:BoundField DataField="YarnLCValue" HeaderText="Unit Price($)" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField> 
                                     
                                      <asp:BoundField DataField="YarnTotalLCValue" HeaderText="Total Price($)" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField> 
                                     
                                     <asp:BoundField DataField="Date" HeaderText="Date" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                 
                                <%-- <asp:BoundField DataField="Notes" HeaderText="Notes" >
                                    <HeaderStyle Wrap="true" Width="10px"/>
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  --%>
                                      <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/btn_edit.gif" 
                                                onclick="btnEdit_Click" Height="16px" />
                                            
                                           
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" 
                                                ImageUrl="~/Images/btn_delete.gif" onclick="btnDelete_Click" 
                                                Height="16px" 
                                            onclientclick="javascript:return confirm('Are you sure you want to delete this row?');" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                                
              <%--      <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>--%>
                    
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnload" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>                     
                    </ContentTemplate>
                        
                        </asp:UpdatePanel> 
                </div>
                
                
                </div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>      
    </div>
</asp:Content>
