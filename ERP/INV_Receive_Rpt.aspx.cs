﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
using System.Reflection;

namespace ERP
{
    public partial class INV_Receive_Rpt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadBasicData();
            }
        }
        private void LoadBasicData()
        {

            string Head = new Inventory_DL().GetInventoryType(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            this.Title = Head + " - Inventory Receive";
            lblHead.Text = "Receive Report For " + Head;
          

            Inventory_DL objDL = new Inventory_DL();
            ddlYarnGroup.DataSource = objDL.GetItemGroup(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlYarnGroup.DataTextField = "GroupName";
            ddlYarnGroup.DataValueField = "SL";
            ddlYarnGroup.DataBind();
            ddlYarnGroup.Items.Insert(0, "All Group");

            ddlType.DataSource = objDL.GetReceiveType(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlType.DataTextField = "TypeName";
            ddlType.DataValueField = "ID";
            ddlType.DataBind();

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            List<EINV_Receive> liProd = new List<EINV_Receive>();
            Int64 gid = 0;

            if (ddlYarnGroup.SelectedItem.Text == "All Group")
            {
                gid = 0;
            }
            else
            {
                gid = Convert.ToInt64(ddlYarnGroup.SelectedValue.ToString());
            }
            liProd = new Inventory_DL().GetInventoryReceiveReport(Convert.ToInt32(Request.QueryString["INVTypeID"]), Convert.ToInt32(gid), Convert.ToInt32(ddlType.SelectedValue), txtFrom0.Text, txtFrom1.Text);



            Export(lblHead.Text+"_Receive_Report_" + txtFrom0.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, liProd);
        }
        public void Export(string fileName, List<EINV_Receive> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2>" + lblHead.Text + " From" + txtFrom0.Text + " To" + txtFrom1.Text + " </h2>  </div>");
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            HttpContext.Current.Response.Write("<Td><B>Sl</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Receive Date</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Country</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Supplier</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>InvoiceNo</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>LCNo</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>GatePassNo</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>LoanCompany</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Group Name</B></Td>");
            //HttpContext.Current.Response.Write("<Td><B>Sub Group</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Item Name</B></Td>");
            
            //HttpContext.Current.Response.Write("<Td><B>BoxQty</B></Td>");
            //HttpContext.Current.Response.Write("<Td><B>BoxUNIT</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>TotalQty</B></Td>");
          

            HttpContext.Current.Response.Write("<Td><B>Unit</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Unit Price</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Total Price</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Cash Memo No</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Purchaser Name</B></Td>");

            HttpContext.Current.Response.Write("<Td><B>Notes</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>ReceiveType</B></Td>");
                            
            
            HttpContext.Current.Response.Write("</TR>");
            int i=0;
            foreach (EINV_Receive emp in empList)
            {
               
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<Td>" + i++ + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.RDate.Value.ToString("dd-MMM-yyyy") + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.Country + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.Supplier + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.InvoiceNo + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.LCNo + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.GatePassNo + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.LoanCompany + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.GroupName + "</Td>");
                //HttpContext.Current.Response.Write("<Td>" + emp.ParentItem + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.ItemName + "</Td>");
                
                //HttpContext.Current.Response.Write("<Td>" + emp.BoxQty + "</Td>");
                //HttpContext.Current.Response.Write("<Td>" + emp.BoxUNIT + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.TotalQty + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.Unit + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.UnitPrice + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.TotalAmount + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.CashMemo + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.Purchaser + "</Td>");

                HttpContext.Current.Response.Write("<Td>" + emp.Notes + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.ReceiveType + "</Td>");
               
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }
    }
}
