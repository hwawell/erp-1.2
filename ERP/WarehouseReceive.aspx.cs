﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDLayer;
namespace ERP
{
    public partial class WebForm31 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (new Utilities_DL().CheckSecurity("501", USession.SUserID) == true)
                //{

               
                    ddColor.DataSource = new Order_DL().GetOrderProductColor(1);
                    ddColor.DataValueField = "OPDID";
                    ddColor.DataTextField = "Color";
                    ddColor.DataBind();
                    ddColor.Items.Add("--Select--");
                    ddColor.Text = "--Select--";
                

                    loadOrder();
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}

            }
        }
        private void loadOrder()
        {
           
            ddOrder.DataSource = new Order_DL().GetActiveOrderList();

            ddOrder.DataTextField = "OrderNo";
            ddOrder.DataValueField = "OrderNo";

            ddOrder.DataBind();
            ddOrder.Items.Insert(0,"Select");
            ddOrder.Text = "Select";



        }
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                ddPcard.DataSource = new ProcessCard_CL().GetPCardForOrder(ddOrder.SelectedValue);
                ddPcard.DataValueField = "PCardNo";
                ddPcard.DataTextField = "Name";
                ddPcard.DataBind();
                ddPcard.Items.Add("--Select--");
                ddPcard.Text = "--Select--";
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownList4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txtQty_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
