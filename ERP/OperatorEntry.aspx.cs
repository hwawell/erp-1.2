﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDLayer;
namespace ERP
{
    public partial class OperatorEntry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.rblNormal.Items.FindByText("Active").Selected = true;
                loadGridData();
            }
        }
        private void loadGridData()
        {


            gvProducts.Columns[3].Visible = true;
            gvProducts.Columns[4].Visible = true;
            gvProducts.Columns[5].Visible = false;
            gvProducts.DataSource = new Utilities_DL().GetActiveOperator();
            gvProducts.DataBind();

        }
        private void loadDeletedData()
        {

            gvProducts.Columns[3].Visible = false;
            gvProducts.Columns[4].Visible = false;
            gvProducts.Columns[5].Visible = true;
            gvProducts.DataSource = new Utilities_DL().GetDeletedOperator();
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            txtOperator.Text = (row.Cells[1].Text);
            txtOthers.Text = (row.Cells[2].Text);

            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Utilities_DL().DeleteActiveOperator(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Utilities_DL().DeleteActiveDesc(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            Operator c = new Operator();
            c.SL = Convert.ToInt64(hdnCode.Value);
            c.OperatorName = txtOperator.Text.ToString();
            c.Section = txtOthers.Text.ToString();


            if (txtOperator.Text.Length > 0)
            {
                new Utilities_DL().SaveOperator(c);
            }
            loadGridData();
            //loadGridData();
            updPanel.Update();
            mpeProduct.Hide();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Operator c = new Operator();
            c.SL = Convert.ToInt64(hdnCode.Value);
            c.OperatorName = txtOperator.Text.ToString();
            c.Section = txtOthers.Text.ToString();


            if (txtOperator.Text.Length > 0)
            {
                new Utilities_DL().SaveOperator(c);
            }
            //loadGridData();
            //loadGridData();
            txtOperator.Text = "";
            txtOthers.Text = "";

            updPanel.Update();
            mpeProduct.Show();
            txtOperator.Focus();

        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            txtOperator.Text = string.Empty;
            txtOthers.Text = string.Empty;




            mpeProduct.Show();
            txtOperator.Focus();
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblNormal.Items.FindByText("Active").Selected == true)
            {
                loadGridData();

            }
            else
            {
                loadDeletedData();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            loadGridData();
        }
    }
}
