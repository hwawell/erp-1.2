﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

using BDLayer;

namespace ERP
{
    public partial class WebForm6 : System.Web.UI.Page
    {


        
        string gvUniqueID = String.Empty;
        int gvNewPageIndex = 0;
        int gvEditIndex = -1;
        string gvSortExpr = String.Empty;
        private string gvSortDir
        {
           
            get { return ViewState["SortDirection"] as string ?? "ASC"; }

            set { ViewState["SortDirection"] = value; }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlCustomer.DataSource = new Customer_DL().GetActiveCustomers();
                ddlCustomer.DataTextField = "CName";
                ddlCustomer.DataValueField = "CustID";
               
                ddlCustomer.DataBind();
                ddlCustomer.Items.Add("Select");
                ddlCustomer.Text = "Select";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                 Order o = new Order();
                o.PINo =  txtOrderNo.Text.Trim();
                o.CustID = Convert.ToInt64(ddlCustomer.Text);
                o.PDate = Convert.ToDateTime(txtPDate.Text).Date;
                o.RcvDate = Convert.ToDateTime(txtRcvDate.Text).Date;
                o.ShpDate = Convert.ToDateTime(txtShpDate.Text).Date;
                o.LCNo = (txtLCNo.Text);

                new Order_DL().Save(o);
                lblShow.Text = "Data Saved Successfully";
            }
            catch(Exception )
            {
                //ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Save Not Possible,Input Valid data!!!');</script>");
                lblShow.Text = "Data not Saved,Input valid Data";

            }
           
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            clear();
        }
        private void clear()
        {
            txtOrderNo.Text = "";
            txtPDate.Text = "";
            txtShpDate.Text = "";
            txtRcvDate.Text = "";
            txtLCNo.Text = "";
            ddlCustomer.Text = "Select";
            lblShow.Text = "";

        }

    

        

        ////This procedure returns the Sort Direction
        private string GetSortDirection()
        {
            switch (gvSortDir)
            {
                case "ASC":
                    gvSortDir = "DESC";
                    break;

                case "DESC":
                    gvSortDir = "ASC";
                    break;
            }
            return gvSortDir;
        }

        ////This procedure prepares the query to bind the child GridView
        //////private AccessDataSource ChildDataSource(string strCustometId, string strSort)
        //////{
        //////    string strQRY = "";
        //////    AccessDataSource dsTemp = new AccessDataSource();
        //////    dsTemp.DataFile = "App_Data/Northwind.mdb";
        //    ////strQRY = "SELECT [Orders].[CustomerID],[Orders].[OrderID]," +
        //    ////                        "[Orders].[ShipAddress],[Orders].[Freight],[Orders].[ShipName] FROM [Orders]" +
        //    ////                        " WHERE [Orders].[CustomerID] = '" + strCustometId + "'" +
        //    ////                        "UNION ALL " +
        //    ////                        "SELECT '" + strCustometId + "','','','','' FROM [Orders] WHERE [Orders].[CustomerID] = '" + strCustometId + "'" +
        //////                            "HAVING COUNT(*)=0 " + strSort;

        //////    dsTemp.SelectCommand = strQRY;
        //////    return dsTemp;
        //////}



        #region GridView1 Event Handlers
        //This event occurs for each row
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            string strSort = string.Empty;

            // Make sure we aren't in header/footer rows
            if (row.DataItem == null)
            {
                return;
            }

            //Find Child GridView control
            GridView gv = new GridView();
            gv = (GridView)row.FindControl("GridView2");

            //Check if any additional conditions (Paging, Sorting, Editing, etc) to be applied on child GridView
            if (gv.UniqueID == gvUniqueID)
            {
                gv.PageIndex = gvNewPageIndex;
                gv.EditIndex = gvEditIndex;
                //Check if Sorting used
                //////if (gvSortExpr != string.Empty)
                //////{
                //////    GetSortDirection();
                //////    strSort = " ORDER BY " + string.Format("{0} {1}", gvSortExpr, gvSortDir);
                //////}
                //Expand the Child grid
                ClientScript.RegisterStartupScript(GetType(), "Expand", "<SCRIPT LANGUAGE='javascript'>expandcollapse('div" + ((DataRowView)e.Row.DataItem)["PINo"].ToString() + "','one');</script>");
            }
            //string pino = "HW0110449";//e.Row.Cells[0].ToString();
            Label btn = e.Row.FindControl("lblPIID") as Label;
            string pi = btn.Text;




            //Prepare the query for Child GridView by passing the Customer ID of the parent row
            //gv.DataSource = new Order_DL().GetOrderProduct(pi);  //ChildDataSource(((DataRowView)e.Row.DataItem)["CustomerID"].ToString(), strSort);
            //gv.DataBind();

            //Add delete confirmation message for Customer
            LinkButton l = (LinkButton)e.Row.FindControl("linkDeleteCust");
            l.Attributes.Add("onclick", "javascript:return " +
            "confirm('Are you sure you want to delete this Customer " +
            DataBinder.Eval(e.Row.DataItem, "PINo") + "')");
           
        }

        //This event occurs for any operation on the row of the grid
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //Check if Add button clicked
            if (e.CommandName == "AddCustomer")
            {
                try
                {
                    //Get the values stored in the text boxes
                    string strCID = ((DropDownList)GridView1.FooterRow.FindControl("ddlCustomer")).Text;
                    string strPDate = ((TextBox)GridView1.FooterRow.FindControl("txtPDATE1")).Text;
                    string strRDate = ((TextBox)GridView1.FooterRow.FindControl("txtRcvDate1")).Text;
                    string strSDate = ((TextBox)GridView1.FooterRow.FindControl("txtShpDate1")).Text;
                    string strLCNo = ((TextBox)GridView1.FooterRow.FindControl("txtLCNo")).Text;

                    //string strContactTitle = ((TextBox)GridView1.FooterRow.FindControl("txtContactTitle")).Text;
                    //string strAddress = ((TextBox)GridView1.FooterRow.FindControl("txtAddress")).Text;
                    string strPID = ((TextBox)GridView1.FooterRow.FindControl("txtPIID")).Text;

                    //Prepare the Insert Command of the DataSource control
                    //string strSQL = "";
                    //strSQL = "INSERT INTO Customers (CustomerID, CompanyName, ContactName, " +
                    //        "ContactTitle, Address) VALUES ('" + strCustomerID + "','" + strCompanyName + "','" +
                    //        strContactName + "','" + strContactTitle + "','" + strAddress + "')";
                    Order o = new Order();
                    o.PINo = strPID;
                    o.CustID = Convert.ToInt64(strCID);
                    o.PDate = Convert.ToDateTime(strPDate).Date;
                    o.RcvDate = Convert.ToDateTime(strRDate).Date;
                    o.ShpDate = Convert.ToDateTime(strSDate).Date;
                    o.LCNo = (strLCNo);

                    new Order_DL().Save(o);
                    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Customer added successfully');</script>");

                    //Re bind the grid to refresh the data
                    GridView1.DataSource = new Order_DL().GetActiveRecord();
                    GridView1.DataBind();
                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + ex.Message.ToString().Replace("'", "") + "');</script>");
                }
            }
        }

        //This event occurs on click of the Update button
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                string strCID = ((DropDownList)GridView1.FooterRow.FindControl("ddlCustomer")).Text;
                string strPDate = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtPDATE")).Text;
                string strRDate = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtRcvDate")).Text;
                string strSDate = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtShpDate")).Text;
                string strLCno = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtLCNo")).Text;



                //string strContactTitle = ((TextBox)GridView1.FooterRow.FindControl("txtContactTitle")).Text;
                //string strAddress = ((TextBox)GridView1.FooterRow.FindControl("txtAddress")).Text;
                string strPID = ((Label)GridView1.Rows[e.RowIndex].FindControl("lblPIID")).Text;

                //Prepare the Insert Command of the DataSource control
                //string strSQL = "";
                //strSQL = "INSERT INTO Customers (CustomerID, CompanyName, ContactName, " +
                //        "ContactTitle, Address) VALUES ('" + strCustomerID + "','" + strCompanyName + "','" +
                //        strContactName + "','" + strContactTitle + "','" + strAddress + "')";
                Order o = new Order();
                o.PINo = strPID;
                o.CustID = Convert.ToInt64(strCID);
                o.PDate = Convert.ToDateTime(strPDate);
                o.RcvDate = Convert.ToDateTime(strRDate).Date;
                o.ShpDate = Convert.ToDateTime(strSDate).Date;
                o.LCNo = (strLCno);

                new Order_DL().Save(o);
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Order Updated successfully');</script>");
            }
            catch { }

        }

        //This event occurs after RowUpdating to catch any constraints while updating
        protected void GridView1_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            //Check if there is any exception while deleting
            if (e.Exception != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + e.Exception.Message.ToString().Replace("'", "") + "');</script>");
                e.ExceptionHandled = true;
            }
        }

        //This event occurs on click of the Delete button
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //Get the value        
            string strCustomerID = ((Label)GridView1.Rows[e.RowIndex].FindControl("lblPIID")).Text;

            //Prepare the delete Command of the DataSource control


            try
            {
                new Order_DL().OrderUpdate(strCustomerID, "D", USession.SUserID);
                GridView1.DataBind();

                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Order deleted successfully');</script>");
            }
            catch { }
        }

        //This event occurs after RowDeleting to catch any constraints while deleting
        protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            //Check if there is any exception while deleting
            if (e.Exception != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + e.Exception.Message.ToString().Replace("'", "") + "');</script>");
                e.ExceptionHandled = true;
            }
        }
        #endregion

        #region GridView2 Event Handlers
        protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView gvTemp = (GridView)sender;
            gvUniqueID = gvTemp.UniqueID;
            gvNewPageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "AddOrder")
            {
                try
                {
                    GridView gvTemp = (GridView)sender;
                    gvUniqueID = gvTemp.UniqueID;

                    //Get the values stored in the text boxes
                    string strPIID = gvTemp.DataKeys[0].Value.ToString();  //Customer ID is stored as DataKeyNames
                    string strDesc = ((TextBox)gvTemp.FooterRow.FindControl("txtDesc")).Text;
                    string strNotes = ((TextBox)gvTemp.FooterRow.FindControl("txtNotes")).Text;
                    string strWeight = ((TextBox)gvTemp.FooterRow.FindControl("txtWeight")).Text;
                    string strWidth = ((TextBox)gvTemp.FooterRow.FindControl("txtWidth")).Text;
                    string strUnit = ((TextBox)gvTemp.FooterRow.FindControl("txtUnit")).Text;
                    string lngPerPrice = ((TextBox)gvTemp.FooterRow.FindControl("txtPerUnitPrice")).Text;

                    //Prepare the Insert Command of the DataSource control
                    OrderProduct op = new OrderProduct();
                    op.OPID = 0;
                    op.PINO = strPIID;
                    op.Description = strDesc;
                    op.Notes = strNotes;
                    op.Weight = (strWeight);
                    op.Width = (strWidth);
                    op.Unit = strUnit;
                    decimal price;
                    if (lngPerPrice.Length < 1)
                        price = 0;
                    else
                        price = Convert.ToDecimal(lngPerPrice);

                   

                    new Order_DL().SaveOP(op);


                    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Order added successfully');</script>");

                    GridView1.DataBind();
                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + ex.Message.ToString().Replace("'", "") + "');</script>");
                }
            }
        }

        protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView gvTemp = (GridView)sender;
            gvUniqueID = gvTemp.UniqueID;
            gvEditIndex = e.NewEditIndex;

        }

        protected void GridView2_CancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView gvTemp = (GridView)sender;
            gvUniqueID = gvTemp.UniqueID;
            gvEditIndex = -1;
            GridView1.DataBind();
        }

        protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                GridView gvTemp = (GridView)sender;
                gvUniqueID = gvTemp.UniqueID;


                string strPIID = gvTemp.DataKeys[0].Value.ToString();  //Customer ID is stored as DataKeyNames
                string strDesc = ((TextBox)gvTemp.Rows[e.RowIndex].FindControl("txtDesc")).Text;
                string strNotes = ((TextBox)gvTemp.Rows[e.RowIndex].FindControl("txtNotes")).Text;
                string strWeight = ((TextBox)gvTemp.Rows[e.RowIndex].FindControl("txtWeight")).Text;
                string strWidth = ((TextBox)gvTemp.Rows[e.RowIndex].FindControl("txtWidth")).Text;
                string strUnit = ((TextBox)gvTemp.Rows[e.RowIndex].FindControl("txtUnit")).Text;
                string lngPerPrice = ((TextBox)gvTemp.Rows[e.RowIndex].FindControl("txtPerUnitPrice")).Text;
                long OPID = Convert.ToInt64(((Label)gvTemp.Rows[e.RowIndex].FindControl("lblOPID")).Text);
                //Get the values stored in the text boxes
                OrderProduct op = new OrderProduct();
                op.OPID = OPID;
                op.PINO = strPIID;
                op.Description = strDesc;
                op.Notes = strNotes;
                op.Weight = (strWeight);
                op.Width = (strWidth);
                op.Unit = strUnit;
                decimal price;
                if (lngPerPrice.Length < 1)
                    price = 0;
                else
                    price = Convert.ToDecimal(lngPerPrice);

              

                new Order_DL().SaveOP(op);

                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Order updated successfully');</script>");

                //Reset Edit Index
                gvEditIndex = -1;

                GridView1.DataBind();
            }
            catch { }
        }

        protected void GridView2_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            //Check if there is any exception while deleting
            if (e.Exception != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + e.Exception.Message.ToString().Replace("'", "") + "');</script>");
                e.ExceptionHandled = true;
            }
        }

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridView gvTemp = (GridView)sender;
            gvUniqueID = gvTemp.UniqueID;

            //Get the value        
            long lngOrderID = Convert.ToInt64(((Label)gvTemp.Rows[e.RowIndex].FindControl("lblOPID")).Text);

            //Prepare the Update Command of the DataSource control


            try
            {
                new Order_DL().OrderProductUpdate(lngOrderID, "D", USession.SUserID);
                GridView1.DataBind();
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Order deleted successfully');</script>");
                GridView1.DataBind();
            }
            catch { }
        }

        protected void GridView2_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            //Check if there is any exception while deleting
            if (e.Exception != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + e.Exception.Message.ToString().Replace("'", "") + "');</script>");
                e.ExceptionHandled = true;
            }
        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //Check if this is our Blank Row being databound, if so make the row invisible
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (((DataRowView)e.Row.DataItem)["OPID"].ToString() == String.Empty) e.Row.Visible = false;
            }
        }

        protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
        {
            GridView gvTemp = (GridView)sender;
            gvUniqueID = gvTemp.UniqueID;
            gvSortExpr = e.SortExpression;
            GridView1.DataBind();
        }
        #endregion

        //protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        //{
        //    e.Arguments.StartRowIndex = 0;
        //    e.Arguments.MaximumRows = 5;                //add your paging limit requirement here
        //    //DataClassesDataContext dc = new DataClassesDataContext();

        //    e.Arguments.TotalRowCount = 9;   //you could store this value or cache
        //    //it to avoid the extra DB hit

        //    //uses an example of ten records/page modify to fit your own paging
        //    //requirements   
        //   // e.Result = new Order_DL().GetOrderProduct(txtOrderNo.Text).Skip(GridView1.PageIndex * 5).Take(5);

        //}

       
    }
}
