﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_RPT_SubContact.aspx.cs" Inherits="ERP.KNIT_RPT_SubContact" Title="Untitled Page" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>
<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();
            List<EKnitingSubContactReport> liProd = new List<EKnitingSubContactReport>();
            this.Window1.Title = "Knit Production Balance Sheet";
            liProd = new KNIT_all_operation().GetKnitingSubContactReport(Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text));
                    
            
            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
   
  
    
    
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div><h3> Sub Contact Grey Receive Report </h3></div>

<div> From 
    <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox> To 
    <asp:TextBox ID="txtTo" runat="server"></asp:TextBox></div>
     <cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom">
                    </cc1:CalendarExtender>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                        Enabled="True" TargetControlID="txtTo">
                    </cc1:CalendarExtender>
                   <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                    </div>
    <div>
    </div>
    <div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
            
            >
            <Reader>
            <ext:JsonReader IDProperty="ID">
                    <Fields>
                    
                   
                     
                         <ext:RecordField Name="SubContactCompanyName" />
                          <ext:RecordField Name="FabricConstruction" />
                           <ext:RecordField Name="MCNo" />
                           
                        <ext:RecordField Name="GM" />
                         <ext:RecordField Name="GSM" />
                         <ext:RecordField Name="ReceiveDate" />
                         <ext:RecordField Name="ReceiveQty" />
                         
                          <ext:RecordField Name="ChallanNo" />
                           <ext:RecordField Name="Notes" />
                         
                        
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Sub Contact Grey Receive Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            
                            
                         
                             <ext:Column ColumnID="SubContactCompanyName" Header="SubContactCompanyName"  DataIndex="SubContactCompanyName" Sortable="true" />
                            <ext:Column ColumnID="FabricConstruction" Header="FabricConstruction"  DataIndex="FabricConstruction" Sortable="true" />
                           <ext:Column ColumnID="MCNo" Header="MCNo"  DataIndex="MCNo" Sortable="true" />
                            <ext:Column ColumnID="GM" Header="GM"  DataIndex="GM" Sortable="true" />
                            <ext:Column ColumnID="GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                            
                            <ext:Column ColumnID="ChallanNo" Header="ChallanNo"  DataIndex="ChallanNo" Sortable="true" />
                            
                            
                            <ext:Column ColumnID="ReceiveDate" Header="ReceiveDate"  DataIndex="ReceiveDate" Sortable="true" />
                            <ext:Column ColumnID="ReceiveQty" Header="ReceiveQty"  DataIndex="ReceiveQty" Sortable="true" />
                            <ext:Column ColumnID="Notes" Header="Notes"  DataIndex="Notes" Sortable="true" />
                            
                            
                               
                                                       
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                             
                                <ext:StringFilter DataIndex="SubContactCompanyName" />
                                <ext:StringFilter DataIndex="FabricConstruction" />
                                <ext:StringFilter DataIndex="GM" />
                                <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="ReceiveDate" />
                                <ext:StringFilter DataIndex="ReceiveQty" />
                                 <ext:StringFilter DataIndex="ChallanNo" />
                                  <ext:StringFilter DataIndex="Notes" />
                               
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>
