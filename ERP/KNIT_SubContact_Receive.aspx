<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_SubContact_Receive.aspx.cs" Inherits="ERP.KNIT_SubContact_Receive" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style4
        {
        width: 143px;
    }
        .style5
        {
        width: 47px;
    }
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
        .style6
        {
            width: 47px;
            height: 2px;
        }
        .style7
        {
            width: 143px;
            height: 2px;
        }
        .style8
        {
            height: 2px;
        }
              
              .mGrid { 
    width: 100%; 
    background-color: #fff; 
    margin: 5px 0 10px 0; 
    border: solid 1px #525252; 
    border-collapse:collapse; 
}
.mGrid td { 
    padding: 2px; 
    border: solid 1px #c1c1c1; 
    color: #717171; 
}
.mGrid th { 
    padding: 4px 2px; 
    color: #fff; 
    background: #424242 url(grd_head.png) repeat-x top; 
    border-left: solid 1px #525252; 
    font-size: 0.7em; 
}
.mGrid .alt { background: #fcfcfc url(grd_alt.png) repeat-x top; }
.mGrid .pgr { background: #424242 url(grd_pgr.png) repeat-x top; }
.mGrid .pgr table { margin: 5px 0; }
.mGrid .pgr td { 
    border-width: 0; 
    padding: 0 6px; 
    border-left: solid 1px #666; 
    font-weight: bold; 
    color: #fff; 
    line-height: 12px; 
 }   
.mGrid .pgr a { color: #666; text-decoration: none; }
.mGrid .pgr a:hover { color: #000; text-decoration: none; }
 
    </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
    <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Script/jquery-1.8.2.js") %>"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Script/jquery.number.js") %>"></script>

 <script type="text/javascript">

     $(function() {
         $('#<%=btnSave.ClientID %>').click(function(evt) {
             // Validate the form and retain the result.
         var valuePer = 0.0;
         if (!isNaN(parseFloat($('#<%=txtKG.ClientID %>').val().replace(',', '')))) {
                  valuePer = valuePer + parseFloat($('#<%=txtKG.ClientID %>').val().replace(',', ''));
               }
               if (!isNaN(parseFloat($('#<%=txtKG0.ClientID %>').val().replace(',', '')))) {
                   valuePer = valuePer + parseFloat($('#<%=txtKG0.ClientID %>').val().replace(',', ''));
               }
               if (!isNaN(parseFloat($('#<%=txtKG1.ClientID %>').val().replace(',', '')))) {
                   valuePer = valuePer + parseFloat($('#<%=txtKG1.ClientID %>').val().replace(',', ''));
               }
               if (!isNaN(parseFloat($('#<%=txtKG2.ClientID %>').val().replace(',', '')))) {
                   valuePer = valuePer + parseFloat($('#<%=txtKG2.ClientID %>').val().replace(',', ''));
               }
               if (!isNaN(parseFloat($('#<%=txtKG3.ClientID %>').val().replace(',', '')))) {
                   valuePer = valuePer + parseFloat($('#<%=txtKG3.ClientID %>').val().replace(',', ''));
               }


               if (valuePer != $('#<%=txtTot.ClientID %>').val().replace(',','')) {
                   alert('Yarn Consumption (=' + valuePer + ' ) must be equal to Production Qty (' + $('#<%=txtTot.ClientID %>').val() + ')');
                 evt.preventDefault();
             }

         });

     });
   
     $(function() {
         $('#<%=txtUserPercent.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtTot.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtTot.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;


             totValue = $.number(totValue, 2);
             $('#<%=txtKG.ClientID %>').val(totValue);
            

         });
     });

     $(function() {
         $('#<%=txtUserPercent0.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtTot.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtTot.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;
             totValue = $.number(totValue, 2);
             $('#<%=txtKG0.ClientID %>').val(totValue);

         });
     });

     $(function() {
         $('#<%=txtUserPercent1.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtTot.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtTot.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;
             totValue = $.number(totValue, 2);
             $('#<%=txtKG1.ClientID %>').val(totValue);

         });
     });

     $(function() {
         $('#<%=txtUserPercent2.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtTot.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtTot.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;
             totValue = $.number(totValue, 2);
             $('#<%=txtKG2.ClientID %>').val(totValue);

         });
     });

     $(function() {
         $('#<%=txtUserPercent3.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtTot.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtTot.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;
             totValue = $.number(totValue, 2);
             $('#<%=txtKG3.ClientID %>').val(totValue);

         });
     });
    </script>
  
    <style type="text/css">
        .style3
        {
            width: 191px;
        }
        .style4
        {
            width: 142px;
        }
        .style5
        {
            width: 54px;
        }
        .style6
        {
            width: 193px;
        }
        .style7
        {
            width: 269px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<div>
   <div style="height:30px; " align="center">
     <h3> Sub Contact Grey Receive Information</h3>
   </div>
   <fieldset>
   <legend>Product Receive</legend>
   
 
    
      
       
    <div style="width: 20%; float: left" align="right">
        Fabric Type :</div>
   <div style="width: 80%; float: left" align="left">
    
        <asp:DropDownList ID="ddlFabricType" runat="server" AutoPostBack="True" 
                          Height="25px" 
                          TabIndex="3" Width="235px" 
            onselectedindexchanged="ddlFabricType_SelectedIndexChanged">
          </asp:DropDownList>
      
   </div>
    <div style="width: 20%; float: left" align="right">
        Fabric :</div>
   <div style="width: 80%; float: left" align="left">
   <asp:UpdatePanel ID="upFabric" runat="server" UpdateMode="Conditional">
   <ContentTemplate>
   
 
        <asp:DropDownList ID="ddlFabric" runat="server" AutoPostBack="True" 
                          Height="25px" 
                          TabIndex="3" Width="400px">
          </asp:DropDownList>
              <cc1:ListSearchExtender ID="ListSearchExtender3" runat="server" Enabled="True" 
          IsSorted="true" PromptText="  --" TargetControlID="ddlFabric">
      </cc1:ListSearchExtender>
            </ContentTemplate>
       <Triggers>
           <asp:AsyncPostBackTrigger ControlID="ddlFabricType" 
               EventName="SelectedIndexChanged" />
       </Triggers>
   </asp:UpdatePanel>
  
   </div>
    <div style="width: 20%; float: left" align="right">
        GM :</div>
   <div style="width: 15%; float: left" align="left">
   
       <asp:TextBox ID="txtGM" runat="server"></asp:TextBox>
   
   </div>
    <div style="width: 10%; float: left" align="right">
        GSM :</div>
   <div style="width: 55%; float: left" align="left">
   
       <asp:TextBox ID="txtGSM" runat="server"></asp:TextBox>
   
       Receive Date
       <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
   <cc1:CalendarExtender ID="txtTo_CalendarExtender" runat="server" Enabled="True" 
                        TargetControlID="txtDate">
                    </cc1:CalendarExtender>
   </div>
   
       
          <div style="width: 20%; float: left" align="right">
         Receive Qty (KG) :</div>
   <div style="width: 80%; float: left" align="left">
   
       <asp:TextBox ID="txtTot"   runat="server" MaxLength="20" TabIndex="102" 
                 Height="23px" Width="131px" Font-Bold="True"></asp:TextBox>
   
   </div>
    <div style="width: 20%; float: left" align="right">
        Sub Contact Company :</div>
   <div style="width: 80%; float: left" align="left">
    
       <asp:TextBox ID="txtSubContact" runat="server" Height="22px"  
           Width="547px"></asp:TextBox>
      
   </div>
    <div style="width: 20%; float: left" align="right">
        Notes :</div>
   <div style="width: 70%; float: left" align="left">
    
       <asp:TextBox ID="txtNotes" runat="server" Height="22px" TextMode="SingleLine" 
           Width="500px"></asp:TextBox>
      
   </div>
     <div style="width: 20%; float: left" align="right">
        Challan No :</div>
   <div style="width: 80%; float: left" align="left">
    
       <asp:TextBox ID="txtChallan" runat="server" Height="22px"  
           Width="547px"></asp:TextBox>
      
   </div>
     <div style="width: 20%; float: left" align="right">
        MC No :</div>
   <div style="width: 80%; float: left" align="left">
       <asp:DropDownList ID="ddlMCNo" runat="server">
       </asp:DropDownList>
      
      
   </div>
   
   </fieldset>
   <div>
      <fieldset>
   <legend> Yarn Composition</legend>
   
        <div>
        
       <table style="width: 100%;">
           <tr>
               <td align="right" class="style3">
                   
                   Yarn Type</td>
               <td align="left" class="style4">
                   
                   <asp:DropDownList ID="ddlYarnType" runat="server" 
                       onselectedindexchanged="ddlYarnType_SelectedIndexChanged" 
                       AutoPostBack="True">
                   </asp:DropDownList>
               </td>
               <td align="left" class="style5">
                   
                   Yarn 1</td>
               <td align="left" class="style6">
                
                    
                     
                     
                          <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                 <ContentTemplate>     
                               <asp:DropDownList ID="ddlYarn" runat="server" Width="200px">
                               </asp:DropDownList>
                           </ContentTemplate>
                                 <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="ddlYarnType" 
                                         EventName="SelectedIndexChanged" />
                                 </Triggers>
                           </asp:UpdatePanel> 
                     
               </td>
               <td align="left" class="style7">
                   Use %<asp:TextBox ID="txtUserPercent" runat="server" Width="57px"></asp:TextBox>KG
                   <asp:TextBox ID="txtKG" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>
               </td>
               <td>
                   &nbsp;</td>
           </tr>
           <tr>
               <td align="right" class="style3">
                   
                   Yarn Type</td>
               <td align="left" class="style4">
                   
                   <asp:DropDownList ID="ddlYarnType0" runat="server" 
                       onselectedindexchanged="ddlYarnType0_SelectedIndexChanged" 
                       AutoPostBack="True">
                   </asp:DropDownList>
               </td>
               <td align="left" class="style5">
                   
                   Yarn 2</td>
               <td align="left" class="style6">
                
                     
                   <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                 <ContentTemplate>               
                   <asp:DropDownList ID="ddlYarn0" runat="server" Width="200px">
                   </asp:DropDownList>
                   </ContentTemplate>
                                 <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="ddlYarnType0" 
                                         EventName="SelectedIndexChanged" />
                                 </Triggers>
                    </asp:UpdatePanel> 
               </td>
               <td align="left" class="style7">
                   Use %<asp:TextBox ID="txtUserPercent0" runat="server" Width="57px"></asp:TextBox>KG
                   <asp:TextBox ID="txtKG0" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>
               </td>
               <td>
                   &nbsp;</td>
           </tr>
           <tr>
               <td align="right" class="style3">
                   
                   Yarn Type</td>
               <td align="left" class="style4">
                   
                   <asp:DropDownList ID="ddlYarnType1" runat="server" 
                       onselectedindexchanged="ddlYarnType1_SelectedIndexChanged" 
                       AutoPostBack="True">
                   </asp:DropDownList>
               </td>
               <td align="left" class="style5">
                   
                   Yarn 3</td>
               <td align="left" class="style6">
                
                     
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                 <ContentTemplate>              
                   <asp:DropDownList ID="ddlYarn1" runat="server" Width="200px">
                   </asp:DropDownList>
                   </ContentTemplate>
                                 <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="ddlYarnType1" 
                                         EventName="SelectedIndexChanged" />
                                 </Triggers>
                           </asp:UpdatePanel> 
               </td>
               <td align="left" class="style7">
                   Use %<asp:TextBox ID="txtUserPercent1" runat="server" Width="57px"></asp:TextBox>KG
                   <asp:TextBox ID="txtKG1" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>
               </td>
               <td>
                   &nbsp;</td>
           </tr>
           <tr>
               <td align="right" class="style3">
                   
                   Yarn Type</td>
               <td align="left" class="style4">
                   
                   <asp:DropDownList ID="ddlYarnType2" runat="server" 
                       onselectedindexchanged="ddlYarnType2_SelectedIndexChanged" 
                       AutoPostBack="True">
                   </asp:DropDownList>
               </td>
               <td align="left" class="style5">
                   
                   Yarn 4</td>
               <td align="left" class="style6">
                
                     
                   <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                 <ContentTemplate>               
                   <asp:DropDownList ID="ddlYarn2" runat="server" Width="200px">
                   </asp:DropDownList>
                   </ContentTemplate>
                                 <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="ddlYarnType2" 
                                         EventName="SelectedIndexChanged" />
                                 </Triggers>
                           </asp:UpdatePanel> 
               </td>
               <td align="left" class="style7">
                   Use %<asp:TextBox ID="txtUserPercent2" runat="server" Width="57px"></asp:TextBox>KG
                   <asp:TextBox ID="txtKG2" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>
               </td>
               <td>
                   &nbsp;</td>
           </tr>
           <tr>
               <td align="right" class="style3">
                   
                   Yarn Type</td>
               <td align="left" class="style4">
                   
                   <asp:DropDownList ID="ddlYarnType3" runat="server" 
                       onselectedindexchanged="ddlYarnType3_SelectedIndexChanged" 
                       AutoPostBack="True">
                   </asp:DropDownList>
               </td>
               <td align="left" class="style5">
                   
                   Yarn 5</td>
               <td align="left" class="style6">
                
                     
                   <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                 <ContentTemplate>               
                   <asp:DropDownList ID="ddlYarn3" runat="server" Width="200px">
                   </asp:DropDownList>
                   </ContentTemplate>
                                 <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="ddlYarnType3" 
                                         EventName="SelectedIndexChanged" />
                                 </Triggers>
                           </asp:UpdatePanel> 
               </td>
               <td align="left" class="style7">
                   Use %<asp:TextBox ID="txtUserPercent3" runat="server" Width="57px"></asp:TextBox>KG
                   <asp:TextBox ID="txtKG3" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>
                   <asp:HiddenField ID="hdnValue" runat="server" />
               </td>
               <td>
                   &nbsp;</td>
           </tr>
           </table>
           
        </div>
        <div>
        </div>
        </fieldset>
   </div>
 
   
  
   
   
   <div>
       <asp:Button ID="btnSave" runat="server" Text="Save" 
           Height="40px" Width="117px" onclick="btnSave_Click" />
       <asp:Button ID="btnCancel" runat="server" Text="Clear" Width="92px" 
           Height="40px" onclick="btnCancel_Click" />
           
            <asp:ImageButton ID="ibtnNew0" runat="server" Height="30px" 
           ImageUrl="~/Images/Rich Text Format.ico" onclick="ibtnNew_Click" 
           ToolTip="View Product Setup Entry" Width="39px" />
           
            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
   </div>
    <%--<asp:UpdatePanel ID="upSave" runat="server" UpdateMode="Conditional">
   <ContentTemplate>--%>
    <asp:Button ID="btnHiddenProduct" runat="Server" Style="display:none"  />
    <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlDataEntry" runat="server"  CssClass="modalBox"  
                        BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                         BorderWidth="2px">
                        <center>
                            <asp:Panel ID="ProductCaption" runat="server"   
                                Style="margin-bottom: 10px; cursor: hand;" 
                                BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                List of Production Setup (Last 20 Records)</asp:Panel></center>
                            <cc1:CollapsiblePanelExtender ID="ProductCaption_CollapsiblePanelExtender" 
                                runat="server" Enabled="True" TargetControlID="ProductCaption">
                            </cc1:CollapsiblePanelExtender>
                         
                       <div>
                               <div>
   
                                    <asp:GridView 
                                ID="gvView" runat="server" 
                                 AutoGenerateColumns="False"
                                GridLines="None"
                                AllowPaging="true"
                                CssClass="mGrid"
                                PagerStyle-CssClass="pgr"
                                AlternatingRowStyle-CssClass="alt"
                                onrowdatabound="gvView_RowDataBound" Height="16px" 
                                ShowFooter="false"
                                Width="400px" PageSize="20" 
                          >
                            
                                 <RowStyle  />
                              
                                <Columns>
                                   
                                    <asp:BoundField  HeaderText="ID" DataField="ID" SortExpression="ID" >
                                       <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    
                                   
                                    
                                   <asp:BoundField 
                                        HeaderText="Fabric" DataField="Fabric" 
                                        SortExpression="Fabric">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="GM" DataField="GM" 
                                        SortExpression="GM">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="GSM" DataField="GSM" 
                                        SortExpression="GSM">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    
                                   
                                    
                                     <asp:BoundField 
                                        HeaderText="Prod. Qty" DataField="ProductionQty" 
                                        SortExpression="ProductionQty">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                     <asp:BoundField 
                                        HeaderText="Date" DataField="Date" 
                                        SortExpression="Date">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    
                                  
                                    
                                     
                                    <asp:TemplateField HeaderText="Edit"  HeaderStyle-HorizontalAlign="Left" >
                                    <ItemTemplate>
                                        <asp:Button ID="btnEdit" runat="server" Text="Edit" 
                                            onclick="btnEdit_Click"
                                        />
                           
                                    </ItemTemplate>
                                        
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    
                                    
                                    <asp:TemplateField HeaderText="Delete"  HeaderStyle-HorizontalAlign="Left" >
                                    <ItemTemplate>
                                        <asp:Button ID="btnClose" runat="server" Text="Delete" 
                                            onclick="btnClose_Click"
                                        />
                           
                                    </ItemTemplate>
                                        
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                   
                                 
                                </Columns>
                                        <PagerStyle CssClass="pgr" />
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                                        <AlternatingRowStyle CssClass="alt" />
                            </asp:GridView>   
                            
                                </div>
                               <div>
                                   <asp:Button ID="Button1" runat="server" Text="Close" Width="60px" 
                                                                    Height="24px" Font-Names="Georgia" 
                                       onclick="btnCancel_Click" />
                               </div>
                       </div>
                    </asp:Panel>
  <%-- </ContentTemplate>
   
   </asp:UpdatePanel>--%>
   
   
</div>
</asp:Content>

