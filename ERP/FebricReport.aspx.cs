﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDLayer;
namespace ERP
{
    public partial class FebricReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadFabricGroup();
                loadOrder();
            }
        }
        private void loadFabricGroup()
        {


            ddGroup.DataSource = new Fabric_CL().GetAllFabricGroup();
            ddGroup.DataValueField = "SL";
            ddGroup.DataTextField = "FGroupName";
            ddGroup.DataBind();
            ddGroup.Items.Add("All");
            ddGroup.Text = "All";
        }
        private void loadOrder()
        {
            IQueryable d = new Order_DL().GetActiveRecord();
            ddOrder.DataSource = d;

            ddOrder.DataTextField = "PINO";
            ddOrder.DataValueField = "PINO";

            ddOrder.DataBind();
            ddOrder.Items.Add("All");
            ddOrder.Text = "All";



        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string jscript = "";

            string ReportName = "";
            string pr = "FABRIC_PRODUCTION";

            ReportName = "rptFabricProduction";



            string Fdate = txtDate.Text.ToString();
            string ToDate = txtTodate.Text.ToString();
            string fid = "";
            string gid = "";
            string order = "";
            if (ddOrder.Text=="All")
            {
                order = "0";
            }
            else
            {
                order = ddOrder.SelectedValue;
            }
            if (ddFabric.Text == "All")
            {
                fid = "0";
            }
            else
            {
                fid = ddFabric.SelectedValue;
            }

            if (ddGroup.Text == "All")
            {
                gid = "0";
                fid = "0";
            }
            else
            {
                gid = ddGroup.SelectedValue;
            }

           
            

            string RTYpe = "Fabric Production Report"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&FromDate=" + Fdate + "&ToDate=" + ToDate + "&FID=" + fid + "&GID=" + gid + "&Order=" + order + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";

            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm23), "ScriptFunction", jscript);
        }

        protected void ddGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (ddGroup.Text == "All")
                {

                }
                else
                {
                    ddFabric.DataSource = new Fabric_CL().GetAlFabricbyGRoup(Convert.ToInt16(ddGroup.SelectedValue));
                    ddFabric.DataValueField = "Fid";
                    ddFabric.DataTextField = "fab";
                    ddFabric.DataBind();
                    ddFabric.Items.Add("All");
                    ddFabric.Text = "All";
                }
               
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string jscript = "";

            string ReportName = "";
            string pr = "FABRIC_PRODUCTION";

            ReportName = "rptFabricDelivery";



            string Fdate = txtDate.Text.ToString();
            string ToDate = txtTodate.Text.ToString();
            string fid = "";
            string gid = "";
            string order = "";
            if (ddOrder.Text == "All")
            {
                order = "0";
            }
            else
            {
                order = ddOrder.SelectedValue;
            }
            if (ddFabric.Text == "All")
            {
                fid = "0";
            }
            else
            {
                fid = ddFabric.SelectedValue;
            }

            if (ddGroup.Text == "All")
            {
                gid = "0";
                fid = "0";
            }
            else
            {
                gid = ddGroup.SelectedValue;
            }




            string RTYpe = "Fabric Delivery Report"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&FromDate=" + Fdate + "&ToDate=" + ToDate + "&FID=" + fid + "&GID=" + gid + "&Order=" + order + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";

            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm23), "ScriptFunction", jscript);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string jscript = "";

            string ReportName = "";
            string pr = "FABRIC_ORDER_STATUS";

            ReportName = "rptFabriceOrderStatus";



            string Fdate = txtDate.Text.ToString();
            string ToDate = txtTodate.Text.ToString();
            string fid = "";
            string gid = "";
            string order = "";
            if (ddOrder.Text == "All")
            {
                order = "0";
            }
            else
            {
                order = ddOrder.SelectedValue;
            }
            if (ddFabric.Text == "All")
            {
                fid = "0";
            }
            else
            {
                fid = ddFabric.SelectedValue;
            }

            if (ddGroup.Text == "All")
            {
                gid = "0";
                fid = "0";
            }
            else
            {
                gid = ddGroup.SelectedValue;
            }




            string RTYpe = "Fabric Order Status Report"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&FromDate=" + Fdate + "&ToDate=" + ToDate + "&FID=" + fid + "&GID=" + gid + "&Order=" + order + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";

            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm23), "ScriptFunction", jscript);
        }
    }
}
