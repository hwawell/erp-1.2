﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace ERP
{
    public static class Common
    {
        public static decimal GetDecimal(string strText)
        {
            try
            {

                return Convert.ToDecimal(strText);


            }
            catch
            {
                return 0;
            }

        }
        public static double GetDouble(string strText)
        {
            try
            {

                return Convert.ToDouble(strText);


            }
            catch
            {
                return 0;
            }

        }
        public static Int32 GetNumber(string strText)
        {
            try
            {

                return Convert.ToInt32(strText);


            }
            catch
            {
                return 0;
            }

        }
        public static DateTime GetDate(string strText)
        {
            try
            {

                return Convert.ToDateTime(strText);


            }
            catch
            {
                return DateTime.Now;
            }

        }
    }
}
