<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_GreyIssueToDeying.aspx.cs" Inherits="ERP.WebForm10" Title="Knitting Delivery" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style4
        {
        width: 180px;
    }
        .style5
        {
        width: 90px;
    }
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
        .style6
        {
            width: 47px;
            height: 2px;
        }
        .style7
        {
            width: 143px;
            height: 2px;
        }
        .style8
        {
            height: 2px;
        }
               
    </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
	<script type="text/javascript">
	    $(function() {
	        $(".tb").autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                url: "DataLoad.asmx/GetProcessCardList",
	                data: "{ 'PCardNO': '" + request.term + "' }",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    dataFilter: function(data) { return data; },
	                    success: function(data) {
	                        response($.map(data.d, function(item) {
	                            return {
	                            value: item.PCardNo
	                            }
	                        }))
	                    },
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                        
	                    }
	                });
	            },
	            minLength: 4
	        });
	    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <div class="grid" >
            <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>Grey Issue To Deying</h2></center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid" 
                        style="overflow:auto; width: 739px; height: 1600px; background-color: "whitesmoke";">     
                <div style="height:25px;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                        AssociatedUpdatePanelID="updPanel">
                        <ProgressTemplate>
                            <img alt="" src="Images/loading.gif" style="height: 15px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    </div>
                <div style="height: 40px; border-bottom-style: none; border-bottom-color: #003399;">
                    <div style="float: left">
                    <asp:ImageButton ID="ibtnNew0" runat="server" Height="30px" 
                        ImageUrl="~/Images/Rich Text Format.ico" onclick="ibtnNew_Click" 
                        ToolTip="New Fabric Delivery Entry" Width="39px" />
                </div>
                <div align="center" style="height: 30px">
                 <asp:UpdatePanel ID="uprbl" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>
                    <asp:RadioButtonList ID="rblNormal" runat="server" Font-Bold="True" 
                        Font-Names="Georgia" Font-Size="Small" Height="12px" 
                        RepeatDirection="Horizontal" Width="228px" AutoPostBack="True" 
                        onselectedindexchanged="rblNormal_SelectedIndexChanged">
                        <asp:ListItem>Active</asp:ListItem>
                        <asp:ListItem>Deleted</asp:ListItem>
                        
                    </asp:RadioButtonList> PCard
                      <asp:TextBox ID="txtPCard" runat="server"></asp:TextBox>
                      <asp:Button ID="btnLoad" runat="server" Text="Load Grey Issue To Deying" 
                          onclick="btnLoad_Click" />
                    </ContentTemplate>
                    </asp:UpdatePanel>
                    </div>
                
                </div>
                <div style="height: 8px; border-bottom-style: groove; border-bottom-color: #003399;">
                
                </div>
                    <!-- Content Goes Here! -->
                    <asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView 
                                ID="gvProducts" runat="server" 
                                 AutoGenerateColumns="False" 
                                AllowPaging="True" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                onpageindexchanging="gvProducts_PageIndexChanging" PageSize="15">
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="�" LastPageText="�" />      
                                <Columns>
                                 
                                    <asp:BoundField 
                                        HeaderText="ID" DataField="ID" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                     <asp:BoundField 
                                        HeaderText="FabricID" DataField="FabricID" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                      <asp:BoundField 
                                        HeaderText="GID" DataField="GID" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="Fabric" DataField="Fabric" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="GM" HeaderText="GM" >
                                     <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                     
                                     <asp:BoundField DataField="GSM" HeaderText="GSM" >
                                     <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>   
                                      
                                    <asp:BoundField DataField="MCNo" HeaderText="MCNo" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                      
                                    <%--<asp:BoundField DataField="PDate" HeaderText="PDate" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  --%> 
                                    <asp:BoundField DataField="PCardNo" HeaderText="Process Card " >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>   
                                      
                                      <asp:BoundField DataField="IssueQty" HeaderText="IssueQty " >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>   
                                    
                                    <asp:BoundField DataField="RollNo" HeaderText="Roll No">
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IssueDate" HeaderText="IssueDate">
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Section" HeaderText="Section">
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                     <asp:BoundField DataField="ReqNo" HeaderText="ReqNo">
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                   <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/btn_edit.gif" 
                                                onclick="btnEdit_Click" Height="16px" />
                                            
                                           
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" 
                                                ImageUrl="~/Images/btn_delete.gif" onclick="btnDelete_Click" 
                                                Height="16px" 
                                            onclientclick="javascript:return confirm('Are you sure you want to delete this row?');" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                   <%-- <asp:TemplateField HeaderText="Active">
                                      <ItemTemplate>
                                     <asp:ImageButton ID="btnActive" runat="server" 
                                                ImageUrl="~/Images/HandleHand.png" onclick="btnActive_Click" 
                                                style="width: 15px" Height="16px" />
                                    </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                            <asp:Button ID="btnHiddenProduct" runat="Server" Style="display:none"  />
                                 
                    <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlDataEntry" runat="server" Style="display:block"   CssClass="modalBox"  
                        BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                        Width="550px" BorderWidth="2px">
                        <center>
                            <asp:Panel ID="ProductCaption" runat="server"   
                                Style="margin-bottom: 10px; cursor: hand;" 
                                BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                Grey Issue To Deying</asp:Panel></center>
                            <cc1:CollapsiblePanelExtender ID="ProductCaption_CollapsiblePanelExtender" 
                                runat="server" Enabled="True" TargetControlID="ProductCaption">
                            </cc1:CollapsiblePanelExtender>
                         <asp:UpdatePanel ID="upEntry" UpdateMode="Conditional" runat="server">
                         <ContentTemplate>
                         
                        
                        <table cellpadding="0" cellspacing="1" style="width:100%;">
                             <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label2" runat="server" Text="Process Card No" 
                                        Font-Names="Georgia"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtPCardId" runat="server" class="tb"></asp:TextBox>
                                    <asp:Button ID="Button1" runat="server" Text="Search Req" 
                                        onclick="Button1_Click" />
                                </td>
                            </tr>
                             
                             <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label6" runat="server" Text="MC No" 
                                        Font-Names="Georgia"></asp:Label>
                                </td>
                                <td align="left">
                            
                                    <asp:DropDownList ID="ddlMCNo" runat="server" Height="21px" onselectedindexchanged="ddlMCNo_SelectedIndexChanged" Width="128px" AutoPostBack="True">
                                        </asp:DropDownList>
                                    
                                </td>
                            </tr>
                           
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label3" runat="server" Text="Knitt (Fabric)" 
                                        Font-Names="Georgia"></asp:Label>
                                </td>
                                <td>
                            
                                          <asp:TextBox ID="txtFabric" runat="server" Width="394px"></asp:TextBox>
                                    
                                    
                                </td>
                            </tr>
                             <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label4" runat="server" Text="Req NO" 
                                        Font-Names="Georgia"></asp:Label>
                                </td>
                                <td>
                            
                                          <asp:TextBox ID="txtReq" ReadOnly="true" runat="server" Width="394px"></asp:TextBox>
                                    
                                    
                                </td>
                            </tr>
                           
                          
                            
                            <tr>
                                <td class="style5"><asp:HiddenField ID="hdnFID" runat="server" />
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label1" runat="server" Text="Delivery Qty" 
                                        Font-Names="Georgia"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtQty" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                           
                            
                             <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label5" runat="server" Text="No of Roll" 
                                        Font-Names="Georgia"></asp:Label>
                                </td>
                                <td align="left"> 
                                    <asp:TextBox ID="txtNoOfRoll" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                           <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                     Issue To :</asp:Label>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlSection" runat="server" Height="22px" TabIndex="4"                                         Width="248px">
                                        
                                        <asp:ListItem>DEYING</asp:ListItem>
                                         <asp:ListItem>PRINTING</asp:ListItem>
                                        <asp:ListItem>RAISING</asp:ListItem>
                                       
                                        <asp:ListItem>PACKING</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                           
                            
                            <tr>
                                <td class="style6">
                                    </td>
                                <td class="style7">
                                    <asp:HiddenField ID="hdnCode" runat="server" Value="-1" />
                                </td>
                                <td class="style8">
                                    <asp:Button ID="btnSave" runat="server" Height="24px"  Text="Save" 
                                        Width="60px" onclick="btnSave_Click" Font-Names="Georgia" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="60px" 
                                        Height="24px" Font-Names="Georgia" onclick="btnCancel_Click" />
                                </td>
                            </tr>
                             <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    
                                </td>
                                <td>
                                  <asp:UpdatePanel ID="upStatus" runat="server" UpdateMode="Conditional">
                                  <ContentTemplate>
                                  
                                  
                                  
                                    <asp:Label ID="lblStatus" runat="server" 
                                        Font-Names="Georgia" Visible="true"></asp:Label>
                                        
                                        </ContentTemplate>
                                  
                                  </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    &nbsp;</td>
                                <td>
                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                        AssociatedUpdatePanelID="updPanel" DisplayAfter="10">
                                    <ProgressTemplate>
                                      <img id="dd" alt="Loading.." src="Images/loading.gif" />
                                    </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                        </table>
                         </ContentTemplate>
                         </asp:UpdatePanel>
                    </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="rblNormal" 
                                EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnLoad" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>                     
    
                </div></div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>      
    </div>
</asp:Content>
