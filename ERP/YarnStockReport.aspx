﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="YarnStockReport.aspx.cs" Inherits="ERP.YarnStockReport" Title="Yarn Stock Report" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();
            List<EYarnStockReport> liProd = new List<EYarnStockReport>();
            this.Window1.Title = "Yarn Stock Report";
            Int64 gid = 0;

            if (ddlYarnGroup.Text == "--All Group--")
            {
                gid = 0;
            }
            else
            {
                gid = Convert.ToInt64(ddlYarnGroup.SelectedValue.ToString());
            }
            liProd = new Yarn_DL().GetYarnStockReport(Convert.ToDateTime(txtFrom0.Text), gid);
                    
            
            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
    
   
   
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> Yarn Stock Report</h2></div>
 <div align="left" style="padding: 10px; ">
                      Yarn Group
                   <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                    </div>
                       
                                    <asp:DropDownList ID="ddlYarnGroup" runat="server" Height="24px" Width="157px" 
                                        TabIndex="1" AutoPostBack="True">
                                    </asp:DropDownList>
                                Date
                    <asp:TextBox ID="txtFrom0" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom0_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom0">
                    </cc1:CalendarExtender>
                       
                </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
           
            >
            <Reader>
            <ext:JsonReader IDProperty="ID">
                    <Fields>
                      <ext:RecordField Name="ID" />
                     
                         <ext:RecordField Name="ID" />
                          <ext:RecordField Name="YarnGroup" />
                          <ext:RecordField Name="YarnName" />
                          <ext:RecordField Name="YarnCompany" />
                          <ext:RecordField Name="YarnCountry" />
                          <ext:RecordField Name="Unit" />
                          
                          <ext:RecordField Name="LotNo" />
                          <ext:RecordField Name="OpeningBalance" />
                          <ext:RecordField Name="Purchase_Receive" />
                          <ext:RecordField Name="Return_Receive" />
                          <ext:RecordField Name="Loan_Receive" />
                          <ext:RecordField Name="SubCOn_Receive" />
                          <ext:RecordField Name="Production_Issue" />
                          <ext:RecordField Name="Return_Issue" />
                          <ext:RecordField Name="Loan_Issue" />
                          <ext:RecordField Name="SubCont_Issue" /> 
                          <ext:RecordField Name="ClosingBalanceQty" />
                          <ext:RecordField Name="AvailableCTN" />
                          
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Yarn Stock Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                         
                            <ext:Column ColumnID="ID" Header="SL"  DataIndex="ID" Sortable="true" />
                             <ext:Column ColumnID="YarnGroup" Header="YarnGroup"  DataIndex="YarnGroup" Sortable="true" />
                            <ext:Column ColumnID="YarnName" Header="YarnName"  DataIndex="YarnName" Sortable="true" />
                            <ext:Column ColumnID="YarnCompany" Header="YarnCompany"  DataIndex="YarnCompany" Sortable="true" />
                            <ext:Column ColumnID="YarnCountry" Header="YarnCountry"  DataIndex="YarnCountry" Sortable="true" />
                            <ext:Column ColumnID="Unit" Header="Unit"  DataIndex="Unit" Sortable="true" />
                            
                            <ext:Column ColumnID="LotNo" Header="LotNo"  DataIndex="LotNo" Sortable="true" />
                            <ext:Column ColumnID="OpeningBalance" Header="OpeningBalance"  DataIndex="OpeningBalance" Sortable="true" />
                            
                            <ext:Column ColumnID="Purchase_Receive" Header="Purchase_Receive"  DataIndex="Purchase_Receive" Sortable="true" />
                            <ext:Column ColumnID="Return_Receive" Header="Return_Receive"  DataIndex="Return_Receive" Sortable="true" />
                            <ext:Column ColumnID="Loan_Receive" Header="Loan_Receive"  DataIndex="Loan_Receive" Sortable="true" />
                            <ext:Column ColumnID="SubCOn_Receive" Header="SubCOn_Receive"  DataIndex="SubCOn_Receive" Sortable="true" />
                            <ext:Column ColumnID="Production_Issue" Header="Production_Issue"  DataIndex="Production_Issue" Sortable="true" />
                            <ext:Column ColumnID="Return_Issue" Header="Return_Issue"  DataIndex="Return_Issue" Sortable="true" />
                            
                            <ext:Column ColumnID="Loan_Issue" Header="Loan_Issue"  DataIndex="Loan_Issue" Sortable="true" />
                            <ext:Column ColumnID="SubCont_Issue" Header="SubCont_Issue"  DataIndex="SubCont_Issue" Sortable="true" />
                            <ext:Column ColumnID="ClosingBalanceQty" Header="Closing Balance Qty"  DataIndex="ClosingBalanceQty" Sortable="true" />
                            <ext:Column ColumnID="AvailableCTN" Header="CTN Available"  DataIndex="AvailableCTN" Sortable="true" />
                        
                       </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="DDate" />
                                <ext:StringFilter DataIndex="GM" />
                                <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="FabricDescription" />
                                <ext:StringFilter DataIndex="YARN" />
                                 <ext:NumericFilter DataIndex="RequistionQty" />
                                <ext:StringFilter DataIndex="OrderNo" />
                               
                                      
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>
