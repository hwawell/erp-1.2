﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
using System.Reflection;
namespace ERP
{
    public partial class DyedFab_Report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                loadOrder();
            }
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            string Title = "";

            if (tbAuto.Text.Length < 5)
            {
                tbAuto.Text = "";
                Title = ddlFor.SelectedItem.Text + " Report From " + txtFrom.Text + " To " + txtTo.Text + " ";
                
            }
            else
            {
                txtFrom.Text = "";
                txtTo.Text = "";
                Title = ddlFor.SelectedItem.Text + " Report For Order No: " + tbAuto.Text + " ";
               
            }

            if (ddlFor.SelectedIndex == 0)
            {
                ExportGreyIssueDetails(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().rpt_GreyFabric_Issue(tbAuto.Text, txtFrom.Text, txtTo.Text),Title);

            }
            else
            {
                ExportGreyReceiveDetails(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().rpt_GreyFabric_Receive(tbAuto.Text, txtFrom.Text, txtTo.Text), Title);

            }



         

            
        }
        private void loadOrder()
        {
           

        }
        public void ExportGreyIssueDetails(string fileName, List<rpt_DYED_FABRIC_IssueResult> empList,string title)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2>" + title + " </h2>  </div>");
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            foreach (PropertyInfo proinfo in new rpt_DYED_FABRIC_IssueResult().GetType().GetProperties())
            {
               
                    if (proinfo.Name != "InvoiceNo")
                    {
                        HttpContext.Current.Response.Write("<Td>");
                        //Get column headers  and make it as bold in excel columns
                        HttpContext.Current.Response.Write("<B>");
                        HttpContext.Current.Response.Write(proinfo.Name);
                        HttpContext.Current.Response.Write("</B>");
                        HttpContext.Current.Response.Write("</Td>");
                    }
               
               
             


            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (rpt_DYED_FABRIC_IssueResult emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");
                foreach (PropertyInfo proinfo in new rpt_DYED_FABRIC_IssueResult().GetType().GetProperties())
                {
                   
                        if (proinfo.Name != "InvoiceNo")
                        {
                            HttpContext.Current.Response.Write("<Td>");
                            //Get column headers  and make it as bold in excel columns
                            HttpContext.Current.Response.Write("<B>");
                            if (proinfo.GetValue(emp, null) != null)
                            {
                                HttpContext.Current.Response.Write(proinfo.GetValue(emp, null).ToString());
                            }
                            else
                            {
                                HttpContext.Current.Response.Write("");
                            }
                            HttpContext.Current.Response.Write("</B>");
                            HttpContext.Current.Response.Write("</Td>");
                        }

                    

                }

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }

        public void ExportGreyReceiveDetails(string fileName, List<rpt_DYED_FABRIC_ReceiveResult> empList, string title)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2>" + title + " </h2>  </div>");
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            foreach (PropertyInfo proinfo in new rpt_DYED_FABRIC_ReceiveResult().GetType().GetProperties())
            {
                if (proinfo.Name == "PCardNo" || proinfo.Name == "Color" || proinfo.Name == "LD_DesignNo")
                {

                }
                else
                {
                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    HttpContext.Current.Response.Write(proinfo.Name);
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");
                }

            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (rpt_DYED_FABRIC_ReceiveResult emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");
                foreach (PropertyInfo proinfo in new rpt_DYED_FABRIC_ReceiveResult().GetType().GetProperties())
                {
                   

                    if (proinfo.Name == "PCardNo" || proinfo.Name == "Color" || proinfo.Name == "LD_DesignNo")
                    {

                    }
                    else
                    {
                        HttpContext.Current.Response.Write("<Td>");
                        //Get column headers  and make it as bold in excel columns
                        HttpContext.Current.Response.Write("<B>");
                        if (proinfo.GetValue(emp, null) != null)
                        {
                            HttpContext.Current.Response.Write(proinfo.GetValue(emp, null).ToString());
                        }
                        else
                        {
                            HttpContext.Current.Response.Write("");
                        }
                        HttpContext.Current.Response.Write("</B>");
                        HttpContext.Current.Response.Write("</Td>");
                    }
                }

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedIndex > 0)
            {

                if (ddlType.SelectedIndex == 1)
                {

                    txtFrom.Visible = true;
                    txtTo.Visible = true;
                    tbAuto.Visible = false;
                    lblFrom.Visible = true;
                    lblTo.Visible = true;
                    lblOrder.Visible = false;
                }
                else
                {
                    txtFrom.Visible = false;
                    txtTo.Visible = false;
                    tbAuto.Visible = true;

                    lblFrom.Visible = false;
                    lblTo.Visible = false;
                    lblOrder.Visible = true;


                }
            }
            upSelect.Update();
        }

       
    }
}
