﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ChemicalIssue.aspx.cs" Inherits="ERP.WebForm29" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="AjaxControls" namespace="AjaxControls" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style5
        {
            width: 20px;
        }
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
        .style6
        {
            width: 20px;
            height: 2px;
        }
        .style7
        {
            width: 128px;
            height: 2px;
        }
        .style8
        {
            height: 2px;
        }
               
        .style9
        {
            width: 128px;
        }
               
    </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <div class="grid" >
            <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>Chemical Issue</h2></center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid" 
                        style="overflow:auto; width: 739px; height: 699px; background-color:"
                        whitesmoke";">     
                <div style="height:25px;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                        AssociatedUpdatePanelID="updPanel">
                        <ProgressTemplate>
                            <img alt="" src="Images/loading.gif" style="height: 15px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    </div>
                    
                 <div align="center">
                    <asp:Panel ID="pnlDataEntry" runat="server"    CssClass="modalBox"  
                        BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                        Width="448px" BorderWidth="2px">
                        <center>
                            <asp:Panel ID="ProductCaption" runat="server"   
                                Style="margin-bottom: 10px; cursor: hand;" 
                                BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                Chemical Issue Entry Form</asp:Panel></center>
                            
                           <asp:UpdatePanel ID="UpEntry" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" >
                        <ContentTemplate>
                          <table cellpadding="0" cellspacing="2" style="width:100%;">
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style9" align="right">
                                    &nbsp;</td>
                                <td align="left">
                            
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style9" align="right">
                                    <asp:Label ID="Label1" runat="server" Font-Names="Georgia" 
                                        Text="Chemical Group"></asp:Label>
                                </td>
                                <td align="left">
                                  <asp:UpdatePanel ID="upDes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlcGroup" runat="server" Height="21px" Width="235px" 
                                            AutoPostBack="True" onselectedindexchanged="ddlcGroup_SelectedIndexChanged" 
                                            TabIndex="2">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                   </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style9" align="right">
                                    <asp:Label ID="Label8" runat="server" Font-Names="Georgia" Text="Chemical"></asp:Label>
                                </td>
                                <td align="left">
                                   <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                    <asp:DropDownList ID="ddlChemical" runat="server" Height="20px" Width="235px" 
                                        AutoPostBack="True" TabIndex="3">
                                    </asp:DropDownList>
                                        
                                    </ContentTemplate>
                                       <Triggers>
                                           <asp:AsyncPostBackTrigger ControlID="ddlcGroup" 
                                               EventName="SelectedIndexChanged" />
                                       </Triggers>
                                    </asp:UpdatePanel>
                                   
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td align="right" class="style9">
                                    Unit</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlUnit" runat="server" Height="22px" Width="235px" 
                                        TabIndex="4">
                                    </asp:DropDownList></td>
                            </tr>
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      <asp:Label ID="Label2" runat="server" Font-Names="Georgia" 
                                          Text="CTN/BOX QTY"></asp:Label>
                                  </td>
                                  <td align="left">
                                      <asp:TextBox ID="txtBOXQTY" runat="server" MaxLength="20" TabIndex="5" 
                                          Width="107px"></asp:TextBox>
                                  </td>
                              </tr>
                            <tr>
                                <td class="style5">
                                    </td>
                                <td class="style9" align="right">
                                    TotalQty(KG/LT)</td>
                                <td align="left">
                                    <asp:TextBox ID="txtTotalQTY" runat="server" Width="109px" TabIndex="6"></asp:TextBox>
                                    &nbsp;&nbsp;</td>
                            </tr>
                           
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Notes</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtNotes" runat="server" Width="272px" TabIndex="10"></asp:TextBox>
                                  </td>
                              </tr>
                            
                            
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Date</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtDate" runat="server" TabIndex="11"></asp:TextBox>
                                      <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                                          Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtDate">
                                      </cc1:CalendarExtender>
                                  </td>
                              </tr>
                            
                            
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style9">
                                    &nbsp;</td>
                                <td>
                                    <asp:Label ID="lblMsg" runat="server" Font-Bold="False" Font-Names="Arial" 
                                        Font-Size="Smaller" ForeColor="#CC3300"></asp:Label>
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td class="style6">
                                    </td>
                                <td class="style7">
                                    <asp:HiddenField ID="hdnCode" runat="server" Value="-1" />
                                </td>
                                <td class="style8">
                                    <asp:Button ID="btnSave" runat="server" Height="24px"  Text="Save" 
                                        Width="60px" onclick="btnSave_Click" Font-Names="Georgia" TabIndex="12" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="60px" 
                                        Height="24px" Font-Names="Georgia" onclick="btnCancel_Click" 
                                        TabIndex="13" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style9">
                                    &nbsp;</td>
                                <td>
                                    
                                      <%--  <cc2:ModalUpdateProgress ID="ModalUpdateProgress1" runat="server" AssociatedUpdatePanelID="UpEntry">
                                        <ProgressTemplate>
                                            <img alt="" src="Images/loading.gif" /><br />
                                            Plz Wait..
                                        </ProgressTemplate>
                                        </cc2:ModalUpdateProgress>--%>
                                   
                                </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        
                    </asp:Panel>
                 </div>
                <div style="height: 40px; border-bottom-style: none; border-bottom-color: #003399;">
                    <div style="float: left">
                </div>
                <div align="center" style="height: 30px">
                 <%--<asp:UpdatePanel ID="uprbl" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>--%>
                      Record From<asp:TextBox ID="txtFrom" runat="server" Width="37px">1</asp:TextBox>
                      to<asp:TextBox ID="txtTo" runat="server" Width="37px">10</asp:TextBox>
                      <asp:Button ID="btnload" runat="server" onclick="btnload_Click" Text="Load" 
                          Width="86px" />
                   <%-- </ContentTemplate>
                    </asp:UpdatePanel>--%>
                    </div>
                
                </div>
                <div style="height: 8px; border-bottom-style: groove; border-bottom-color: #003399;">
                
                </div>
                    <!-- Content Goes Here! -->
                    <asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView 
                                ID="gvProducts" runat="server" 
                                 AutoGenerateColumns="False" 
                                AllowPaging="True" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                onpageindexchanging="gvProducts_PageIndexChanging" PageSize="6">
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="«" LastPageText="»" />      
                                <Columns>
                                    
                                    <asp:BoundField 
                                        HeaderText="SL" DataField="SL" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CID" HeaderText="CID" />
                                    <asp:BoundField 
                                        HeaderText="ChemicalName" DataField="ChemicalName" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                     <asp:BoundField DataField="UNIT" HeaderText="UNIT" >
                                     <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>   
                                      
                                    <asp:BoundField DataField="CTNQTy" HeaderText="CTNQty" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                      
                                    <asp:BoundField DataField="Qty" HeaderText="TotalQty" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField> 
                                     
                                    
                                      
                                    <%--<asp:BoundField DataField="PDate" HeaderText="PDate" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField> 
                                      <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/btn_edit.gif" 
                                                onclick="btnEdit_Click" Height="16px" 
                                                />
                                            
                                           
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" 
                                                ImageUrl="~/Images/btn_delete.gif" onclick="btnDelete_Click" 
                                                Height="16px" 
                                            onclientclick="javascript:return confirm('Are you sure you want to delete this row?');" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Active">
                                      <ItemTemplate>
                                     <asp:ImageButton ID="btnActive" runat="server" 
                                                ImageUrl="~/Images/HandleHand.png" onclick="btnActive_Click" 
                                                style="width: 15px" Height="16px" />
                                    </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                            <asp:Button ID="btnHiddenProduct" runat="Server" Style="display:none"  />
                                 
              <%--      <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>--%>
                    
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnload" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>                     
    
                </div></div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>      
    </div>
</asp:Content>
