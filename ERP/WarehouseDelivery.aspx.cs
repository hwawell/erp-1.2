﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class WebForm28 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlOrder.DataSource = new Order_DL().GetActiveOrderList();

                ddlOrder.DataTextField = "OrderNo";
                ddlOrder.DataValueField = "OrderNo";
                ddlOrder.DataBind();
                ddlOrder.Items.Insert(0, "--Select--");
               // LoadList();
            }
        }
        private void LoadList()
        {

            grvData.DataSource = new ProcessCard_CL().GetProcessCard(ddlOrder.SelectedValue.ToString(),0);
            grvData.DataBind();
           
           string s= new ProcessCard_CL().GetCustomer(ddlOrder.SelectedValue.ToString());
           string[] sr = s.Split('|');

           txtID.Text = sr[0];
            txtCustomerName.Text = sr[1];
            txtAddress.Text = sr[2];
        }

        protected void grvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CheckBox chk =(CheckBox) e.Row.Cells[1].FindControl("chk1");
            //string s = e.Row.Cells[3].Text.ToString();
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[6].Visible = false;
                string s = e.Row.Cells[0].Text.ToString();
                if (e.Row.Cells[0].Text.ToString() == "1")
                {
                    chk.Checked = true;

                }
              
            }
        }

        protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
        {

           
        }
        private DateTime getdate()
        {
            DateTime d;
            try
            {
                d=Convert.ToDateTime(txtDate.Text) ;
                return d;
            }
            catch (Exception)
            {
                d = DateTime.Now.Date;
                return d;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            CheckBox chk;
            long w=0;
            long s = new Knit_Process_CL().GetMax();
            decimal weight=0;
            int count=0;
            if (ddlOrder.SelectedIndex != 0) 
            {
                for (int i = 0; i < grvData.Rows.Count - 1; i++)
                {

                    chk = (CheckBox)grvData.Rows[i].Cells[1].FindControl("chk1");

                    if (chk.Checked == true)
                    {
                        w = Convert.ToInt32(grvData.Rows[i].Cells[6].Text);
                        new Knit_Process_CL().UpdateDelivery(s, w);

                        count = count + 1;
                        weight = weight + Convert.ToDecimal(grvData.Rows[i].Cells[4].Text);

                    }
                    else
                    {
                        if (grvData.Rows[i].Cells[0].Text == "1")
                        {
                            new Knit_Process_CL().UpdateDelivery(0, w);
                        }

                    }
                }
                Delivery d = new Delivery();
                d.WID = s;
                d.CID = Convert.ToInt32(txtID.Text);
                d.PID = ddlOrder.SelectedValue.ToString();
                d.DDate = getdate();
                d.ChallanNo = txtChallan.Text;
                d.Description = txtDescription.Text;
                d.TotalRoll = count;
                d.TotQty = weight;
                if (count > 0)
                {
                    new Knit_Process_CL().SaveDelivery(d);
                }
                Clear();
            }

        }

        private void Clear()
        {
            txtID.Text = "0";
            txtDescription.Text = "";
            txtChallan.Text = "";
            ddlOrder.SelectedIndex = 0;
            txtAddress.Text = "";
            txtCustomerName.Text = "";
            grvData.DataSource = null;
            grvData.DataBind();
            upentry.Update();
            upGrid.Update();
               
        }
        protected void btnCalculate_Click(object sender, EventArgs e)
        {

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            LoadList();
        }
    }
}
