﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_RPT_GREY_ISSUE_TO_DEYING.aspx.cs" Inherits="ERP.KNIT_RPT_GREY_ISSUE_TO_DEYING" Title="Report : Grey Issue to Deying" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();

            List<ERptGreyIssueToDeying> liProd = new List<ERptGreyIssueToDeying>();
            this.Window1.Title = "Knit Grey Issue to Deying";
            string Dt = "";// = "1";
            if (chkDate.Checked == true)
            {
                Dt = txtFrom.Text;
            }
            else
            {
                Dt = "1/1/2000";
            }
            string OrderNo = "";
            if (tbAuto.Text.Length > 3)
            {
                OrderNo = tbAuto.Text;
            }
            else
            {
                OrderNo = "--All Order--";
            }
            
            liProd = new KNIT_all_operation().GetRPT_GreyIssueToDeying(Dt,OrderNo);
                    
            
            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
    
   
   
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
	<script type="text/javascript">
	    $(function() {
	        $(".tb").autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                url: "DataLoad.asmx/GetOrderList",
	                    data: "{ 'PINO': '" + request.term + "' }",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    dataFilter: function(data) { return data; },
	                    success: function(data) {
	                        response($.map(data.d, function(item) {
	                            return {
	                            value: item.OrderNo
	                            }
	                        }))
	                    },
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                       
	                    }
	                });
	            },
	            minLength: 2
	        });
	    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> Grey Issue To Deying Report</h2></div>
 <div align="left" style="padding: 10px; ">
                      <asp:CheckBox ID="chkDate" runat="server" Text="Date" />
                      &nbsp;<asp:TextBox ID="txtFrom" runat="server"></asp:TextBox><cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom">
                    </cc1:CalendarExtender>
                   <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                    </div>
                       
                      &nbsp;&nbsp; Order No&nbsp;<asp:TextBox ID="tbAuto" class="tb" runat="server">
             </asp:TextBox></div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
           
            >
            <Reader>
            <ext:JsonReader IDProperty="SL">
                    <Fields>
                      <ext:RecordField Name="SL" />
                     
                        <ext:RecordField Name="OrderNo" />
                         <ext:RecordField Name="Customer" />
                         <ext:RecordField Name="ItemConstruction" />
                            <ext:RecordField Name="LotNo" />
                         <ext:RecordField Name="GM" />
                           <ext:RecordField Name="GSM" />
                         <ext:RecordField Name="MCNo" />
                          <ext:RecordField Name="Color" />
                          <ext:RecordField Name="ProcessCard" />
                          <ext:RecordField Name="GreyIssueToDeying" />
                          <ext:RecordField Name="GreyIssueDate" />
                            

                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Production Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            
                            <ext:Column ColumnID="OrderNo" Header="OrderNo"  DataIndex="OrderNo" Sortable="true" />
                            <ext:Column ColumnID="Customer" Header="Customer"  DataIndex="Customer" Sortable="true" />
                            <ext:Column ColumnID="ItemConstruction" Header="ItemConstruction"  DataIndex="ItemConstruction" Sortable="true" />
                            <ext:Column ColumnID="GM" Header="GM"  DataIndex="GM" Sortable="true" />
                            <ext:Column ColumnID="GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                             <ext:Column ColumnID="LotNo" Header="LotNo"  DataIndex="LotNo" Sortable="true" />
                            
                            <ext:Column ColumnID="MCNo" Header="MCNo"  DataIndex="MCNo" Sortable="true" />
                            <ext:Column ColumnID="Color" Header="Color"  DataIndex="Color" Sortable="true" />
                            <ext:Column ColumnID="ProcessCard" Header="ProcessCard"  DataIndex="ProcessCard" Sortable="true" />

                             <ext:Column ColumnID="GreyIssueToDeying" Header="GreyIssueToDeying"  DataIndex="GreyIssueToDeying" Sortable="true" />
                             <ext:Column ColumnID="GreyIssueDate" Header="GreyIssueDate"  DataIndex="GreyIssueDate" Sortable="true" />
                            
                           </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                             
                                <ext:StringFilter DataIndex="OrderNo" />
                                <ext:StringFilter DataIndex="Customer" />
                                <ext:StringFilter DataIndex="ItemConstruction" />
                                <ext:StringFilter DataIndex="GM" />
                                 <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="MCNo" />
                                 <ext:StringFilter DataIndex="Color" />
                                  <ext:StringFilter DataIndex="ProcessCard" />
                                  <ext:NumericFilter DataIndex="GreyIssueToDeying" />
                                    <ext:DateFilter DataIndex="GreyIssueDate" />
                               
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>
