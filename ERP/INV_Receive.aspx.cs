﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
namespace ERP
{
    public partial class INV_Receive : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (new Utilities_DL().CheckSecurity("502", USession.SUserID) == true)
                //{

                loadGridData();
                HideControl();
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
        private void HideControl()
        {

            //if (Request.QueryString["INVTypeID"] == "2")
            //{
                trBox.Visible = false;
                txtBoxQty.Text = "1";
            //}
        }
        private void LoadBasicData()
        {
            Inventory_DL obj = new Inventory_DL();

            IQueryable li =obj.GetUnit(Convert.ToInt32(Request.QueryString["INVTypeID"]));

            ddlBoxType.DataSource = li;
            ddlBoxType.DataTextField = "UnitName";
            ddlBoxType.DataValueField = "ID";
            ddlBoxType.DataBind();

            ddlUnit.DataSource = li;
            ddlUnit.DataTextField = "UnitName";
            ddlUnit.DataValueField = "ID";
            ddlUnit.DataBind();

            ddlStore.DataSource = obj.GetStore(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlStore.DataTextField = "StoreName";
            ddlStore.DataValueField = "ID";
            ddlStore.DataBind();

            ddlType.DataSource = obj.GetReceiveType(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlType.DataTextField = "TypeName";
            ddlType.DataValueField = "ID";
            ddlType.DataBind();

            ddlLander.DataSource = obj.GetLander(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlLander.DataTextField = "LName";
            ddlLander.DataValueField = "ID";
            ddlLander.DataBind();

            ddlLander.Items.Insert(0, "NONE");

            ddlSupplier.DataSource = obj.GetSupplier(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlSupplier.DataTextField = "CountryOrSupplier";
            ddlSupplier.DataValueField = "ID";
            ddlSupplier.DataBind();

            ddlCountry.DataSource = obj.GetCountry(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlCountry.DataTextField = "CountryOrSupplier";
            ddlCountry.DataValueField = "ID";
            ddlCountry.DataBind();

           // ddlGroup0.Items.Insert(0, "NONE");



        }
        private void loadGridData()
        {


            string Head = new Inventory_DL().GetInventoryType(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            this.Title = Head + " - Inventory Receive";
            lblHeader.Text = "Inventory Receive Entry for " + Head;

            LoadGroup();

            LoadBasicData();

            loadGrid();

        }
        private void loadGrid()
        {
            gvProducts.DataSource = new Inventory_DL().GetInventoryReceive(Convert.ToInt32(Request.QueryString["INVTypeID"]), Convert.ToInt32(ddlGroup.SelectedValue));
            gvProducts.DataBind();
        }

       
        private void LoadGroup()
        {
            Inventory_DL objDL = new Inventory_DL();
            ddlGroup.DataSource = objDL.GetItemGroup(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlGroup.DataTextField = "GroupName";
            ddlGroup.DataValueField = "SL";
            ddlGroup.DataBind();

        }


        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            lblResult.Text = string.Empty;
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            txtItemID.Text = row.Cells[1].Text;
            txtItem.Text = row.Cells[4].Text;
            txtItem1.Text = row.Cells[4].Text;
            ddlType.Text = row.Cells[2].Text;

            if (row.Cells[3].Text == "0")
            {
                ddlLander.Text = "NONE";
            }
            else
                ddlLander.SelectedValue = row.Cells[3].Text;


            ddlBoxType.SelectedItem.Text = row.Cells[5].Text;
            txtBoxQty.Text = row.Cells[6].Text;
            ddlUnit.SelectedItem.Text = row.Cells[7].Text;
            txtCashMemo.Text =  RemoveSpace(row.Cells[8].Text);

            txtTotalQty.Text = row.Cells[9].Text;
            txtTotalAmount.Text = row.Cells[11].Text;
           
            txtPurchaserName.Text =  RemoveSpace(row.Cells[12].Text);





            txtLCNO.Text = RemoveSpace(row.Cells[13].Text);
            txtInvoiceNo.Text = RemoveSpace(row.Cells[14].Text);
            txtGatePass.Text = RemoveSpace(row.Cells[15].Text);
            txtReceiveDate.Text = RemoveSpace(row.Cells[16].Text);
            ddlSupplier.SelectedItem.Text = RemoveSpace(row.Cells[17].Text);
            ddlCountry.SelectedItem.Text = RemoveSpace(row.Cells[18].Text);

            txtNotes.Text = RemoveSpace(row.Cells[19].Text);
            ddlStore.SelectedValue = row.Cells[22].Text;
            updPanel.Update();

            mpeProduct.Show();
        }
        private string RemoveSpace(string str)
        {
            return str.Replace("&nbsp;", "").Trim();
        }
        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            txtItemID.Text = row.Cells[1].Text;
            txtItem.Text = row.Cells[4].Text;
            txtItem1.Text = row.Cells[4].Text;
            ddlType.Text = row.Cells[2].Text;

            if (row.Cells[3].Text == "0")
            {
                ddlLander.Text = "NONE";
            }
            else
                ddlLander.SelectedValue = row.Cells[3].Text;

            
            ddlBoxType.Text = row.Cells[5].Text;
            txtBoxQty.Text = row.Cells[6].Text;
            ddlUnit.Text = row.Cells[7].Text;
            txtTotalQty.Text = row.Cells[9].Text;

            txtTotalAmount.Text = row.Cells[11].Text;
            txtCashMemo.Text = row.Cells[8].Text;
            txtPurchaserName.Text = row.Cells[12].Text;

            txtLCNO.Text = row.Cells[13].Text;
            txtInvoiceNo.Text = row.Cells[14].Text;
            txtGatePass.Text = row.Cells[15].Text;
            txtReceiveDate.Text = row.Cells[16].Text;
            ddlSupplier.SelectedItem.Text = row.Cells[17].Text;
            ddlCountry.SelectedItem.Text = row.Cells[18].Text;

            txtNotes.Text = row.Cells[19].Text;

           
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
           
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGrid();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                if (e.Row.Cells.Count > 2)
                {
                    e.Row.Cells[0].Visible = false;
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[2].Visible = false;
                    e.Row.Cells[3].Visible = false;
                    e.Row.Cells[10].Visible = false;
                    e.Row.Cells[8].Visible = false;
                    e.Row.Cells[22].Visible = false;
                    e.Row.Cells[14].Visible = false;
                    e.Row.Cells[19].Visible = false;
                    //if (Request.QueryString["INVTypeID"] == "2")
                    //{
                        e.Row.Cells[5].Visible = false;
                        e.Row.Cells[6].Visible = false;
                    //}
                }


            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            SaveData();
            mpeProduct.Hide();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
            mpeProduct.Show();

        }

        private bool SaveData()
        {
            try
            {
                if (txtTotalAmount.Text.Length < 1)
                {
                    txtTotalAmount.Text = "0";
                }
                lblResult.Text = string.Empty;
                EINV_Receive c = new EINV_Receive();
                c.ID = Convert.ToInt32(hdnCode.Value);
                c.ItemID = Convert.ToInt32(txtItemID.Text);
                c.ReceiveTypeID = Convert.ToInt32(ddlType.SelectedValue);
                if (ddlLander.SelectedItem.Text == "NONE")
                    c.LanderID = 0;
                else
                    c.LanderID = Convert.ToInt32(ddlLander.SelectedValue);

                c.StoreID = Convert.ToInt32(ddlStore.SelectedValue);
                c.BoxUNIT = ddlBoxType.SelectedItem.Text;
                c.BoxQty = Convert.ToDouble(txtBoxQty.Text);
                c.Unit = ddlUnit.SelectedItem.Text;
                c.TotalQty = Convert.ToDouble(txtTotalQty.Text);
                c.TotalAmount = Convert.ToDouble(txtTotalAmount.Text);
                c.CashMemo = txtCashMemo.Text;
                c.Purchaser = txtPurchaserName.Text;
                c.LCNo = txtLCNO.Text;
                c.InvoiceNo = txtInvoiceNo.Text;
                c.GatePassNo = txtGatePass.Text;
                c.RDate = OpenFunction.ConvertDate(txtReceiveDate.Text);
                c.Supplier = ddlSupplier.SelectedItem.Text;
                c.Country = ddlCountry.SelectedItem.Text;
                c.Notes = txtNotes.Text;
                c.EMode = "E";
                c.EntryID = USession.SUserID;
                
                c.INVTypeID = Convert.ToInt32(Request.QueryString["INVTypeID"]);
               
                string res = "0";
                if (c.ItemID > 0)
                {
                    res = new Inventory_DL().SaveInventoryReceive(c);
                    lblResult.Text = res;
                }
                if (res.Substring(0, 1) == "1")
                {
                    Clear();
                    
                    updPanel.Update();
                }
                return true;
               // mpeProduct.Show();
            }

            catch (Exception ex)
            {
                lblResult.Text = ex.Message.ToString();
                return false;
               // lblResult.Text = ex.Message.ToString();
            }
        }
        private void Clear()
        {
            //lblResult.Text = string.Empty;
            txtNotes.Text = string.Empty;
            txtItem.Text = string.Empty;
            txtItem1.Text = string.Empty;
            txtItemID.Text = string.Empty;
            txtLCNO.Text = string.Empty;
            txtReceiveDate.Text = string.Empty;
            txtGatePass.Text = string.Empty;
            txtInvoiceNo.Text = string.Empty;
          
            txtTotalQty.Text = string.Empty;
            ddlLander.Text = "NONE";
            hdnCode.Value = "0";
            txtItem.Text = string.Empty;
        }
        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            
            
            Clear();
            lblResult.Text = "";
            updPanel.Update();


            

            mpeProduct.Show();
            txtItem.Focus();
        }



        protected void btnCancel_Click(object sender, EventArgs e)
        {
           // loadGridData();
            txtItem1.Text = string.Empty;
           
            mpeProduct.Hide();
           
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            loadGrid();
        }
    }
}

