﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class WebForm21 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                


                //if (new Utilities_DL().CheckSecurity("517", USession.SUserID) == true)
                //{
                    this.rblNormal.Items.FindByText("Active").Selected = true;
                    ////loadGridData();
                    ddlChemicalGroup.DataSource = new Chemical_DL().GetChemicalGroup();
                    ddlChemicalGroup.DataTextField = "GroupName";
                    ddlChemicalGroup.DataValueField = "SL";
                    ddlChemicalGroup.DataBind();

                    ddlgrp.DataSource = new Chemical_DL().GetChemicalGroup();
                    ddlgrp.DataTextField = "GroupName";
                    ddlgrp.DataValueField = "SL";
                    ddlgrp.DataBind();
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}

            }
        }
        private void loadGridData()
        {

        
            gvProducts.Columns[5].Visible = true;
            gvProducts.Columns[6].Visible = true;
            gvProducts.Columns[7].Visible = false;
            gvProducts.DataSource = new Chemical_DL().GetActiveRecord(Convert.ToInt64(ddlgrp.SelectedValue));
            gvProducts.DataBind();

        }
        private void loadDeletedData()
        {

            gvProducts.Columns[5].Visible = false;
            gvProducts.Columns[6].Visible = false;
            gvProducts.Columns[7].Visible = true;
            gvProducts.DataSource = new Chemical_DL().GetDeletedRecord();
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            ddlChemicalGroup.Text = (row.Cells[1].Text);
            txtChemicalName.Text = (row.Cells[2].Text.ToString().Trim());


            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Chemical_DL().DeleteActive(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Chemical_DL().DeleteActive(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            ChemicalList c = new ChemicalList();
            c.SL = Convert.ToInt64(hdnCode.Value);
            c.GID = Convert.ToInt64(ddlChemicalGroup.Text);
            c.ChemicalName = txtChemicalName.Text.ToString();

            c.EntryID = USession.SUserID;
          
            if (txtChemicalName.Text.Length > 0)
            {
                new Chemical_DL().Save(c);
            }
            loadGridData();
            //loadGridData();
            updPanel.Update();
            mpeProduct.Hide();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            ChemicalList c = new ChemicalList();
            c.SL = Convert.ToInt64(hdnCode.Value);
            c.GID = Convert.ToInt64(ddlChemicalGroup.Text);
            c.ChemicalName = txtChemicalName.Text.ToString();

            c.EntryID = USession.SUserID;
            //TBL_Product t = new TBL_Product();
            //t.ProductCode = txtPCode.Text.ToString();
            //t.ProductName = txtProduct.Text.ToString();
            //t.ProductDescription = txtDescription.Text.ToString();
            if (txtChemicalName.Text.Length > 0)
            {
                new Chemical_DL().Save(c);
            }
            //loadGridData();
            //loadGridData();
            txtChemicalName.Text = "";
            
            updPanel.Update();
            mpeProduct.Show();
            ddlChemicalGroup.Focus();

        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            txtChemicalName.Text = string.Empty;
           



            mpeProduct.Show();
            ddlChemicalGroup.Focus();
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (rblNormal.Items.FindByText("Active").Selected == true)
            //{
            //    loadGridData();

            //}
            //else
            //{
            //    loadDeletedData();
            //}
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            loadGridData();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            loadGridData();
        }
    }
}
