<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="YarnRequisitionList.aspx.cs" Inherits="ERP.YarnRequisitionList" Title="Yarn Requisition List" %>

<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();
            List<EYarnRequistion> liProd = new List<EYarnRequistion>();
            this.Window1.Title = "Knit Production Balance Sheet";
            liProd = new Yarn_DL().GetYarnRequisition(Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtFrom0.Text));
                    
            
            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
    
   
   
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> Yarn Requistion List</h2></div>
 <div align="left" style="padding: 10px; ">
                      From
                    <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom">
                    </cc1:CalendarExtender>
                   <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                    </div>
                       
                      To
                    <asp:TextBox ID="txtFrom0" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom0_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom0">
                    </cc1:CalendarExtender>
                       
                </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
           
            >
            <Reader>
            <ext:JsonReader IDProperty="ID">
                    <Fields>
                      <ext:RecordField Name="ID" />
                     
                 
                          <ext:RecordField Name="ReqNo" />
                          <ext:RecordField Name="ReqDate" />
                          <ext:RecordField Name="GreyDescription" />
                          <ext:RecordField Name="KNIT_MC" />
                          <ext:RecordField Name="ReqQty" />
                          <ext:RecordField Name="IssuedQty" />
                          <ext:RecordField Name="BalanceQty" />
                          <ext:RecordField Name="GM" />
                          <ext:RecordField Name="GSM" />
  
                          <ext:RecordField Name="YARN" />
                          <ext:RecordField Name="TotalProductionQty" />
                          <ext:RecordField Name="RequistionQty" />
                          <ext:RecordField Name="OrderNo" />
                          <ext:RecordField Name="KNIT_MC" />
                         
                          
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Yarn Requistion List"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                         
                          
                          
                            <ext:Column ColumnID="ID" Header="SL"  DataIndex="ID" Sortable="true" />
                            <ext:Column ColumnID="ReqNo" Header="ReqNo"  DataIndex="ReqNo" Sortable="true" />
                              <ext:Column ColumnID="ReqDate" Header="Setup Date"  DataIndex="ReqDate" Sortable="true" />
                            <ext:Column ColumnID="GreyDescription" Header="GreyDescription"  DataIndex="GreyDescription" Sortable="true" />
                             <ext:Column ColumnID="GM" Header="GM"  DataIndex="GM" Sortable="true" />
                            <ext:Column ColumnID="GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                           
                            <ext:Column ColumnID="KNIT_MC" Header="KNIT_MC"  DataIndex="KNIT_MC" Sortable="true" />
                             <ext:Column ColumnID="YARN" Header="YARN"  DataIndex="YARN" Sortable="true" />
                            
                            <ext:Column ColumnID="ReqQty" Header="ReqQty"  DataIndex="ReqQty" Sortable="true" />
                            <ext:Column ColumnID="IssuedQty" Header="IssuedQty"  DataIndex="IssuedQty" Sortable="true" />
                            <ext:Column ColumnID="BalanceQty" Header="BalanceQty"  DataIndex="BalanceQty" Sortable="true" />
                             <ext:Column ColumnID="OrderNo" Header="OrderNo"  DataIndex="OrderNo" Sortable="true" />
                             
                       </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="ReqDate" />
                                <ext:StringFilter DataIndex="GM" />
                                <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="GreyDescription" />
                                <ext:StringFilter DataIndex="YARN" />
                                 <ext:NumericFilter DataIndex="RequistionQty" />
                                <ext:StringFilter DataIndex="OrderNo" />
                                <ext:StringFilter DataIndex="KNIT_MC" />
                               
                                      
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>
