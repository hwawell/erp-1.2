﻿
<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ProcessCardReprocess.aspx.cs" Inherits="ERP.ProcessCardReprocess" Title="Process Card Entry Form" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="AjaxControls" namespace="AjaxControls" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style5
        {
            width: 20px;
        }
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
                       
        .style9
        {
            width: 189px;
        }
               
    </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <div class="grid" >
            <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>List of Process Cards</h2></center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid" 
                        style="overflow:auto; width: 739px; height: auto; background-color:"
                        whitesmoke";">     
                <div style="height:25px;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                        AssociatedUpdatePanelID="updPanel">
                        <ProgressTemplate>
                            <img alt="" src="Images/loading.gif" style="height: 15px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    </div>
                    
                 <div align="center">
                    <asp:Panel ID="pnlDataEntry" runat="server"    CssClass="modalBox"  
                        BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                        Width="693px" BorderWidth="2px">
                        <center>
                            <asp:Panel ID="ProductCaption" runat="server"   
                                Style="margin-bottom: 10px; cursor: hand;" 
                                BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                ReProcess Card Entry Form</asp:Panel></center>
                            
                           <asp:UpdatePanel ID="UpEntry" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" >
                        <ContentTemplate>
                          <table cellpadding="0" cellspacing="2" style="width:98%;">
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style9" align="right">
                                    Re Processcard</td>
                                <td align="left">
                            
                                    <asp:TextBox ID="txtProcessCard" runat="server" Font-Bold="True" 
                                        Font-Names="Courier New" Font-Size="Large" MaxLength="20" 
                                        TabIndex="1" Width="150px"></asp:TextBox>
                                    
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                        onclick="btnSearch_Click" TabIndex="2" />
                                    
                                </td>
                            </tr>
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Order</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtOrder" runat="server" ReadOnly="True"></asp:TextBox>
                                      Color<asp:TextBox ID="txtColor" runat="server" ReadOnly="True"></asp:TextBox></td>
                              </tr>
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Order Product</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtOrderProduct" runat="server" ReadOnly="True" Width="400px"></asp:TextBox>
                                  </td>
                              </tr>
                            <tr>
                                <td class="style5">
                                    <div style="height: 8px; border-bottom-style: groove; border-bottom-color: #003399;">
                                    </div>
                                </td>
                                <td class="style9" align="right">
                                    Issued Qty</td>
                                <td align="left">
                                    <asp:TextBox ID="txtIssuedQty" runat="server" ReadOnly="True" Font-Bold="True" 
                                        Font-Size="Larger" Height="22px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style9" align="right">
                                    Width</td>
                                <td align="left">
                                    <asp:TextBox ID="txtWidths" runat="server" ReadOnly="True"></asp:TextBox>
                                    Weight<asp:TextBox ID="txtWeights" runat="server" ReadOnly="True" Width="102px"></asp:TextBox>GSM<asp:TextBox ID="txtGSM" runat="server" ReadOnly="True" Width="102px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td align="right" class="style9">
                                    New Process Card</td>
                                <td align="left">
                                    <asp:TextBox ID="txtNewProcessCard" runat="server" Font-Bold="True" 
                                        Font-Names="Courier New" Font-Size="Large" MaxLength="20" TabIndex="3" 
                                        Width="150px" Height="22px"></asp:TextBox>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td align="right" class="style9" valign="top">
                                    ReProcess Roll Select<br />
                                </td>
                                <td align="left">
                                   <div style="overflow: auto; height: 140px">
                                    <asp:UpdatePanel ID="upFa" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                         
                                            <asp:GridView ID="gvRoll" AutoGenerateColumns="False"  runat="server" 
                                                Font-Size="Medium" Width="247px" onrowdatabound="gvRoll_RowDataBound">
                                           
                                            <Columns>            
                                                <asp:TemplateField HeaderText="Select">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkStatus" runat="server" OnCheckedChanged="chkStatus_OnCheckedChanged"
                                                            AutoPostBack="true" 
                                                            Checked='<%# Convert.ToBoolean(Eval("Select")) %>'
                                                            Text='' />
                                                    </ItemTemplate>                   
                                                </asp:TemplateField>
                                               
                                                <asp:BoundField DataField="RollNO" HeaderText="Roll No" />                   
                                                <asp:BoundField DataField="Qty" HeaderText="Qty"  />
                                                <asp:BoundField DataField="SL" HeaderText="SL" />
                                            </Columns>
                                            </asp:GridView>
                                           
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                     </div>
                                    
                            </tr>
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Total Reprocess Roll Qty</td>
                                  <td align="left">
                                  <asp:UpdatePanel ID="upRollQty" UpdateMode="Conditional" runat="server">
                                  <ContentTemplate>
                                  
                                      <asp:TextBox ID="txtRollQty" runat="server" Font-Bold="True" 
                                          Font-Names="Courier New" Font-Size="Large" MaxLength="20" TabIndex="1" 
                                          Width="150px"  
                                          ReadOnly="True" Height="22px"></asp:TextBox>
                                          </ContentTemplate>
                                  </asp:UpdatePanel>
                                  </td>
                              </tr>
                            
                            
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Non Packing Qty</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtWithoutPackQty" runat="server" AutoPostBack="True" 
                                          Font-Bold="True" Font-Names="Courier New" Font-Size="Large" MaxLength="20" 
                                          ontextchanged="txtWithoutPackQty_TextChanged" TabIndex="4" Width="150px" 
                                          Height="22px"></asp:TextBox>
                                  </td>
                              </tr>
                            
                            
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Total Production Qty</td>
                                  <td align="left">
                                     <asp:UpdatePanel ID="upNet" UpdateMode="Conditional" runat="server">
                                  <ContentTemplate>
                                      <asp:TextBox ID="txtNetQty" runat="server" Font-Bold="True" 
                                          Font-Names="Courier New" Font-Size="Large" MaxLength="20" ReadOnly="True" 
                                          TabIndex="1" Width="150px" ForeColor="#CC3300" 
                                          ontextchanged="txtNetQty_TextChanged" Height="22px"></asp:TextBox>
                                             </ContentTemplate>
                                  </asp:UpdatePanel>
                                  </td>
                              </tr>
                            
                            
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Lot No</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtLotNo" runat="server" Font-Bold="True" 
                                          Font-Names="Courier New" Font-Size="Large" MaxLength="20" 
                                          ontextchanged="txtNetQty_TextChanged" TabIndex="1" 
                                          Width="150px" ForeColor="#CC3300" Height="22px"></asp:TextBox>
                                  </td>
                              </tr>
                            
                            
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Notes</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtNotes" runat="server" Font-Bold="True" 
                                          Font-Names="Courier New" Font-Size="Large" MaxLength="20" 
                                          ontextchanged="txtNetQty_TextChanged" TabIndex="5" TextMode="MultiLine" 
                                          Width="451px"></asp:TextBox>
                                  </td>
                              </tr>
                            
                            
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Re Process Type</td>
                                  <td align="left">
                                      <asp:DropDownList ID="ddlReProcessType" runat="server" Height="18px" 
                                          Width="280px">
                                          <asp:ListItem>DEYING</asp:ListItem>
                                          <asp:ListItem>PRINTING</asp:ListItem>
                                          <asp:ListItem>RAISING</asp:ListItem>
                                          <asp:ListItem>PACKING</asp:ListItem>
                                      </asp:DropDownList>
                                  </td>
                              </tr>
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Re Process Description</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtReprocessDes" runat="server" Font-Bold="True" 
                                          Font-Names="Courier New" Font-Size="Large" MaxLength="20" 
                                          ontextchanged="txtNetQty_TextChanged" TabIndex="5" TextMode="MultiLine" 
                                          Width="451px"></asp:TextBox>
                                  </td>
                              </tr>
                            
                            
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      <asp:HiddenField ID="hdnCode" runat="server" Value="-1" />
                                  </td>
                                  <td align="left">
                                      <asp:Button ID="btnSave" runat="server" Font-Names="Georgia" Height="24px" 
                                          onclick="btnSave_Click" TabIndex="6" Text="Save" Width="60px" />
                                      <asp:Button ID="btnCancel" runat="server" Font-Names="Georgia" Height="24px" 
                                          onclick="btnCancel_Click" TabIndex="7" Text="Cancel" Width="60px" />
                                  </td>
                              </tr>
                              
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      &nbsp;</td>
                                  <td align="left">
                                      <asp:Label ID="lblMsg" runat="server" Font-Bold="False" Font-Names="Arial" 
                                          Font-Size="Smaller" ForeColor="#CC3300"></asp:Label>
                                  </td>
                              </tr>
                              
                            
                            
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        
                    </asp:Panel>
                 </div>
                <div style="height: 40px; border-bottom-style: none; border-bottom-color: #003399;">
                    <div style="float: left">
                                      <div align="center" style="height: 30px; width: 684px;" 
                            __designer:mapid="3f2">
                                         
                                          <asp:Button ID="btnload" runat="server" onclick="btnload_Click" 
                                              Text="Load PCard" />
                                          &nbsp;&nbsp;&nbsp;&nbsp; Pcard:
                                        
                                          <asp:TextBox ID="txtLastPCard" 
                                              runat="server" Height="21px" 
                                              Width="96px"></asp:TextBox>
                                           
                                          <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                                              Text="Print PCard" />
                                          <asp:Button ID="btnEditPCard" runat="server" Text="Edit PCard" 
                                              onclick="btnEditPCard_Click" Visible="False" />
                                      </div>
                </div>
                
                </div>
                    <!-- Content Goes Here! -->
                    <asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                                 
              <%--      <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>--%><%--      <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>--%>
                    
                        </ContentTemplate>
                        <Triggers>
                            
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnload" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnEditPCard" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>                     
    
                </div></div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>      
    </div></div>
</asp:Content>