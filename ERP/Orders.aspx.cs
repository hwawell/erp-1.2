﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

using BDLayer;

namespace ERP
{
    public partial class WebForm7 : System.Web.UI.Page
    {
        string gvUniqueID = String.Empty;
        int gvNewPageIndex = 0;
        int gvEditIndex = -1;
        string gvSortExpr = String.Empty;
        private string gvSortDir
        {
          
            get { return ViewState["SortDirection"] as string ?? "ASC"; }

            set { ViewState["SortDirection"] = value; }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (new Utilities_DL().CheckSecurity("501", USession.SUserID) == true)
                //{

                ddlCustomer.DataSource = new Customer_DL().GetActiveCustomers();
                ddlCustomer.DataTextField = "CName";
                ddlCustomer.DataValueField = "CustID";

                ddlCustomer.DataBind();
                ddlCustomer.Items.Add("Select");
                ddlCustomer.Text = "Select";

                loadOrder();
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
                
            }
        }
        private void loadOrder()
        {
            ddlOrderList.DataSource = new Order_DL().GetActiveOrderList();

            ddlOrderList.DataTextField = "OrderNo";
            ddlOrderList.DataValueField = "OrderNo";

            ddlOrderList.DataBind();
            ddlOrderList.Items.Add("Select");
            ddlOrderList.Text = "Select";

            

        }
        private void loadOrderData()
        {
            DataSet ds = new Order_DL().GetOrderData(ddlOrderList.SelectedValue.ToString());
            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtPDate.Text = ds.Tables[0].Rows[0]["Pdate"].ToString();
                    txtRcvDate.Text = ds.Tables[0].Rows[0]["RcvDate"].ToString();
                    txtShpDate.Text = ds.Tables[0].Rows[0]["Shpdate"].ToString();
                    txtLCNo.Text = ds.Tables[0].Rows[0]["LCNO"].ToString();
                    ddlCustomer.SelectedValue = ds.Tables[0].Rows[0]["CustID"].ToString();
                }
            }
            catch (Exception)
            {

            }
           
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtOrderNo.Text.Length > 3)
                {
                    Order o = new Order();
                    o.PINo = txtOrderNo.Text.Trim();
                    o.CustID = Convert.ToInt64(ddlCustomer.Text);
                    o.PDate = Convert.ToDateTime(txtPDate.Text).Date;
                    o.RcvDate = Convert.ToDateTime(txtRcvDate.Text).Date;
                    o.ShpDate = Convert.ToDateTime(txtShpDate.Text).Date;
                    o.LCNo = (txtLCNo.Text);
                    loadGridOrder();
                    new Order_DL().Save(o);
                    loadOrder();

                    lblShow.Text = "Data Saved Successfully";
                }
                else
                {
                    lblShow.Text = "Order No is not given";
                }
            }
            catch (Exception)
            {
                //ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Save Not Possible,Input Valid data!!!');</script>");
                lblShow.Text = e.ToString();// "Data not Saved,Input valid Data";

            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            clear();
        }
        private void clear()
        {
            txtOrderNo.Text = "";
            txtPDate.Text = "";
            txtShpDate.Text = "";
            txtRcvDate.Text = "";
            txtLCNo.Text = "";
            ddlCustomer.Text = "Select";
            lblShow.Text = "";
         
        }


        private string GetSortDirection()
        {
            switch (gvSortDir)
            {
                case "ASC":
                    gvSortDir = "DESC";
                    break;

                case "DESC":
                    gvSortDir = "ASC";
                    break;
            }
            return gvSortDir;
        }
        private void loadGridOrder()
        {
            try
            {
                string or;
                or = txtOrderNo.Text;
                if (or.Length < 1)
                {
                    or = "100";
                }


                GridView1.DataSource = new Order_DL().GetOrderProduct(or);
                GridView1.DataBind();


                DropDownList d = new DropDownList();

                d = (DropDownList)GridView1.FooterRow.FindControl("ddlDes");
                var dd = new Order_DL().GetDes();
                d.DataSource = dd;
                d.DataTextField = "descriptions";
                d.DataValueField = "descriptions";
                d.DataBind();

                DropDownList n = new DropDownList();
                n = (DropDownList)GridView1.FooterRow.FindControl("ddlNotes");
                
                n.DataSource = dd;
                n.DataTextField = "Others";
                n.DataValueField = "Others";
                n.DataBind();
                    
               
            }
            catch (Exception)
            {
            }
           
        }

        

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "AddCustomer")
            {
                try
                {

                    string strDesc = ((DropDownList )GridView1.FooterRow.FindControl("ddlDes")).Text;
                    string strNotes = ((DropDownList)GridView1.FooterRow.FindControl("ddlNotes")).Text;
                    string strWeight = ((TextBox)GridView1.FooterRow.FindControl("txtWeight")).Text;
                    string strWidth = ((TextBox)GridView1.FooterRow.FindControl("txtWidth")).Text;
                    string strPerUnitKG = ((TextBox)GridView1.FooterRow.FindControl("txtPCS")).Text;
                    string strUnit = ((DropDownList)GridView1.FooterRow.FindControl("ddlUnit")).Text;
                  

                    //string strContactTitle = ((TextBox)GridView1.FooterRow.FindControl("txtContactTitle")).Text;
                    //string strAddress = ((TextBox)GridView1.FooterRow.FindControl("txtAddress")).Text;
                    string strPID = txtOrderNo.Text;

                    //Prepare the Insert Command of the DataSource control
                    //string strSQL = "";
                    //strSQL = "INSERT INTO Customers (CustomerID, CompanyName, ContactName, " +
                    //        "ContactTitle, Address) VALUES ('" + strCustomerID + "','" + strCompanyName + "','" +
                    //        strContactName + "','" + strContactTitle + "','" + strAddress + "')";
                    OrderProduct o = new OrderProduct();
                    o.PINO = strPID;
                    o.OPID = 0;
                    o.Description = strDesc;
                    o.Notes = strNotes;
                    o.Weight =(strWeight);
                    o.Width = (strWidth);
                    o.Unit = strUnit;
                    if (isNumber(strPerUnitKG) == true)
                    {
                        o.PerUnitKG = Convert.ToDecimal(strPerUnitKG);
                    }
                    else
                    {
                        o.PerUnitKG = 0;
                    }
                    //if (isNumber(strUnitPrice) == true)
                    //{
                    //    o.PerUnitPrice = Convert.ToDecimal(strUnitPrice);
                    //}
                    //else
                    //{
                      
                    //}
                    o.EntryID = USession.SUserID;
                    new Order_DL().SaveOP(o);


                    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Order Product added successfully');</script>");

                    //Re bind the grid to refresh the data
                    loadGridOrder();
                    
                }
                //else if(e.CommandName == "AddCustomer")
                //{


                //}
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + ex.Message.ToString().Replace("'", "") + "');</script>");
                }
            }
            else if (e.CommandName == "Edit")
            {
                GridView _gridView = (GridView)sender;
                 int _rowIndex = int.Parse(e.CommandArgument.ToString());
                _gridView.SelectedIndex = _rowIndex;
             
                Control _displayControl = _gridView.Rows[_rowIndex].Cells[2].Controls[1];
                string strDesc = ((TextBox)_displayControl).Text; 
                // Clear the attributes from the selected cell to remove the click event
                _displayControl = _gridView.Rows[_rowIndex].Cells[3].Controls[1];
                string strWeight = ((TextBox)_displayControl).Text; 
                _displayControl = _gridView.Rows[_rowIndex].Cells[4].Controls[1];
                string strWidth = ((TextBox)_displayControl).Text; 
                _displayControl = _gridView.Rows[_rowIndex].Cells[6].Controls[1];
                string strUnit = ((TextBox)_displayControl).Text.ToUpper();
                

                
               
             


                string strPID = txtOrderNo.Text;
                _displayControl = _gridView.Rows[_rowIndex].Cells[1].Controls[1];
                long lngOPID = Convert.ToInt32(((Label)_displayControl).Text) ;


                try
                {
                    //Prepare the Update Command of the DataSource control
                    OrderProduct o = new OrderProduct();
                    o.PINO = strPID;
                    o.OPID = lngOPID;
                    o.Description = strDesc;
                    o.Notes = "";
                    o.Weight = (strWeight);
                    o.Width = (strWidth);
                    o.Unit = strUnit;
                 
                    //if (isNumber(strUnitPrice) == true)
                    //{
                    //    o.PerUnitPrice = Convert.ToDecimal(strUnitPrice);
                    //}
                    //else
                    //{
                   
                    //}
                    o.EntryID = USession.SUserID;
                    new Order_DL().SaveOP(o);
                    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Data Updated Successfully');</script>");

                }
                catch {
                    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Update Failed');</script>");
                }
            }
        }
        private bool isNumber(string s)
        {
            try
            {
                Decimal d = Convert.ToDecimal(s);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            string strSort = string.Empty;
           
           
            // Make sure we aren't in header/footer rows
            if (row.DataItem == null)
            {
                return;
            }

            //Find Child GridView control
            GridView gv = new GridView();
            gv = (GridView)row.FindControl("GridView2");

           

            //Check if any additional conditions (Paging, Sorting, Editing, etc) to be applied on child GridView
            if (gv.UniqueID == gvUniqueID)
            {
                gv.PageIndex = gvNewPageIndex;
                gv.EditIndex = gvEditIndex;
                //Check if Sorting used
                //////if (gvSortExpr != string.Empty)
                //////{
                //////    GetSortDirection();
                //////    strSort = " ORDER BY " + string.Format("{0} {1}", gvSortExpr, gvSortDir);
                //////}
                //Expand the Child grid
                ClientScript.RegisterStartupScript(GetType(), "Expand", "<SCRIPT LANGUAGE='javascript'>expandcollapse('div" + ((DataRowView)e.Row.DataItem)["PINo"].ToString() + "','one');</script>");
            }
            //string pino = "HW0110449";//e.Row.Cells[0].ToString();
            Label btn = e.Row.FindControl("lblPIID") as Label;
            long pi =Convert.ToInt64(btn.Text);




            //Prepare the query for Child GridView by passing the Customer ID of the parent row
            gv.DataSource = new Order_DL().GetOrderProductDetails(pi);  //ChildDataSource(((DataRowView)e.Row.DataItem)["CustomerID"].ToString(), strSort);
            gv.DataBind();

            DropDownList d = new DropDownList();
            d = (DropDownList)gv.FooterRow.FindControl("ddlColor");
            d.DataSource = new Order_DL().GetColor();
            d.DataTextField = "colorname";
            d.DataValueField = "colorname";
            d.DataBind();
            //Add delete confirmation message for Customer
            LinkButton l = (LinkButton)e.Row.FindControl("linkDeleteCust");
            l.Attributes.Add("onclick", "javascript:return " +
            "confirm('Are you sure you want to delete this Customer " +
            DataBinder.Eval(e.Row.DataItem, "OPID") + "')");

           
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                //e.Row.Cells[1].Visible = false;
                TextBox lbl = (TextBox)e.Row.Cells[2].FindControl("txtDesc");

                if (lbl.Text == "")
                {
                    e.Row.Visible = false;
                }
            }
           
        }

        protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            if (e.Exception != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + e.Exception.Message.ToString().Replace("'", "") + "');</script>");
                e.ExceptionHandled = true;
            }

        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //long  lngOPid = ((Label)GridView1.Rows[e.RowIndex].FindControl("lblCustomerID")).Text;
            long lngOPID =Convert.ToInt64(((Label)GridView1.Rows[e.RowIndex].FindControl("lblPIID")).Text);
            //Prepare the delete Command of the DataSource control
           

            try
            {
                new Order_DL().DeleteOrderProduct(lngOPID, USession.SUserID);
                loadGridOrder();
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Customer deleted successfully');</script>");
            }
            catch { }
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
        

        protected void GridView1_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            if (e.Exception != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + e.Exception.Message.ToString().Replace("'", "") + "');</script>");
                e.ExceptionHandled = true;
            }
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            //string strDesc = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtDesc")).Text;
            //string strWeight = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtWeight")).Text;
            //string strWidth = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtWidth")).Text;
            //string strUnit = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("txtUnit")).Text;
         

           
            //string strPID = txtOrderNo.Text;
            //long lngOPID = Convert.ToInt64(((Label)GridView1.Rows[e.RowIndex].FindControl("lblPIID")).Text);

            
            //try
            //{
            //    //Prepare the Update Command of the DataSource control
            //    OrderProduct o = new OrderProduct();
            //    o.PINO = strPID;
            //    o.OPID = lngOPID;
            //    o.Description = strDesc;
            //    o.Notes = "";
            //    o.Weight = strWeight;
            //    o.Width = strWidth;
            //    o.Unit = strUnit;
            //    //if (isNumber(strUnitPrice) == true)
            //    //{
            //    //    o.PerUnitPrice = Convert.ToDecimal(strUnitPrice);
            //    //}
            //    //else
            //    //{
            //        o.PerUnitPrice = 0;
            //    //}
            //    o.EntryID = USession.SUserID;
            //    new Order_DL().SaveOP(o);
               
            //}
            //catch { }
        }

        protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void GridView2_CancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "AddOrder")
            {
                try
                {
                    GridView gvTemp = (GridView)sender;
                    gvUniqueID = gvTemp.UniqueID;

                    //Get the values stored in the text boxes
                    string strid = gvTemp.DataKeys[0].Value.ToString();  //Customer ID is stored as DataKeyNames
                    string strcolor = ((DropDownList)gvTemp.FooterRow.FindControl("ddlColor")).Text;
                    string strcolorno = ((TextBox)gvTemp.FooterRow.FindControl("txtColorNo")).Text;
                    string FRNO = ((TextBox)gvTemp.FooterRow.FindControl("txtFRNo")).Text;
                    string dcmQty = ((TextBox)gvTemp.FooterRow.FindControl("txtQty")).Text;
                    //string Unit =((TextBox)gvTemp.FooterRow.FindControl("txtUnit")).Text.ToUpper();
                    
                    //long yfg = ((Label)gvTemp.FooterRow.FindControl("lblOPDID")).Text;

                    //Prepare the Insert Command of the DataSource control
                    OrderProductDetail o = new OrderProductDetail();
                    o.OPDID = 0;
                    o.OPID =Convert.ToInt64(strid);
                    o.Color = strcolor;
                    o.ColorNo = strcolorno;
                    o.Idesc = FRNO;
                    o.Qty = Convert.ToDecimal(dcmQty);

                    new Order_DL().SaveOPDetails(o);


                    gvTemp.DataSource = new Order_DL().GetOrderProductDetails(Convert.ToInt32(strid));
                    gvTemp.DataBind();

                    DropDownList d = new DropDownList();
                    d = (DropDownList)gvTemp.FooterRow.FindControl("ddlColor");
                    d.DataSource = new Order_DL().GetColor();
                    d.DataTextField = "colorname";
                    d.DataValueField = "colorname";
                    d.DataBind();
                    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Order Product Details added successfully');</script>");

                  
                   // loadGridOrder();
                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + ex.Message.ToString().Replace("'", "") + "');</script>");
                }
            }
            else if (e.CommandName == "Edit")
            {

                try
                {
               
                    GridView _gridView = (GridView)sender;
                    int _rowIndex = int.Parse(e.CommandArgument.ToString());
                    _gridView.SelectedIndex = _rowIndex;

                    Control _displayControl = _gridView.Rows[_rowIndex].Cells[1].Controls[1];
                    string strcolor = ((TextBox)_displayControl).Text;
                    // Clear the attributes from the selected cell to remove the click event
                    _displayControl = _gridView.Rows[_rowIndex].Cells[2].Controls[1];
                    string strcolorno = ((TextBox)_displayControl).Text;

                    _displayControl = _gridView.Rows[_rowIndex].Cells[3].Controls[1];
                    string strFRNO = ((TextBox)_displayControl).Text;


                    _displayControl = _gridView.Rows[_rowIndex].Cells[4].Controls[1];
                    string dcmQty = ((TextBox)_displayControl).Text;
                    

                    string strid = _gridView.DataKeys[0].Value.ToString();




                    string strPID = txtOrderNo.Text;

                    _displayControl = _gridView.Rows[_rowIndex].Cells[0].Controls[1];
                    long lngOPID = Convert.ToInt32(((Label)_displayControl).Text);


               

                    
                    //Prepare the Update Command of the DataSource control
                    OrderProductDetail o = new OrderProductDetail();
                    o.OPDID = lngOPID;
                    o.OPID = Convert.ToInt64(strid);
                    o.Color = strcolor;
                    o.ColorNo = strcolorno;
                    o.Idesc = strFRNO;
                    o.Qty = Convert.ToDecimal(dcmQty);

                    new Order_DL().SaveOPDetails(o);



                     ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Order Product Details Updated successfully');</script>");



                    //Reset Edit Index
                    gvEditIndex = -1;

                    loadGridOrder();
                }
                catch { }
            }
        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (e.Row.RowType != DataControlRowType.Pager)
                {
                    //e.Row.Cells[1].Visible = false;
                    TextBox lbl = (TextBox)e.Row.Cells[1].FindControl("txtColor");

                    if (lbl.Text == "")
                    {
                         e.Row.Visible = false;
                    }
                }
            }
            catch (Exception)
            {

            }
            //Check if this is our Blank Row being databound, if so make the row invisible
            ////if (e.Row.RowType == DataControlRowType.DataRow)
            ////{
            ////    if (((DataRowView)e.Row.DataItem)["OrderID"].ToString() == String.Empty) e.Row.Visible = false;
            ////}
           
            

                // GridView DataKeys Collection object is used here to set the SelectedValue property of nested DropDownList in each row.
                
            
        }

        //protected void GridView2_RowCreated(object sender, GridViewDeletedEventArgs e)
        //{
       
        //}

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridView gvTemp = (GridView)sender;
            gvUniqueID = gvTemp.UniqueID;

            //Get the value        
            string strOrderID = ((Label)gvTemp.Rows[e.RowIndex].FindControl("lblOPDID")).Text;

            //Prepare the Update Command of the DataSource control
           

            try
            {
                string strid = gvTemp.DataKeys[0].Value.ToString();
                new Order_DL().DeleteOrderProductDetails(Convert.ToInt64(strOrderID), USession.SUserID);
                gvTemp.DataSource = new Order_DL().GetOrderProductDetails(Convert.ToInt32(strid));
                gvTemp.DataBind();
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Order Product Details deleted successfully');</script>");
                
            }
            catch { }
        }

        protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView gvTemp = (GridView)sender;
            gvUniqueID = gvTemp.UniqueID;
            gvEditIndex = e.NewEditIndex;
            loadGridOrder();
        }

        protected void GridView2_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            if (e.Exception != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + e.Exception.Message.ToString().Replace("'", "") + "');</script>");
                e.ExceptionHandled = true;
            }
        }

        protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
           
        }

        protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            loadGridOrder();
        }

        protected void txtOrderNo_TextChanged(object sender, EventArgs e)
        {

        }

        protected void GridView2_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {

        }

        protected void ddlOrderList_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtOrderNo.Text = ddlOrderList.SelectedValue.ToString();
            loadOrderData();
            loadGridOrder();
        }
    }
}
