﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="ERP.WebForm7" Title="Order Record" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
    function expandcollapse(obj,row)
    {
        var div = document.getElementById(obj);
        var img = document.getElementById('img' + obj);
        
        if (div.style.display == "none")
        {
            div.style.display = "block";
            if (row == 'alt')
            {
                img.src = "minus.gif";
            }
            else
            {
                img.src = "minus.gif";
            }
            img.alt = "Close to view other Customers";
        }
        else
        {
            div.style.display = "none";
            if (row == 'alt')
            {
                img.src = "plus.gif";
            }
            else
            {
                img.src = "plus.gif";
            }
            img.alt = "Expand to show Orders";
        }
    } 
    </script>
    <style type="text/css">
        .style3
        {
            height: 35px;
        }
        .style4
        {
            height: 20px;
        }
        .style5
        {
            height: 15px;
        }
        .style6
        {
            height: 29px;
        }
        .style7
        {
            height: 490px;
        }
        .style8
        {
            height: 35px;
            width: 205px;
        }
        .style9
        {
            height: 20px;
            width: 205px;
        }
        .style10
        {
            height: 15px;
            width: 205px;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   <div class="grid" >
            <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>Order Details</h2></center>
                </div></div></div>
                     <div class="mid-outer"><div class="mid-inner">
                        <div class="mid" style="overflow:auto; width: 850px; height: auto;  top: 0px; left: 0px; background-color: whitesmoke;">   
                            <div align="center">
                          <%--  <asp:UpdatePanel ID="upEntry" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>  --%>
                                  <table cellpadding="0" cellspacing="2"  style="border: thin solid #003399; width:100%; height: 518px;" align="right">
                        <tr>
                            <td align="right" class="style3">
                                &nbsp;</td>
                                                                                   <td class="style3" align="center" colspan="3">
                                                                                       Choose Order<cc1:ListSearchExtender ID="ListSearchExtender1" runat="server" TargetControlID="ddlOrderList" PromptText="Type to Search Order" IsSorted="True">
                                                                                       </cc1:ListSearchExtender>
                                                                                       <asp:DropDownList ID="ddlOrderList" runat="server" 
                                                                                        
                                                                                        Height="25px" Width="170px" 
                                                                                           onselectedindexchanged="ddlOrderList_SelectedIndexChanged" 
                                                                                           AutoPostBack="True">
                                                                                    </asp:DropDownList>
                                                                                   </td>
                                                                               </tr>
                        <tr>
                            <td align="right" class="style3">
                                Order No:</td>
                                                                                   <td class="style8" 
                                align="left">
                                                                                       <asp:TextBox ID="txtOrderNo" runat="server" Width="162px" 
                                                                                           ontextchanged="txtOrderNo_TextChanged"></asp:TextBox>
                                                                                      <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                                                           ControlToValidate="txtOrderNo" Display="None" 
                                                                                           ErrorMessage="Order No Is Required">*</asp:RequiredFieldValidator>
                                                                                       <cc1:ValidatorCalloutExtender ID="RequiredFieldValidator1_ValidatorCalloutExtender" 
                                                                                           runat="server" Enabled="True" TargetControlID="RequiredFieldValidator1">
                                                                                       </cc1:ValidatorCalloutExtender>--%>
                                                                                       <asp:ImageButton ID="ImageButton1" runat="server" Height="21px" 
                                                                                           ImageUrl="~/Images/REFBAR.ICO" onclick="ImageButton1_Click" />
                                                                                   </td>
                                                                                   <td align="right" 
                                class="style3">
                                                                                       Receive Date :</td>
                                                                                   <td class="style3">
                                                                                       <asp:TextBox ID="txtRcvDate" runat="server" Width="152px" TabIndex="4"></asp:TextBox>
                                                                                       <cc1:CalendarExtender ID="txtRcvDate_CalendarExtender" runat="server" 
                                                                                           Enabled="True" TargetControlID="txtRcvDate">
                                                                                       </cc1:CalendarExtender>
                            </td>
                                                                               </tr>
                                                                               <tr>
                                                                                   <td align="right" class="style4">
                                                                                       Customer:</td>
                                                                                   <td class="style9" align="left">
                                                                                       <asp:DropDownList ID="ddlCustomer" runat="server" 
                                                                                        
                                                                                        Height="25px" Width="170px">
                                                                                    </asp:DropDownList>
                                                                                   </td>
                                                                                   <td align="right" class="style4">
                                                                                       Shipment Date :</td>
                                                                                   <td class="style4">
                                                                                       <asp:TextBox ID="txtShpDate" runat="server" Width="152px" 
                                                                                           style="margin-left: 0px" TabIndex="5"></asp:TextBox>
                                                                                       <cc1:CalendarExtender ID="txtShpDate_CalendarExtender" runat="server" 
                                                                                           Enabled="True" TargetControlID="txtShpDate">
                                                                                       </cc1:CalendarExtender>
                                                                                   </td>
                                                                               </tr>
                                                                               <tr>
                                                                                   <td align="right" class="style5">
                                                                                       PI Date :</td>
                                                                                    <td class="style10" align="left">
                                                                                        <asp:TextBox ID="txtPDate" runat="server" Width="162px" TabIndex="3"></asp:TextBox>
                                                                                        <cc1:CalendarExtender ID="txtPDate_CalendarExtender" runat="server" 
                                                                                            Enabled="True" TargetControlID="txtPDate">
                                                                                        </cc1:CalendarExtender>
                                                                                        [mm/dd/yyyy]</td>
                                                        <td align="right" class="style5">
                                                            L/C No :</td>
                                                        <td class="style5">
                                                            <asp:TextBox ID="txtLCNo" runat="server" Width="152px" TabIndex="6"></asp:TextBox>
                                                       </td>
                                             </tr>
                                             <tr align="center">
                                               <td align="right" class="style6" colspan="3">
                                                   <asp:Label ID="lblShow" runat="server" Font-Bold="True" Font-Size="Smaller" 
                                                       ForeColor="#993300"></asp:Label>
                                                   <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" TabIndex="7" 
                                                       Text="Save" Width="74px" />
                                                   <asp:Button ID="btnClear" runat="server" onclick="btnClear_Click" TabIndex="8" 
                                                       Text="Clear" Width="61px" />
                                                   </td>
                                                <td class="style6">
                                          <%--     <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                                   AssociatedUpdatePanelID="upEntry" DisplayAfter="10">
                                                   <ProgressTemplate>
                                                       <img alt="" src="Images/Loading_image/loading.gif" />
                                                   </ProgressTemplate>
                                               </asp:UpdateProgress>--%>
                                               </td>
                                                </tr>
                                                <tr align="center">
                                                    <td align="right" class="style7" colspan="4">
                                                       <div align="center" style="height: 477px">
                                                            <asp:GridView ID="GridView1" BackColor="#F1F1F1"     AutoGenerateColumns="False"
            style="Z-INDEX: 101; LEFT: 3px; " ShowFooter=True Font-Size=Small
            Font-Names="Verdana" runat="server" GridLines=None OnRowDataBound="GridView1_RowDataBound" 
            OnRowCommand = "GridView1_RowCommand" OnRowUpdating = "GridView1_RowUpdating" BorderStyle=Outset
            OnRowDeleting = "GridView1_RowDeleting" OnRowDeleted = "GridView1_RowDeleted"
            OnRowUpdated = "GridView1_RowUpdated" onrowediting="GridView1_RowEditing">
            <RowStyle BackColor="Gainsboro" />
            <AlternatingRowStyle BackColor="White" />
            <HeaderStyle BackColor="#0083C1" ForeColor="White"/>
            <FooterStyle BackColor="White" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="javascript:expandcollapse('div<%# Eval("OPID") %>', 'one');">
                            <img id="imgdiv<%# Eval("OPID") %>" 
                            alt="Click to show/hide Orders for Record: <%# Eval("OPID") %>"  width="9px" 
                            border="0" src="plus.gif"/>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="  " SortExpression="OPID">
                    <ItemTemplate>
                        <asp:Label ID="lblPIID" Text='<%# Eval("OPID") %>' runat="server" Visible="false"></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="lblPIID" Text='<%# Eval("OPID") %>' runat="server" Visible="false"></asp:Label>
                    </EditItemTemplate>
                    
                </asp:TemplateField>
               
               <asp:TemplateField HeaderText="Product Description" SortExpression="Description">
                    <ItemTemplate>
                      <asp:TextBox ID="txtDesc" runat="server" Text='<%# Eval("Description") %>' Width="350px" BorderStyle="None"></asp:TextBox>
                      <asp:TextBox ID="txtNotes" runat="server" Text='<%# Eval("Notes") %>' Width="350px" BorderStyle="None"></asp:TextBox>
                    </ItemTemplate>
                    
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlDes" runat="server" Height="22px" Width="320px">
                              </asp:DropDownList>                                                     
                         <asp:DropDownList ID="ddlNotes" runat="server" Height="22px" Width="320px">
                              </asp:DropDownList>     
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Weight" SortExpression="Weight">
                    <ItemTemplate>
                        <asp:TextBox ID="txtWeight" runat="server" Text='<%# Eval("Weight") %>' Width="50px" ></asp:TextBox>
                    </ItemTemplate>
                    
                    <FooterTemplate>
                        <asp:TextBox ID="txtWeight" Text='' runat="server" Height="21px" Width="50px"></asp:TextBox>
                       
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Width" SortExpression="RcvDate">
                    <ItemTemplate>
                    <asp:TextBox ID="txtWidth" runat="server" Text='<%# Eval("Width") %>' Width="50px"  ></asp:TextBox>
                    
                    </ItemTemplate>
                    
                    <FooterTemplate>
                        <asp:TextBox ID="txtWidth" Text='' runat="server" Height="21px" Width="50px"></asp:TextBox>
                    </FooterTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="No Of PCS/KG" SortExpression="PerUnitKG">
                    <ItemTemplate>
                        <asp:TextBox ID="txtPCS" runat="server" Text='<%# Eval("PerUnitKG") %>' Width="50px" ></asp:TextBox>
                    </ItemTemplate>
                    
                    <FooterTemplate>
                        <asp:TextBox ID="txtPCS" Text='' runat="server" Height="21px" Width="50px"></asp:TextBox>
                       
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Unit" SortExpression="UNIT">
                    <ItemTemplate>
                        <asp:TextBox ID="txtUnit" runat="server" Text='<%# Eval("Unit") %>' Width="50px"  ></asp:TextBox>
                    
                    </ItemTemplate>
                 
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlUnit" runat="server" Height="25px" Width="85px">
                            <asp:ListItem>YDS</asp:ListItem>
                            <asp:ListItem>KGS</asp:ListItem>
                            <asp:ListItem>METER</asp:ListItem>
                            <asp:ListItem>PCS</asp:ListItem>
                        </asp:DropDownList>
                    </FooterTemplate>
                </asp:TemplateField>
               
                
                
			    
			    <asp:CommandField HeaderText="Edit" ShowEditButton="True" />
			    <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <asp:LinkButton ID="linkDeleteCust" CommandName="Delete" runat="server">Delete</asp:LinkButton>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:LinkButton ID="linkAddCust" CommandName="AddCustomer" runat="server">Add</asp:LinkButton>
                    </FooterTemplate>
                </asp:TemplateField>
			    
			    <asp:TemplateField>
			        <ItemTemplate>
			            <tr>
                            <td colspan="100%">
                                <div id="div<%# Eval("OPID") %>" 
                                    
                                    style="display:none; position:relative;left:15px;OVERFLOW: auto;WIDTH:97%; top: 0px; height: 159px;" >
                                    <asp:GridView ID="GridView2" AllowPaging="False" AllowSorting="False" 
                                                                BackColor="White" Width="100%" Font-Size="X-Small"
                                                                AutoGenerateColumns="false" Font-Names="Verdana" runat="server" 
                                                                DataKeyNames="OPID" ShowFooter="true"
                                                                OnPageIndexChanging="GridView2_PageIndexChanging" OnRowUpdating = "GridView2_RowUpdating"
                                                                OnRowCommand = "GridView2_RowCommand" 
                                                                OnRowEditing = "GridView2_RowEditing" GridLines="None"
                                                                OnRowUpdated = "GridView2_RowUpdated" 
                                                                OnRowCancelingEdit = "GridView2_CancelingEdit" OnRowDataBound = "GridView2_RowDataBound"
                                                                OnRowDeleting = "GridView2_RowDeleting" 
                                                                OnRowDeleted = "GridView2_RowDeleted" OnSorting = "GridView2_Sorting"
                                                                BorderStyle="Double" BorderColor="#0083C1" 
                                        Height="47px">
                                                                <RowStyle BackColor="Gainsboro" />
                                                                <AlternatingRowStyle BackColor="White" />
                                                                <HeaderStyle BackColor="#0083C1" ForeColor="White"/>
                                                                <FooterStyle BackColor="White" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText=" " SortExpression="OPDID">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblOPDID" Text='<%# Eval("OPDID") %>' runat="server" Visible="false"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:Label ID="lblOPDID" Text='<%# Eval("OPDID") %>' runat="server" Visible="false"></asp:Label>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Color" SortExpression="Color">
                                                                        <ItemTemplate>
                                                                        
                                                                            <asp:TextBox ID="txtColor" runat="server" Text='<%# Eval("Color") %>' Width="140px" BorderStyle="None"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                      
                                                                        
                                                                        <FooterTemplate>
                                                                            <asp:DropDownList ID="ddlColor" runat="server" Height="22px" Width="140px">
                                                                                                                                                          </asp:DropDownList>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Color Desc" SortExpression="ColorNo">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtColorNo" runat="server" Text='<%# Eval("ColorNo") %>' Width="100px" BorderStyle="None"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox ID="txtColorNo" runat="server" Height="21px" Width="108px"></asp:TextBox>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                   
                                                                    <asp:TemplateField HeaderText="FR No" SortExpression="Idesc">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtFRNo" runat="server" Text='<%# Eval("Idesc") %>' Width="100px" BorderStyle="None"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox ID="txtFRNo" runat="server" Height="21px" Width="108px"></asp:TextBox>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Qty" SortExpression="Qty">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtQty" runat="server" Text='<%# Eval("Qty") %>' Width="100px" BorderStyle="None"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:TextBox ID="txtQty" Text='' runat="server" Height="19px" Width="100px"></asp:TextBox>
                                                                        </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                    
                                                                    
                                                                    
                                                                    
			                                                        <asp:CommandField HeaderText="Edit" ShowEditButton="True" />
			                                                        <asp:TemplateField HeaderText="Delete">
                                                                         <ItemTemplate>
                                                                            <asp:LinkButton ID="linkDeleteCust0" CommandName="Delete" runat="server">Delete</asp:LinkButton>
                                                                         </ItemTemplate>
                                                                         <FooterTemplate>
                                                                            <asp:LinkButton ID="linkAddOrder" CommandName="AddOrder" runat="server">Add</asp:LinkButton>
                                                                         </FooterTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                           </asp:GridView>
                                                        </div>
                                                     </td>
                                                </tr>
			                                </ItemTemplate>			       
			                            </asp:TemplateField>			    
			                        </Columns>
                                </asp:GridView>
                                                        </div>
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                   <%--</ContentTemplate>
                                   </asp:UpdatePanel>--%>
                            </div>
                             <div>
                            </div>
                          
                        </div>
                    </div>
                 </div>
                <%# Eval("Weight")%>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>      
    </div>
</asp:Content>

