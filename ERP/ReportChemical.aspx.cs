﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class WebForm23 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlGroup.DataSource = new Chemical_DL().GetChemicalGroup();
            ddlGroup.DataTextField = "GroupName";
            ddlGroup.DataValueField = "SL";
            ddlGroup.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string jscript = "";

            string ReportName = "";
            string pr = "ChemicalInventory";

            ReportName = "rptChemicalInventory";
           
           

            string Fdate = txtFrom.Text.ToString();
            string ToDate = txtTo.Text.ToString();


            string RTYpe = "Chemical Balance Report"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&CDate=" + Fdate + "&GID=" + ddlGroup.SelectedValue.ToString() + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";

            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm23), "ScriptFunction", jscript);
        }
    }
}
