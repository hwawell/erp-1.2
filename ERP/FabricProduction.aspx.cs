﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
namespace ERP
{
    public partial class WebForm9 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //if (new Utilities_DL().CheckSecurity("506", USession.SUserID) == true)
                //{
                this.rblNormal.Items.FindByText("Active").Selected = true;
               
                loadFabricGroup();
                loadOperator();
               // loadOrder();
                loadYarn();
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
        private void loadFabricGroup()
        {


            ddFGroup.DataSource = new Fabric_CL().GetAllFabricGroup();
            ddFGroup.DataValueField = "SL";
            ddFGroup.DataTextField = "FGroupName";
            ddFGroup.DataBind();
            
        }
        private void loadYarn()
        {


            IQueryable val = new Yarn_DL().GetActiveRecord();
            ddlY1.DataSource = val;
            ddlY1.DataValueField = "YID";
            ddlY1.DataTextField = "YarnName";
            ddlY1.DataBind();
            ddlY1.Items.Add("--Select--");
            ddlY1.Text = "--Select--";

            ddlY2.DataSource = val;
            ddlY2.DataValueField = "YID";
            ddlY2.DataTextField = "YarnName";
            ddlY2.DataBind();
            ddlY2.Items.Add("--Select--");
            ddlY2.Text = "--Select--";

            ddlY3.DataSource = val;
            ddlY3.DataValueField = "YID";
            ddlY3.DataTextField = "YarnName";
            ddlY3.DataBind();
            ddlY3.Items.Add("--Select--");
            ddlY3.Text = "--Select--";

            ddlY4.DataSource = val;
            ddlY4.DataValueField = "YID";
            ddlY4.DataTextField = "YarnName";
            ddlY4.DataBind();
            ddlY4.Items.Add("--Select--");
            ddlY4.Text = "--Select--";
        }
        ////private void loadOrder()
        ////{
        ////    IQueryable d = new Order_DL().GetActiveRecord();
        ////    ddlOrderList.DataSource = d;

        ////    ddlOrderList.DataTextField = "PINO";
        ////    ddlOrderList.DataValueField = "PINO";

        ////    ddlOrderList.DataBind();
        ////    ddlOrderList.Items.Add("None");
        ////    ddlOrderList.Text = "None";



        ////}
        private void loadOperator()
        {
            ddlOperator.DataSource = new Fabric_CL().GetAlOperator();
            ddlOperator.DataValueField = "sl";
            ddlOperator.DataTextField = "OperatorName";
            ddlOperator.DataBind();
        }
        private void loadFabric()
        {
            ddlFabric.DataSource = new Fabric_CL().GetAlFabric();
            ddlFabric.DataValueField = "Fid";
            ddlFabric.DataTextField = "fab";
            ddlFabric.DataBind();
        }
        private void loadGridData()
        {
            if (rblNormal.Items.FindByText("Active").Selected == true)
            {

                gvProducts.Columns[12].Visible = true;
                gvProducts.Columns[13].Visible = true;
                gvProducts.Columns[14].Visible = false;
                gvProducts.DataSource = new Fabric_CL().GetActiveProduction();
                gvProducts.DataBind();

            }
            else
            {
                loadDeletedData();
            }



        }
        private void loadDeletedData()
        {

            gvProducts.Columns[12].Visible = false;
            gvProducts.Columns[13].Visible = false;
            gvProducts.Columns[14].Visible = true;
            gvProducts.DataSource = new Fabric_CL().GetDeletedProduction();
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ddlFabric.DataSource = new Fabric_CL().GetAlFabric();
            ddlFabric.DataValueField = "Fid";
            ddlFabric.DataTextField = "fab";
            ddlFabric.DataBind();


            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            ddlFabric.Text = (row.Cells[1].Text);
            txtQty.Text = (row.Cells[4].Text);
            if ((row.Cells[10].Text.Replace("&nbsp;", "None")) != "None" || (row.Cells[10].Text.Replace("&nbsp;", "").Length > 4))
            {
                txtOrderNo.Text = (row.Cells[10].Text.Replace("&nbsp;", ""));
            }
            
            txtMCNo.Text = (row.Cells[6].Text);
            txtNiddle.Text = (row.Cells[8].Text);
            txtPDate.Text = (row.Cells[5].Text);
            ddShift.Text = (row.Cells[9].Text);
            ddlOperator.Text = (row.Cells[7].Text.ToString().Trim());
           
            if (txtPDate.Text.Length > 3)
            {
                DateTime d = Convert.ToDateTime(txtPDate.Text);
                txtPDate.Text = d.Date.ToString();
            }
          
           

            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Fabric_CL().ProductionDeleteActive(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Fabric_CL().ProductionDeleteActive(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                //e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {

            
            List<EYarnConsumption> liYC = new List<EYarnConsumption>();

            if (ddlY1.Text != "--Select--" && IsDouble(txtY1Per.Text))
            {
                EYarnConsumption obj1 = new EYarnConsumption();
                obj1.YID = Convert.ToInt32(ddlY1.SelectedValue);
                obj1.QtyPer = Convert.ToDouble(txtY1Per.Text);
                obj1.QtyKG = Convert.ToDouble(txtY1KG.Text);

                liYC.Add(obj1);
            }

            if (ddlY2.Text != "--Select--" && IsDouble(txtY2Per.Text))
            {
                EYarnConsumption obj2 = new EYarnConsumption();
                obj2.YID = Convert.ToInt32(ddlY2.SelectedValue);
                obj2.QtyPer = Convert.ToDouble(txtY2Per.Text);
                obj2.QtyKG = Convert.ToDouble(txtY2KG.Text);

                liYC.Add(obj2);
            }


            if (ddlY3.Text != "--Select--" && IsDouble(txtY1Per.Text))
            {
                EYarnConsumption obj3 = new EYarnConsumption();
                obj3.YID = Convert.ToInt32(ddlY3.SelectedValue);
                obj3.QtyPer = Convert.ToDouble(txtY3Per.Text);
                obj3.QtyKG = Convert.ToDouble(txtY3KG.Text);

                liYC.Add(obj3);
            }

            if (ddlY4.Text != "--Select--" && IsDouble(txtY4Per.Text))
            {
                EYarnConsumption obj4 = new EYarnConsumption();
                obj4.YID = Convert.ToInt32(ddlY4.SelectedValue);
                obj4.QtyPer = Convert.ToDouble(txtY4Per.Text);
                obj4.QtyKG = Convert.ToDouble(txtY4KG.Text);

                liYC.Add(obj4);
            }


            FabricProduction c = new FabricProduction();
            if (txtNiddle.Text == "")
            {
                txtNiddle.Text = "0";
            }
            if (checkDecimal(txtQty.Text) == false)
            {
                txtQty.Text = "0";
            }
            c.SL = Convert.ToInt64(hdnCode.Value);
            c.FID = Convert.ToInt64(ddlFabric.Text);
            c.MCNo = txtMCNo.Text;
            c.PDate =Convert.ToDateTime(txtPDate.Text);
            c.OperatorID = Convert.ToInt64(ddlOperator.Text);
            c.Niddle =Convert.ToInt16(txtNiddle.Text);
            c.Shift = ddShift.Text;
            c.ProductionQty = Convert.ToDecimal(txtQty.Text);
            c.OrderNo = txtOrderNo.Text;
          
            c.EntryID = USession.SUserID;
            if( c.ProductionQty>0 )
            {
                new Fabric_CL().SaveProduction(c,liYC);
            }
           // loadGridData();
            //loadGridData();
            updPanel.Update();
            mpeProduct.Hide();

        }
        private bool checkDecimal(string s)
        {

            try
            {
                decimal v;
                v = Convert.ToDecimal(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            FabricProduction c = new FabricProduction();
            List<EYarnConsumption> liYC = new List<EYarnConsumption>();
            
            if (ddlY1.Text != "--Select--" && IsDouble(txtY1Per.Text))
            {
                EYarnConsumption obj1 = new EYarnConsumption();
                obj1.YID =Convert.ToInt32(ddlY1.SelectedValue);
                obj1.QtyPer = Convert.ToDouble(txtY1Per.Text);
                obj1.QtyKG = Convert.ToDouble(txtY1KG.Text);

                liYC.Add(obj1);
            }

            if (ddlY2.Text != "--Select--" && IsDouble(txtY2Per.Text))
            {
                EYarnConsumption obj2 = new EYarnConsumption();
                obj2.YID = Convert.ToInt32(ddlY2.SelectedValue);
                obj2.QtyPer = Convert.ToDouble(txtY2Per.Text);
                obj2.QtyKG = Convert.ToDouble(txtY2KG.Text);

                liYC.Add(obj2);
            }


            if (ddlY3.Text != "--Select--" && IsDouble(txtY1Per.Text))
            {
                EYarnConsumption obj3 = new EYarnConsumption();
                obj3.YID = Convert.ToInt32(ddlY3.SelectedValue);
                obj3.QtyPer = Convert.ToDouble(txtY3Per.Text);
                obj3.QtyKG = Convert.ToDouble(txtY3KG.Text);

                liYC.Add(obj3);
            }

            if (ddlY4.Text != "--Select--" && IsDouble(txtY4Per.Text))
            {
                EYarnConsumption obj4 = new EYarnConsumption();
                obj4.YID = Convert.ToInt32(ddlY4.SelectedValue);
                obj4.QtyPer = Convert.ToDouble(txtY4Per.Text);
                obj4.QtyKG = Convert.ToDouble(txtY4KG.Text);

                liYC.Add(obj4);
            }




            if (txtNiddle.Text == "")
            {
                txtNiddle.Text = "0";
            }
            if (checkDecimal(txtQty.Text) == false)
            {
                txtQty.Text = "0";
            }

            c.SL = Convert.ToInt64(hdnCode.Value);
            c.FID = Convert.ToInt64(ddlFabric.Text);
            c.MCNo = txtMCNo.Text;
            c.PDate = Convert.ToDateTime(txtPDate.Text);
            c.OperatorID = Convert.ToInt64(ddlOperator.Text);
            c.Niddle = Convert.ToInt16(txtNiddle.Text);
            c.Shift = ddShift.Text;
            c.ProductionQty = Convert.ToDecimal(txtQty.Text);
            c.OrderNo = txtOrderNo.Text;

            c.EntryID = USession.SUserID;

           


            c.EntryID = USession.SUserID;
           

            if (c.ProductionQty > 0)
            {
                new Fabric_CL().SaveProduction(c, liYC);
            }

            
            txtQty.Text = "";
            txtNiddle.Text = "0";
            updPanel.Update();
            mpeProduct.Show();
            ddlFabric.Focus();

        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            // txtYarn.Text = string.Empty;
            // txtYarnDesc.Text = string.Empty;

            txtQty.Text = "";
            txtNiddle.Text = "0";


            mpeProduct.Show();
            ddlFabric.Focus();
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
           // loadGridData();
        }
        protected void ddFGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                ddlFabric.DataSource = new Fabric_CL().GetAlFabricbyGRoup(Convert.ToInt16(ddFGroup.SelectedValue));
                ddlFabric.DataValueField = "Fid";
                ddlFabric.DataTextField = "fab";
                ddlFabric.DataBind();
                upFa.Update();
                mpeProduct.Show();
            }
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            loadGridData();
        }

        protected void TextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBox6_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBox4_TextChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txtY1Per_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (IsDouble(txtQty.Text))
                {
                    if (IsDouble(txtY1Per.Text))
                    {
                        txtY1KG.Text = (((Convert.ToDouble(txtY1Per.Text) * Convert.ToDouble(txtQty.Text)))/100).ToString();

                    }
                    else
                    {
                        txtY1KG.Text = "0";
                        txtY1Per.Text = "";
                    }
                    upY1.Update();
                }
            }
            catch
            {
                txtY1KG.Text = "0";
            }
        }
        bool IsDouble(string s)
        {
            try
            {
                double sp = Convert.ToDouble(s);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void txtY2Per_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (IsDouble(txtQty.Text))
                {
                    if (IsDouble(txtY2Per.Text))
                    {
                        txtY2KG.Text = (((Convert.ToDouble(txtY2Per.Text) * Convert.ToDouble(txtQty.Text)))/100).ToString();

                    }
                    else
                    {
                        txtY2KG.Text = "0";
                        txtY2Per.Text = "";
                    }
                    upY2.Update();
                }
            }
            catch
            {
                txtY2KG.Text = "0";
            }
        }

        protected void txtY3Per_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (IsDouble(txtQty.Text))
                {
                    if (IsDouble(txtY3Per.Text))
                    {
                        txtY3KG.Text = (((Convert.ToDouble(txtY3Per.Text) * Convert.ToDouble(txtQty.Text)))/100).ToString();

                    }
                    else
                    {
                        txtY3KG.Text = "0";
                        txtY3Per.Text = "";
                    }
                    upY3.Update();
                }
            }
            catch
            {
                txtY3KG.Text = "0";
            }
        }

        protected void txtY4Per_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (IsDouble(txtQty.Text))
                {
                    if (IsDouble(txtY4Per.Text))
                    {
                        txtY4KG.Text = (((Convert.ToDouble(txtY4Per.Text) * Convert.ToDouble(txtQty.Text)))/100).ToString();

                    }
                    else
                    {
                        txtY4KG.Text = "0";
                        txtY4Per.Text = "";
                    }
                    upY4.Update();
                }
            }
            catch
            {
                txtY4KG.Text = "0";
            }
        }

      
    }
}
