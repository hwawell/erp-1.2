﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Order.aspx.cs" Inherits="ERP.WebForm6" Title="Untitled Page" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
    function expandcollapse(obj,row)
    {
        var div = document.getElementById(obj);
        var img = document.getElementById('img' + obj);
        
        if (div.style.display == "none")
        {
            div.style.display = "block";
            if (row == 'alt')
            {
                img.src = "minus.gif";
            }
            else
            {
                img.src = "minus.gif";
            }
            img.alt = "Close to view other Customers";
        }
        else
        {
            div.style.display = "none";
            if (row == 'alt')
            {
                img.src = "plus.gif";
            }
            else
            {
                img.src = "plus.gif";
            }
            img.alt = "Expand to show Orders";
        }
    } 
    </script>
    <style type="text/css">
        .style9
        {
            width: 124px;
            height: 28px;
        }
        .style10
        {
            height: 28px;
        }
        .style13
        {
            width: 124px;
            height: 27px;
        }
        .style14
        {
            height: 27px;
        }
        .style17
        {
            width: 124px;
            height: 29px;
        }
        .style18
        {
            height: 29px;
        }
        .style19
        {
        }
        .style23
        {
            width: 192px;
            height: 28px;
        }
        .style24
        {
            width: 192px;
            height: 27px;
        }
        .style25
        {
            width: 192px;
            height: 29px;
        }
        .style26
        {
            width: 208px;
            height: 28px;
        }
        .style27
        {
            width: 208px;
            height: 27px;
        }
        .style28
        {
            width: 208px;
            height: 29px;
        }
        .style29
        {
            height: 495px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   <div class="grid" >
            <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>Order Details</h2></center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid"  
                        
                        style="overflow:auto; position:relative;width: 739px; height: 617px;  top: 0px; left: 0px;"
                        whitesmoke";">     
                    
     <div style="position: relative">
                    <asp:UpdatePanel ID="upEntry" runat="server" UpdateMode="Conditional">
                       <ContentTemplate>                                        
    
                    <table cellpadding="0" cellspacing="2"  style="border: thin solid #003399; width:100%; height: 518px;" align="right">
                        <tr>
                            <td align="right" class="style26">
                                Order No:</td>
                                                                                   <td class="style23">
                                                                                       <asp:TextBox ID="txtOrderNo" runat="server" Width="162px"></asp:TextBox>
                                                                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                                                           ControlToValidate="txtOrderNo" Display="None" 
                                                                                           ErrorMessage="Order No Is Required">*</asp:RequiredFieldValidator>
                                                                                       <cc1:ValidatorCalloutExtender ID="RequiredFieldValidator1_ValidatorCalloutExtender" 
                                                                                           runat="server" Enabled="True" TargetControlID="RequiredFieldValidator1">
                                                                                       </cc1:ValidatorCalloutExtender>
                                                                                       <asp:ImageButton ID="ImageButton1" runat="server" Height="21px" 
                                                                                           ImageUrl="~/Images/REFBAR.ICO" />
                                                                                   </td>
                                                                                   <td align="right" 
                                class="style9">
                                                                                       Receive Date :</td>
                                                                                   <td class="style10">
                                                                                       <asp:TextBox ID="txtRcvDate" runat="server" Width="152px" TabIndex="4"></asp:TextBox>
                                                                                       <cc1:CalendarExtender ID="txtRcvDate_CalendarExtender" runat="server" 
                                                                                           Enabled="True" TargetControlID="txtRcvDate">
                                                                                       </cc1:CalendarExtender>
                            </td>
                                                                               </tr>
                                                                               <tr>
                                                                                   <td align="right" class="style27">
                                                                                       Customer:</td>
                                                                                   <td class="style24">
                                                                                       <asp:DropDownList ID="ddlCustomer" runat="server" 
                                                                                        
                                                                                        Height="25px" Width="170px">
                                                                                    </asp:DropDownList>
                                                                                   </td>
                                                                                   <td align="right" class="style13">
                                                                                       Shipment Date :</td>
                                                                                   <td class="style14">
                                                                                       <asp:TextBox ID="txtShpDate" runat="server" Width="152px" 
                                                                                           style="margin-left: 0px" TabIndex="5"></asp:TextBox>
                                                                                       <cc1:CalendarExtender ID="txtShpDate_CalendarExtender" runat="server" 
                                                                                           Enabled="True" TargetControlID="txtShpDate">
                                                                                       </cc1:CalendarExtender>
                                                                                   </td>
                                                                               </tr>
                                                                               <tr>
                                                                                   <td align="right" class="style28">
                                                                                       PI Date :</td>
                                                                                    <td class="style25">
                                                                                        <asp:TextBox ID="txtPDate" runat="server" Width="162px" TabIndex="3"></asp:TextBox>
                                                                                        <cc1:CalendarExtender ID="txtPDate_CalendarExtender" runat="server" 
                                                                                            Enabled="True" TargetControlID="txtPDate">
                                                                                        </cc1:CalendarExtender>
                                                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                                                        ControlToValidate="txtPDate" Display="None" 
                                                                                        ErrorMessage="PI Date  Is Required">*</asp:RequiredFieldValidator>
                                                                                    <cc1:ValidatorCalloutExtender ID="RequiredFieldValidator2_ValidatorCalloutExtender" 
                                                                                        runat="server" Enabled="True" TargetControlID="RequiredFieldValidator2">
                                                                                    </cc1:ValidatorCalloutExtender>
                                                                                   </td>
                            <td align="right" class="style17">
                                L/C No :</td>
                            <td class="style18">
                                <asp:TextBox ID="txtLCNo" runat="server" Width="152px" TabIndex="6"></asp:TextBox>
                                                                                   </td>
                        </tr>
                                                                               <tr align="center">
                                                                                   <td align="right" class="style19" colspan="3">
                                                                                       <asp:Label ID="lblShow" runat="server" Font-Bold="True" Font-Size="Smaller" 
                                                                                           ForeColor="#993300"></asp:Label>
                                                                                       <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" TabIndex="7" 
                                                                                           Text="Save" Width="74px" />
                                                                                       <asp:Button ID="btnClear" runat="server" onclick="btnClear_Click" TabIndex="8" 
                                                                                           Text="Clear" Width="61px" />
                                                                                       </td>
                            <td>
                                                                                   <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                                                                       AssociatedUpdatePanelID="upEntry" DisplayAfter="10">
                                                                                       <ProgressTemplate>
                                                                                           <img alt="" src="Images/Loading_image/loading.gif" />
                                                                                       </ProgressTemplate>
                                                                                   </asp:UpdateProgress>
                                                                                   </td>
                        </tr>
                        <tr align="center">
                            <td align="right" class="style29" colspan="4">
                               <div>
                                  
                                </div>
                                <asp:LinqDataSource ID="LinqDataSource1" runat="server" 
                                    ContextTypeName="BDLayer.DBAccessDataContext" 
                                    onselecting="LinqDataSource1_Selecting" 
                                    Select="new (OPID, Description, Notes, Weight, Width, Unit, PerUnitPrice)" 
                                    TableName="OrderProducts" Where="PINO == @PINO &amp;&amp; EMode == @EMode">
                                    <WhereParameters>
                                        <asp:ControlParameter ControlID="txtOrderNo" Name="PINO" PropertyName="Text" 
                                            Type="String" />
                                        <asp:Parameter DefaultValue="E" Name="EMode" Type="String" />
                                    </WhereParameters>
                                </asp:LinqDataSource>
                            </td>
                        </tr>
                    </table>
                           <asp:GridView ID="GridView1" runat="server">
                           </asp:GridView>
                    </ContentTemplate> 
                      
                    </asp:UpdatePanel>
                   </div>
   
    
                </div></div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>      
    </div>
</asp:Content>
