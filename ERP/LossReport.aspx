﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="LossReport.aspx.cs" Inherits="ERP.WebForm35" %>

<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> Production Loss Report</h2></div>
<div style="font-family: Tahoma; font-size: small">
                
                <div style="border-style: solid none none none; border-width: thin; border-color: #C0C0C0; padding: 10px; height: 30px; "  
                  align="left">
                        <%-- Row 1--%>
                      <div style="float: left; text-align: right;width: 30%;"> Order No  :  </div>
                      <div style="padding-left:1%; float: left;width: 69%;"><asp:TextBox ID="txtOrderNo" runat="server"></asp:TextBox> 
                          <asp:Button ID="btnSearch" runat="server" Text="Search" 
                              onclick="btnSearch_Click" />
                      </div>
                       <%-- Row 2--%>
                      <div style="clear:both; width: 30%;clear: both; float: left;text-align: right; " >Product :</div>
                      <div style="padding-left:1%; float: left;width: 69%;"> 
                          <asp:DropDownList ID="ddlProduct" runat="server" Width="300px" 
                              onselectedindexchanged="ddlProduct_SelectedIndexChanged" 
                              AutoPostBack="True"></asp:DropDownList> </div>
                        <%-- Row 3--%>
                      <div style="clear:both; width: 30%;clear: both; float: left;text-align: right; " >Color : </div>
                      <div style="padding-left:1%; float: left;width: 69%;"> 
                          <asp:DropDownList ID="ddlColor" runat="server" Width="300px" 
                              AutoPostBack="True"> </asp:DropDownList> </div>
                      
                      <div style="clear:both; width: 100%;clear: both; text-align: center; " ><asp:Button ID="btnLoad" runat="server" Text="Search" onclick="btnLoad_Click" /> 
                          <asp:Button ID="btnExport" runat="server" onclick="btnExport_Click" 
                              Text="Export" />
                        </div>
                      <%-- Row 4--%>
                      <div style="width: 20%; float: left;text-align: right; " >Booking Qty (KGS): </div>
                      <div style="width: 13%; float: left; " ><asp:TextBox ID="txtBookingQty" ReadOnly="true" runat="server"></asp:TextBox>  </div>
                      
                      <div style="width: 20%; float: left;text-align: right; " >Grey Issue to Deying (KGS): </div>
                      <div style="width: 13%; float: left; " ><asp:TextBox ReadOnly="true" ID="txtDeying" runat="server"></asp:TextBox>  </div>
                      
                      <div style="width: 20%; float: left;text-align: right; " >Total Packing Qty (KGS) : </div>
                      <div style="width: 13%; float: left; " ><asp:TextBox ID="txtPacking" ReadOnly="true" runat="server"></asp:TextBox>  </div>
                      
                       <%-- Row 5--%>
                      <div style="clear:both; width: 20%; float: left;text-align: right; " >Total Loss (KGS) : </div>
                      <div style="width: 13%; float: left; " ><asp:TextBox ID="txtLoss" ReadOnly="true" runat="server"></asp:TextBox>  </div>
                      
                      <div style="width: 20%; float: left;text-align: right; " >Loss (%) : </div>
                      <div style="width: 13%; float: left; " ><asp:TextBox ID="txtLossPer" ReadOnly="true" runat="server"></asp:TextBox>  </div>
                      
                      <div style="width: 20%; float: left;text-align: right; " >Balance Qty (KGS) : </div>
                      <div style="width: 13%; float: left; " ><asp:TextBox ID="txtBalance" ReadOnly="true" runat="server"></asp:TextBox>  </div>
                      
                      
                      <div style="clear:both;">
                      
                      
                          
                      </div>
                     <div style="padding-top:10px;  ">
    
                        <ext:Store ID="Store1" runat="server">
            <Reader>
                <ext:JsonReader IDProperty="OPID">
                    <Fields>
                        <ext:RecordField Name="Order" />
                        <ext:RecordField Name="Customer" />
                         <ext:RecordField Name="Product" />
                         <ext:RecordField Name="Color" />
                         <ext:RecordField Name="ProcessCard" />
                         <ext:RecordField Name="GreyIssueQty" />
                         <ext:RecordField Name="PackingQty" />
                         <ext:RecordField Name="LossKG" />
                          <ext:RecordField Name="LossPer" />
                           <ext:RecordField Name="LotNo" />
                          
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>
        
        
                        <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="true"
                    Border="true"
                    Title="Loss Report"
                    Frame="true"
                    Height="450px" 
                    >
                    <LoadMask ShowMask="false" />
                   
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>            
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                           
                          

                               <ext:Column 
                                ColumnID="Order" 
                                Header="Order" 
                                DataIndex="Order" 
                                Sortable="true" />  
                                
                                
                                <ext:Column 
                                ColumnID="Customer" 
                                Header="Customer" 
                                DataIndex="Customer" 
                                Sortable="true" />
                                  
                                            
                                                    
                               <ext:Column 
                                ColumnID="Product" 
                                Header="Product" 
                                DataIndex="Product" 
                                Sortable="true" />  
                                
                                
                                <ext:Column 
                                ColumnID="Color" 
                                Header="Color" 
                                DataIndex="Color" 
                                Sortable="true" />
                                
                                
                                 <ext:Column 
                                ColumnID="LotNo" 
                                Header="LotNo" 
                                DataIndex="LotNo" 
                                Sortable="true" />
                                
                                  <ext:Column 
                                ColumnID="ProcessCard" 
                                Header="ProcessCard" 
                                DataIndex="ProcessCard" 
                                Sortable="true" />
                                
                                
                                <ext:Column 
                                ColumnID="GreyIssueQty" 
                                Header="GreyIssueQty" 
                                DataIndex="GreyIssueQty" 
                                Sortable="true" />
                                
                                
                               <ext:Column 
                                ColumnID="PackingQty" 
                                Header="PackingQty" 
                                DataIndex="PackingQty" 
                                Sortable="true" />  
                                
                                
                                <ext:Column 
                                ColumnID="LossKG" 
                                Header="LossKG" 
                                DataIndex="LossKG" 
                                Sortable="true" />
                                
                                 <ext:Column 
                                ColumnID="LossPer" 
                                Header="Loss (%)" 
                                DataIndex="LossPer" 
                                Sortable="true" />
                                                           
                                
                                 
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="OrderNo" />
                                <ext:StringFilter DataIndex="Merchandiser" />
                                <ext:StringFilter DataIndex="Customer" />
                                <ext:StringFilter DataIndex="Product" />
                                <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="Width" />
                                 <ext:StringFilter DataIndex="LotNo" />
                                <ext:NumericFilter DataIndex="BookingQty" />
                                <ext:NumericFilter DataIndex="Qty" />
                                <ext:NumericFilter DataIndex="Balance" />
                           
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="15" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
           
                    </div>
                  
                </div>
                <%--<div style="clear:both; padding-left: 10px">
                     
                     <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" 
                             Text="Production Report" ondirectclick="Button1_DirectClick">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.DoSomething();" />
                        </Listeners>
                        </ext:Button>
                    </div>
                    <div style="float: left">
                       <asp:Button ID="btnPacking" runat="server" Text="Export" 
                        onclick="btnPacking_Click" Height="32px" Width="106px" 
                        BorderStyle="None" />
                    </div>
                    
                </div>--%>
                
   
    </div>

</asp:Content>
