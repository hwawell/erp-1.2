﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDLayer;
using System.Data;
namespace ERP
{
    public partial class WebForm34 : System.Web.UI.Page
    {

        string gvUniqueID = String.Empty;
        int gvNewPageIndex = 0;
        int gvEditIndex = -1;
        string gvSortExpr = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadOrder();
                cboCustomer.DataSource = new Customer_DL().GetActiveCustomers();
                cboCustomer.DataTextField = "CName";
                cboCustomer.DataValueField = "CustID";
                cboCustomer.DataBind();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void btnRevised_Click(object sender, EventArgs e)
        {

        }

      

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            string jscript = "";

            string ReportName = "";
            string pr = "PI";

            ReportName = "rptProformaInvoice";

            Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            string PINO = row.Cells[0].Text;
           
            

         
            string RTYpe = "PI Print"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&PI=" + PINO + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";


           
           //ScriptManager.RegisterStartupScript(this.gvProducts, typeof(WebForm34), Guid.NewGuid().ToString(), jscript, true);

            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm34), "ScriptFunction", jscript);

            ////////string jscript = "";
            ////////string ReportPar = "";
            ////////string ReportName = "";
            ////////string pr = "PI";

            ////////ReportName = "rptProformaInvoice";



            ////////string Fdate = txtDate.Text.ToString();


            ////////string RTYpe = "Yarn Balance Report"; //rdoStatement.SelectedItem.Text.ToString();
            ////////jscript += "<script language=javascript>";
            ////////jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&Fdate=" + Fdate + "&Process=" + pr + "'";
            ////////jscript += ",''";
            //////////jscript += ",'scrollbars=2'";
            //////////jscript += ",'resizable=1'";
            ////////jscript += ",'status,resizable,scrollbars'";
            ////////jscript += "); ";
            ////////jscript += "</script>";

            ////////this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm24), "ScriptFunction", jscript);
        }

        protected void btnFinish1_Click(object sender, EventArgs e)
        {
            string jscript = "";

            string ReportName = "";
            string pr = "PI";

            ReportName = "rptProformaInvoice";

           
            string PINO = txtOrderNo.Text;




            string RTYpe = "PI Print"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&PI=" + PINO + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";


 
            //ScriptManager.RegisterStartupScript(this.gvProducts, typeof(WebForm34), Guid.NewGuid().ToString(), jscript, true);

            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm34), "ScriptFunction", jscript);

         
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            string PINO = row.Cells[0].Text;

            DataSet ds = new Order_DL().GetOrderDataQ(PINO);
            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtApproveFor.Text = ds.Tables[0].Rows[0]["ProductionType"].ToString();
                    txtOrderNo.Text = ds.Tables[0].Rows[0]["PINo"].ToString();
                    txtOrderType.Text = ds.Tables[0].Rows[0]["OrderTypeName"].ToString();
                    txtNotes.Text = ds.Tables[0].Rows[0]["Notes"].ToString();
                    txtRevisedNotes.Text = ds.Tables[0].Rows[0]["RevisedNotes"].ToString();
                    lblVarifiedCustomer.Text = ds.Tables[0].Rows[0]["CustomerName"].ToString();
                    cboCustomer.SelectedValue = ds.Tables[0].Rows[0]["CustID"].ToString();
                    //upOrder.Update();


                    GridView1.DataSource = new Order_DL().GetOrderProductQ(PINO);
                    GridView1.DataBind();
                    //upGrid.Update();
             
                    mpeProduct.Show();
                }
            }
            catch (Exception)
            {

            }
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           // LoadOrder();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            LoadOrder();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
           
        }
        private void LoadOrder()
        {
            string SorderNo = "";
            string CName = "";

            if (ddlSearch.SelectedIndex == 0)
            {
                CName = txtSearchText.Text;
            }
            else if (ddlSearch.SelectedIndex == 1)
            {
                SorderNo = txtSearchText.Text;
            }
            if (rdoList.Items.FindByText("New Order").Selected == true)
            {
                gvProducts.DataSource = new Order_DL().GetNewOrderQueque(CName, SorderNo);
                gvProducts.DataBind();
            }
            else if (rdoList.Items.FindByText("Revised Order").Selected == true)
            {
                gvProducts.DataSource = new Order_DL().GetRevisedOrderQueque(CName, SorderNo);
                gvProducts.DataBind();
            }
             else
            {
                gvProducts.DataSource = new Order_DL().GetReceivedOrder(CName, SorderNo);
                gvProducts.DataBind();
            }
        }

      

        protected void rdoList_SelectedIndexChanged1(object sender, EventArgs e)
        {
            LoadOrder();
            
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            string strSort = string.Empty;


            // Make sure we aren't in header/footer rows
            if (row.DataItem == null)
            {
                return;
            }

            //Find Child GridView control
            GridView gv = new GridView();
            gv = (GridView)row.FindControl("GridView2");


            //Check if any additional conditions (Paging, Sorting, Editing, etc) to be applied on child GridView
            if (gv.UniqueID == gvUniqueID)
            {
                gv.PageIndex = gvNewPageIndex;
                gv.EditIndex = gvEditIndex;
                //Check if Sorting used
                //////if (gvSortExpr != string.Empty)
                //////{
                //////    GetSortDirection();
                //////    strSort = " ORDER BY " + string.Format("{0} {1}", gvSortExpr, gvSortDir);
                //////}
                //Expand the Child grid
                ClientScript.RegisterStartupScript(GetType(), "Expand", "<SCRIPT LANGUAGE='javascript'>expandcollapse('div" + ((DataRowView)e.Row.DataItem)["PINo"].ToString() + "','one');</script>");
            }
            //string pino = "HW0110449";//e.Row.Cells[0].ToString();
            Label btn = e.Row.FindControl("lblPIID") as Label;
            long pi = Convert.ToInt64(btn.Text);




            //Prepare the query for Child GridView by passing the Customer ID of the parent row
            gv.DataSource = new Order_DL().GetOrderProductDetailsQ(pi);  //ChildDataSource(((DataRowView)e.Row.DataItem)["CustomerID"].ToString(), strSort);
            gv.DataBind();
        }

        protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {

        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void GridView1_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {

        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }
        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (e.Row.RowType != DataControlRowType.Pager)
                {
                    //e.Row.Cells[1].Visible = false;
                    TextBox lbl = (TextBox)e.Row.Cells[1].FindControl("txtColor");

                    if (lbl.Text == "")
                    {
                        e.Row.Visible = false;
                    }
                }
            }
            catch (Exception)
            {

            }
            //Check if this is our Blank Row being databound, if so make the row invisible
            ////if (e.Row.RowType == DataControlRowType.DataRow)
            ////{
            ////    if (((DataRowView)e.Row.DataItem)["OrderID"].ToString() == String.Empty) e.Row.Visible = false;
            ////}



            // GridView DataKeys Collection object is used here to set the SelectedValue property of nested DropDownList in each row.


        }

        //protected void GridView2_RowCreated(object sender, GridViewDeletedEventArgs e)
        //{

        //}

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //GridView gvTemp = (GridView)sender;
            //gvUniqueID = gvTemp.UniqueID;

            ////Get the value        
            //string strOrderID = ((Label)gvTemp.Rows[e.RowIndex].FindControl("lblOPDID")).Text;

            ////Prepare the Update Command of the DataSource control


            //try
            //{
            //    string strid = gvTemp.DataKeys[0].Value.ToString();
            //    new Order_DL().DeleteOrderProductDetails(Convert.ToInt64(strOrderID), USession.SUserID);
            //    gvTemp.DataSource = new Order_DL().GetOrderProductDetails(Convert.ToInt32(strid));
            //    gvTemp.DataBind();
            //    ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Order Product Details deleted successfully');</script>");

            //}
            //catch { }
        }

        protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //GridView gvTemp = (GridView)sender;
            //gvUniqueID = gvTemp.UniqueID;
            //gvEditIndex = e.NewEditIndex;
            //loadGridOrder();
        }

        protected void GridView2_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            if (e.Exception != null)
            {
                ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + e.Exception.Message.ToString().Replace("'", "") + "');</script>");
                e.ExceptionHandled = true;
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            mpeProduct.Dispose();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadOrder();
        }

        protected void btnReceive_Click(object sender, EventArgs e)
        {
            new Order_DL().SaveOrderReceive(txtOrderNo.Text,Convert.ToInt32( cboCustomer.SelectedValue), USession.SUserID);

        }

        protected void btnSearch0_Click(object sender, EventArgs e)
        {
            string jscript = "";

            string ReportName = "";
            string pr = "PI";

            ReportName = "rptProformaInvoice";

            
            string PINO = txtSearchText.Text;




            string RTYpe = "PI Print"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&PI=" + PINO + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";



            //ScriptManager.RegisterStartupScript(this.gvProducts, typeof(WebForm34), Guid.NewGuid().ToString(), jscript, true);

            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm34), "ScriptFunction", jscript);

        }

    }
}
