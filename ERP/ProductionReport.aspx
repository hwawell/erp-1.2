<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ERP.ProductionReport.aspx.cs" Inherits="ERP.ProductionReport" %>

<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

 <script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void DoSomething()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            List<EProductionReport> liProd = new List<EProductionReport>();

            if (rdoDate.Checked == true)
            {
                string PValue = rdoPList.SelectedValue.ToString();
                if (PValue == "DEYING")
                {
                    this.Window1.Title = "Production Report Summary : Dyeing";
                    liProd = new Order_DL().GetAllProductionSummary(Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text), "", 0, "", 1);
                }
                else if (PValue == "PACKING")
                {
                    this.Window1.Title = "Production Report Summary : Packing";
                    liProd = new Order_DL().GetAllProductionSummary(Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text), "", 0, "", 2);
                }
                else if (PValue == "DELIVERY")
                {
                    this.Window1.Title = "Production Report Summary : Delivery";
                    liProd = new Order_DL().GetAllProductionSummary(Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text), "", 0, "", 3);
                }
            }
            else
            {
                string PValue = rdoPList.SelectedValue.ToString();
                string Order = "";
                string Merchandiser = "";
                Int64 CustID = 0;
                if (tbAuto.Text.Length == 0)
                {
                    Order = "All Order";

                }
                else
                {
                    Order = tbAuto.Text;
                }
                if (ddlMerchandiser.Text == "All Merchandiser")
                {
                    Merchandiser = "";

                }
                else
                {
                    Merchandiser = ddlMerchandiser.Text;
                }
                if (ddlCustomer.Text == "All Customer")
                {
                    CustID = 0;

                }
                else
                {
                    CustID =Convert.ToInt64( ddlCustomer.SelectedValue);
                }
                if (Order == "" && Merchandiser == "" && CustID == 0)
                {

                }
                else
                {
                    if (PValue == "DEYING")
                    {
                        this.Window1.Title = "Production Report Summary : Dyeing";
                        liProd = new Order_DL().GetAllProductionSummaryAll(Order, CustID, Merchandiser, 1);
                    }
                    else if (PValue == "PACKING")
                    {
                        this.Window1.Title = "Production Report Summary : Packing";
                        liProd = new Order_DL().GetAllProductionSummaryAll(Order, CustID, Merchandiser, 2);
                    }
                    else if (PValue == "DELIVERY")
                    {
                        this.Window1.Title = "Production Report Summary : Delivery";
                        liProd = new Order_DL().GetAllProductionSummaryAll(Order, CustID, Merchandiser, 3);
                    }
                }
            }
            
            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style3
        {
            width: 319px;
        }
    </style>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
	<script type="text/javascript">
	    $(function() {
	        $(".tb").autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                url: "DataLoad.asmx/GetOrderList",
	                    data: "{ 'PINO': '" + request.term + "' }",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    dataFilter: function(data) { return data; },
	                    success: function(data) {
	                        response($.map(data.d, function(item) {
	                            return {
	                            value: item.OrderNo
	                            }
	                        }))
	                    },
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                        
	                    }
	                });
	            },
	            minLength: 2
	        });
	    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> Production Summary Report</h2></div>
<div style="font-family: Tahoma; font-size: small">
              <div align="left" 
                  style="border-width: thin; border-color: #C0C0C0; padding: 10px; border-top-style: solid;">
                    <asp:RadioButton ID="rdoAl" runat="server" Text="Search By : " GroupName="HeadG" Font-Bold="True" />
                        Order
             
              <asp:TextBox ID="tbAuto" class="tb" runat="server">
             </asp:TextBox>Customer :<asp:DropDownList ID="ddlCustomer" runat="server">
                        </asp:DropDownList>
                    Merchandiser :
                        <asp:DropDownList ID="ddlMerchandiser" runat="server">
                        </asp:DropDownList>
                </div>
                <div align="left" style="padding: 10px; ">
                     <asp:RadioButton ID="rdoDate" runat="server" Text="Search By Date Range : " GroupName="HeadG" Font-Bold="True" /> 
                     From
                    <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom">
                    </cc1:CalendarExtender>
                        to
                    <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtTo_CalendarExtender" runat="server" Enabled="True" 
                        TargetControlID="txtTo">
                    </cc1:CalendarExtender>
                </div>
                <div>
                
                </div>
                
                <div style="border-style: solid none none none; border-width: thin; border-color: #C0C0C0; padding: 10px; height: 30px; width : 1300px;"  
                  align="left">
                   
                   <asp:RadioButtonList ID="rdoPList" runat="server" RepeatDirection="Horizontal" 
                    Font-Bold="True" ForeColor="#000099" >
                    <asp:ListItem Selected="True" Value="DEYING">Deying Report</asp:ListItem>
                    <asp:ListItem Value="PACKING">Packing Report</asp:ListItem>
                    <asp:ListItem Value ="DELIVERY">Delivery Report</asp:ListItem>
                     </asp:RadioButtonList>
                </div>
                <div style="padding-left: 10px">
                     
                     <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.DoSomething();" />
                        </Listeners>
                        </ext:Button>
                    </div>
                    <div style="float: left">
                       <asp:Button ID="btnPacking" runat="server" Text="Export" 
                        onclick="btnPacking_Click" Height="32px" Width="106px" 
                        BorderStyle="None" />
                    </div>
                    
                </div>
                
   
    </div>
<div>
    
        <ext:Store ID="Store1" runat="server">
            <Reader>
                <ext:JsonReader IDProperty="OPID">
                    <Fields>
                        <ext:RecordField Name="OrderNo" />
                        <ext:RecordField Name="Merchandiser" />
                         <ext:RecordField Name="Customer" />
                         <ext:RecordField Name="Product" />
                         <ext:RecordField Name="GSM" />
                         <ext:RecordField Name="Width" />
                         <ext:RecordField Name="BookingQty" />
                         <ext:RecordField Name="Qty" />
                          <ext:RecordField Name="Balance" />
                           
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Lorry" 
            Title="Production Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                    <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>            
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                           
                               
                               <ext:Column 
                                ColumnID="OrderNo" 
                                Header="OrderNo" 
                                DataIndex="OrderNo" 
                                Sortable="true" />  
                                
                                
                                <ext:Column 
                                ColumnID="Merchandiser" 
                                Header="Merchandiser" 
                                DataIndex="Merchandiser" 
                                Sortable="true" />
                                  
                                            
                                                    
                               <ext:Column 
                                ColumnID="Customer" 
                                Header="Customer" 
                                DataIndex="Customer" 
                                Sortable="true" />  
                                
                                
                                <ext:Column 
                                ColumnID="Product" 
                                Header="Product" 
                                DataIndex="Product" 
                                Sortable="true" />
                                
                                
                                 <ext:Column 
                                ColumnID="GSM" 
                                Header="GSM" 
                                DataIndex="GSM" 
                                Sortable="true" />
                                
                                <ext:Column 
                                ColumnID="Width" 
                                Header="Width" 
                                DataIndex="Width" 
                                Sortable="true" />
                                
                               <ext:Column 
                                ColumnID="BookingQty" 
                                Header="BookingQty" 
                                DataIndex="BookingQty" 
                                Sortable="true" />  
                                
                                
                                <ext:Column 
                                ColumnID="Qty" 
                                Header="Qty" 
                                DataIndex="Qty" 
                                Sortable="true" />
                                
                                 <ext:Column 
                                ColumnID="Balance" 
                                Header="Balance" 
                                DataIndex="Balance" 
                                Sortable="true" />
                                                           
                                
                                 
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="OrderNo" />
                                <ext:StringFilter DataIndex="Merchandiser" />
                                <ext:StringFilter DataIndex="Customer" />
                                <ext:StringFilter DataIndex="Product" />
                                <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="Width" />
                                <ext:NumericFilter DataIndex="BookingQty" />
                                <ext:NumericFilter DataIndex="Qty" />
                                <ext:NumericFilter DataIndex="Balance" />
                           
                            </Filters>
                        </ext:GridFilters>
                    </Plugins>
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>
    </div>
</asp:Content>
