﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="PrductionReportAll.aspx.cs" Inherits="ERP.PrductionReportAll" Title="Production Report" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();
            X.Mask.Hide();
            this.Window1.Title = "Report";
            GridPanel gp=(GridPanel) this.Window1.Items[0].FindControl("GridPanel1");
            string Title = "";
            for (int i = 0; i <= 24; i++)
            {
                this.GridPanel1.ColumnModel.SetHidden(i, false);
            }
            if (ddlFor.SelectedIndex < 2)
            {
                if (ddlType.SelectedIndex == 1)
                {
                    tbAuto.Text = "";
                    Title = ddlType.SelectedItem.Text + " Report From " + txtFrom.Text + " To " + txtTo.Text + " For " + ddlFor.SelectedItem.Text;
                   // ExportProcessDetails(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().RPT_GETPROCESS_DETAILS(tbAuto.Text, txtFrom.Text, txtTo.Text, ddlFor.SelectedItem.Text));
                    this.Store1.DataSource = new GreyFabric_DL().RPT_GETPROCESS_DETAILS(txtFrom.Text, txtTo.Text, ddlFor.SelectedItem.Text);
                    this.Store1.DataBind();
                    this.GridPanel1.ColumnModel.SetColumnHeader(17, "" + ddlFor.SelectedItem.Text + " Qty");


                    this.GridPanel1.ColumnModel.SetHidden(12, true);
                    this.GridPanel1.ColumnModel.SetHidden(13, true);
                    this.GridPanel1.ColumnModel.SetHidden(14, true);
                    this.GridPanel1.ColumnModel.SetHidden(15, true);
                    this.GridPanel1.ColumnModel.SetHidden(16, true);
                    this.GridPanel1.ColumnModel.SetHidden(18, true);
                    this.GridPanel1.ColumnModel.SetHidden(19, true);
                    this.GridPanel1.ColumnModel.SetHidden(20, true);
                    this.GridPanel1.ColumnModel.SetHidden(21, true);
                }
                else if (ddlType.SelectedIndex == 2)
                {
                    txtFrom.Text = "";
                    txtTo.Text = "";
                    Title = ddlType.SelectedItem.Text + " Report for Order  " + tbAuto.Text + " For " + ddlFor.SelectedItem.Text;
                    //ExportProcessDetails(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().RPT_GETPROCESS_DETAILS(tbAuto.Text, txtFrom.Text, txtTo.Text, ddlFor.SelectedItem.Text));

                    this.Store1.DataSource = new GreyFabric_DL().RPT_GETPROCESS_DETAILS_ByOrder(tbAuto.Text, ddlFor.SelectedItem.Text);
                    this.Store1.DataBind();
                    this.GridPanel1.ColumnModel.SetColumnHeader(17, "" + ddlFor.SelectedItem.Text + " Qty");


                    this.GridPanel1.ColumnModel.SetHidden(12, true);
                    this.GridPanel1.ColumnModel.SetHidden(13, true);
                    this.GridPanel1.ColumnModel.SetHidden(14, true);
                    this.GridPanel1.ColumnModel.SetHidden(15, true);
                    this.GridPanel1.ColumnModel.SetHidden(16, true);
                    this.GridPanel1.ColumnModel.SetHidden(18, true);
                    this.GridPanel1.ColumnModel.SetHidden(19, true);
                    this.GridPanel1.ColumnModel.SetHidden(20, true);
                    this.GridPanel1.ColumnModel.SetHidden(21, true);
                }
                else   //summary Report
                {
                    txtFrom.Text = "";
                    txtTo.Text = "";
                    Title = ddlType.SelectedItem.Text + " Summary Report for Order  " + tbAuto.Text + " For " + ddlFor.SelectedItem.Text;
                    //ExportProcessSummary(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().RPT_Process_Summary(tbAuto.Text, ddlFor.SelectedItem.Text));



                    txtBooking.Visible = true;
                    txtQty.Visible = true;
                    lblBooking.Visible = true;
                    lblProduction.Visible = true;
                    txtBooking.Text = string.Empty;
                    txtQty.Text = string.Empty;
                    List<RPT_KNIT_PROCESS_SUMMARYResult> li=new GreyFabric_DL().RPT_Process_Summary(tbAuto.Text, ddlFor.SelectedItem.Text);

                    txtBooking.Text = li.Sum(o => o.BookingQty).Value.ToString();
                    txtQty.Text = li.Sum(o => o.PQty).ToString();
                    upSelect.Update();
                    this.Store1.DataSource = li;
                 
                    this.Store1.DataBind();

                    this.GridPanel1.ColumnModel.SetColumnHeader(17, "Net" + ddlFor.SelectedItem.Text + "Qty");


                    
                    
                    this.GridPanel1.ColumnModel.SetHidden(9, true);
                    this.GridPanel1.ColumnModel.SetHidden(10, true);
                    this.GridPanel1.ColumnModel.SetHidden(11, true);
                    this.GridPanel1.ColumnModel.SetHidden(18, true);
                    this.GridPanel1.ColumnModel.SetHidden(19, true);

                    this.GridPanel1.ColumnModel.SetHidden(22, true);
                    this.GridPanel1.ColumnModel.SetHidden(23, true);
                    this.GridPanel1.ColumnModel.SetHidden(24, true);
                }

            }
            else //Packing Repot 
            {
                if (ddlType.SelectedIndex == 1)
                {
                    tbAuto.Text = "";
                    Title = ddlType.SelectedItem.Text + " Report From " + txtFrom.Text + " To " + txtTo.Text + " For " + ddlFor.SelectedItem.Text;
                   // ExportPackingDetails(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().RPT_PACKING_DETAILS(tbAuto.Text, txtFrom.Text, txtTo.Text));
                    this.GridPanel1.ColumnModel.SetColumnHeader(17, "" + ddlFor.SelectedItem.Text + " Qty");

                    this.Store1.DataSource = new GreyFabric_DL().RPT_PACKING_DETAILS(tbAuto.Text, txtFrom.Text, txtTo.Text);
                    this.Store1.DataBind();
                    this.GridPanel1.ColumnModel.SetHidden(11, true);
                    this.GridPanel1.ColumnModel.SetHidden(12, true);
                    this.GridPanel1.ColumnModel.SetHidden(20, true);
                    this.GridPanel1.ColumnModel.SetHidden(21, true);
                }
                else if (ddlType.SelectedIndex == 2)
                {
                    txtFrom.Text = "";
                    txtTo.Text = "";
                    Title = ddlType.SelectedItem.Text + " Report for Order  " + tbAuto.Text + " For " + ddlFor.SelectedItem.Text;
                   // ExportPackingDetails(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().RPT_PACKING_DETAILS(tbAuto.Text, txtFrom.Text, txtTo.Text));
                    this.GridPanel1.ColumnModel.SetColumnHeader(17, "" + ddlFor.SelectedItem.Text + " Qty");

                    this.Store1.DataSource = new GreyFabric_DL().RPT_PACKING_DETAILSByOrder(tbAuto.Text);
                    this.Store1.DataBind();
                    this.GridPanel1.ColumnModel.SetHidden(11, true);
                    this.GridPanel1.ColumnModel.SetHidden(12, true);
                    this.GridPanel1.ColumnModel.SetHidden(20, true);
                    this.GridPanel1.ColumnModel.SetHidden(21, true);
                }
                else
                {
                    txtFrom.Text = "";
                    txtTo.Text = "";
                    Title = ddlType.SelectedItem.Text + " Summary Report for Order  " + tbAuto.Text + " For " + ddlFor.SelectedItem.Text;
                    //ExportPackingSummary(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().RPT_PACKING_Summary(tbAuto.Text));


                    txtBooking.Visible = true;
                    txtQty.Visible = true;
                    lblBooking.Visible = true;
                    lblProduction.Visible = true;
                    txtBooking.Text = string.Empty;
                    txtQty.Text = string.Empty;
                    List<RPT_KNIT_PACKING_SUMMARYResult> li = new GreyFabric_DL().RPT_PACKING_Summary(tbAuto.Text);

                    txtBooking.Text = li.Sum(o => o.BookingQty).Value.ToString();
                    txtQty.Text = li.Sum(o => o.PQty).ToString();
                    upSelect.Update();
                    this.GridPanel1.ColumnModel.SetColumnHeader(17, "Net" + ddlFor.SelectedItem.Text + " Qty");

                    this.Store1.DataSource = li;
                    this.Store1.DataBind();
                    this.GridPanel1.ColumnModel.SetHidden(9, true);
                    this.GridPanel1.ColumnModel.SetHidden(10, true);
                    this.GridPanel1.ColumnModel.SetHidden(11, true);
                    this.GridPanel1.ColumnModel.SetHidden(22, true);
                    this.GridPanel1.ColumnModel.SetHidden(23, true);
                    this.GridPanel1.ColumnModel.SetHidden(24, true);
                }

            }
           
            
           
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
    
   
   
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
	<script type="text/javascript">
	    $(function() {
	        $(".tb").autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                url: "DataLoad.asmx/GetOrderList",
	                    data: "{ 'PINO': '" + request.term + "' }",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    dataFilter: function(data) { return data; },
	                    success: function(data) {
	                        response($.map(data.d, function(item) {
	                            return {
	                            value: item.OrderNo
	                            }
	                        }))
	                    },
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                       
	                    }
	                });
	            },
	            minLength: 2
	        });
	    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> &nbsp;Report</h2></div>


             
              <div align="left" style="padding: 10px; ">
                  Report For
                  <asp:DropDownList ID="ddlFor" runat="server">
                      <asp:ListItem>DEYING</asp:ListItem>
                      <asp:ListItem>PRINTING</asp:ListItem>
                      <asp:ListItem>PACKING</asp:ListItem>
                  </asp:DropDownList>
                  
                  Type of Report
                  <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true"
                      onselectedindexchanged="ddlType_SelectedIndexChanged">
                       <asp:ListItem>--Select Type--</asp:ListItem>
                      <asp:ListItem>Details (Date Range)</asp:ListItem>
                      <asp:ListItem>Details By Order</asp:ListItem>
                      <asp:ListItem>Summary By Order</asp:ListItem>
                  </asp:DropDownList>
              </div>
               <asp:UpdatePanel ID="upSelect" UpdateMode="Conditional" runat="server">
 <ContentTemplate>
 

 <div align="left" style="padding: 10px; ">
                    <asp:Label ID="lblFrom" runat="server" Text="From Date :" Visible="false"></asp:Label>
                      <asp:TextBox ID="txtFrom" runat="server" Visible="false"></asp:TextBox><cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom"  >
                    </cc1:CalendarExtender>
                
                       
                      <asp:Label ID="lblTo" runat="server" Text="To" Visible="false"></asp:Label><asp:TextBox ID="txtTo" runat="server" Visible="false"></asp:TextBox><cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                        Enabled="True" TargetControlID="txtTo">
                    </cc1:CalendarExtender> 
     <asp:Label ID="lblOrder" runat="server" Text="Order No" Visible="false"></asp:Label><asp:TextBox ID="tbAuto" class="tb" runat="server" Visible="false">
             </asp:TextBox>
             
              <asp:Label ID="lblBooking" runat="server" Text="Booking Qty :" ></asp:Label>
             <asp:TextBox ID="txtBooking" runat="server"  ReadOnly="True"></asp:TextBox>
             <asp:Label ID="lblProduction" runat="server" Text="Qty :" ></asp:Label>
             <asp:TextBox ID="txtQty" runat="server"  ReadOnly="True"></asp:TextBox>
             
             </div>
             
         
             
            
  
   </ContentTemplate>
     <Triggers>
         <asp:AsyncPostBackTrigger ControlID="ddlType" 
             EventName="SelectedIndexChanged" />
     </Triggers>
 </asp:UpdatePanel>
                <div align="left" style="padding: 10px; ">
                      <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                    </div>
              </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server" >  
                       
            <Reader>
            <ext:JsonReader IDProperty="SL">
                    <Fields>
                      <ext:RecordField Name="SL" />
                      <ext:RecordField Name="ProcessDate" />
                        <ext:RecordField Name="OrderNo" />
                         <ext:RecordField Name="Customer" />
                            <ext:RecordField Name="PCardNo" />
                         <ext:RecordField Name="ItemDescription" />
                           <ext:RecordField Name="Color" />
                         <ext:RecordField Name="LD_DESIGN_NO" />
                          <ext:RecordField Name="LotNo" />
                             <ext:RecordField Name="MCNo" />
                          <ext:RecordField Name="GSM" />
                          <ext:RecordField Name="Width" />
                           <ext:RecordField Name="BookingQty" />
                           
                           <ext:RecordField Name="DyedFabricReprocess" />
                           <ext:RecordField Name="ReprocessQTy" />
                          <ext:RecordField Name="GreyIssued" />
                           <ext:RecordField Name="DyedFabricQty" />
                           
                             <ext:RecordField Name="ProcessSection" />
                          <ext:RecordField Name="ProcessDescription" />
                           <ext:RecordField Name="PCardSource" />
                            
                          <ext:RecordField Name="PQty" />
                           
                              <ext:RecordField Name="LossKG" />
                                <ext:RecordField Name="LossPert" />
                                  <ext:RecordField Name="BalanceQty" />
                                    <ext:RecordField Name="Status" />  

                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Production Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server" >
                        <Columns>
                            
                          
                          
                            <ext:Column ColumnID="0SL" Header="SL"  DataIndex="SL" Sortable="true" />
                            <ext:Column ColumnID="1ProcessDate" Header="ProcessDate"  DataIndex="ProcessDate" Sortable="true" />
                            <ext:Column ColumnID="2OrderNo" Header="OrderNo"  DataIndex="OrderNo" Sortable="true" />
                            <ext:Column ColumnID="3Customer" Header="Customer"  DataIndex="Customer" Sortable="true" />
                            <ext:Column ColumnID="4ItemDescription" Header="ItemDescription"  DataIndex="ItemDescription" Sortable="true" />
                            <ext:Column ColumnID="5GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                            <ext:Column ColumnID="6Width" Header="Width"  DataIndex="Width" Sortable="true" />
                            <ext:Column ColumnID="7Color" Header="Color"  DataIndex="Color" Sortable="true" />
                            <ext:Column ColumnID="8LD_DESIGN_NO" Header="LD_DESIGN_NO"  DataIndex="LD_DESIGN_NO" Sortable="true" />
                            <ext:Column ColumnID="9LotNo" Header="LotNo"  DataIndex="LotNo" Sortable="true"  />
                            <ext:Column ColumnID="10PCardNo" Header="ProcessCard"  DataIndex="PCardNo" Sortable="true"  />
                            <ext:Column ColumnID="11MCNo" Header="MCNo"  DataIndex="MCNo" Sortable="true"  />
                            <ext:Column ColumnID="12BookingQty" Header="BookingQty"  DataIndex="BookingQty" Sortable="true" />
                            <ext:Column ColumnID="13GreyIssued" Header="GreyIssued"  DataIndex="GreyIssued" Sortable="true" />
                            <ext:Column ColumnID="14DyedFabricQty" Header="DyedFabricQty"  DataIndex="DyedFabricQty" Sortable="true" />
                            <ext:Column ColumnID="15DyedFabricReprocess" Header="DyedFabricReprocess"  DataIndex="DyedFabricReprocess" Sortable="true" />
                            <ext:Column ColumnID="16ReprocessQTy" Header="ReprocessQTy"  DataIndex="ReprocessQTy" Sortable="true" />
                            <ext:Column ColumnID="17PQty" Header="ProcessQty"  DataIndex="PQty" Sortable="true" />
                            <ext:Column ColumnID="18LossKG" Header="LossKG"  DataIndex="LossKG" Sortable="true" />
                            <ext:Column ColumnID="19LossPert" Header="Loss(%)"  DataIndex="LossPert" Sortable="true" />
                            <ext:Column ColumnID="20BalanceQty" Header="BalanceQty"  DataIndex="BalanceQty" Sortable="true" />
                            <ext:Column ColumnID="21Status" Header="Status"  DataIndex="Status" Sortable="true" />
                            <ext:Column ColumnID="22ProcessSection" Header="ProcessSection"  DataIndex="ProcessSection" Sortable="true" />
                            <ext:Column ColumnID="23ProcessDescription" Header="ProcessDescription"  DataIndex="ProcessDescription" Sortable="true" />
                            <ext:Column ColumnID="24PCardSource" Header="PCardSource"  DataIndex="PCardSource" Sortable="true" />
                            

                            
                           </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                    
                                <ext:DateFilter DataIndex="ProcessDate" />
                                <ext:StringFilter DataIndex="OrderNo" />
                                <ext:StringFilter DataIndex="Customer" />
                                <ext:StringFilter DataIndex="PCardNo" />
                                 <ext:StringFilter DataIndex="ItemDescription" />
                                <ext:StringFilter DataIndex="Color" />
                            <ext:StringFilter DataIndex="GSM" />
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
                
                 
            </Items>
        </ext:Window>

</div>
</asp:Content>
