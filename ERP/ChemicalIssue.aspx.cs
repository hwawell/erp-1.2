﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;

namespace ERP
{
    public partial class WebForm29 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //if (new Utilities_DL().CheckSecurity("503", USession.SUserID) == true)
                //{

                LoadChemical();
                LoadUnit();
                txtDate.Text = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year;
                    
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
        private void LoadChemical()
        {
            ddlcGroup.DataSource = new Chemical_DL().GetChemicalGroup();
            ddlcGroup.DataTextField = "GroupName";
            ddlcGroup.DataValueField = "SL";
            ddlcGroup.DataBind();
            ddlcGroup.Items.Add("--Select--");
            ddlcGroup.Text = "--Select--";
        }

        private void LoadUnit()
        {
            ddlUnit.DataSource = new Chemical_DL().GetUnit();
            ddlUnit.DataTextField = "UNIT1";
            ddlUnit.DataValueField = "UNIT1";
            ddlUnit.DataBind();
            ddlUnit.Items.Add("--Select--");
            ddlUnit.Text = "--Select--";
        }
       
       

      
        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Chemical_DL().DeleteIssue(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        private void loadGridData()
        {
            gvProducts.DataSource = new Chemical_DL().GetActiveIssue(Common.GetNumber(txtFrom.Text), Common.GetNumber(txtTo.Text));
            gvProducts.DataBind();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
           
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
      
      

        

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            txtBOXQTY.Text = "";
            txtFrom.Text = "";
            txtTotalQTY.Text = "";
            txtNotes.Text = "";
            ddlChemical.Focus();
            UpEntry.Update();
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            loadGridData();
        }

        protected void btnload_Click(object sender, EventArgs e)
        {
            loadGridData();
        }

        protected void ddlcGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlcGroup.Text != "--Select--")
            {
                ddlChemical.DataSource = new Chemical_DL().GetActiveRecord(Convert.ToInt32(ddlcGroup.SelectedValue.ToString()));
                ddlChemical.DataValueField = "SL";
                ddlChemical.DataTextField = "ChemicalName";
                ddlChemical.DataBind();
                ddlChemical.Items.Add("--Select--");
                ddlChemical.Text = "--Select--";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckEntry() == true)
                {
                    ChemicalIssue obj = new ChemicalIssue();
                    obj.Qty = Common.GetDecimal(txtTotalQTY.Text);
                    obj.Unit = ddlUnit.SelectedItem.Text;
                    obj.CID = Convert.ToInt64(ddlChemical.SelectedValue);
                    obj.CTNQTy = Common.GetDecimal(txtBOXQTY.Text);
                    obj.EntryID=USession.SUserID;
                    obj.IssueDesc = txtNotes.Text;
                    obj.Idate = Common.GetDate(txtDate.Text);
                    if (obj.Qty > 0)
                    {
                        if (new Chemical_DL().SaveISSUE(obj) == true)
                        {
                            lblMsg.Text = "Saved Successfully";
                            txtBOXQTY.Text = "";
                            txtFrom.Text = "";
                            txtTotalQTY.Text = "";
                            txtNotes.Text = "";

                            ddlChemical.Focus();


                        }
                        else
                        {
                            lblMsg.Text = "Saved Failed! Try Again";
                        }
                    }
                    else
                    {
                        lblMsg.Text = "Enter Qty";
                    }
                }
            }
            catch
            {
                lblMsg.Text = "Saved Failed! Try Again";
            }
            UpEntry.Update();
        }
        private bool CheckEntry()
        {
            if (ddlUnit.Text == "--Select--")
            {
                lblMsg.Text = "Select Unit";
                return false;
            }
            else if (ddlcGroup.Text == "--Select--")
            {
                lblMsg.Text = "Select Chemical Group";
                return false;
            }
            else if (ddlChemical.Text == "--Select--")
            {
                lblMsg.Text = "Select Chemical";
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
