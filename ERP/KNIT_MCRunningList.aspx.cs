﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class KNIT_MCRunningList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnOperation_Click(object sender, EventArgs e)
        {
            Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            if (btnEdit.Text == "Stop")
            {
                row.ForeColor = System.Drawing.Color.Red;
                row.Font.Bold = true;
                Int64 ID=Common.GetNumber( row.Cells[0].Text);
                btnEdit.Text = "Stopped";
                btnEdit.Enabled = false;
                bool res = new KNIT_all_operation().StopRunningMC(ID,row.Cells[1].Text.Trim(), false);
            }
           
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            gvProducts.DataSource = new KNIT_all_operation().GetRunningMCList();
            gvProducts.DataBind();

        }

        protected void gvProducts_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.ForeColor = System.Drawing.Color.Green;
            }
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                // e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void gvProducts1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
    }
}
