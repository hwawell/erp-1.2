﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="WarehouseDelivery.aspx.cs" Inherits="ERP.WebForm28" Title="Warehouse Delivery" %>
<%@ Register assembly="AjaxControls" namespace="AjaxControls" tagprefix="cc2" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style3
        {
            height: 48px;
        }
        .style4
        {
            width: 92px;
        }
        .style5
        {
            width: 256px;
        }
        .style6
        {
            height: 388px;
        }
        .style8
        {
            width: 45px;
        }
        .style9
        {
            width: 378px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="width: 101%; height: 547px;">
        <tr>
            <td class="style3" colspan="3">
             <asp:UpdatePanel ID="upentry" runat="server" UpdateMode="Conditional">
             <ContentTemplate>
                 <table cellpadding="0" cellspacing="0" style="width: 96%;">
                    <tr>
                        <td class="style4">
                            &nbsp;</td>
                        <td class="style5">
                            <asp:TextBox ID="txtID" runat="server" Height="22px" Visible="False" 
                                Width="46px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style4">
                            Order No</td>
                        <td class="style5">
                <asp:DropDownList ID="ddlOrder" runat="server" Height="27px" Width="187px" 
                                AutoPostBack="True" onselectedindexchanged="ddlOrder_SelectedIndexChanged" 
                                TabIndex="2">
                </asp:DropDownList>
                            <asp:Button ID="btnGo" runat="server" onclick="Button3_Click" Text="Go" />
                        </td>
                        <td>
                            Date<asp:TextBox ID="txtDate" runat="server" BorderStyle="Groove" 
                                BorderWidth="1px" Height="22px"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                                Enabled="True" TargetControlID="txtDate">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="style4">
                            Customer</td>
                        <td class="style5">
                            <asp:TextBox ID="txtCustomerName" runat="server" BorderStyle="None" Height="21px" 
                                Width="253px" BackColor="#DDDDDD" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddress" runat="server" BorderStyle="None" Height="21px" 
                                Width="355px" BackColor="#DDDDDD" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style4">
                            Challan</td>
                        <td class="style5">
                            <asp:TextBox ID="txtChallan" runat="server" BorderStyle="None" Height="21px" 
                                TabIndex="3"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Description</td>
                        <td>
                            <asp:TextBox ID="txtDescription" runat="server" BorderStyle="None" Height="20px" 
                                Width="354px" TabIndex="4"></asp:TextBox>
                        </td>
                    </tr>
                </table>
             </ContentTemplate>
                 <Triggers>
                     <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                     <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                 </Triggers>
             </asp:UpdatePanel>
               
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3" valign="top" class="style6">
              <div style="overflow: auto; height: 391px;">
                &nbsp;
                <asp:UpdatePanel ID ="upGrid" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                           
                    <asp:GridView ID="grvData" runat="server" AutoGenerateColumns="False" 
                    Height="69px" onrowdatabound="grvData_RowDataBound">
                    <Columns>
                         <asp:BoundField DataField="W" HeaderText="" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="chk1" runat="server" Checked="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="PCardNo" HeaderText="Process Card" />
                        <asp:BoundField DataField="RollNo" HeaderText="Roll No" />
                        <asp:BoundField DataField="WeightQty" HeaderText="Weight" />
                        <asp:BoundField DataField="Date" HeaderText="Date" />
                        <asp:BoundField DataField="SL" />
                    </Columns>
                </asp:GridView>
                 
                 </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlOrder" 
                            EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="btnGo" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                  <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                      AssociatedUpdatePanelID="upGrid">
                  <ProgressTemplate>
                      <img alt="" src="Images/loading.gif" />
                      <br />
                      Plz. wait..
                  </ProgressTemplate>
                  
                  </asp:UpdateProgress>
              </div>
            </td>
        </tr>
        <tr>
            <td class="style9">
                &nbsp;</td>
            <td class="style8">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style9">
                <asp:Button ID="btnClear" runat="server" Text="Clear" />
            </td>
            <td class="style8">
                <asp:Button ID="btnCalculate" runat="server" Text="Calculate " 
                    onclick="btnCalculate_Click" />
            </td>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="Save Delivery" 
                    onclick="btnSave_Click" TabIndex="6" />
            </td>
        </tr>
        <tr>
            <td align="right" class="style9">
                &nbsp;</td>
            <td class="style8">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
