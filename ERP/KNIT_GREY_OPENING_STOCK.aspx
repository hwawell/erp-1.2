﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_GREY_OPENING_STOCK.aspx.cs" Inherits="ERP.KNIT_GREY_OPENING_STOCK" Title="Grey Opening Stock Setup" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div><h3>Grey Opening Stock Setup</h3></div>
    <div>Fabric Group
                <asp:DropDownList ID="ddGroup" runat="server" AutoPostBack="True" Height="21px" 
                     Width="170px">
                </asp:DropDownList>  
        <asp:Button ID="btnLoad" runat="server" Text="Load" onclick="btnLoad_Click" />
            </div>
            <div style="overflow: auto; height: 500px">
                <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" />
                        <asp:BoundField DataField="Fabric" HeaderText="Fabric" />
                        <asp:BoundField DataField="GM" HeaderText="GM" />
                        <asp:BoundField DataField="GSM" HeaderText="GSM" />
                        <asp:BoundField DataField="MCNo" HeaderText="MCNo" />
                        
                        <asp:TemplateField HeaderText="Opening Qty" >
                        <ItemTemplate>
                            <asp:TextBox ID="txtQty" runat="server"  Width="120px" BorderStyle="None"></asp:TextBox>
                        </ItemTemplate>
                        
                    </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle BackColor="#FFCCFF" />
                </asp:GridView>
            </div>
            <div>
                <asp:Button ID="btnSave" runat="server" Text="Save Opening Stock" 
                    onclick="btnSave_Click" />
            </div>
            <asp:Button ID="btnHiddenProduct" runat="Server" Style="display:none"  />
                              
             <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlDataEntry" runat="server" Style="display:none"  CssClass="modalBox"  
                        BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                        Width="448px" BorderWidth="2px">
                        <center>
                            <asp:Panel ID="ProductCaption" runat="server"   
                                Style="margin-bottom: 10px; cursor: hand;" 
                                BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                Result</asp:Panel></center>
                            <cc1:CollapsiblePanelExtender ID="ProductCaption_CollapsiblePanelExtender" 
                                runat="server" Enabled="True" TargetControlID="ProductCaption">
                            </cc1:CollapsiblePanelExtender>
                            <div>
                                <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                            </div>
                            <div>
                                <asp:Button ID="btnOK" runat="server" Text="OK" onclick="btnOK_Click" />
                            </div>
                    </asp:Panel>
                       
                    
</asp:Content>
