﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="WarehouseReceive.aspx.cs" Inherits="ERP.WebForm31" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style3
        {
            width: 232px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%; height: 79px;">
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td style="color: #003366; font-weight: bold">
                Warehouse Receive</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                <asp:Label ID="Label1" runat="server" Text="Order No"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddOrder" runat="server" AutoPostBack="True" Height="21px" 
                    onselectedindexchanged="DropDownList1_SelectedIndexChanged" Width="282px">
                </asp:DropDownList>
                <cc1:ListSearchExtender ID="ddOrder_ListSearchExtender" runat="server" 
                    Enabled="True" TargetControlID="ddOrder">
                </cc1:ListSearchExtender>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                <asp:Label ID="Label4" runat="server" Text="Process Card No"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddPcard" runat="server" AppendDataBoundItems="True" 
                    Height="21px" onselectedindexchanged="DropDownList2_SelectedIndexChanged" 
                    Width="150px">
                </asp:DropDownList>
                
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                <asp:Label ID="Label5" runat="server" Text="Lot No"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddLotNo" runat="server" AppendDataBoundItems="True" 
                    Height="21px" onselectedindexchanged="DropDownList3_SelectedIndexChanged" 
                    Width="150px">
                </asp:DropDownList>
                <cc1:ListSearchExtender ID="ddLotNo_ListSearchExtender" runat="server" 
                    Enabled="True" TargetControlID="ddLotNo">
                </cc1:ListSearchExtender>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                <asp:Label ID="Label6" runat="server" Text="Roll No"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddRoll" runat="server" AppendDataBoundItems="True" 
                    Height="21px" onselectedindexchanged="DropDownList4_SelectedIndexChanged" 
                    Width="150px">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                Color</td>
            <td>
                <asp:DropDownList ID="ddColor" runat="server" Height="21px" Width="284px">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                Weight(Qty)</td>
            <td>
                <asp:TextBox ID="txtQty" runat="server" ontextchanged="txtQty_TextChanged"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                Date</td>
            <td>
                <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
