<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Warehouse_Packing_Delivery_Report.aspx.cs" Inherits="ERP.Warehouse_Packing_Delivery" Title="Delivery Report" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
   [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window3.Hide();
            Window2.Hide();
            Window1.Show();
            List<EWareHouseDelivery> liProd = new List<EWareHouseDelivery>();
            this.Window1.Title = "Delivery Detail Report";
            liProd = new KNIT_all_operation().GetWareHouseDelivery(OpenFunction.ConvertDate(txtFrom.Text), OpenFunction.ConvertDate(txtFrom0.Text), tbAuto.Text);
                    
            
            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
     [DirectMethod(Namespace = "NETBEE")]
     public void ShowReport1()
     {
         //System.Threading.Thread.Sleep(3000);

         try
         {
             Window1.Hide();
             Window3.Hide();
             Window2.Show();
             List<EWareHouseDeliverySummary> liProd = new List<EWareHouseDeliverySummary>();
             this.Window1.Title = "Delivery Summary Report";
             liProd = new KNIT_all_operation().GetWareHouseDeliverySummary(OpenFunction.ConvertDate(txtFrom.Text), OpenFunction.ConvertDate(txtFrom0.Text), tbAuto.Text);


             this.Store2.DataSource = liProd;
             this.Store2.DataBind();

             X.Mask.Hide();
         }
         catch
         {
             X.Mask.Hide();
         }

     }
     [DirectMethod(Namespace = "NETBEE")]
     public void ShowDeliveryReport1()
     {
         //System.Threading.Thread.Sleep(3000);

         try
         {
             Window1.Hide();
             Window2.Hide();
            
             Window3.Show();
             List<rpt_packing_Delivery_summary_byColorResult> liProd = new List<rpt_packing_Delivery_summary_byColorResult>();
             this.Window1.Title = "Delivery Summary Report";
             liProd = new KNIT_all_operation().GetWareHouseDeliveryByColor(tbAuto.Text);


             this.Store3.DataSource = liProd;
             this.Store3.DataBind();

             X.Mask.Hide();
         }
         catch
         {
             X.Mask.Hide();
         }

     }
    
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
   <script type="text/javascript">
       $(function() {

           function split(val) {
               return val.split(/,\s*/);
           }
           function extractLast(term) {
               
               return split(term).pop();
           }
           $(".tb")
           .bind("keydown", function(event) {
               if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("ui-autocomplete").menu.active) {
                   event.preventDefault();
               }
           })
           .autocomplete({
               source: function(request, response) {
                   $.ajax({
                       url: "DataLoad.asmx/GetOrderList",
                       data: "{ 'PINO': '" + extractLast(request.term) + "' }",
                       dataType: "json",
                       type: "POST",
                       contentType: "application/json; charset=utf-8",
                       dataFilter: function(data) { return data; },
                       success: function(data) {
                           response($.map(data.d, function(item) {
                               return {
                                   value: extractLast(item.OrderNo)
                               }
                           }))
                       },
                       error: function(XMLHttpRequest, textStatus, errorThrown) {

                       }
                   });
               },


               search: function() {
                   // custom minLength
                   var term = extractLast(this.value);
                   if (term.length < 2) {
                       return false;
                   }
               },
               focus: function() {
                   // prevent value inserted on focus
                   return false;
               },
               select: function(event, ui) {
                   var terms = split(this.value);
                   // remove the current input
                   terms.pop();
                   // add the selected item
                   terms.push(ui.item.value);
                   // add placeholder to get the comma-and-space at the end
                   terms.push("");
                   this.value = terms.join(", ");
                   return false;
               }
           });



       });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> Delivery Report</h2></div>
<div align="left">
    Select Order(s)  <asp:TextBox ID="tbAuto" runat="server" class="tb" Width="700px"> </asp:TextBox>
</div>
 <div align="left" style="padding: 10px; ">
                      Date                     <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                      To                     <asp:TextBox ID="txtFrom0" runat="server"></asp:TextBox>
                             <cc1:CalendarExtender ID="txtFrom0_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom0">
                    </cc1:CalendarExtender>
                                    &nbsp;
                             <cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom">
                    </cc1:CalendarExtender>
                                   
             </div>
               <div style="float: left; width: 846px;" align="left">
                         <div style="float:left;width:20%;padding-top:10px;" align="left">
                             <ext:Button ID="Button1" runat="server" Height="22" Width ="150" Text="Fabric-Detail">
                            <Listeners>
                                <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                            </Listeners>
                            </ext:Button>
                         </div>
                          <div style="float:left;width:80%;padding-top:10px;">
                       <asp:Button ID="btnExport" runat="server" Text="Fabric-Detail-Export To XL" Width="250" Height="22"
                             onclick="btnExport_Click" />
                            </div> 
                        
                         <div style="float:left;width:20%;padding-top:10px;">
                              <ext:Button ID="Button2" runat="server" Height="22" Width ="150" Text="Fabric-Summary Report">
                            <Listeners>
                                <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport1();" />
                            </Listeners>
                            </ext:Button>
                         </div>
                         
                          <div style="float:left;width:80%;padding-top:10px;">
                                <asp:Button ID="Button3" runat="server" Text="Fabric-Summary- Export To XL" Width="250" Height="22"
                                onclick="btnExport1_Click" />
                          </div> 
                          
                          
                             <div style="float:left;width:20%;padding-top:10px;">
                              <ext:Button ID="Button5" runat="server" Height="22" Width ="150" Text="Delivery Summary">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowDeliveryReport1();" />
                        </Listeners>
                        </ext:Button>
                        </div>
                          <div style="float:left;text-align:left;padding-top:10px; width:80%" align="left">
                         <asp:Button ID="Button6" runat="server" 
                             Text="Delivery Summary- Export To XL" Width="250px" Height="22px" onclick="Button6_Click"
                             />
                              </div>
                              <div style="float:left;width:100%;padding-top:10px;">
                            <asp:Button ID="Button4" runat="server" 
                             Text="General-Delivery-Detail- Export To XL" Width="250px" Height="22px"
                             onclick="btnGEN_Click" />
                             </div>
                               <br />
                    </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
           
            >
            <Reader>
         
            <ext:JsonReader IDProperty="SL">
                    <Fields>
                      <ext:RecordField Name="SL" />
                      <ext:RecordField Name="DeliveryDate" />
                         <ext:RecordField Name="Merchandiser" />
                          <ext:RecordField Name="OrderNo" />
                           <ext:RecordField Name="Customer" />
                           
                        <ext:RecordField Name="ItemDescription" />
                         <ext:RecordField Name="GSM" />
                         <ext:RecordField Name="Width" />
                       
                          <ext:RecordField Name="Challan" />
                           <ext:RecordField Name="GetPassNo" />
                           <ext:RecordField Name="VehicleNo" />
                         <ext:RecordField Name="Color" />
                         <ext:RecordField Name="Lotno" />
                          <ext:RecordField Name="ProcessCard" />
                          <ext:RecordField Name="DeliveredRollQty" />
                          <ext:RecordField Name="DeliveredQtyKGS" />
                          
                           <ext:RecordField Name="Unit" />
                        <ext:RecordField Name="DeliveredQtyByUnit" />
                          <ext:RecordField Name="DeliveryStatus" />
                          <ext:RecordField Name="AssignFrom" />
                          <ext:RecordField Name="DeliveryNotes" />
                  
                       
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        <ext:Store ID="Store2" runat="server"   
           
            >
            <Reader>
         
            <ext:JsonReader IDProperty="SL">
                    <Fields>
                     
                        <ext:RecordField Name="SL" />
                        <ext:RecordField Name="Customer" />
                        <ext:RecordField Name="OrderNo" />
                        <ext:RecordField Name="Merchandiser" />
                        <ext:RecordField Name="LastDelivery" />
                           
                        <ext:RecordField Name="BookingKG" />
                        <ext:RecordField Name="BookingYDS" />
                        <ext:RecordField Name="DeliveryKG" />
                        <ext:RecordField Name="DeliveryYDS" />
                        <ext:RecordField Name="Unit" />
                         
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        
        <ext:Store ID="Store3" runat="server"   >
           
            
            <Reader>
         
            <ext:JsonReader IDProperty="SL">
                    <Fields>
                     
                        <ext:RecordField Name="SL" />
                        <ext:RecordField Name="Customer" />
                        <ext:RecordField Name="Merchandiser" />
                        <ext:RecordField Name="PINO" />
                        <ext:RecordField Name="Description" />
                          <ext:RecordField Name="Notes" /> 
                        <ext:RecordField Name="GSM" />
                        <ext:RecordField Name="Width" />
                        <ext:RecordField Name="Color" />
                        
                        <ext:RecordField Name="BookingKGS" />
                        <ext:RecordField Name="DeliveryQty" />
                          <ext:RecordField Name="AssignDeliveryQty" />
                        <ext:RecordField Name="ManualDeliveryQty" />
                         <ext:RecordField Name="NetDeliveryQty" />
                        <ext:RecordField Name="Balance" />
                         <ext:RecordField Name="Short_Excess" />
                        <ext:RecordField Name="DeliveryPercent" />
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Production Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            
                            
                             <ext:Column ColumnID="SL" Header="SL"  DataIndex="SL" Sortable="true" />
                             <ext:Column ColumnID="Merchandiser" Header="Merchandiser"  DataIndex="Merchandiser" Sortable="true" />
                            <ext:Column ColumnID="DeliveryDate" Header="DeliveryDate"    DataIndex="DeliveryDate" Sortable="true" />
                            <ext:Column ColumnID="OrderNo" Header="OrderNo"  DataIndex="OrderNo" Sortable="true" />
                            <ext:Column ColumnID="Customer" Header="Customer"  DataIndex="Customer" Sortable="true" />
                            <ext:Column ColumnID="ItemDescription" Header="ItemDescription"  DataIndex="ItemDescription" Sortable="true" />
                            <ext:Column ColumnID="GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                            <ext:Column ColumnID="Width" Header="Width"  DataIndex="Width" Sortable="true" />
                            
                            <ext:Column ColumnID="Color" Header="Color"  DataIndex="Color" Sortable="true" />
                            <ext:Column ColumnID="Lotno" Header="Lotno"  DataIndex="Lotno" Sortable="true" />
                            <ext:Column ColumnID="ProcessCard" Header="ProcessCard"  DataIndex="ProcessCard" Sortable="true" />
                            <ext:Column ColumnID="Challan" Header="Challan"  DataIndex="Challan" Sortable="true" />
                            <ext:Column ColumnID="GetPassNo" Header="GetPassNo"  DataIndex="GetPassNo" Sortable="true" />
                            <ext:Column ColumnID="VehicleNo" Header="VehicleNo"  DataIndex="VehicleNo" Sortable="true" />
                            
                            <ext:Column ColumnID="DeliveredRollQty" Header="DeliveredRollQty"  DataIndex="DeliveredRollQty" Sortable="true" />
                            <ext:Column ColumnID="DeliveredQtyKGS" Header="Delivered Qty (KGS)"  DataIndex="DeliveredQtyKGS" Sortable="true" />
                             <ext:Column ColumnID="Unit" Header="Unit"  DataIndex="Unit" Sortable="true" />
                             <ext:Column ColumnID="DeliveredQtyByUnit" Header="Delivered Qty (By Unit)"  DataIndex="DeliveredQtyByUnit" Sortable="true" />
                             
                                <ext:Column ColumnID="DeliveryStatus" Header="DeliveryStatus"  DataIndex="DeliveryStatus" Sortable="true" />
                             <ext:Column ColumnID="AssignFrom" Header="AssignFrom"  DataIndex="AssignFrom" Sortable="true" />
                             <ext:Column ColumnID="DeliveryNotes" Header="DeliveryNotes"  DataIndex="DeliveryNotes" Sortable="true" />
                             

                                                       
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                             
                                <ext:StringFilter DataIndex="Merchandiser" />
                                <ext:StringFilter DataIndex="OrderNo" />
                                <ext:StringFilter DataIndex="Customer" />
                                <ext:StringFilter DataIndex="ItemDescription" />
                                <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="Width" />
                                <ext:StringFilter DataIndex="Color" />
                                
                                <ext:StringFilter DataIndex="DeliveryDate" />
                                <ext:StringFilter DataIndex="Challan" />
                                <ext:StringFilter DataIndex="GetPassNo" />
                                
                                <ext:NumericFilter DataIndex="Lotno" />
                                <ext:NumericFilter DataIndex="ProcessCard" />
                                 <ext:NumericFilter DataIndex="DeliveredRollQty" />
                                  <ext:NumericFilter DataIndex="DeliveredQtyKGS" />
                                   <ext:NumericFilter DataIndex="Unit" />
                                    <ext:NumericFilter DataIndex="DeliveredQtyByUnit" />
                                  <ext:StringFilter DataIndex="DeliveryStatus" />
                                <ext:StringFilter DataIndex="AssignFrom" />
                                <ext:StringFilter DataIndex="DeliveryNotes" />
                                                   
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>
        
         <ext:Window 
            ID="Window2" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Production Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel2" 
                    runat="server" 
                    StoreID="Store2"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="RowSelectionModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel2" runat="server">
                        <Columns>
                            <ext:Column ColumnID="SL" Header="SL"  DataIndex="SL" Sortable="true" />
                            <ext:Column ColumnID="Customer" Header="Customer"  DataIndex="Customer" Sortable="true" />
                            <ext:Column ColumnID="OrderNo" Header="OrderNo"  DataIndex="OrderNo" Sortable="true" />
                            <ext:Column ColumnID="Merchandiser" Header="Merchandiser"  DataIndex="Merchandiser" Sortable="true" />
                            <ext:Column ColumnID="BookingKG" Header="Booking Qty KG"  DataIndex="BookingKG" Sortable="true" />
                            <ext:Column ColumnID="BookingYDS" Header="Booking Qty(Order unit)"  DataIndex="BookingYDS" Sortable="true" />
                            <ext:Column ColumnID="Unit" Header="Order Unit"  DataIndex="Unit" Sortable="true" />
                            <ext:Column ColumnID="DeliveryKG" Header="Delivery Qty (KG)"  DataIndex="DeliveryKG" Sortable="true" />
                            <ext:Column ColumnID="DeliveryYDS" Header="Delivery Qty (Order Unit)"  DataIndex="DeliveryYDS" Sortable="true" />
                                                 
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters2" Local="true">
                            <Filters>
                             
                                <ext:StringFilter DataIndex="Customer" />
                                <ext:StringFilter DataIndex="OrderNo" />
                                <ext:StringFilter DataIndex="Merchandiser" />
                                <ext:StringFilter DataIndex="LastDelivery" />
                                <ext:StringFilter DataIndex="BookingKG" />
                                <ext:StringFilter DataIndex="BookingYDS" />
                                <ext:StringFilter DataIndex="DeliveryKG" />
                                
                                
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar2" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

          <ext:Window 
            ID="Window3" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Production Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel3" 
                    runat="server" 
                    StoreID="Store3"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="RowSelectionModel2" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel3" runat="server">
                        <Columns>
                       
                            <ext:Column ColumnID="SL" Header="SL"  DataIndex="SL" Sortable="true" />
                            <ext:Column ColumnID="Customer" Header="Customer"  DataIndex="Customer" Sortable="true" />
                            <ext:Column ColumnID="Merchandiser" Header="Merchandiser"  DataIndex="Merchandiser" Sortable="true" />
                            <ext:Column ColumnID="PINO" Header="PINO"  DataIndex="PINO" Sortable="true" />
                            <ext:Column ColumnID="Description" Header="Description"  DataIndex="Description" Sortable="true" />
                            <ext:Column ColumnID="Notes" Header="Notes"  DataIndex="Notes" Sortable="true" />
                            <ext:Column ColumnID="GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                            <ext:Column ColumnID="Width" Header="Width"  DataIndex="Width" Sortable="true" />
                            <ext:Column ColumnID="Color" Header="Color"  DataIndex="Color" Sortable="true" />
                              
                              
                               <ext:Column ColumnID="BookingKGS" Header="BookingKGS"  DataIndex="BookingKGS" Sortable="true" />
                            <ext:Column ColumnID="DeliveryQty" Header="DeliveryQty"  DataIndex="DeliveryQty" Sortable="true" />
                            <ext:Column ColumnID="AssignDeliveryQty" Header="AssignDeliveryQty"  DataIndex="AssignDeliveryQty" Sortable="true" />
                            <ext:Column ColumnID="ManualDeliveryQty" Header="ManualDeliveryQty"  DataIndex="ManualDeliveryQty" Sortable="true" />
                            
                             <ext:Column ColumnID="NetDeliveryQty" Header="NetDeliveryQty"  DataIndex="NetDeliveryQty" Sortable="true" />
                            <ext:Column ColumnID="Balance" Header="Balance"  DataIndex="Balance" Sortable="true" />
                            <ext:Column ColumnID="Short_Excess" Header="Short_Excess"  DataIndex="Short_Excess" Sortable="true" />
                            <ext:Column ColumnID="DeliveryPercent" Header="Delivery(%)"  DataIndex="DeliveryPercent" Sortable="true" />                   
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters3" Local="true">
                            <Filters>
                             
                                <ext:StringFilter DataIndex="Customer" />
                                <ext:StringFilter DataIndex="PINO" />
                                <ext:StringFilter DataIndex="Merchandiser" />
                                <ext:StringFilter DataIndex="Description" />
                                <ext:StringFilter DataIndex="Notes" />
                                
                                
                                
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar3" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>

