<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_RollSummary.aspx.cs" Inherits="ERP.KNIT_RollSummary" Title="Untitled Page" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script runat="server">
   
 
    [DirectMethod]
    public void RowClick(int i, string id, string data)
    {

      
        
        
        
        XmlNode xml = JSON.DeserializeXmlNode("{records:{record:" + data + "}}");

            string ID ="" ;
            string Fabric ="" ;
            string GM ="" ;
            string GSM ="" ;
            string MCNo ="" ;

            string StartDate ="" ;
            string EndDate ="" ;

        foreach (XmlNode row in xml.SelectNodes("records/record"))
        {
             ID = row.SelectSingleNode("ID").InnerXml;
             Fabric = row.SelectSingleNode("Fabric").InnerXml;
             GM = row.SelectSingleNode("GM").InnerXml;
             GSM = row.SelectSingleNode("GSM").InnerXml;
             MCNo = row.SelectSingleNode("MCNo").InnerXml;

             StartDate = row.SelectSingleNode("StartDate").InnerXml;
             EndDate = row.SelectSingleNode("StopDate").InnerXml;

        }
        
        string d=data.Replace("{","").Replace("''","");
        List<EProuctionRoll> li = new KNIT_all_operation().GetProuctionRoll(Convert.ToInt64(id));
        Store2.DataSource = li;
        this.Store2.DataBind();
        
        hdnValue.Value = id;
       // Window2.Title = data;
        lblStatus.Text = "MCNo :"+ MCNo +" Fabric : " + Fabric + " GM :"+ GM + " GSM:"+ GSM;
        Window2.Show();
        //X.Msg.Alert("RowClick", i + "<br/>" + id + "<br/>" + data).Show();
        
    }
    

   
     
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div>

</div>
<div>
<%--<asp:UpdatePanel ID="upEntry" UpdateMode="Conditional" runat="server">
<ContentTemplate>--%>


  <div>
  
        <fieldset>
<legend><h4> Knit Roll Summary Finish</h4></legend>
  
    <div style="float: left; width: 20%" align="right">
        MC No :
    </div>
    <div style="float: left; width: 80%" align="left">
    
    
                <asp:DropDownList ID="ddlMCNo" runat="server" Height="33px" Width="138px">
                </asp:DropDownList>
                                      From Date
                <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtFrom">
                </cc1:CalendarExtender>
                &nbsp;To
                <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="txtFrom0_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtTo">
                </cc1:CalendarExtender>
                                      <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                          onclick="btnSearch_Click" />
                                      <asp:Button ID="btnExport" runat="server" Text="Export" 
                                          onclick="btnExport_Click" />
                                      <asp:Button ID="btnExportDetailsSelected" runat="server" Text="Export Details" 
                                          onclick="btnExportDetails_Click" />
                <asp:HiddenField ID="hdnValue" runat="server" />
    </div>
    
   
    
 </fieldset>   
 </div>
<%-- 
 </ContentTemplate>
</asp:UpdatePanel>--%>
    
 
    <div>
        <div> 
                <ext:Store ID="Store1" runat="server"   
           
            >
            <Reader>
            <ext:JsonReader IDProperty="ID">
                    <Fields>
                      <ext:RecordField Name="ID" />
                     
                        
                        <ext:RecordField Name="MCNo" />
                         <ext:RecordField Name="Fabric" />
                         <ext:RecordField Name="GM" />
                         <ext:RecordField Name="GSM" />
                         
                         <ext:RecordField Name="StartDate" />
                         <ext:RecordField Name="StopDate" />
                          <ext:RecordField Name="Status" />
                          <ext:RecordField Name="TotalRoll" />
                          <ext:RecordField Name="TotalKG" />
                       
                          
                     
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Roll Summary Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                      <SelectionModel>
            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" />
        </SelectionModel>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                         
                          
                             <ext:Column ColumnID="MCNo" Header="MCNo"  DataIndex="MCNo" Sortable="true" />
                            <ext:Column ColumnID="Fabric" Header="Fabric"  DataIndex="Fabric" Sortable="true" />
                            <ext:Column ColumnID="GM" Header="GM"  DataIndex="GM" Sortable="true" />
                            <ext:Column ColumnID="GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                            <ext:Column ColumnID="StartDate" Header="StartDate"  DataIndex="StartDate" Sortable="true" />
                            <ext:Column ColumnID="StopDate" Header="StopDate"  DataIndex="StopDate" Sortable="true" />
                            
                            <ext:Column ColumnID="Status" Header="Status"  DataIndex="Status" Sortable="true" />
                            <ext:Column ColumnID="TotalRoll" Header="TotalRoll"  DataIndex="TotalRoll" Sortable="true" />
                            <ext:Column ColumnID="TotalKG" Header="TotalKG"  DataIndex="TotalKG" Sortable="true" />
                            
                            
                            
                       </Columns>
                    </ColumnModel>
                    
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="MCNo" />
                                <ext:StringFilter DataIndex="Fabric" />
                                <ext:StringFilter DataIndex="GM" />
                                <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="StartDate" />
                                 <ext:NumericFilter DataIndex="StopDate" />
                                <ext:StringFilter DataIndex="Status" />
                                 <ext:NumericFilter DataIndex="TotalRoll" />
                                  <ext:NumericFilter DataIndex="TotalKG" />
                                
                               
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                     
                    <Listeners>
                        <RowDblClick Handler="Ext.net.DirectMethods.RowClick(
                                                rowIndex,
                                                this.store.getAt(rowIndex).id, 
                                                Ext.encode(this.store.getAt(rowIndex).data));" />
                    </Listeners>
                </ext:GridPanel>
            </Items>
        </ext:Window>
        
        
        
        
          
         </div>
            
         <div>
              <ext:Store ID="Store2" runat="server"  > 
                <Reader>
            <ext:JsonReader IDProperty="RollNo">
                    <Fields>
                      <ext:RecordField Name="RollNo" />
                     
                        
                        <ext:RecordField Name="RollQty" />
                                                 
                     
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
               <ext:Window 
            ID="Window2" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="DoorOpen"
            Title="Roll Summary Details Report"
            Width="500px"
            Height="200"
             Y="500"
             X="400"
           
            Layout="FitLayout" >
            <Items>
               
                <ext:GridPanel
                    ID="GridPanel2" 
                    runat="server" 
                    StoreID="Store2"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="RowSelectionModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>  
                    <TopBar>
                                            <ext:Toolbar ID="Toolbar1" runat="server">
                                                <Items>
                                                    
                                                    <ext:Label ID="lblStatus" runat="server"></ext:Label>
                                                </Items>
                                            </ext:Toolbar>
                         </TopBar>     
                    <ColumnModel ID="ColumnModel2" runat="server">
                        <Columns>
                         
                          
                             <ext:Column ColumnID="RollNo" Header="RollNo"  DataIndex="RollNo" Sortable="true" />
                            <ext:Column ColumnID="RollQty" Header="RollQty"  DataIndex="RollQty" Sortable="true" />
                            
                            
                            
                       </Columns>
                    </ColumnModel>
                    
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters2" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="RollNo" />
                                <ext:StringFilter DataIndex="RollQty" />
                             
                                
                               
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                   
                   
                     
                   
                </ext:GridPanel>
            </Items>
           
            
        </ext:Window>
         </div>
    </div>
   
</div>
</asp:Content>
