﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
namespace ERP
{
    public partial class DyedFab_Receive : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (new Utilities_DL().CheckSecurity("502", USession.SUserID) == true)
                //{

                loadGridData();
                HideControl();
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
        private void HideControl()
        {

            
        }
       
        private void loadGridData()
        {




           

           

            loadGrid();

        }
        private void loadGrid()
        {
            gvProducts.DataSource = new GreyFabric_DL().GetReceive(txtOrderNo.Text);
            gvProducts.DataBind();

            LoadOrderProductDesc();
        }

       
       
        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            
            lblResult.Text = string.Empty;
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            ddlProduct.SelectedValue = row.Cells[1].Text;
            txtReceiveQty.Text = row.Cells[3].Text;
            txtRoll.Text = row.Cells[4].Text;
            txtInvoiceNo.Text = row.Cells[5].Text;
            txtNotes.Text = row.Cells[7].Text;
            txtReceiveDate.Text = Common.GetDate(row.Cells[8].Text).ToString("mm/dd/yyyy");
            ddlSection.SelectedValue = row.Cells[6].Text;

            updPanel.Update();

            mpeProduct.Show();
        }
        private string RemoveSpace(string str)
        {
            return str.Replace("&nbsp;", "").Trim();
        }
        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
         


            new GreyFabric_DL().DeyedFabricDelete(Common.GetNumber(row.Cells[0].Text), false);
            loadGrid();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
           
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGrid();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                if (e.Row.Cells.Count > 2)
                {
                    e.Row.Cells[0].Visible = false;
                    e.Row.Cells[1].Visible = false;
               
                    //}
                }


            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            SaveData();
            mpeProduct.Hide();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
            mpeProduct.Show();

        }

        private bool SaveData()
        {
            try
            {
                
                lblResult.Text = string.Empty;
                EDeydFab_Receive c = new EDeydFab_Receive();
                c.ID = Convert.ToInt32(hdnCode.Value);
                c.Qty = Common.GetDouble(txtReceiveQty.Text);
                c.Roll = Common.GetNumber(txtRoll.Text);
               

                c.OPID = Convert.ToInt32(ddlProduct.SelectedValue);
                c.OrderNo = txtOrderNo.Text;
                c.Invoice = txtInvoiceNo.Text;
          
                c.TypeOfProduction = ddlSection.SelectedItem.Value.ToString();
                c.RcvDate = OpenFunction.ConvertDate(txtReceiveDate.Text);
                c.Notes = txtNotes.Text;
                c.EntryID = USession.SUserID;

                if (c.Qty > 0 && c.OPID>0)
                {

                    string res = "0";

                    res = new GreyFabric_DL().SaveReceive(c);
                    lblResult.Text = res;

                    if (res.Substring(0, 1) == "1")
                    {
                        Clear();

                        updPanel.Update();
                    }
                }
                else
                {
                    lblResult.Text = "Qty is not given/ Product not selected";
                }
                return true;
               // mpeProduct.Show();
            }

            catch (Exception ex)
            {
                lblResult.Text = ex.Message.ToString();
                return false;
               // lblResult.Text = ex.Message.ToString();
            }
        }
        private void Clear()
        {
            //lblResult.Text = string.Empty;
            txtNotes.Text = string.Empty;
            txtReceiveQty.Text = string.Empty;
            txtRoll.Text = string.Empty;
            txtInvoiceNo.Text = string.Empty;
            hdnCode.Value = "0";
            
        }
        private void LoadOrderProductDesc()
        {
            if (txtOrderNo.Text.Length > 5)
            {
                ddlProduct.DataSource = new Order_DL().GetOrderProductDesc(txtOrderNo.Text);
                ddlProduct.DataValueField = "OPID";
                ddlProduct.DataTextField = "DESC";

                ddlProduct.DataBind();
                ddlProduct.Items.Add("--Select--");
                ddlProduct.Text = "--Select--";
            }
        }
        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            
            
            Clear();
            lblResult.Text = "";
            hdnCode.Value = "0";
            LoadOrderProductDesc();
            updPanel.Update();


            

            mpeProduct.Show();
          
        }



        protected void btnCancel_Click(object sender, EventArgs e)
        {
           
           
            mpeProduct.Hide();
           
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            loadGrid();
        }
    }
}

