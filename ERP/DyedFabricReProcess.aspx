﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="DyedFabricReProcess.aspx.cs" Inherits="ERP.DyedFabricReProcess" Title="Dyed Fabric Re Process" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="AjaxControls" namespace="AjaxControls" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
        .style7
        {
            width: 128px;
            height: 2px;
        }
        .style8
        {
            height: 2px;
        }
               
        .style9
        {
            width: 150px;
        }
               
        .style11
        {
            width: 128px;
            height: 246px;
        }
        .style12
        {
            height: 246px;
        }
               
    </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
     <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
	<script type="text/javascript">
	    $(function() {
	        $(".tb").autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                url: "DataLoad.asmx/GetOrderList",
	                    data: "{ 'PINO': '" + request.term + "' }",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    dataFilter: function(data) { return data; },
	                    success: function(data) {
	                        response($.map(data.d, function(item) {
	                            return {
	                            value: item.OrderNo
	                            }
	                        }))
	                    },
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                        
	                    }
	                });
	            },
	            minLength: 2
	        });
	    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <div class="grid" >
            <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>Dyed Fabric Reprocess</h2></center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid" 
                        style="overflow:auto; width: 739px; height: auto; background-color:"
                        whitesmoke";">     
                <div style="height:25px;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                        AssociatedUpdatePanelID="updPanel">
                        <ProgressTemplate>
                            <img alt="" src="Images/loading.gif" style="height: 15px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    </div>
                    
                 <div align="center">
                  
                            
                           <asp:UpdatePanel ID="UpEntry" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" >
                        <ContentTemplate>
                          <table cellpadding="0" cellspacing="2" style="width:98%;">
                              <tr>
                                  <td align="right" class="style9">
                                      <asp:Label ID="Label3" runat="server" Font-Names="Georgia" Text="Order No:"></asp:Label>
                                  </td>
                                  <td align="left">
                                      <asp:TextBox ID="tbAuto" runat="server" class="tb">
             </asp:TextBox>
                                      <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                          onclick="btnSearch_Click" />
                                  </td>
                              </tr>
                            <tr>
                                <td class="style9" align="right">
                                    <asp:Label ID="Label1" runat="server" Font-Names="Georgia" Text="Order Product"></asp:Label>
                                </td>
                                <td align="left">
                                  <asp:UpdatePanel ID="upDes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlDesc" runat="server" Height="22px" Width="399px" 
                                            AutoPostBack="True" onselectedindexchanged="ddlDesc_SelectedIndexChanged" 
                                            TabIndex="4">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                      <Triggers>
                                          <asp:AsyncPostBackTrigger ControlID="btnSearch" 
                                              EventName="Click" />
                                      </Triggers>
                                   </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="style9" align="right">
                                    <asp:Label ID="Label8" runat="server" Font-Names="Georgia" Text="Color"></asp:Label>
                                </td>
                                <td align="left">
                                   <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                    <asp:DropDownList ID="ddlColor" runat="server" Height="20px" Width="235px" 
                                        AutoPostBack="True" TabIndex="5" 
                                            onselectedindexchanged="ddlColor_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    </ContentTemplate>
                                       <Triggers>
                                           <asp:AsyncPostBackTrigger ControlID="ddlDesc" 
                                               EventName="SelectedIndexChanged" />
                                       </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                          
                            
                            
                              <tr>
                                  <td align="right" class="style11" valign="top">
                                      Select Return Roll(s):
                                  </td>
                                  <td align="left" class="style12">
                             
                                     <div style="overflow: auto; height: 200px">
                                    <asp:UpdatePanel ID="upMCGrid" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                        
                <asp:GridView 
                                ID="GridView1" runat="server" 
                                  AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                ShowFooter="true"
                                Width="400px" onrowcreated="gvProducts_RowCreated" 
                          AutoGenerateColumns="False" onselectedindexchanged="GridView1_SelectedIndexChanged">
                            
                               
                                <RowStyle CssClass="row" />
                              
                                <Columns>
                                     <asp:TemplateField HeaderText="Select">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkStatus" runat="server" OnCheckedChanged="chkStatus_OnCheckedChanged"
                                                            AutoPostBack="true" 
                                                            Checked='<%# Convert.ToBoolean(Eval("Select")) %>'
                                                            Text='' />
                                                    </ItemTemplate>                   
                                      </asp:TemplateField>
                                    <asp:BoundField 
                                        HeaderText="SL" DataField="SL" 
                                        SortExpression="SL" >
                                    
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                   
                                    <asp:BoundField 
                                        HeaderText="ProcessCardNo" DataField="ProcessCardNo" 
                                        SortExpression="ProcessCardNo" >
                                    
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                 
                                    <asp:BoundField 
                                        HeaderText="RollNo" DataField="RollNo" 
                                        SortExpression="RollNo">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                     <asp:BoundField 
                                        HeaderText="RollQty" DataField="RollQty" 
                                        SortExpression="RollQty">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                   
                                  
                                  
                                    
                                 
                                   
                                 
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                                        </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnLoad" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlColor" 
                                        EventName="SelectedIndexChanged" />
                                    </Triggers>
                                    </asp:UpdatePanel> 
                            
                    </div> 
                    
                          
                  
                            <asp:UpdatePanel ID="upSumm" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                            
                           
                                    <div>
                                        Total Qty :   <asp:TextBox ID="txtQty" runat="server" Width="120px" TabIndex="16" 
                                          ReadOnly="True"></asp:TextBox> Total Roll:     <asp:TextBox ID="txtRoll" runat="server" Width="120px" TabIndex="17" ReadOnly="True" 
                                          ></asp:TextBox>
                                    </div>
                              
                         </ContentTemplate>
                            </asp:UpdatePanel>
                                  
                                  </td>
                              </tr>
                          
                            
                               
                              <tr>
                                  <td align="right" class="style9">
                                      Process Card No </td>
                                  <td align="left">
                                      <asp:TextBox ID="txtProcessCard" runat="server" TabIndex="18" Width="120px"></asp:TextBox>
                                      Reason<asp:DropDownList ID="ddlReason" runat="server" Height="25px" 
                                          Width="147px">
                                          <asp:ListItem>PackingReturn</asp:ListItem>
                                          <asp:ListItem>DeliveryReturn</asp:ListItem>
                                      </asp:DropDownList>
                                      </td>
                              </tr>
                            
                            
                              <tr>
                                  <td align="right" class="style9">
                                      Date</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtDate" runat="server" TabIndex="19" Height="22px" 
                                          Width="120px"></asp:TextBox>
                                      <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                                          Enabled="True" Format="MM/dd/yyyy" TargetControlID="txtDate">
                                      </cc1:CalendarExtender>
                                  </td>
                              </tr>
                              
                               <tr>
                                  <td align="right" class="style9">
                                      ReProduction Section </td>
                                  <td align="left">
                                       <asp:DropDownList ID="ddlSection" runat="server" Height="20px" 
                                           TabIndex="4"                                         Width="136px">
                                        
                                        <asp:ListItem>DEYING</asp:ListItem>
                                         <asp:ListItem>PRINTING</asp:ListItem>
                                        <asp:ListItem>RAISING</asp:ListItem>
                                       
                                        <asp:ListItem>PACKING</asp:ListItem>
                                    </asp:DropDownList>
                                      
                                       &nbsp;
                                      
                                      </td>
                              </tr>
                              
                               <tr>
                                  <td align="right" class="style9">
                                      ReProduction Description</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtDescription" runat="server" TabIndex="18" Width="419px"></asp:TextBox>
                                      </td>
                              </tr>
                            
                            
                            
                            
                            
                            <tr>
                                <td class="style7">
                                    <asp:HiddenField ID="hdnCode" runat="server" Value="-1" />
                                </td>
                                <td class="style8">
                                    <asp:Button ID="btnSave" runat="server" Height="24px"  Text="Save" 
                                        Width="60px" onclick="btnSave_Click" Font-Names="Georgia" TabIndex="22" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="60px" 
                                        Height="24px" Font-Names="Georgia" onclick="btnCancel_Click" 
                                        TabIndex="23" />
                                    <asp:Label ID="lblMsg" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        
                  
                 </div>
                <div style="height: 40px; border-bottom-style: none; border-bottom-color: #003399;">
                    <div style="float: left">
                                      <div align="center" style="height: 30px; width: 684px;" 
                            __designer:mapid="3f2">
                                         
                                          <asp:Button ID="btnload" runat="server" onclick="btnload_Click" 
                                              Text="Load Dyed Fabric of Selected Order" />
                                          &nbsp;&nbsp;&nbsp;&nbsp;
                                      </div>
                </div>
                
                </div>
                    <!-- Content Goes Here! -->
                    <asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView 
                                ID="gvProducts" runat="server" 
                                 AutoGenerateColumns="False" 
                                AllowPaging="True" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="23px" 
                                onpageindexchanging="gvProducts_PageIndexChanging" PageSize="5" 
                                onselectedindexchanged="gvProducts_SelectedIndexChanged">
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="«" LastPageText="»" />      
                                <Columns>
                                   
                                    <asp:BoundField 
                                        HeaderText="ID" DataField="ID" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Product" HeaderText="Product" />
                                    <asp:BoundField 
                                        HeaderText="Color" DataField="Color" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                     <asp:BoundField DataField="PCardNo" HeaderText="PCardNo" >
                                     <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>   
                                      
                                    <asp:BoundField DataField="RollNo" HeaderText="RollNo" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                      
                                    <asp:BoundField DataField="RollQty" HeaderText="RollQty" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>   
                                      
                                    <asp:BoundField DataField="DDate" HeaderText="DDate" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField> 
                                    <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" 
                                                ImageUrl="~/Images/btn_delete.gif" onclick="btnDelete_Click" 
                                                Height="16px" 
                                            onclientclick="javascript:return confirm('Are you sure you want to delete this row?');" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                      <ItemTemplate>
                                     <asp:ImageButton ID="btnActive" runat="server" 
                                                ImageUrl="~/Images/HandleHand.png" onclick="btnActive_Click" 
                                                style="width: 15px" Height="16px" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                            <asp:Button ID="btnHiddenProduct" runat="Server" Style="display:none"  />
                                 
              <%--      <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>--%>
                    
                        </ContentTemplate>
                        <Triggers>
                            
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnload" EventName="Click" />
                           
                        </Triggers>
                    </asp:UpdatePanel>                     
    
                </div></div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>    
    </div></div>
</asp:Content>

