﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class WebForm16 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (new Utilities_DL().CheckSecurity("515", USession.SUserID) == true)
                {
                    this.rblNormal.Items.FindByText("Active").Selected = true;
                    loadGridData();
                }
                else
                {
                    Response.Redirect("Home.aspx");
                }

            }
        }

        private void loadGridData()
        {

            gvProducts.Columns[7].Visible = true;
            gvProducts.Columns[8].Visible = true;
            gvProducts.Columns[9].Visible = true;
            gvProducts.Columns[10].Visible = false;
            gvProducts.DataSource = new Knit_Process_CL().GetActiveData("ANTIPILLING");
            gvProducts.DataBind();

        }
        private void loadDeletedData()
        {
            gvProducts.Columns[7].Visible = false;
            gvProducts.Columns[8].Visible = false;
            gvProducts.Columns[9].Visible = false;
            gvProducts.Columns[10].Visible = true;
            gvProducts.DataSource = new Knit_Process_CL().GetDeletedData("ANTIPILLING");
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            hdnQty.Value = (row.Cells[3].Text.ToString().Trim());
            hdnPcard.Value = (row.Cells[11].Text.ToString().Trim());
            hdnCCard.Value = (row.Cells[12].Text.ToString().Trim());
            lblProcessCard.Text = (row.Cells[2].Text);
            txtQty.Text = (row.Cells[3].Text.ToString().Trim());
            txtDate.Text = row.Cells[6].Text.Replace("12:00:00 AM", "");
            txtMCNo.Text = row.Cells[4].Text.Replace("&nbsp;", "");
            txtNotes.Text = row.Cells[5].Text.Replace("&nbsp;", "");
            if (isValidDate(txtDate.Text) == false)
                txtDate.Text = DateTime.Now.Date.ToShortDateString();


            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            //ImageButton btnEdit = sender as ImageButton;
            //GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            //new Knit_Process_CL().DeleteActiveData(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            //loadGridData();

            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('You Can Delete this Record on Deying Section ! Sent back to deying then Delete!!!');</script>", false);

        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Knit_Process_CL().DeleteActiveData(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                if (e.Row.Cells.Count > 10)
                {
                    e.Row.Cells[11].Visible = false;
                    e.Row.Cells[12].Visible = false;
                }
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }

        private bool checkDecimal(string s)
        {

            try
            {
                decimal v;
                v = Convert.ToDecimal(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
              decimal ret,qty;
            KnitProcessing c = new KnitProcessing();
            c.SL = Convert.ToInt64(hdnCode.Value);
            c.PCardNo = hdnPcard.Value.ToString();
            string cd = hdnCCard.Value.Trim().Replace("&nbsp;", "").ToString();
            if (cd != "")
            {
                c.CCardNo = cd;
            }
           
            if (checkDecimal(txtQty.Text) == false)
            
                qty = 0; 
            else
                qty = Convert.ToDecimal(txtQty.Text);

           
            c.Section = "ANTIPILLING";
            c.PQty = qty;
            c.MCNo = txtMCNo.Text.ToString();
            c.Notes = txtNotes.Text.Trim();
            c.Status = "P";
            c.EntryID = USession.SUserID;
            if (isValidDate(txtDate.Text.ToString()) == true)
                c.EntryDate = Convert.ToDateTime(txtDate.Text);
            else
                c.EntryDate = DateTime.Now.Date;
            c.SHIFT = ddlShift.SelectedItem.Text.ToString();
            if (checkDecimal(txtReturn.Text) == false)
            {
                ret = 0;
                new Knit_Process_CL().Save(c);
            }
            else
            {
                ret = Convert.ToDecimal(txtReturn.Text);
                qty = Convert.ToDecimal(hdnQty.Value) - ret;
                if (ret > 0)
                {
                    if (txtCPCard.Text.Length > 0)
                    {
                        c.PQty = qty;
                        new Knit_Process_CL().Save(c);
                        c.SL = 0;
                        c.CCardNo = txtCPCard.Text.ToString();
                        c.PQty = Convert.ToDecimal(txtReturn.Text);
                        c.Section = "SHEARING";
                        c.Status = "R";
                        c.MCNo = "";
                        c.Notes = "Sub Procces Card Issued";
                        c.SHIFT = ddlShift.SelectedItem.Text.ToString();
                        c.EntryID = USession.SUserID;
                        if (isValidDate(txtDate.Text.ToString()) == true)
                            c.EntryDate = Convert.ToDateTime(txtDate.Text);
                        else
                            c.EntryDate = DateTime.Now.Date;
                        new Knit_Process_CL().Save(c);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page,Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Sub Process Card No Not given .Try Again');</script>",false);
                    }
                }
                else
                {
                    new Knit_Process_CL().Save(c);
                }
            }
          
            
            loadGridData();
            
            txtQty.Text = "";
            txtMCNo.Text = "";
            txtNotes.Text = "";
            
         
            updPanel.Update();
            mpeProduct.Hide();

            

        }



        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblNormal.Items.FindByText("Active").Selected == true)
            {
                loadGridData();

            }
            else
            {
                loadDeletedData();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            loadGridData();
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            Button btnSent = sender as Button;
            GridViewRow row = (GridViewRow)btnSent.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            CheckBox chk = (CheckBox)(row.Cells[6].FindControl("chkSkip"));

            KnitProcessing c = new KnitProcessing();
            c.SL = 0;
            c.PCardNo = (row.Cells[11].Text);
            string cd = row.Cells[12].Text.Trim().Replace("&nbsp;", "").ToString();
            if (cd != "")
            {
                c.CCardNo = cd;
            }
            if (checkDecimal((row.Cells[3].Text)) == false)
            {
                (row.Cells[3].Text) = "0";
            }
            if (isValidDate(row.Cells[6].Text.ToString()) == true)
                c.EntryDate = Convert.ToDateTime(row.Cells[6].Text);
            else
                c.EntryDate = DateTime.Now.Date;
            c.PQty = Convert.ToDecimal((row.Cells[3].Text));
            c.Section = "SETTING";
            c.Status = "P";
            c.MCNo = "";
            c.SHIFT = ddlShift.SelectedItem.Text.ToString();
            c.Notes = "";

            c.EntryID = USession.SUserID;
            if (chk.Checked == true)
            {
                if (c.PQty > 0)
                {
                    c.SL = Convert.ToInt64(row.Cells[0].Text);
                    new Knit_Process_CL().Save(c);
                    loadGridData();
                }
            }
            else
            {
                if (((row.Cells[4].Text.Replace("&nbsp;", "").Trim().Length) > 0))// && (row.Cells[5].Text.Replace("&nbsp;", "").Trim().Length > 3))
                {

                    if (c.PQty > 0)
                    {
                        new Knit_Process_CL().Save(c);

                        c.SL = Convert.ToInt64(row.Cells[0].Text);
                        c.Section = "ANTIPILLING";
                        c.Status = "F";
                        c.MCNo = row.Cells[4].Text.Trim();
                        c.Notes = row.Cells[5].Text.Replace("&nbsp;", "");
                        new Knit_Process_CL().Save(c);
                        loadGridData();
                    }
                }
            }


        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Button btnSent = sender as Button;
            GridViewRow row = (GridViewRow)btnSent.NamingContainer;





            //if (((row.Cells[4].Text.Replace("&nbsp;", "").Trim().Length) > 0) && (row.Cells[5].Text.Replace("&nbsp;", "").Trim().Length > 2))
            //{
                KnitProcessing c = new KnitProcessing();
                c.SL = Convert.ToInt64(row.Cells[0].Text);
                //new Knit_Process_CL().deleteData(c);


                //c.SL = 0;
                c.PCardNo = (row.Cells[11].Text);
                c.CCardNo = (row.Cells[12].Text);


                //c.SL = new Knit_Process_CL().getReturnSL(c.PCardNo.Trim(), "SHEARING");


                c.PQty = Convert.ToDecimal((row.Cells[3].Text));
                c.Section = "SHEARING";
                c.Status = "R";
              
                c.SHIFT = ddlShift.SelectedItem.Text.ToString();
                c.MCNo = row.Cells[4].Text.Replace("&nbsp;", "").Trim();
                c.Notes = row.Cells[5].Text.Replace("&nbsp;", "");
                if (isValidDate(row.Cells[6].Text.ToString()) == true)
                    c.EntryDate = Convert.ToDateTime(row.Cells[6].Text);
                else
                    c.EntryDate = DateTime.Now.Date;
                c.EntryID = USession.SUserID;
                if (c.PQty > 0)
                {
                    new Knit_Process_CL().Save(c);
                    loadGridData();
                }
            //}

        }

        protected void gvProducts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private bool isValidDate(string s)
        {
            DateTime d;
            try
            {
                d = Convert.ToDateTime(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
