﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
using System.Reflection;


namespace ERP
{
    public partial class INV_STORE_RPT : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadBasicData();
            }
        }
        private void LoadBasicData()
        {

            string Head = new Inventory_DL().GetInventoryType(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            this.Title = Head + " - Inventory Stock";
            lblHead.Text = "Stock Report For " + Head;


            Inventory_DL objDL = new Inventory_DL();
            ddlYarnGroup.DataSource = objDL.GetItemGroup(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlYarnGroup.DataTextField = "GroupName";
            ddlYarnGroup.DataValueField = "SL";
            ddlYarnGroup.DataBind();
            ddlYarnGroup.Items.Insert(0, "All Group");
            if (Convert.ToInt32(Request.QueryString["INVTypeID"]) == 1)
            {
                lblParent.Visible = false;
                txtParent.Visible = false;
            }

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            List<EINV_StoreReport> liProd = new List<EINV_StoreReport>();
            Int64 gid = 0;

            if (ddlYarnGroup.SelectedItem.Text == "All Group")
            {
                gid = 0;
            }
            else
            {
                gid = Convert.ToInt64(ddlYarnGroup.SelectedValue.ToString());
            }


            liProd = new Inventory_DL().GetInventoryStockReport(Convert.ToInt32(Request.QueryString["INVTypeID"]), Convert.ToInt32(gid), txtFrom0.Text,txtFrom1.Text,txtItem.Text);

            if (txtParent.Text.Length > 2)
            {
                liProd = liProd.FindAll(o => (o.ParentName.Trim().ToUpper() == txtParent.Text.Trim().ToUpper()));

            }
            liProd = liProd.FindAll(o => (txtItem.Text.Length<1 ||  o.ItemName.Trim().ToUpper() == txtItem.Text.Trim().ToUpper()));

            if (liProd.Count > 0)
            {
                Export("Stock_Report_" + txtFrom0.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, liProd);
            }
        }
        public void Export(string fileName, List<EINV_StoreReport> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2>" + lblHead.Text + " From  " + txtFrom0.Text + " To  " + txtFrom1.Text + " </h2>  </div>");
            HttpContext.Current.Response.Write("<div> Group : " + ddlYarnGroup.SelectedItem.Text.ToString() + "  </div>");



            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            HttpContext.Current.Response.Write("<Td><B>Sl</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Item Group</B></Td>");
            if (Convert.ToInt32(Request.QueryString["INVTypeID"]) == 2)
            {
                HttpContext.Current.Response.Write("<Td><B>Parent Item</B></Td>");
            }
            HttpContext.Current.Response.Write("<Td><B>Item</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Opening Balance</B></Td>");


          
            if (Convert.ToInt32(Request.QueryString["INVTypeID"]) == 2)
            {
                HttpContext.Current.Response.Write("<Td><B>Total Received</B></Td>");
                HttpContext.Current.Response.Write("<Td><B>Total Issued</B></Td>");
                HttpContext.Current.Response.Write("<Td><B>Actual Stock</B></Td>");
                

            }
            else
            {
                List<EINV_Store> li = new Inventory_DL().GetStoreList(Convert.ToInt32(Request.QueryString["INVTypeID"]));
                string[] str = empList[0].IssTotStr.Split(';');
                for (int i = 0; i < str.Count(); i++)
                {
                    EINV_Store objStore = li.SingleOrDefault(o => o.ID == Convert.ToInt32(str[i].Substring(0, str[i].IndexOf('='))));
                    if (objStore != null)
                    {
                        HttpContext.Current.Response.Write("<Td><B>" + objStore.StoreName + " Receive</B></Td>");
                    }
                    //ob.RcvTot = ob.RcvTot + Convert.ToDouble(rcvval[i].Substring(str[i].IndexOf('=') + 1));
                }
                for (int i = 0; i < str.Count(); i++)
                {
                    EINV_Store objStore = li.SingleOrDefault(o => o.ID == Convert.ToInt32(str[i].Substring(0, str[i].IndexOf('='))));
                    if (objStore != null)
                    {
                        HttpContext.Current.Response.Write("<Td><B>" + objStore.StoreName + " Issue</B></Td>");
                    }
                    //ob.RcvTot = ob.RcvTot + Convert.ToDouble(rcvval[i].Substring(str[i].IndexOf('=') + 1));
                }

                for (int i = 0; i < str.Count(); i++)
                {
                    EINV_Store objStore = li.SingleOrDefault(o => o.ID == Convert.ToInt32(str[i].Substring(0, str[i].IndexOf('='))));
                    if (objStore != null)
                    {
                        HttpContext.Current.Response.Write("<Td><B>" + objStore.StoreName + " Stock</B></Td>");
                    }
                    //ob.RcvTot = ob.RcvTot + Convert.ToDouble(rcvval[i].Substring(str[i].IndexOf('=') + 1));
                }
                HttpContext.Current.Response.Write("<Td><B>Actual Stock</B></Td>");
            }
                       
          
           


            HttpContext.Current.Response.Write("</TR>");
            int j = 1;
            foreach (EINV_StoreReport emp in empList)
            {

                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<Td>" + j++ + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.GroupName + "</Td>");
                if (Convert.ToInt32(Request.QueryString["INVTypeID"]) == 2)
                {
                    HttpContext.Current.Response.Write("<Td>" + emp.ParentName + "</Td>");
                }
                HttpContext.Current.Response.Write("<Td>" + emp.ItemName + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.OpenBal + "</Td>");

                double totIssue = 0, totReceive = 0;
                string[] str1 = emp.RcvTotStr.Split(';');
                for (int i = 0; i < str1.Count(); i++)
                {
                   
                    totReceive = totReceive + Convert.ToDouble(str1[i].Substring(str1[i].IndexOf('=') + 1));
                    if (Convert.ToInt32(Request.QueryString["INVTypeID"]) != 2)
                    {

                        HttpContext.Current.Response.Write("<Td>" + Convert.ToDouble(str1[i].Substring(str1[i].IndexOf('=') + 1)) + " </Td>");
                    }
                   
                }
                string[] str2 = emp.IssTotStr.Split(';');
                for (int i = 0; i < str2.Count(); i++)
                {

                    totIssue = totIssue + Convert.ToDouble(str2[i].Substring(str2[i].IndexOf('=') + 1));
                    if (Convert.ToInt32(Request.QueryString["INVTypeID"]) != 2)
                    {
                        HttpContext.Current.Response.Write("<Td>" + Convert.ToDouble(str2[i].Substring(str2[i].IndexOf('=') + 1)) + " </Td>");
                    }
                    
                }
                string[] str3 = emp.StockStr.Split(';');
                double totStock = 0;
                for (int i = 0; i < str3.Count(); i++)
                {

                    totStock = totStock + Convert.ToDouble(str3[i].Substring(str3[i].IndexOf('=') + 1));
                    if (Convert.ToInt32(Request.QueryString["INVTypeID"]) != 2)
                    {
                        HttpContext.Current.Response.Write("<Td>" + (Convert.ToDouble(str3[i].Substring(str3[i].IndexOf('=') + 1))) + " </Td>");
                    }

                }
                if (Convert.ToInt32(Request.QueryString["INVTypeID"]) == 2)
                {
                    HttpContext.Current.Response.Write("<Td>" + totReceive + " </Td>");
                    HttpContext.Current.Response.Write("<Td>" + totIssue + " </Td>");
                }
                HttpContext.Current.Response.Write("<Td>" + totStock + " </Td>");

                
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }
        private List<EINV_StoreReportDetails> GetStoreReport(List<EINV_StoreReport> li)
        {
            List<EINV_StoreReportDetails> liD = new List<EINV_StoreReportDetails>();
           
            foreach (EINV_StoreReport emp in li)
            {

             EINV_StoreReportDetails objE=new EINV_StoreReportDetails();
             objE.ItemID = emp.ItemID;
             objE.ParentName = emp.ParentName;
             objE.ItemName = emp.ItemName;

             objE.GroupName = emp.GroupName;
             objE.OpenBal = emp.OpenBal;

                string[] str1 = emp.RcvTotStr.Split(';');
                string[] str2 = emp.IssTotStr.Split(';');

                if (str1.Count() >= 1)
                {
                    objE.MainStoreRCV = Convert.ToDouble(str1[0].Substring(str1[0].IndexOf('=') + 1));
                    objE.MainStoreISS = Convert.ToDouble(str2[0].Substring(str2[0].IndexOf('=') + 1));
                }
                if (str1.Count() >= 2)
                {
                    objE.SubStore1RCV = Convert.ToDouble(str1[1].Substring(str1[1].IndexOf('=') + 1));
                    objE.SubStore1ISS= Convert.ToDouble(str2[1].Substring(str2[1].IndexOf('=') + 1));
                }

                if (str1.Count() >= 3)
                {
                    objE.SubStore2RCV = Convert.ToDouble(str1[2].Substring(str1[2].IndexOf('=') + 1));
                    objE.SubStore2ISS = Convert.ToDouble(str2[2].Substring(str2[2].IndexOf('=') + 1));
                }

                if (str1.Count() >= 4)
                {
                    objE.SubStore3RCV = Convert.ToDouble(str1[3].Substring(str1[3].IndexOf('=') + 1));
                    objE.SubStore3ISS = Convert.ToDouble(str2[3].Substring(str2[3].IndexOf('=') + 1));
                }

                objE.MainStoreBAL = objE.MainStoreRCV- objE.MainStoreISS ;
                objE.SubStore1BAL = objE.SubStore1RCV - objE.SubStore1ISS;
                objE.SubStore2BAL = objE.SubStore2RCV - objE.SubStore2ISS;
                objE.SubStore3BAL = objE.SubStore3RCV - objE.SubStore3ISS;




                objE.Balance = objE.OpenBal + objE.MainStoreBAL + objE.SubStore1BAL + objE.SubStore2BAL + objE.SubStore3BAL;

                liD.Add(objE);


            }

            return liD;
        }
    }
}
