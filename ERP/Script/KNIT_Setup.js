﻿$(function() {
    // Calculation Production Qty based on Loss Per & Booking Using Client Side JQuery
    $('#<%=txtUserPercent.ClientID %>').bind('keyup', function(e) {
        // if (e.keyCode == 13) {
        // alert($(this).val());
        var valuePer = 0.0;
        var valueBook = 0.0;
        var totValue = 0.0;
        if (isNaN(parseFloat($('#<%=txtTot.ClientID %>').val()))) {
            valueBook = 0;
        } else {
            valueBook = parseFloat($('#<%=txtTot.ClientID %>').val());
        }

        if (isNaN(parseFloat($(this).val()))) {
            valuePer = 0;
        } else {
            valuePer = $(this).val();
        }



        totValue = (valueBook * valuePer) / 100;

        $('#<%=txtKG.ClientID %>').val(totValue);

        //}

    });


});


$(document).ready(function() {
    // Calculation Production Qty based on Loss Per & Booking Using Client Side JQuery
    $('#<%=txtPer.ClientID %>').bind('keyup', function(e) {
        // if (e.keyCode == 13) {
        // alert($(this).val());
        var valuePer = 0.0;
        var valueBook = 0.0;
        var totValue = 0.0;
        if (isNaN(parseFloat($('#<%=txtBookingQty.ClientID %>').val()))) {
            valueBook = 0;
        } else {
            valueBook = parseFloat($('#<%=txtBookingQty.ClientID %>').val());
        }

        if (isNaN(parseFloat($(this).val()))) {
            valuePer = 0;
        } else {
            valuePer = $(this).val();
        }



        totValue = valueBook + valueBook * valuePer / 100;

        $('#<%=txtTot.ClientID %>').val(totValue);

        //}

    });


});

