﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class KNIT_Machine : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                //if (new Utilities_DL().CheckSecurity("500", USession.SUserID) == true)
                //{
                   
                   loadGridData();
                  
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }

      
        private void loadGridData()
        {


           // gvProducts.Columns[7].Visible = true;
            //gvProducts.Columns[8].Visible = true;
            //gvProducts.Columns[9].Visible = false;
            gvProducts.DataSource = new KNIT_all_operation().GetAlMCNo();
            gvProducts.DataBind();

        }
       

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;

            txtMCNo.Text = row.Cells[0].Text;
            txtDia.Text = row.Cells[1].Text;
            txtGG.Text = row.Cells[2].Text;
            txtFloor.Text = row.Cells[3].Text;
            txtCapacity.Text = row.Cells[4].Text;

           

            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            //ImageButton btnEdit = sender as ImageButton;
            //GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            //new Fabric_CL().DeleteActiveFabric(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            //loadGridData();
        }

       

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
               // e.Row.Cells[0].Visible = false;
                //e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            try
            {
                EKnitingMachine c = new EKnitingMachine();
                c.MCNo = txtMCNo.Text;
                c.GG = txtGG.Text;
                c.MFloor = txtFloor.Text;
                c.Dia = txtDia.Text;
                c.Capacity =Common.GetDouble( txtCapacity.Text);


                if (txtMCNo.Text.Length > 0)
                {
                    new KNIT_all_operation().SaveKnitMachineSetup(c);
                }
                loadGridData();
                //loadGridData();
                updPanel.Update();
                mpeProduct.Hide();
            }
            catch (Exception ex)
            {

            }
        }
        

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            txtMCNo.Text = string.Empty;
            txtDia.Text = string.Empty;
            txtCapacity.Text = string.Empty;
            txtFloor.Text = string.Empty;

            


            mpeProduct.Show();
            txtMCNo.Focus();
        }

       

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            loadGridData();
        }
       
    }
}
