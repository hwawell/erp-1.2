﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDLayer;
namespace ERP
{
    public partial class YarnGroupView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (new Utilities_DL().CheckSecurity("502", USession.SUserID) == true)
                //{
                this.rblNormal.Items.FindByText("Active").Selected = true;
                loadGridData();
                
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
        private void loadGridData()
        {


            gvProducts.Columns[2].Visible = true;
            gvProducts.Columns[3].Visible = true;
            gvProducts.Columns[4].Visible = false;
            gvProducts.DataSource = new Yarn_DL().GetActiveYarnGroup();
            gvProducts.DataBind();
        }
       
        private void loadDeletedData()
        {

            gvProducts.Columns[2].Visible = false;
            gvProducts.Columns[3].Visible = false;
            gvProducts.Columns[4].Visible = true;
            gvProducts.DataSource = new Yarn_DL().GetDeletedRecord();
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            
            txtYarn.Text = (row.Cells[1].Text);
           


            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Yarn_DL().DeleteYarnGroup(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Yarn_DL().DeleteActive(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
               
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            YarnGroup c = new YarnGroup();
            c.ID = Convert.ToInt64(hdnCode.Value);
            c.YarnGroupName = txtYarn.Text.ToString();
            
          
         
            if (txtYarn.Text.Length > 0)
            {
                new Yarn_DL().SaveYarnGroup(c);
            }
            loadGridData();
            //loadGridData();
            updPanel.Update();
            mpeProduct.Hide();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            YarnGroup c = new YarnGroup();
            c.ID = Convert.ToInt64(hdnCode.Value);
            c.YarnGroupName = txtYarn.Text.ToString();
           
            //TBL_Product t = new TBL_Product();
            //t.ProductCode = txtPCode.Text.ToString();
            //t.ProductName = txtProduct.Text.ToString();
            //t.ProductDescription = txtDescription.Text.ToString();
            if (txtYarn.Text.Length > 0)
            {
                new Yarn_DL().SaveYarnGroup(c);
            }
            //loadGridData();
            //loadGridData();
            txtYarn.Text = "";
          
            updPanel.Update();
            mpeProduct.Show();
            txtYarn.Focus();

        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            txtYarn.Text = string.Empty;
         



            mpeProduct.Show();
            txtYarn.Focus();
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblNormal.Items.FindByText("Active").Selected == true)
            {
                loadGridData();

            }
            else
            {
                loadDeletedData();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            loadGridData();
        }
    }
}
