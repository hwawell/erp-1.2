﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

using BDLayer;
using System.Collections.Generic;
using System.Reflection;
namespace ERP
{
    public partial class KNIT_RollSummary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadProductionSetup();
                Window2.Hide();
                Window1.Hide();
            }

        }
        private void LoadProductionSetup()
        {

            ddlMCNo.DataSource = new KNIT_all_operation().GetAlMCNo();
            ddlMCNo.DataValueField = "MCNo";
            ddlMCNo.DataTextField = "MCNo";
            ddlMCNo.DataBind();
            ddlMCNo.Items.Insert(0, "All MC");

        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {

            Window1.Show();
            Window2.Hide();
            List<EProuctionSetup> liroll = new KNIT_all_operation().GetProuctionSetup(ddlMCNo.Text,Convert.ToDateTime(txtFrom.Text),Convert.ToDateTime(txtTo.Text));
            Store1.DataSource = liroll;
            this.Store1.DataBind();
         
        }

       
        protected void btnExport_Click(object sender, EventArgs e)
        {
            List<EProuctionSetup> liProd = new List<EProuctionSetup>();
            this.Window1.Title = "Knit Roll Summary Report";
            liProd = new KNIT_all_operation().GetProuctionSetup(ddlMCNo.Text, Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text));
            Export("KNIT_ROLL_SUMMARY_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, liProd);
        }
        public void ExportAll(string fileName, List<EProuctionSetup> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2>Knit Roll Summary Report From " + txtFrom.Text + " To "+ txtTo.Text + " </h2>  </div>");
            
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
         

           
           
            foreach (EProuctionSetup emp in empList)
            {
                TableRow row1 = new TableRow();
                
               
                HttpContext.Current.Response.Write("<div>");
                    //Get column headers  and make it as bold in excel columns
                 
                    HttpContext.Current.Response.Write("<div>");
                    HttpContext.Current.Response.Write("<B>");
                    HttpContext.Current.Response.Write("MC No : " + emp.MCNo + " ");
                    HttpContext.Current.Response.Write("Fabric : " + emp.Fabric + " ,GM : " + emp.GM + " ,GSM: " + emp.GSM + "  ");
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</div>");
                    HttpContext.Current.Response.Write("<div>");
                    HttpContext.Current.Response.Write("StartDate : " + emp.StartDate + " ,StopDate: " + emp.StopDate + " ,Status: " + emp.Status + "");
                    HttpContext.Current.Response.Write("</div>");
                    HttpContext.Current.Response.Write("<div>");
                    HttpContext.Current.Response.Write("TotalRoll: " + emp.TotalRoll + " ,TotalKG: " + emp.TotalKG + " ");
                    HttpContext.Current.Response.Write("</div>");
                    
                  
                  
               
                HttpContext.Current.Response.Write("</div> ");
             
                List<EProuctionRoll> liObj = new KNIT_all_operation().GetProuctionRoll(emp.ID);
                ExportDetails(liObj);

                HttpContext.Current.Response.Write("<BR>");

            }

            HttpContext.Current.Response.End();

        }

        public void ExportDetails( List<EProuctionRoll> empList)
        {

          

            
            HttpContext.Current.Response.Write("<div>  </div>");
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            foreach (PropertyInfo proinfo in new EProuctionRoll().GetType().GetProperties())
            {
                if (proinfo.Name != "MCNo")
                {
                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    HttpContext.Current.Response.Write(proinfo.Name);
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");
                }

            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (EProuctionRoll emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");
                foreach (PropertyInfo proinfo in new EProuctionRoll().GetType().GetProperties())
                {
                    if (proinfo.Name != "MCNo")
                    {
                        HttpContext.Current.Response.Write("<Td>");
                        //Get column headers  and make it as bold in excel columns
                        HttpContext.Current.Response.Write("<B>");
                        if (proinfo.GetValue(emp, null) != null)
                        {
                            HttpContext.Current.Response.Write(proinfo.GetValue(emp, null).ToString());
                        }
                        else
                        {
                            HttpContext.Current.Response.Write("");
                        }
                        HttpContext.Current.Response.Write("</B>");
                        HttpContext.Current.Response.Write("</Td>");
                    }
                }

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
      

        }

        protected void btnExportDetails_Click(object sender, EventArgs e)
        {
            List<EProuctionSetup> liProd = new List<EProuctionSetup>();
            this.Window1.Title = "Knit Roll Summary Details Report";
            liProd = new KNIT_all_operation().GetProuctionSetup(ddlMCNo.Text, Convert.ToDateTime(txtFrom.Text), Convert.ToDateTime(txtTo.Text));
            ExportAll("KNIT_ROLL_SUMMARY_Details" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, liProd);
        }
        public void Export(string fileName, List<EProuctionSetup> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2>Knit Roll Summary Report From " + txtFrom.Text + " To " + txtTo.Text + " </h2>  </div>");
            HttpContext.Current.Response.Write("<div> <h3>Machiine No : " + ddlMCNo.Text + " </h3>  </div>");
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            foreach (PropertyInfo proinfo in new EProuctionSetup().GetType().GetProperties())
            {

                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(proinfo.Name);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");


            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (EProuctionSetup emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");
                foreach (PropertyInfo proinfo in new EProuctionSetup().GetType().GetProperties())
                {
                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    if (proinfo.GetValue(emp, null) != null)
                    {
                        HttpContext.Current.Response.Write(proinfo.GetValue(emp, null).ToString());
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("");
                    }
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");

                }

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }
    }
}
