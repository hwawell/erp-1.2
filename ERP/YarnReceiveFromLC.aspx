﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="YarnReceiveFromLC.aspx.cs" Inherits="ERP.YarnReceiveFromLC" Title="Yarn Receive From LC" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="AjaxControls" namespace="AjaxControls" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
        .style7
        {
            width: 220px;
            height: 2px;
            text-align:right;
        }
        .style8
        {
            height: 2px;
            text-align: left;
        }
               
        </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <div class="grid" >
            <div class="rounded"  >
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>Yarn Receive From LC</h2></center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid" 
                        style="overflow:auto; width: 850px; height: 699px; background-color:"
                        whitesmoke";"> 
                        
                        <asp:UpdatePanel ID="upALl" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                        
                        
                          
                <div style="height:25px;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                        AssociatedUpdatePanelID="updPanel">
                        <ProgressTemplate>
                            <img alt="" src="Images/loading.gif" style="height: 15px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    </div>
                    
                 <div align="center">
                            <asp:Button ID="btnHiddenProduct" runat="Server" Style="display:none"  />
                              
                    <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlDataEntry" runat="server"    CssClass="modalBox"  
                        BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                        Width="550px" BorderWidth="2px">
                        <center>
                            <asp:Panel ID="ProductCaption" runat="server"   
                                Style="margin-bottom: 10px; cursor: hand;" 
                                BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                &quot;Yarn Receive From LC&quot;- Entry Form</asp:Panel></center>
                            
                           <asp:UpdatePanel ID="UpEntry" runat="server" UpdateMode="Conditional" 
                            ChildrenAsTriggers="False" >
                        <ContentTemplate>
                        <asp:HiddenField ID="hdnValue" runat="server" />
                         <table cellpadding="1" cellspacing="0" style="width:100%;">
                             <tr>
                                 <td class="style7">
                                     <asp:Label ID="Label12" runat="server" Font-Names="Georgia" Text="Yarn Name"></asp:Label>
                                 </td>
                                 <td style="text-align: left">
                                     <asp:TextBox ID="txtYarn" runat="server" BackColor="WhiteSmoke" ReadOnly="True" 
                                         TabIndex="5" Width="258px"></asp:TextBox>
                                     <asp:HiddenField ID="hdnYarnID" runat="server" />
                                 </td>
                             </tr>
                            
                            
                             <tr>
                                <td class="style7">
                                    <asp:Label ID="Label18" runat="server" Text="Yarn Supllier" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtSupplier" runat="server" BackColor="WhiteSmoke" 
                                        ReadOnly="True" TabIndex="5" Width="258px"></asp:TextBox>
                                </td>
                            </tr>
                             <tr>
                                <td class="style7">
                                    <asp:Label ID="Label1" runat="server" Text="Yarn Country(Origin)" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtCountry" runat="server" BackColor="WhiteSmoke" 
                                        ReadOnly="True" TabIndex="5" Width="258px"></asp:TextBox>
                                </td>
                            </tr>
                             <tr>
                                 <td class="style7">
                                     LC No</td>
                                 <td style="text-align: left">
                                     <asp:TextBox ID="txtLCNO" runat="server" BackColor="WhiteSmoke" ReadOnly="True" 
                                         TabIndex="9" Width="150px"></asp:TextBox>
                                 </td>
                             </tr>
                            <tr>
                                <td class="style7">
                                    LC Qty</td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtTotQty" runat="server" TabIndex="5" BackColor="WhiteSmoke" 
                                        ReadOnly="True" Width="101px"></asp:TextBox>
                                    Rcv Qty
                                    <asp:TextBox ID="txtRcvQty" runat="server" BackColor="WhiteSmoke" 
                                        ReadOnly="True" TabIndex="5" Width="100px"></asp:TextBox>
                                    &nbsp;</td>
                            </tr>
                             <tr>
                                
                                <td class="style7">
                                    <asp:Label ID="Label19" runat="server" Text="Lot No" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtLOTNO" runat="server" Width="150px" TabIndex="20"></asp:TextBox>
                                </td>
                            </tr>
                             <tr>
                                
                                <td class="style7">
                                    <asp:Label ID="Label14" runat="server" Text="Unit" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:DropDownList ID="ddlUnit" runat="server" Height="25px" 
                                        Width="125px" TabIndex="21">
                                       
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                
                                <td class="style7">
                                    <asp:Label ID="Label15" runat="server" Text="CTN/BAG Qty" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtQty" runat="server" TabIndex="22" style="text-align: left"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                
                                <td class="style7">
                                    <asp:Label ID="Label16" runat="server" Text="Receive Qty" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtReceiveQty" runat="server" TabIndex="23"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                
                                <td class="style7">
                                    <asp:Label ID="Label2" runat="server" Text="From Barcode No" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtBarcodeFrom" runat="server" TabIndex="23" Width="62px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                
                                <td class="style7">
                                    <asp:Label ID="Label3" runat="server" Text="To Barcode No" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtBarcodeTo" runat="server" TabIndex="23" Width="63px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    Rcv
                                    <asp:Label ID="Label17" runat="server" Text="Date" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtPDate" runat="server" TabIndex="24"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txtPDate_CalendarExtender" runat="server" 
                                        Enabled="True" TargetControlID="txtPDate">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                           
                           
                             <tr>
                                <td class="style7">
                                    Invoice No</td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtInvoiceNo" runat="server" TabIndex="25"></asp:TextBox>
                                 </td>
                            </tr>
                           
                            
                            
                             <tr>
                                 <td class="style7">
                                     Notes</td>
                                 <td style="text-align: left">
                                     <asp:TextBox ID="txtNotes" runat="server" Height="35px" TabIndex="26" 
                                         Width="259px" TextMode="MultiLine"></asp:TextBox>
                                 </td>
                             </tr>
                             <tr>
                                 <td class="style7">
                                     &nbsp;</td>
                                 <td style="text-align: left">
                                     <asp:Label ID="lblMsg" runat="server" Font-Bold="True" Font-Size="Smaller" 
                                         ForeColor="#CC3300"></asp:Label>
                                 </td>
                             </tr>
                            
                            
                            <tr>
                                <td class="style7">
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value="-1" />
                                </td>
                                <td class="style8">
                                    <asp:Button ID="btnSave" runat="server" Height="24px"  Text="Save" 
                                        Width="60px" onclick="btnSave_Click" Font-Names="Georgia" TabIndex="27" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="60px" 
                                        Height="24px" Font-Names="Georgia" onclick="btnCancel_Click" 
                                        TabIndex="28" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style4">
                                    &nbsp;</td>
                                <td>
                                    <asp:UpdateProgress ID="UpdateProgress3" runat="server" 
                                        AssociatedUpdatePanelID="updPanel" DisplayAfter="10">
                                    <ProgressTemplate>
                                      <img id="dd" alt="Loading.." src="Images/loading.gif" />
                                    </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                        </table>
                        <div>
                              <asp:UpdatePanel ID="upGrid" UpdateMode="Conditional" runat="server">
                              <ContentTemplate>
                              
                              
                               <asp:GridView 
                                ID="gvProducts1" runat="server" 
                                 AutoGenerateColumns="False" 
                                AllowPaging="True" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts1_RowDataBound" Height="16px" 
                                onpageindexchanging="gvProducts1_PageIndexChanging" PageSize="6">
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="«" LastPageText="»" />      
                                <Columns>
                                   
                         

                                    <asp:BoundField 
                                        HeaderText="SL" DataField="SL" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="LotNo" DataField="LotNo" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                     </asp:BoundField>
                                     <asp:BoundField 
                                        HeaderText="UNIT" DataField="UNIT" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                     </asp:BoundField>
                                     <asp:BoundField 
                                        HeaderText="CTNQty" DataField="CTNQty" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                     </asp:BoundField>
                                     <asp:BoundField 
                                        HeaderText="Receive Qty" DataField="TotalQty" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                     </asp:BoundField>
                                     <asp:BoundField 
                                        HeaderText="RCV Date" DataField="PDate" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                     </asp:BoundField>
                                     <asp:BoundField 
                                        HeaderText="Invoice No" DataField="InvoiceNo" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                     </asp:BoundField>
                                     <asp:BoundField 
                                        HeaderText="Notes" DataField="Notes" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                     </asp:BoundField>
                                    
                        
                                    <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" 
                                                ImageUrl="~/Images/btn_delete.gif" onclick="btnDelete1_Click" 
                                                Height="16px" 
                                            onclientclick="javascript:return confirm('Are you sure you want to delete this row?');" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView> 
                            
                               </ContentTemplate>
                              </asp:UpdatePanel> 
                        </div>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        
                    </asp:Panel>
                 </div>
                <div style="height: 40px; border-bottom-style: none; border-bottom-color: #003399;">
                    <div style="float: left">
                </div>
                <div align="center" style="height: 30px">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LCNo<%-- </ContentTemplate>
                    </asp:UpdatePanel>--%> <asp:TextBox ID="txtSearchLC" runat="server" Width="87px"></asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Record From<asp:TextBox ID="txtFrom" runat="server" Width="37px">1</asp:TextBox>to<asp:TextBox ID="txtTo" runat="server" Width="37px">10</asp:TextBox><asp:Button ID="btnload" runat="server" onclick="btnload_Click" Text="Load" 
                          Width="86px" />
                                    <%--      <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>--%>
                                    
                    </div>
                
                </div>
                <div style="height: 8px; border-bottom-style: groove; border-bottom-color: #003399;">
                
                </div>
                    <!-- Content Goes Here! -->
                    <asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView 
                                ID="gvProducts" runat="server" 
                                 AutoGenerateColumns="False" 
                                AllowPaging="True" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                onpageindexchanging="gvProducts_PageIndexChanging" PageSize="6">
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="«" LastPageText="»" />      
                                <Columns>
                                   
       
     
                                    <asp:BoundField 
                                        HeaderText="ID" DataField="ID" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="YID" HeaderText="YID" />
                                    <asp:BoundField DataField="YGID" HeaderText="YGID" />
                                    <asp:BoundField 
                                        HeaderText="Yarn Name" DataField="Yarn" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                     <asp:BoundField DataField="Supplier" HeaderText="Supplier" >
                                     <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>   
                                      
                                    <asp:BoundField DataField="Country" HeaderText="Country" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                      
                                  
                                     
                                           <asp:BoundField DataField="LCNo" HeaderText="LCNo" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                      
                                      <asp:BoundField DataField="LCQty" HeaderText="LCQty" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                     
                                      
                                    
                                     <asp:BoundField DataField="ReceiveQty" HeaderText="ReceiveQty" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField> 
                                     
                                     
                                 
                                 
                                      <asp:TemplateField HeaderText="Receive">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/btn_edit.gif" 
                                                onclick="btnEdit_Click" AlternateText="Receive Yarn" Height="16px" />
                                            
                                           
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                    
                                   
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                                
              <%--      <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>--%>
                    
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnload" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>                     
                    </ContentTemplate>
                        
                        </asp:UpdatePanel> 
                </div>
                
                
                </div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>      
    </div>
</asp:Content>

