﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_MCRunningList.aspx.cs" Inherits="ERP.KNIT_MCRunningList" Title="Running Machine List" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="Script/jquery-1.8.2.js" type="text/javascript"></script>
       <style type="text/css">
       
       table {
    border-collapse: collapse;
    
    color: #000;
    font-family: "Lucida Bright", "Times New Roman", serif;
    font-size: 0.85em;
}

/* We add letter spacing because all caps makes the letters scrunchy
   Also, left aligned because it was favoured by my university for table
   captions, but yours might be different. Actually for a scientific paper,
   the caption would be more detailed, resembling a table 'summary' */
caption {
    padding-bottom: 5px;
    font-variant: small-caps;
    letter-spacing: 0.1em;
    text-align: left;
}

/* Table headers should be clear, but concise and discreet.
   The double border separates the row from caption and the table body 
   without needing a different background colour. Because we've specified
   a text colour, we also specify a background colour (even though it's the
   same as the main table colour) to accommodate personal stylesheets. */
thead th {
    border-top: 3px double #ccc;
    border-bottom: 3px double #ccc;
    padding: 2px 10px;
    background-color: #fff;
    color: #aaa;
    text-align: left;
    font-variant: small-caps;
    letter-spacing: 0.1em;
    white-space: nowrap;
}

/* We've given the table footer the same double border treatment for the
   same reason.  It also acts as a nice "end of table" indicator. It's 
   part of the data so we leave the text black. We also treat the header
   and data the same way. */
tfoot th, tfoot td {
    border-top: 3px double #ccc;
    border-bottom: 3px double #ccc;
    padding: 2px 10px;
    font-variant: small-caps;
    letter-spacing: 0.1em;
}

/* We now revert to discreet single pixel horizontal borders to separate
   each entry.  If your table content is numerical data, you might want the
   vertical borders too, but I find it's visually more pleasing and easier to
   read online when there is plenty of padding instead of borders. 
   NOTE: we treat the headers and data cells the same here, visually it's clear
   enough (header text is bold) and does not affect screen-reader software */
tbody th, tbody td {
    border-bottom: 1px solid #ccc;
    padding: 2px 10px;
    text-align: left;
    vertical-align: top;
}

/* Faux alpha transparency.  It's just a 16px square image (a PNG, but
   could be a GIF) filled with a colour (I chose orange because it
   contrasts pleasingly with blue - opposite on colour wheel) and then
   every other pixel is filled with another colour (e.g. white) which is
   made the transparent colour (ordinary transparency) - anything behind
   the transparent pixels shows through and our wonderful brain fills in
   the rest, thanks to Gestalt psychology.  Only really works when a solid
   colour is required, such as table row rollovers. NOTE: IE doesn't like
   tr:hover anyway so "pprrffffft!" to that! */
tbody tr:hover {
    
}

/* CSS2 selectors - this just means the link in the fourth TD along
   (the last column).  It simply adds a "download" icon and makes the
   text all caps - will be ignored by older browsers. We also add a 
   link colour (same as all other links) for personal stylesheets. */
tbody td+td+td+td a {
    padding-right: 16px;
  
    color: #00d;
    font-variant: small-caps;
    letter-spacing: 0.1em;
}

/* Finally we have the link styles: background has been rendered as
   transparent so the background image isn't blocked in an ugly way. */
tbody a {
    background-color: transparent;
    color: #00d;
    text-decoration: none;
}
tbody a:visited {
    background-color: transparent;
    color: #00a;
}
tbody a:hover {
    background-color: transparent;
    color: #00f;
}
tbody a:active {
    background-color: transparent;
    color: #f00;
}
       </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
<div>
    <asp:Button ID="btnLoad" runat="server" Text="Load Running M/C List" 
        onclick="btnLoad_Click" />
</div>


 <fieldset>
    

 
    
    

   <div style="overflow: auto; height: 600px">
               <asp:UpdatePanel ID="upMCGrid" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>
                        
                <asp:GridView 
                                ID="gvProducts" runat="server" 
                                 
                               
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                ShowFooter="False"
                                Width="400px" onrowcreated="gvProducts_RowCreated" 
                          AutoGenerateColumns="False" GridLines="None">
                            
                               
                               
                              
                                <Columns>
                                   
                                    <asp:BoundField 
                                        HeaderText="ID" DataField="ID" 
                                        SortExpression="ID" >
                                      <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    
                                    <asp:BoundField 
                                        HeaderText="Machine" DataField="Machine" 
                                        SortExpression="Machine" >
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" Width="50px" />
                                    </asp:BoundField>
                                 
                                    <asp:BoundField 
                                        HeaderText="AssignDate" DataField="AssignDate" 
                                        SortExpression="AssignDate">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" Width="80px"/>
                                    </asp:BoundField>
                                    
                                    
                                    
                                    <asp:BoundField 
                                        HeaderText="Fabric" DataField="Fabric" 
                                        SortExpression="Fabric">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" Width="150px"/>
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="GM" DataField="GM" 
                                        SortExpression="GM">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" Width="60px"/>
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="GSM" DataField="GSM" 
                                        SortExpression="GSM">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" Width="60px"/>
                                    </asp:BoundField>
                                    
                                    
                                   
                                    
                                   
                                    
                                  
                                    
                                    
                                    <asp:TemplateField HeaderText="Operation"  HeaderStyle-HorizontalAlign="Left" >
                                    <ItemTemplate>
                                        <asp:Button ID="btnOperation" runat="server" Text="Stop" onclick="btnOperation_Click"
                                        onclientclick="javascript:return confirm('Do you want to Stop this Machine?');"
                                        />
                           
                                    </ItemTemplate>
                                        
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                   
                                 
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                            
                  </ContentTemplate>
                      <Triggers>
                          <asp:AsyncPostBackTrigger ControlID="btnLoad" EventName="Click" />
                      </Triggers>
                  </asp:UpdatePanel>   
   </div>
       </fieldset>


</asp:Content>
