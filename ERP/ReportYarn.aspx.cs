﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace ERP
{
    public partial class WebForm24 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            string jscript = "";
            string ReportPar = "";
            string ReportName = "";
            string pr = "YarnBalance";

            ReportName = "rptYarnBalance";



            string Fdate = txtDate.Text.ToString();
           

            string RTYpe = "Yarn Balance Report"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&Fdate=" + Fdate + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";

            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm24), "ScriptFunction", jscript);
        }

        protected void btnShow0_Click(object sender, EventArgs e)
        {
            string jscript = "";
            string ReportPar = "";
            string ReportName = "";
            string pr = "YarnISSUE";

            ReportName = "rptYarnIssue";



            string Fdate = txtDate.Text.ToString();
            string Tdate = txtToDate.Text.ToString();


            string RTYpe = "Yarn Balance Report"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&Fdate=" + Fdate + "&Tdate=" + Tdate + "&SType=" + ddlIssueTo.Text + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";

            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm24), "ScriptFunction", jscript);
        }

        protected void btnShow1_Click(object sender, EventArgs e)
        {
            string jscript = "";
            string ReportPar = "";
            string ReportName = "";
            string pr = "YarnRECEIVE";

            ReportName = "rptYarnPurchase";



            string Fdate = txtDate.Text.ToString();
            string Tdate = txtToDate.Text.ToString();


            string RTYpe = "Yarn Balance Report"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&Fdate=" + Fdate + "&Tdate=" + Tdate + "&SType=" + ddlType.Text + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";

            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm24), "ScriptFunction", jscript);
        }
    }
}
