﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class WebForm5 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              

                //if (new Utilities_DL().CheckSecurity("503", USession.SUserID) == true)
                //{
                   
                    
                    loadYarn();
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
        private void  loadYarn()
        {
            ddlCountry.DataSource = new Yarn_DL().GetYarnCountry();
            ddlCountry.DataValueField = "CountryOrSupplier";
            ddlCountry.DataTextField = "CountryOrSupplier";
            ddlCountry.DataBind();

            ddlSupplier.DataSource = new Yarn_DL().GetYarnSupplier();
            ddlSupplier.DataValueField = "CountryOrSupplier";
            ddlSupplier.DataTextField = "CountryOrSupplier";
            ddlSupplier.DataBind();


            ddlUnit.DataSource = new Yarn_DL().GetAllUnits();
            ddlUnit.DataValueField = "UNIT1";
            ddlUnit.DataTextField = "UNIT1";
            ddlUnit.DataBind();
            ddlUnit.Items.Add("--Select--");
            ddlUnit.Text = "--Select--";
            LoadYarnGroup();
        }
        private void LoadYarn(Int64 ygID)
        {
            ddlYarn.DataSource = new Yarn_DL().GetYarnByYarnGroup(ygID);
            ddlYarn.DataValueField = "YID";
            ddlYarn.DataTextField = "YarnName";
            ddlYarn.DataBind();
            ddlYarn.Items.Add("--Select--");
            ddlYarn.Text = "--Select--";
        }
        private void LoadYarnGroup()
        {


            ddlYarnGroup.DataSource = new Yarn_DL().GetActiveYarnGroup();
            ddlYarnGroup.DataValueField = "ID";
            ddlYarnGroup.DataTextField = "YarnGroupName";
            ddlYarnGroup.DataBind();
            ddlYarnGroup.Items.Add("--Select--");
            ddlYarnGroup.Text = "--Select--";


        }

        private void loadGridData()
        {


           
            gvProducts.DataSource = new Yarn_DL().GetActivePurchaseRecord(Common.GetNumber(txtFrom.Text), Common.GetNumber(txtTo.Text));
            gvProducts.DataBind();

        }
       

       

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Yarn_DL().PurchaseDeleteActive(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

     

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
      
        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (CheckEntry() == true)
            {

                try
                {
                    YarnPurchase c = new YarnPurchase();
                    c.SL = 0;
                    c.YID = Convert.ToInt64(ddlYarn.SelectedValue);

                    c.Status = ddlType.Text.ToString();
                    c.CTNQty = Convert.ToDecimal(txtQty.Text);
                    c.TotalQty = Convert.ToDecimal(txtTotQty.Text);
                    c.PDate = Convert.ToDateTime(txtPDate.Text);
                    c.Supplier = ddlSupplier.Text.ToString();
                    c.Country = ddlCountry.Text.ToString();
                    c.PurchaseType = ddlType.Text;
                    c.InvoiceNo = txtInvoiceNo.Text.ToString();
                    c.LotNo = txtLOTNO.Text.ToString();
                    c.LCNo = txtLCNO.Text.ToString();
                    c.UNIT = ddlUnit.Text;
                    c.Notes = txtNotes.Text;

                    c.FromBarcodeID = Convert.ToInt32(txtBarcodeFrom.Text);
                    c.ToBarcodeID = Convert.ToInt32(txtBarcodeTo.Text);
                    c.EntryID = USession.SUserID;


                    if (new Yarn_DL().SavePurchase(c) == true)
                    {
                        lblMsg.Text = "Saved Successfully";
                    }


                    txtPDate.Text = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year;
                    Clear();
                    
                   

                }
                catch
                {

                }
            }
            UpEntry.Update();
        }


        private bool CheckEntry()
        {
            if (ddlUnit.Text == "--Select--")
            {
                lblMsg.Text = "Select Unit";
                return false;
            }
            else if (ddlYarn.Text == "--Select--")
            {
                lblMsg.Text = "Select Yarn";
                return false;
            }
            else if (Common.GetDecimal(ddlYarn.SelectedValue) == 0)
            {
                lblMsg.Text = "Select Yarn";
                return false;
            }
            else if (Common.GetDecimal(txtQty.Text) == 0)
            {
                lblMsg.Text = "Give QTY";
                return false;
            }
            
            

          
            else if (Common.GetNumber(txtBarcodeFrom.Text) == 0)
            {
                lblMsg.Text = "Starting Barcode No is needed";
                return false;
            }
            else if (Common.GetNumber(txtBarcodeTo.Text) == 0)
            {
                lblMsg.Text = "Ending Barcode No is needed";
                return false;
            }
            else if (new Yarn_DL().IsDuplicateBarcode(Common.GetNumber(txtBarcodeFrom.Text), Common.GetNumber(txtBarcodeTo.Text)) == true)
            {
                lblMsg.Text = "Duplicate Barcode,This Barcode Range is duplicated, Please try new ID";
                return false;
            }
            else if (Common.GetNumber(txtBarcodeTo.Text) > 0)
            {
                int tot = (Common.GetNumber(txtBarcodeTo.Text)  - Common.GetNumber(txtBarcodeFrom.Text))+1;
                if (Common.GetNumber(txtQty.Text) > 0)
                {
                    int tot1 =Common.GetNumber(txtQty.Text);
                    if (tot != tot1)
                    {
                        lblMsg.Text = "Wrong Barcode serial";
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    lblMsg.Text = "Wrong Barcode serial";
                    return false;
                }
             
            }
            else
            {
                return true;
            }
       
     
        }
     

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
            lblMsg.Text = "";
        }

        protected void gvProducts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnload_Click(object sender, EventArgs e)
        {
            loadGridData();
        }
        private void Clear()
        {
            txtQty .Text = "";
           
            txtTotQty.Text = "";
            txtLCNO.Text = "";
            txtInvoiceNo.Text = "";
            txtNotes.Text="";
            txtLOTNO.Text="";
            ddlYarn.Text = "--Select--";
            ddlYarn.Focus();
            UpEntry.Update();
        }

        protected void ddlYarnGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
            else
            {
                Int64 GID = Convert.ToInt64(ddlYarnGroup.SelectedValue);
                LoadYarn(GID);
                UpEntry.Update();
            }
        }
    }
}
