﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDLayer;
using System.Reflection;
using System.IO;

namespace ERP
{
    public partial class WebForm35 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPacking_Click(object sender, EventArgs e)
        {

        }

        protected void Button1_DirectClick(object sender, Ext.Net.DirectEventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadOrderProductDesc();
        }
        private void LoadOrderProductDesc()
        {
            ddlProduct.DataSource = new Order_DL().GetOrderProductDesc(txtOrderNo.Text);
            ddlProduct.DataValueField = "OPID";
            ddlProduct.DataTextField = "DESC";

            ddlProduct.DataBind();
            ddlProduct.Items.Add("--Select--");
            ddlProduct.Text = "--Select--";
        }

        protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProduct.SelectedItem.Text != "--Select--")
            {


                LoadOrderproductDetails();
            }
        }
        private void LoadOrderproductDetails()
        {
            try
            {
                
                ddlColor.DataSource = new Order_DL().GetOrderProductColorList(Convert.ToInt64(ddlProduct.SelectedValue.ToString()));
                ddlColor.DataValueField = "OPDID";
                ddlColor.DataTextField = "Color";
                ddlColor.DataBind();
                ddlColor.Items.Add("--Select--");
                ddlColor.Text = "--Select--";


                //string s = ddlDesc.SelectedItem.Text.Replace(',', ' ');
                //string[] desc = s.Split('>');
                //if (desc.Length > 1)
                //{
                //    txtDescNotes.Text = desc[1];
                //    txtGSM.Text = desc[desc.Length - 2];
                //    txtwidth.Text = desc[desc.Length - 1];
                //    UpEntry.Update();
                //}
            }
            catch
            {
            }
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            EProductionLoss obj=new EProductionLoss();
            obj.Order=txtOrderNo.Text;
            obj.Color=ddlColor.SelectedItem.Text;
            obj.Product = ddlProduct.SelectedItem.Text;
            if (ddlColor.Text != "--Select--")
            {
                List<EProductionLoss> li = new Order_DL().GetProductLoss(Convert.ToInt64(ddlColor.SelectedValue), obj);

                if (li != null)
                {
                    double Deying = li.Sum(o => o.GreyIssueQty);
                    double Packing = li.Sum(o => o.PackingQty);
                    txtBookingQty.Text = li[0].BookingQty.ToString();
                    txtDeying.Text = Deying.ToString();
                    txtPacking.Text = Packing.ToString();

                    txtLoss.Text = (Deying - Packing).ToString("0.00");
                    if (Packing > 0)
                    {
                        txtLossPer.Text = ((100 * (Deying - Packing)) / Deying).ToString("0");
                    }
                    else
                    {
                        txtLossPer.Text = "N/A";
                    }

                    txtBalance.Text = (li[0].BookingQty - Packing).ToString("0.00");
                    Store1.DataSource = li;
                    Store1.DataBind();
                }
                else
                {
                    txtBookingQty.Text = string.Empty;
                    txtDeying.Text = string.Empty;
                    txtPacking.Text = string.Empty;
                    txtLossPer.Text = string.Empty;
                    txtLoss.Text = string.Empty;
                    txtBalance.Text = string.Empty;


                }
            }
        }


        public void Export(string fileName, List<EProductionLoss> empList)
        {

            //The Clear method erases any buffered HTML output.

            HttpContext.Current.Response.Clear();

            //The AddHeader method adds a new HTML header and value to the response sent to the client.

            HttpContext.Current.Response.AddHeader(

               "content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));

            //The ContentType property specifies the HTTP content type for the response.

            HttpContext.Current.Response.ContentType = "application/ms-excel";

            //Implements a TextWriter for writing information to a string. The information is stored in an underlying StringBuilder.

            using (StringWriter sw = new StringWriter())
            {

                //Writes markup characters and text to an ASP.NET server control output stream. This class provides formatting capabilities that ASP.NET server controls use when rendering markup to clients.

                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {

                    //  Create a form to contain the List

                    Table table = new Table();

                    TableRow row0 = new TableRow();

                    TableHeaderCell col10 = new TableHeaderCell();
                    col10.Text = "Loss Report  " ;
                   

                    row0.Cells.Add(col10);
                   

                    //  add each of the data item to the table
                    table.Rows.Add(row0);



                    TableRow row = new TableRow();

                     TableHeaderCell hcell = new TableHeaderCell();
                     hcell.Text = "Order No";
                     row.Cells.Add(hcell);

                     hcell = new TableHeaderCell();
                     hcell.Text = "Customer";
                     row.Cells.Add(hcell);

                     hcell = new TableHeaderCell();
                     hcell.Text = "Product";
                     row.Cells.Add(hcell);

                     hcell = new TableHeaderCell();
                     hcell.Text = "Color";
                     row.Cells.Add(hcell);

                     hcell = new TableHeaderCell();
                     hcell.Text = "LotNo";
                     row.Cells.Add(hcell);


                     hcell = new TableHeaderCell();
                     hcell.Text = "ProcessCard";
                     row.Cells.Add(hcell);

                     hcell = new TableHeaderCell();
                     hcell.Text = "GreyIssueQty";
                     row.Cells.Add(hcell);

                     hcell = new TableHeaderCell();
                     hcell.Text = "PackingQty";
                     row.Cells.Add(hcell);

                     hcell = new TableHeaderCell();
                     hcell.Text = "LossKG";
                     row.Cells.Add(hcell);

                     hcell = new TableHeaderCell();
                     hcell.Text = "LossPer";
                     row.Cells.Add(hcell);
                    //foreach (PropertyInfo proinfo in new EProductionReport().GetType().GetProperties())
                    //{

                    //    TableHeaderCell hcell = new TableHeaderCell();

                    //    hcell.Text = proinfo.Name;

                    //    row.Cells.Add(hcell);

                    //}
      
                    table.Rows.Add(row);


                    foreach (EProductionLoss emp in empList)
                    {


                        TableRow row1 = new TableRow();

                        TableCell col1 = new TableCell();
                        col1.Text = "" + emp.Order;
                        TableCell col2 = new TableCell();
                        col2.Text = "" + emp.Customer;
                        TableCell col3 = new TableCell();
                        col3.Text = "" + emp.Product;
                        TableCell col4 = new TableCell();
                        col4.Text = "" + emp.Color;
                        TableCell col51 = new TableCell();
                        col51.Text = "" + emp.LotNo;
                        TableCell col5 = new TableCell();
                        col5.Text = "" + emp.ProcessCard;
                       

                        TableCell col6 = new TableCell();
                        col6.Text = "" + emp.GreyIssueQty;
                        TableCell col7 = new TableCell();
                        col7.Text = "" + emp.PackingQty;
                        TableCell col8 = new TableCell();
                        col8.Text = "" + emp.LossKG;
                        TableCell col9 = new TableCell();
                        col9.Text = "" + emp.LossPer;

                      
                        row1.Cells.Add(col1);
                        row1.Cells.Add(col2);
                        row1.Cells.Add(col3);
                        row1.Cells.Add(col4);
                        row1.Cells.Add(col51);
                        row1.Cells.Add(col5);
                        row1.Cells.Add(col6);
                        row1.Cells.Add(col7);
                        row1.Cells.Add(col8);
                        row1.Cells.Add(col9);
                        table.Rows.Add(row1);

                    }

                    //  render the table into the htmlwriter

                    table.RenderControl(htw);

                    //  render the htmlwriter into the response

                    HttpContext.Current.Response.Write(sw.ToString());

                    HttpContext.Current.Response.End();

                }

            }

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (ddlColor.Text != "--Select--")
            {
                EProductionLoss obj = new EProductionLoss();
                obj.Order = txtOrderNo.Text;
                obj.Color = ddlColor.SelectedItem.Text;
                obj.Product = ddlProduct.SelectedItem.Text;
                List<EProductionLoss> li = new Order_DL().GetProductLoss(Convert.ToInt64(ddlColor.SelectedValue), obj);
                Export("Los_Report", li);
            }
        }
    }
}
