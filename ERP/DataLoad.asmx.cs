﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
using System.Web.Script.Services;

namespace ERP
{
    /// <summary>
    /// Summary description for DataLoad
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    [ScriptService]
    public class DataLoad : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public List<EOrderList> GetOrderList(string PINO)
        {
            return new Order_DL().GetActiveOrderList().FindAll(m => m.OrderNo.ToLower().StartsWith(PINO.ToLower())); 
        }

        [WebMethod]
        public List<EPCardList> GetProcessCardList(string PCardNO)
        {
            return new ProcessCard_CL().GetPCardList().FindAll(m => m.PCardNo.ToLower().StartsWith(PCardNO.ToLower()));
        }

        [WebMethod]
        public List<EINV_Item> GetItemList(int TI, int GI, string item1, int StoreID)
        {
            return new Inventory_DL().GetInventoryItemBasic(TI, GI, StoreID).FindAll(m => m.ItemName.ToLower().StartsWith(item1.ToLower()));
        }

        [WebMethod]
        public List<EINV_Item> GetItemParentList(int TI, int GI, string item1, int StoreID)
        {
            return new Inventory_DL().GetInventoryItem(TI, 0, GI).FindAll(m => m.ParentItem.ToLower().StartsWith(item1.ToLower()));

        }

       

    }


 
}
