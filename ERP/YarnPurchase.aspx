<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="YarnPurchase.aspx.cs" Inherits="ERP.WebForm5" Title="Yarn Purchase Form" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="AjaxControls" namespace="AjaxControls" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style5
        {
            width: 20px;
        }
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
        .style6
        {
            width: 20px;
            height: 2px;
        }
        .style7
        {
            width: 128px;
            height: 2px;
        }
        .style8
        {
            height: 2px;
            text-align: left;
        }
               
        </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <div class="grid" >
            <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>Yarn Receive</h2></center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid" 
                        style="overflow:auto; width: 739px; height: 699px; background-color:"
                        whitesmoke";">     
                <div style="height:25px;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                        AssociatedUpdatePanelID="updPanel">
                        <ProgressTemplate>
                            <img alt="" src="Images/loading.gif" style="height: 15px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    </div>
                    
                 <div align="center">
                    <asp:Panel ID="pnlDataEntry" runat="server"    CssClass="modalBox"  
                        BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                        Width="448px" BorderWidth="2px">
                        <center>
                            <asp:Panel ID="ProductCaption" runat="server"   
                                Style="margin-bottom: 10px; cursor: hand;" 
                                BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                Yarn Receive Entry Form</asp:Panel></center>
                            
                           <asp:UpdatePanel ID="UpEntry" runat="server" UpdateMode="Conditional" 
                            ChildrenAsTriggers="False" >
                        <ContentTemplate>
                         <table cellpadding="1" cellspacing="0" style="width:100%;">
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    Yarn Group</td>
                                <td style="text-align: left">
                            
                                    <asp:DropDownList ID="ddlYarnGroup" runat="server" AutoPostBack="True" 
                                        Height="24px" onselectedindexchanged="ddlYarnGroup_SelectedIndexChanged" 
                                        TabIndex="1" Width="157px">
                                    </asp:DropDownList>
                                    
                                </td>
                            </tr>
                             <tr>
                                 <td class="style5">
                                     &nbsp;</td>
                                 <td class="style4">
                                     <asp:Label ID="Label12" runat="server" Font-Names="Georgia" Text="Yarn Name"></asp:Label>
                                 </td>
                                 <td style="text-align: left">
                                     <asp:DropDownList ID="ddlYarn" runat="server" Height="24px" TabIndex="1" 
                                         Width="157px">
                                     </asp:DropDownList>
                                 </td>
                             </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label13" runat="server" Text="Type" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:DropDownList ID="ddlType" runat="server" Height="24px" 
                                        Width="157px" TabIndex="2">
                                        <asp:ListItem Value="RECEIVE">RECEIVE</asp:ListItem>
                                        <asp:ListItem Value="RETURN">RETURN</asp:ListItem>
                                        <asp:ListItem Value="LOAN">LOAN</asp:ListItem>
                                        <asp:ListItem Value="SC">SC</asp:ListItem>
                                        
                                        
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            
                            
                             <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label18" runat="server" Text="Yarn Supllier" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:DropDownList ID="ddlSupplier" runat="server" Height="24px" TabIndex="1" 
                                        Width="157px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label1" runat="server" Text="Yarn Country(Origin)" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:DropDownList ID="ddlCountry" runat="server" Height="24px" TabIndex="1" 
                                        Width="157px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label19" runat="server" Text="Lot No" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtLOTNO" runat="server" Width="150px" TabIndex="8"></asp:TextBox>
                                </td>
                            </tr>
                             <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label14" runat="server" Text="Unit" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:DropDownList ID="ddlUnit" runat="server" Height="25px" 
                                        Width="125px" TabIndex="3">
                                       
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label15" runat="server" Text="CTN/BAG Qty" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtQty" runat="server" TabIndex="4" style="text-align: left"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label16" runat="server" Text="Total Quantity" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtTotQty" runat="server" TabIndex="5"></asp:TextBox>
                                </td>
                            </tr>
                             <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label2" runat="server"  Text="From Barcode No" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtBarcodeFrom" runat="server" TabIndex="5" Width="90px"></asp:TextBox>
                                </td>
                            </tr>
                             <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label3" runat="server" Text="To Barcode No" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtBarcodeTo" runat="server" TabIndex="5" Width="87px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label17" runat="server" Text="Date" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtPDate" runat="server" TabIndex="6"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txtPDate_CalendarExtender" runat="server" 
                                        Enabled="True" TargetControlID="txtPDate">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                           
                           
                             <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label20" runat="server" Text="LC No" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtLCNO" runat="server" Width="150px" TabIndex="9"></asp:TextBox>
                                </td>
                            </tr>
                              <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label21" runat="server" Text="Invoice No" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="text-align: left">
                                    <asp:TextBox ID="txtInvoiceNo" runat="server" Width="150px" TabIndex="10"></asp:TextBox>
                                </td>
                            </tr>
                           
                            
                            
                             <tr>
                                 <td class="style5">
                                     &nbsp;</td>
                                 <td class="style4">
                                     Notes</td>
                                 <td style="text-align: left">
                                     <asp:TextBox ID="txtNotes" runat="server" Height="22px" TabIndex="12" 
                                         Width="259px"></asp:TextBox>
                                 </td>
                             </tr>
                             <tr>
                                 <td class="style5">
                                     &nbsp;</td>
                                 <td class="style4">
                                     &nbsp;</td>
                                 <td style="text-align: left">
                                     <asp:Label ID="lblMsg" runat="server" Font-Bold="True" Font-Size="Smaller" 
                                         ForeColor="#CC3300"></asp:Label>
                                 </td>
                             </tr>
                            
                            
                            <tr>
                                <td class="style6">
                                    </td>
                                <td class="style7">
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value="-1" />
                                </td>
                                <td class="style8">
                                    <asp:Button ID="btnSave" runat="server" Height="24px"  Text="&amp;Save" 
                                        Width="60px" onclick="btnSave_Click" Font-Names="Georgia" TabIndex="13" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="60px" 
                                        Height="24px" Font-Names="Georgia" onclick="btnCancel_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    &nbsp;</td>
                                <td>
                                    <asp:UpdateProgress ID="UpdateProgress3" runat="server" 
                                        AssociatedUpdatePanelID="updPanel" DisplayAfter="10">
                                    <ProgressTemplate>
                                      <img id="dd" alt="Loading.." src="Images/loading.gif" />
                                    </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        
                    </asp:Panel>
                 </div>
                <div style="height: 40px; border-bottom-style: none; border-bottom-color: #003399;">
                    <div style="float: left">
                </div>
                <div align="center" style="height: 30px">
                 <%--<asp:UpdatePanel ID="uprbl" runat="server" UpdateMode="Conditional">
                  <ContentTemplate>--%>
                      Record From<asp:TextBox ID="txtFrom" runat="server" Width="37px">1</asp:TextBox>to<asp:TextBox ID="txtTo" runat="server" Width="37px">10</asp:TextBox><asp:Button ID="btnload" runat="server" onclick="btnload_Click" Text="Load" 
                          Width="86px" />
                   <%-- </ContentTemplate>
                    </asp:UpdatePanel>--%>
                    </div>
                
                </div>
                <div style="height: 8px; border-bottom-style: groove; border-bottom-color: #003399;">
                
                </div>
                    <!-- Content Goes Here! -->
                    <asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView 
                                ID="gvProducts" runat="server" 
                                 AutoGenerateColumns="False" 
                                AllowPaging="True" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                onpageindexchanging="gvProducts_PageIndexChanging" PageSize="6">
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="�" LastPageText="�" />      
                                <Columns>
                                    
                                    <asp:BoundField 
                                        HeaderText="SL" DataField="SL" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="YID" HeaderText="YID" />
                                    <asp:BoundField 
                                        HeaderText="YarnName" DataField="YarnName" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                     <asp:BoundField DataField="TotalQty" HeaderText="Qty" >
                                     <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>   
                                      
                                    <asp:BoundField DataField="Status" HeaderText="Type" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                      
                                  
                                     
                                           <asp:BoundField DataField="Pdate" HeaderText="Date" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                      
                                     
                                    
                                 
                                    <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" 
                                                ImageUrl="~/Images/btn_delete.gif" onclick="btnDelete_Click" 
                                                Height="16px" 
                                            onclientclick="javascript:return confirm('Are you sure you want to delete this row?');" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                            <asp:Button ID="btnHiddenProduct" runat="Server" Style="display:none"  />
                                 
              <%--      <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>--%>
                    
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnload" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>                     
    
                </div></div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>      
    </div>
</asp:Content>

 <%-- --%>