﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;

namespace ERP
{
    public partial class WebForm22 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              

               
                //if (new Utilities_DL().CheckSecurity("503", USession.SUserID) == true)
                //{
                   
                    loadGridData();
                    txtStockDate.Text = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year;
                   
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }

        private void loadGridData()
        {
            ddlChemicalGroup.DataSource = new Chemical_DL().GetChemicalGroup();
            ddlChemicalGroup.DataTextField = "GroupName";
            ddlChemicalGroup.DataValueField = "SL";
            ddlChemicalGroup.DataBind();
            ddlChemicalGroup.Items.Add("All Chemical");
            ddlChemicalGroup.Text = "All Chemical";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string jscript = "";

            string ReportName = "";
            string pr = "ChemicalInventory";

            ReportName = "rptChemicalInventory";



            string Fdate = txtStockDate.Text.ToString();
          
            string gid = "0";
            if (ddlChemicalGroup.Text == "All Chemical")
            {
                gid = "0";
            }
            else
            {
                gid = ddlChemicalGroup.SelectedValue.ToString();
            }
            string RTYpe = "Chemical Balance Report"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&CDate=" + Fdate + "&GID=" + gid + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";

            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm23), "ScriptFunction", jscript);
        }
        

        
      
    }
}
