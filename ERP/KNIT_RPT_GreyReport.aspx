﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_RPT_GreyReport.aspx.cs" Inherits="ERP.KNIT_RPT_GreyReport" Title="Report : Grey Report" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();
           
            this.Window1.Title = "Knit Grey Report";
            this.Store1.DataSource = new KNIT_all_operation().GetGreyReport(Convert.ToDateTime(txtFrom.Text), ddlMCNo.Text);
                    
            
          //  this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
     [DirectMethod(Namespace = "NETBEE")]
     public void XLExport()
     {
         List<REPORT_KNIT_PROD_GREY_STOCKResult> liProd = new List<REPORT_KNIT_PROD_GREY_STOCKResult>();
         
         liProd = new KNIT_all_operation().GetGreyReport(Convert.ToDateTime(txtFrom.Text),ddlMCNo.Text);
         string json = JSON.Serialize(liProd);
         StoreSubmitDataEventArgs eSubmit = new StoreSubmitDataEventArgs(json, null);
         XmlNode xml = eSubmit.Xml;

         this.Response.Clear();
         this.Response.ContentType = "application/octet-stream";
         this.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xlsx");
         XslCompiledTransform xtCsv = new XslCompiledTransform();
         xtCsv.Load(Server.MapPath("Excel.xslx"));
         xtCsv.Transform(xml, null, this.Response.OutputStream);
         this.Response.End();

         //string json = GridData.Value.ToString();
         //StoreSubmitDataEventArgs eSubmit = new StoreSubmitDataEventArgs(json, null);
         //XmlNode xml = eSubmit.Xml;

         //this.Response.Clear();
         //this.Response.ContentType = "application/vnd.ms-excel";
         //this.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls");
         //XslCompiledTransform xtExcel = new XslCompiledTransform();
         //xtExcel.Load(Server.MapPath("Excel.xsl"));
         //xtExcel.Transform(xml, null, this.Response.OutputStream);
         //this.Response.End();

     }
     protected void SubmitGrids(object sender, DirectEventArgs e)
     {
         //////JSON representation
         ////string grid1Json = e.ExtraParams["Grid1"];

         ////StoreSubmitDataEventArgs eSubmit = new StoreSubmitDataEventArgs(grid1Json, null);
         ////XmlNode xml = eSubmit.Xml;

         ////this.Response.Clear();
         ////this.Response.ContentType = "application/octet-stream";
         ////this.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls");
         ////XslCompiledTransform xtCsv = new XslCompiledTransform();
         ////xtCsv.Load(Server.MapPath("Excel.xsl"));
         ////xtCsv.Transform(xml, null, this.Response.OutputStream);
         ////this.Response.End();

         string json = e.ExtraParams["Grid1"];
         StoreSubmitDataEventArgs eSubmit = new StoreSubmitDataEventArgs(json, null);
         XmlNode xml = eSubmit.Xml;

         this.Response.Clear();
         this.Response.ContentType = "application/vnd.ms-excel";
         this.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls");
         XslCompiledTransform xtExcel = new XslCompiledTransform();
         xtExcel.Load(Server.MapPath("Excel.xls"));
         xtExcel.Transform(xml, null, this.Response.OutputStream);
         this.Response.End();
     }
    
     protected void Store1_RefreshData(object sender, StoreRefreshDataEventArgs e)
     {
         List<REPORT_KNIT_PROD_GREY_STOCKResult> liProd = new List<REPORT_KNIT_PROD_GREY_STOCKResult>();
         this.Window1.Title = "Grey Report";
         liProd = new KNIT_all_operation().GetGreyReport(Convert.ToDateTime(txtFrom.Text),ddlMCNo.Text);


         this.Store1.DataSource = liProd;
         this.Store1.DataBind();
     }
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> Grey Stock Report</h2></div>
 <div align="left" style="padding: 10px; ">
                      From
                    <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                    &nbsp;MC No
       <asp:DropDownList ID="ddlMCNo" runat="server" Height="23px" Width="105px">
       </asp:DropDownList>
      
      
                    <cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom">
                    </cc1:CalendarExtender>
                   <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" 
                             Text="Show Report" ondirectclick="Button1_DirectClick1">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                         
                    </div>
                       
                </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
            OnRefreshData="Store1_RefreshData"
            >
            <Reader>
            <ext:JsonReader IDProperty="ID">
                    <Fields>
                      <ext:RecordField Name="ID" />
                     
                         <ext:RecordField Name="ReqNo" />
                          <ext:RecordField Name="Orders" />
                           <ext:RecordField Name="ReqProdQty" />
                           
                        <ext:RecordField Name="RunningTime" />
                         <ext:RecordField Name="Fabric" />
                         <ext:RecordField Name="GM" />
                         <ext:RecordField Name="GSM" />
                        
                          <ext:RecordField Name="MCNo" />
                         
                         <ext:RecordField Name="MCStatus" />
                         <ext:RecordField Name="Fabricgroup" />
                          <ext:RecordField Name="AssignDate" />
                          <ext:RecordField Name="StopDate" />
                          <ext:RecordField Name="BookingForLab" />
                          <ext:RecordField Name="IssueToProduction" />
                          <ext:RecordField Name="ProdAsOnDate" />
                          <ext:RecordField Name="ProdToday" />
                           <ext:RecordField Name="DueProd" />
                           
                           <ext:RecordField Name="ClosingBalance" />
                                   <ext:RecordField Name="Location" />
                                           <ext:RecordField Name="Notes" />
                        
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Production Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            
                           <ext:Column ColumnID="ID" Header="ID"  DataIndex="ID" Sortable="true" />
                                <ext:Column ColumnID="ReqNo" Header="ReqNo"  DataIndex="ReqNo" Sortable="true" />
                            <ext:Column ColumnID="Orders" Header="Orders"  DataIndex="Orders" Sortable="true" />
                            <ext:Column ColumnID="Fabricgroup" Header="Fabricgroup"  DataIndex="Fabricgroup" Sortable="true" />
                            <ext:Column ColumnID="Fabric" Header="Fabric"  DataIndex="Fabric" Sortable="true" />
                                                        
                            <ext:Column ColumnID="GM" Header="GM"  DataIndex="GM" Sortable="true" />
                            <ext:Column ColumnID="GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                            <ext:Column ColumnID="ReqProdQty" Header="ReqProdQty"  DataIndex="ReqProdQty" Sortable="true" />
                            <ext:Column ColumnID="MCNo" Header="MCNo"  DataIndex="MCNo" Sortable="true" />                            
                            <ext:Column ColumnID="MCStatus" Header="MCStatus"  DataIndex="MCStatus" Sortable="true" />                            
                            <ext:Column ColumnID="AssignDate" Header="AssignDate"  DataIndex="AssignDate" Sortable="true" />
                            <ext:Column ColumnID="StopDate" Header="StopDate"  DataIndex="StopDate" Sortable="true" />
                            <ext:Column ColumnID="ProdAsOnDate" Header="ProdAsOnDate"  DataIndex="ProdAsOnDate" Sortable="true" />
                            <ext:Column ColumnID="ProdToday" Header="ProdToday"  DataIndex="ProdToday" Sortable="true" />
                            <ext:Column ColumnID="BookingForLab" Header="BookingForLab"  DataIndex="BookingForLab" Sortable="true" />
                            <ext:Column ColumnID="DueProd" Header="DueProd"  DataIndex="DueProd" Sortable="true" />
                            <ext:Column ColumnID="IssueToProduction" Header="IssueToProduction"  DataIndex="IssueToProduction" Sortable="true" />                          
                                                                                    
                            <ext:Column ColumnID="ClosingBalance" Header="ClosingBalance"  DataIndex="ClosingBalance" Sortable="true" />
                                                                                     
                            <ext:Column ColumnID="Notes" Header="Notes"  DataIndex="Notes" Sortable="true" />
                            <ext:Column ColumnID="RunningTime" Header="RunningTime"  DataIndex="RunningTime" Sortable="true" />
                            <ext:Column ColumnID="Location" Header="Location"  DataIndex="Location" Sortable="true" />
                         
                                                   
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                                  <ext:NumericFilter DataIndex="ID" />
                                <ext:StringFilter DataIndex="ReqNo" />
                                <ext:StringFilter DataIndex="Orders" />
                                <ext:StringFilter DataIndex="ReqProdQty" />
                                <ext:StringFilter DataIndex="RunningTime" />
                                <ext:StringFilter DataIndex="Fabric" />
                                <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="GM" />
                                 
                                 <ext:StringFilter DataIndex="MCNo" />
                                
                                <ext:StringFilter DataIndex="MCStatus" />
                                <ext:StringFilter DataIndex="Fabricgroup" />
                                 <ext:DateFilter DataIndex="AssignDate" />
                                  <ext:DateFilter DataIndex="StopDate" />
                                   <ext:NumericFilter DataIndex="BookingForLab" />
                                    <ext:NumericFilter DataIndex="IssueToProduction" />
                                    
                                     <ext:DateFilter DataIndex="ProdAsOnDate" />
                                      <ext:NumericFilter DataIndex="ProdToday" />
                                       <ext:NumericFilter DataIndex="DueProd" />
                                       
                                        <ext:NumericFilter DataIndex="ClosingBalance" />
                                       <ext:StringFilter DataIndex="Location" />
                             <ext:StringFilter DataIndex="Notes" />
                                 
     
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>
