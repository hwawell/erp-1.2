﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class KNIT_GreyIssue : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadOrder();
                LoadFabricType();
                LoadMCGrid();
            }
        }
        private void loadOrder()
        {
            ddlOrder.DataSource = new ProcessCard_CL().GetAlOrder();
            ddlOrder.DataValueField = "PINO";
            ddlOrder.DataTextField = "PINO";
            ddlOrder.DataBind();
            ddlOrder.Items.Insert(0, "--Select Order No--");

        }
        private void LoadFabricType()
        {


            ddlFG.DataSource = new Fabric_CL().GetAllFabricGroup();
            ddlFG.DataValueField = "SL";
            ddlFG.DataTextField = "FGroupName";
            ddlFG.DataBind();

        }
        private void LoadOrderProductDesc()
        {
            ddlOP.DataSource = new Order_DL().GetOrderProductDesc(ddlOrder.SelectedItem.Text);
            ddlOP.DataValueField = "OPID";
            ddlOP.DataTextField = "DESC";

            ddlOP.DataBind();
            ddlOP.Items.Add("--Select--");
            ddlOP.Text = "--Select--";
        }
        private void LoadFabric()
        {
            ddlFabric.DataSource = new Fabric_CL().GetAlFabricbyGRoup(Convert.ToInt32(ddlFG.SelectedValue));
            ddlFabric.DataValueField = "Fid";
            ddlFabric.DataTextField = "fab";
            ddlFabric.DataBind();
        }

        private void LoadMCGrid()
        {




            ddlMCNo.DataSource = new KNIT_all_operation().GetAlMCNo();
            ddlMCNo.DataValueField = "MCNo";
            ddlMCNo.DataTextField = "MCNo";
            ddlMCNo.DataBind();
        

        }
        private void LoadOrderproductDetails()
        {
            try
            {

                ddlColor.DataSource = new Order_DL().GetOrderProductColorList(Convert.ToInt32(ddlOP.SelectedValue.ToString()));
                ddlColor.DataValueField = "OPDID";
                ddlColor.DataTextField = "Color";
                ddlColor.DataBind();
                ddlColor.Items.Add("--Select--");
                ddlColor.Text = "--Select--";


               
            }
            catch
            {
            }
        }

        protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadOrderProductDesc();
        }

        protected void ddlOP_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOP.SelectedItem.Text != "--Select--")
            {


                LoadOrderproductDetails();
            }
        }

        protected void ddlFG_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadFabric();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            EGreyFabricIssue obj = new EGreyFabricIssue();
            obj.PINO = ddlOrder.SelectedValue.ToString();
            obj.OPDID = Convert.ToInt32(ddlColor.SelectedValue.ToString());
            
            obj.IssueQty = Common.GetDouble(txtIssueQty.Text);
            obj.FabricID = Convert.ToInt64(ddlFabric.SelectedValue.ToString());
            obj.MCNo = ddlMCNo.Text;
            obj.GM = txtGM.Text;

            obj.GSM = txtGSM.Text;
            obj.IssueDate = Common.GetDate(txtIssueDate.Text);
            obj.EntryUserID = USession.SUserID;
            obj.IsActive = true;

            if (IsValidEntry() == true)
            {
                txtIssueQty.Text = string.Empty;
               
                txtGM.Text = string.Empty;
                txtGSM.Text = string.Empty;
                txtIssueQty.Text = string.Empty;
                lblStatus.Text = new KNIT_all_operation().SaveGreyBooking(obj);
            }
       
        }
        private bool IsValidEntry()
        {
            bool res;

            res = true;

            if (ddlColor.Text == "--Select--" || Common.GetNumber(ddlColor.SelectedValue) < 1)
            {
                lblStatus.Text = "Select a Product Color !!!";

                res = false;
            }


            if (txtGSM.Text.Length < 1)
            {
                lblStatus.Text = "Plz Give valid GSM !!!";

                res= false;
            }

          

            if (ddlMCNo.Text =="--Select--")
            {
                lblStatus.Text = "Select any Machine No !!!";

                res = false;
            }

            if (ddlFabric.Text == "--Select--" || Common.GetNumber(ddlFabric.SelectedValue)<1)
            {
                lblStatus.Text = "Select a Fabric(Knit) !!!";

                res = false;
            }

            if ( Common.GetDouble(txtIssueQty.Text) ==0)
            {
                lblStatus.Text = "Issued Qty is not given !!!";

                res = false;
            }
            return res;
        }
        protected void btnOperation_Click(object sender, EventArgs e)
        {
             Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            Int64 ID =Common.GetNumber( row.Cells[1].Text);
            if (ID > 0)
            {
                new KNIT_all_operation().DeleteGreyBooking(ID, USession.SUserID);
                LoadGrid();
            }

           
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            LoadGrid();
        }
        private void LoadGrid()
        {
            gvData.DataSource = new KNIT_all_operation().GetGreyIssue(ddlOrder.SelectedValue.ToString(), Common.GetNumber(ddlOP.SelectedValue.ToString()), Common.GetNumber(ddlColor.SelectedValue.ToString()));
            gvData.DataBind();
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtIssueQty.Text = string.Empty;
            
            txtGM.Text = string.Empty;
            txtGSM.Text = string.Empty;
            
        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[1].Visible = false;
                // e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
    }
}
