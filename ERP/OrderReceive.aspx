<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="OrderReceive.aspx.cs" Inherits="ERP.WebForm34" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
                       
    </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
    <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
    function expandcollapse(obj,row)
    {
        var div = document.getElementById(obj);
        var img = document.getElementById('img' + obj);
        
        if (div.style.display == "none")
        {
            div.style.display = "block";
            if (row == 'alt')
            {
                img.src = "minus.gif";
            }
            else
            {
                img.src = "minus.gif";
            }
            img.alt = "Close to view other Customers";
        }
        else
        {
            div.style.display = "none";
            if (row == 'alt')
            {
                img.src = "plus.gif";
            }
            else
            {
                img.src = "plus.gif";
            }
            img.alt = "Expand to show Orders";
        }
    } 
    </script>
    <style type="text/css">
        .style10
        {
        }
        .style12
        {
        }
        .style15
        {
            height: 25px;
        }
        .style17
        {
            height: 25px;
        }
        .style18
        {
            width: 209px;
            height: 25px;
        }
        .style19
        {
            width: 442px;
        }
        .style20
        {
            width: 442px;
            height: 25px;
        }
        .style22
        {
            width: 162px;
            height: 25px;
        }
        </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <div class="grid" >
            <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>Order Details</h2></center>
                </div></div></div>
                     <div class="mid-outer"><div class="mid-inner">
                         <table style="width: 100%; height: 664px;">
                             <tr>
                                 <td>
                                     &nbsp;
                                 </td>
                                 <td align="center">
                              
                                     <asp:RadioButtonList ID="rdoList" runat="server" AutoPostBack="True" 
                                         RepeatDirection="Horizontal" 
                                         onselectedindexchanged="rdoList_SelectedIndexChanged1" Width="386px">
                                         <asp:ListItem Selected="True">New Order</asp:ListItem>
                                         <asp:ListItem>Revised Order</asp:ListItem>
                                         <asp:ListItem>Received Order</asp:ListItem>
                                     </asp:RadioButtonList>
                                        Search<asp:DropDownList ID="ddlSearch" runat="server" Height="22px" 
                                            Width="140px">
                                            <asp:ListItem>Customer</asp:ListItem>
                                            <asp:ListItem>Order</asp:ListItem>
                                            <asp:ListItem>Approval Date</asp:ListItem>
                                        </asp:DropDownList>
                                        Text<asp:TextBox ID="txtSearchText" runat="server" Height="20px"></asp:TextBox>
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                            onclick="btnSearch_Click" />
                                    
                                     
                                     </td>
                                 <td>
                                     &nbsp;
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     &nbsp;
                                 </td>
                                 <td valign="top">
                                    <div style="height: 400px; width: 853px;">
                                  
                            <asp:GridView 
                                ID="gvProducts" runat="server" 
                                 AutoGenerateColumns="False" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                onpageindexchanging="gvProducts_PageIndexChanging" PageSize="15" 
                                AllowPaging="True" Width="831px">
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="�" LastPageText="�" />      
                                <Columns>
                                   
                                    <asp:BoundField 
                                        HeaderText="Order No" DataField="PINo" 
                                        SortExpression="PINo" >
                                    
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                 
                                    <asp:BoundField 
                                        HeaderText="Customer" DataField="CustomerName" 
                                        SortExpression="CustomerName">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="Order Type" DataField="OrderTypeName" 
                                        SortExpression="OrderTypeName">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                       <asp:BoundField 
                                        HeaderText="Merchandiser" DataField="EntryID" 
                                        SortExpression="EntryID">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="ApprovalDate" HeaderText="ApprovalDate">
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            
                                           <asp:Button ID="btnView" runat="server" Text="View" onclick="btnView_Click" 
                                                Width="75px"></asp:Button>
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            
                                           <asp:Button ID="btnFinish" runat="server" Text="Print" onclick="btnFinish_Click" 
                                                Width="75px"></asp:Button>
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                    
                                    
                                    <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" 
                                                ImageUrl="~/Images/btn_delete.gif" onclick="btnDelete_Click" 
                                                Height="16px"
                                            onclientclick="javascript:return confirm('Are you sure you want to delete this row?');" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                 
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                            <asp:Button ID="btnHiddenProduct" runat="Server" Style="display:none"  />
                                 
                                 
                                  <div>
                                
                               
                                        <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                                            TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                                             PopupDragHandleControlID="ProductCaption" 
                                            Drag="True" BackgroundCssClass="modalBackground" >
                                        </cc1:ModalPopupExtender>
                                        <asp:Panel ID="pnlDataEntry" runat="server" style="display:none" CssClass="modalBox"  
                                            BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                                            Width="850px" Height="600px" BorderWidth="2px">
                                            <center>
                                        <asp:Panel ID="ProductCaption" runat="server"   
                                            Style="margin-bottom: 10px; cursor: hand;" 
                                            BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                            Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                            Order Information</asp:Panel></center>
                                        <cc1:CollapsiblePanelExtender ID="ProductCaption_CollapsiblePanelExtender" 
                                            runat="server" Enabled="True" TargetControlID="ProductCaption">
                                        </cc1:CollapsiblePanelExtender>
                                        
                                         <div>
                             <div>
                              
                           
                             <table style="width: 99%; font-family: Cambria; height: 86px;" cellpadding="0" 
                                 cellspacing="0">
                                 <tr>
                                     <td class="style19" valign="top" align="right">
                                         &nbsp; </td>
                                     <td align="left" class="style10">
                                         &nbsp;
                                     </td>
                                     <td class="style12" valign="top" align="left" colspan="4">
                                         &nbsp;</td>
                                     <td>
                                         &nbsp;</td>
                                 </tr>
                                 <tr>
                                     <td class="style20" valign="top" align="right">
                                        
                                         Order No :</td>
                                     <td class="style15" valign="top" align="left">
                                         <asp:TextBox ID="txtOrderNo" runat="server" ReadOnly="True"></asp:TextBox>
                                     </td>
                                     <td class="style17" valign="top" align="left" colspan="5">
                                         Customer :<asp:Label ID="lblVarifiedCustomer" runat="server" 
                                             Font-Names="Cambria" Font-Size="Smaller" Font-Bold="True" ForeColor="Red"></asp:Label>
                                         <asp:DropDownList ID="cboCustomer" runat="server" Height="22px" Width="200px">
                                         </asp:DropDownList>
                                        
                                        
                                     </td>
                                 </tr>
                                 <tr>
                                     <td class="style20" valign="top" align="right">
                                        
                                         Order Type :</td>
                                     <td class="style15" valign="top" align="left">
                                         <asp:TextBox ID="txtOrderType" runat="server" ReadOnly="True"></asp:TextBox>
                                     </td>
                                     <td class="style17" valign="top" align="right">
                                         Approve For :</td>
                                     <td class="style17" valign="top" align="left">
                                         <asp:TextBox ID="txtApproveFor" runat="server" Width="194px" ReadOnly="True"></asp:TextBox>
                                         </td>
                                     <td valign="top" class="style22">
                                         
                                     </td>
                                     <td valign="top" class="style18">
                                         
                                     </td>
                                     <td class="style15">
                                         </td>
                                 </tr>
                                 <tr>
                                     <td class="style19" align="right">
                                         
                                         Notes:</td>
                                     <td class="style10" align="left" colspan="4">
                                         
                                         <asp:TextBox ID="txtNotes" runat="server" Width="599px" ReadOnly="True"></asp:TextBox>
                                     </td>
                                     <td class="style10" align="left">
                                         
                                         &nbsp;</td>
                                     <td>
                                         &nbsp;</td>
                                 </tr>
                                 <tr>
                                     <td class="style19" align="right">
                                         
                                         Revised Note</td>
                                     <td class="style10" align="left" colspan="4">
                                         
                                         <asp:TextBox ID="txtRevisedNotes" runat="server" Width="599px" ReadOnly="True"></asp:TextBox>
                                     </td>
                                     <td class="style10" align="left">
                                         
                                         &nbsp;</td>
                                     <td>
                                         &nbsp;</td>
                                 </tr>
                             </table>
                           
                             </div>
                             <div align="center" style="overflow: auto; height: 400px">
                           
                           
                         
                            <asp:GridView ID="GridView1" BackColor="#F1F1F1"     AutoGenerateColumns="False"
                            style="Z-INDEX: 101; LEFT: 3px; " ShowFooter=True Font-Size=Small
                            Font-Names="Verdana" runat="server" GridLines=None OnRowDataBound="GridView1_RowDataBound" 
                            OnRowCommand = "GridView1_RowCommand" OnRowUpdating = "GridView1_RowUpdating" BorderStyle=Outset
                            OnRowDeleting = "GridView1_RowDeleting" OnRowDeleted = "GridView1_RowDeleted"
                            OnRowUpdated = "GridView1_RowUpdated" onrowediting="GridView1_RowEditing">
                            <RowStyle BackColor="Gainsboro" />
                            <AlternatingRowStyle BackColor="White" />
                            <HeaderStyle BackColor="#0083C1" ForeColor="White"/>
                            <FooterStyle BackColor="White" />
                             <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:expandcollapse('div<%# Eval("OPID") %>', 'one');">
                                                <img id="imgdiv<%# Eval("OPID") %>" 
                                                alt="Click to show/hide Orders for Record: <%# Eval("OPID") %>"  width="9px" 
                                                border="0" src="plus.gif"/>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="  " SortExpression="OPID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPIID" Text='<%# Eval("OPID") %>' runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                      
                                        
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Product Description" SortExpression="Description">
                                        <ItemTemplate>
                                          <asp:Label ID="lblDesc" Text='<%# Eval("Description") %>' Width="320px" runat="server" ></asp:Label>
                                         
                                            <br />
                                         
                                          <asp:Label ID="lblNotes" Text='<%# Eval("Notes") %>' Width="320px" runat="server" ></asp:Label>
                                          
                                        </ItemTemplate>
                                        
                                       
                                    </asp:TemplateField>
                                    
                                    <asp:BoundField DataField="Weight" HeaderText="Weight"  >
                                        <ItemStyle Width="70px" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Width" HeaderText="Width" >
                                    <ItemStyle Width="70px" HorizontalAlign="Center"/>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Unit" HeaderText="Unit" >
                                    <ItemStyle Width="70px" HorizontalAlign="Center"/>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UPDown" HeaderText="UPDown" >
                                    <ItemStyle Width="70px" HorizontalAlign="Center"/>
                                    </asp:BoundField>
                                   <%-- <asp:CommandField HeaderText="Receive" ShowEditButton="True" />--%>
                    			 
                    			    
			                        <asp:TemplateField>
			                            <ItemTemplate>
			                                <tr>
                                                <td colspan="100%">
                                                    <div id="div<%# Eval("OPID") %>" 
                                                        
                                                        style="display:none; position:relative;left:15px;OVERFLOW: auto;WIDTH:97%; top: 0px; height: 159px;" >
                                                        <asp:GridView ID="GridView2" AllowPaging="False" AllowSorting="False" 
                                                                                    BackColor="White" Width="100%" Font-Size="X-Small"
                                                                                    AutoGenerateColumns="false" Font-Names="Verdana" runat="server" 
                                                                                    DataKeyNames="OPID" ShowFooter="true"
                                                                                    
                                                                                    OnRowCommand = "GridView2_RowCommand" 
                                                                                   GridLines="None"
                                                                                  
                                                                                     OnRowDataBound = "GridView2_RowDataBound"
                                                                                 
                                                                                    BorderStyle="Double" BorderColor="#0083C1" 
                                                                                     Height="47px">
                                                                                    <RowStyle BackColor="Gainsboro" />
                                                                                    <AlternatingRowStyle BackColor="White" />
                                                                                    <HeaderStyle BackColor="#0083C1" ForeColor="White"/>
                                                                                    <FooterStyle BackColor="White" />
                                                                                    <Columns>
                                                                                         <asp:BoundField DataField="BaseColor" HeaderText="BaseColor"  >
                                                                                                <ItemStyle Width="70px" HorizontalAlign="Center" />
                                                                                            </asp:BoundField>
                                                                                         <asp:BoundField DataField="Color" HeaderText="Color"   >
                                                                                                <ItemStyle Width="70px" HorizontalAlign="Center" />
                                                                                            </asp:BoundField>
                                                                                         <asp:BoundField DataField="ColorNo" HeaderText="Color NO"  >
                                                                                                <ItemStyle Width="70px" HorizontalAlign="Center" />
                                                                                            </asp:BoundField>
                                                                                         <asp:BoundField DataField="Idesc" HeaderText="L/D Desc"  >
                                                                                                <ItemStyle Width="70px" HorizontalAlign="Center" />
                                                                                            </asp:BoundField>
                                                                                         <asp:BoundField DataField="Qty" HeaderText="Qty" >
                                                                                                <ItemStyle Width="70px" HorizontalAlign="Center" />
                                                                                            </asp:BoundField>
                                                                                         <asp:BoundField DataField="Size" HeaderText="Size" >
                                                                                                <ItemStyle Width="70px" HorizontalAlign="Center" />
                                                                                            </asp:BoundField>
                                                                                         
                                                                                    </Columns>
                                                                               </asp:GridView>
                                                                            </div>
                                                                         </td>
                                                                    </tr>
			                                                    </ItemTemplate>			       
			                                                </asp:TemplateField>			    
                    			                        
			                      </Columns>
                                </asp:GridView>
                               
                         </div>
                         <div align="center">
                              <asp:Button ID="btnReceive" runat="server" Text="Receive Order" 
                                  onclick="btnReceive_Click" />
                             
                             <asp:Button ID="Button1" runat="server" Text="Exit" />
                            
                         </div>
                         </div> 
                                        </asp:Panel>
                                      
                              
                         
                         </div>
                    
    
                                    </div>
                                 </td>
                                 <td>
                                     &nbsp;
                                 </td>
                             </tr>
                             <tr>
                                 <td>
                                     &nbsp;</td>
                                 <td>
                                   
                                   
                                   </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>
                         </table>
                         
                        
                        
                    </div>
                 </div>
               
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>      
    </div>
</asp:Content>
