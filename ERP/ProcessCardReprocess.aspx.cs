﻿using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
using System;

namespace ERP
{
    
    public partial class ProcessCardReprocess : System.Web.UI.Page
    {
        List<EOrderProductDetails> liColorobj = new List<EOrderProductDetails>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //if (new Utilities_DL().CheckSecurity("502", USession.SUserID) == true)
                //{
                
                //  loadGridData();
                loadOrder();
                loadFabricGroup();
                GenerateProcessCardNo();
                //}
                //else
                //{
                //    this.rblNormal.Items.FindByText("Active").Selected = true;
                //    //  loadGridData();
                //    loadOrder();
                //    loadFabricGroup();

                //    //Response.Redirect("Home.aspx");
                //}
            }
        }
        private void loadFabricGroup()
        {


            

        }
        private void loadFabric()
        {
          
        }
        private void loadOrder()
        {

            EOrderRollQty obj = new EOrderRollQty();
            obj.RollNo = "";
            obj.Select = false;
            obj.Qty = 0;

            List<EOrderRollQty> li = new List<EOrderRollQty>();
            li.Add(obj);

            

        
            gvRoll.DataSource = li;
            gvRoll.DataBind();

        }
        private void loadGridData()
        {


            //gvProducts.Columns[7].Visible = true;
            //gvProducts.Columns[8].Visible = false;

            //if (txtLastPCard.Text.Length > 2)
            //{
            //    gvProducts.DataSource = new ProcessCard_CL().GetActiveSinglePCard(txtLastPCard.Text.ToUpper());
            //}
            //else
            //{
            //    gvProducts.DataSource = new ProcessCard_CL().GetActivePCard();
            //}

            //gvProducts.DataBind();

        }
       

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
           
            //txtWeight.Text = (row.Cells[5].Text);

            //txtPerKG.Text = (row.Cells[8].Text);
            //txtUnit.Text = (row.Cells[9].Text);




            updPanel.Update();

            /////mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new ProcessCard_CL().DeleteActivePCard(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
           ///////// gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                //e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            ProcessCard c = new ProcessCard();

            if (new ProcessCard_CL().CheckProcessCard(txtProcessCard.Text.Trim()) == false && (CheckEntryData() == true))
            {
                ////if (checkDecimal(txtTotRoll.Text) == false)
                ////{
                ////  //  txtTotRoll.Text = "0";
                ////}
                c.SL = Convert.ToInt64(hdnCode.Value);
               




                c.EntryID = USession.SUserID;
                if (c.PCardNo.Length > 2)
                {
                    new ProcessCard_CL().Save(c);
                }
                loadGridData();
            }
            else
                lblMsg.Text = "Process Card is already Exist";
            // ScriptManager.RegisterClientScriptBlock(Page,Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Process Card is already Exists');</script>",false);

            //loadGridData();
            // updPanel.Update();
            //// mpeProduct.Hide();

        }
        private bool CheckEntryData()
        {
            string str = "";
            bool check = true;

            if (txtNewProcessCard.Text.Length < 2 || txtProcessCard.Text.Length < 2)
            {
                check = false;
                str = "Process Card not Available to Reprocess";
            }
            if (check == false)
            {
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('"+ str +"');</script>", false);
                lblMsg.Text = str;

            }
            return check;
        }
        private bool checkDecimal(string s)
        {

            try
            {
                decimal v;
                v = Convert.ToDecimal(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private void SaveData()
        {


            try
            {
                lblMsg.ForeColor = System.Drawing.Color.Red;
                if (CheckEntryData() == true)
                {

                    if ( new ProcessCard_CL().CheckProcessCard(txtNewProcessCard.Text.Trim()) == false)
                    {



                        List<EOrderRollQty> li = new List<EOrderRollQty>();
                        EOrderRollQty obj;
                        foreach (GridViewRow ob in gvRoll.Rows)
                        {
                            CheckBox s = (CheckBox)ob.Cells[0].FindControl("chkStatus");
                            if (s.Checked == true)
                            {
                                obj = new EOrderRollQty();
                                obj.SL = Convert.ToInt32(ob.Cells[3].Text);
                                li.Add(obj);

                            }
                        }
                        bool res = new ProcessCard_CL().INS_UPD_REPROCESS_PCARD(txtNewProcessCard.Text, txtProcessCard.Text,ddlReProcessType.SelectedItem.Text,txtReprocessDes.Text, ConvertToDoucble(txtNetQty.Text),txtLotNo.Text,txtNotes.Text, li);
                        if (res == true)
                        {
                            txtProcessCard.Text = "";
                           
                            GenerateProcessCardNo();
                            Clear();
                            lblMsg.Text = "Process Card  Saved Successfully";
                        }
                        else
                        {
                            lblMsg.Text = "Process Card is not Saved";
                        }
                    }
                    else
                    {
                        lblMsg.Text = "Process Card is already Exist";

                    }
                }

                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Process Card is already Exists');</script>", false);
                GenerateProcessCardNo();
                UpEntry.Update();

            }
            catch (Exception)
            {
                lblMsg.Text = "Process Card Not saved";
                UpEntry.Update();
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {




            SaveData();



            /////mpeProduct.Show();


        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
           
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

            ClearALl();

        }

        private void ClearALl()
        {


            try
            {
                GenerateProcessCardNo();
               
                hdnCode.Value = "0";


                GenerateProcessCardNo();
                UpEntry.Update();
            }
            catch
            {

            }
        }
        protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        private void LoadOrderProductDesc()
        {
           
        }
        protected void ddlDesc_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        private void LoadOrderproductDetails()
        {
           
        }
        protected void btnload_Click(object sender, EventArgs e)
        {
            
                loadGridData();

            
        }

        protected void txtCalculation1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {


            string jscript = "";

            string ReportName = "";
            string pr = "PROCESS_CARD";

            ReportName = "rptProcesssCardNew";


            //string Fdate = ddlOrder.SelectedValue.ToString();


            string pCard = txtLastPCard.Text;
            string RTYpe = "Process Card Print"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&PCARD=" + pCard + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";
            //string script = "var windowObject = window.self; windowObject.opener = window.self; windowObject.close();";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Close Window", script, true);
            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm11), "ScriptFunction", jscript);
        }

        protected void gvProducts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddFGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                LoadFabric();

            }
        }
        private void LoadFabric()
        {
            try
            {
                
            }
            catch
            {
            }
        }
        protected void txtGSM_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnEditPCard_Click(object sender, EventArgs e)
        {
            EPcardInfo obj = new ProcessCard_CL().GetSinglePCardInfo(txtLastPCard.Text);
            if (obj != null)
            {
                hdnCode.Value = obj.ID.ToString();
                txtProcessCard.Text = obj.PCardNo;





                UpEntry.Update();

            }
        }

        protected void ddlColor_SelectedIndexChanged(object sender, EventArgs e)
        {

           



        }
        private void BalanceQty()
        {
            

        }
        private double ConvertToDoucble(string val)
        {
            double res = 0;
            try
            {
                res = Convert.ToDouble(val);
            }
            catch
            {
                res = 0;
            }
            return res;
        }
        private void GenerateProcessCardNo()
        {
            txtNewProcessCard.Text = new ProcessCard_CL().GenearteProcessCardNo();

        }
        protected void txtMCCapacity_TextChanged(object sender, EventArgs e)
        {

            BalanceQty();
            

        }

        protected void txtExtraPer_TextChanged(object sender, EventArgs e)
        {

            BalanceQty();
           

        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            string jscript = "";

            string ReportName = "";
            string pr = "PROCESS_CARD";

            ReportName = "rptProcesssCardNew";


            //string Fdate = ddlOrder.SelectedValue.ToString();


            string pCard = txtLastPCard.Text;
            string RTYpe = "Process Card Print"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&PCARD=" + pCard + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";
            //string script = "var windowObject = window.self; windowObject.opener = window.self; windowObject.close();";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Close Window", script, true);
            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm11), "ScriptFunction", jscript);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            EOrderPcard obj = new ProcessCard_CL().GetOrderProductByPcard(txtProcessCard.Text);
            if (obj != null)
            {
                txtOrder.Text = obj.PINO;
                txtWeights.Text = obj.Weight;
                txtWidths.Text = obj.Width;
                txtGSM.Text = obj.GSM;
                txtOrderProduct.Text = obj.OrderProduct;
                txtColor.Text = obj.Color;
                txtIssuedQty.Text = obj.IssuedQty.ToString();
                List<EOrderRollQty> obj1 = new ProcessCard_CL().GetPackingProductByPcard(txtProcessCard.Text);

                gvRoll.DataSource = obj1;
                gvRoll.DataBind();


                UpEntry.Update();
            }
            else
            {
                Clear();
            }
        }
        private void Clear()
        {
            txtOrder.Text = string.Empty;
            txtWeights.Text = string.Empty;
            txtWidths.Text = string.Empty;
            txtOrderProduct.Text = string.Empty;
            txtColor.Text = string.Empty;
            txtGSM.Text = string.Empty;
            txtIssuedQty.Text = string.Empty;
            EOrderRollQty obj1 = new EOrderRollQty();
            obj1.RollNo = "";
            obj1.Select = false;
            obj1.Qty = 0;

            List<EOrderRollQty> li = new List<EOrderRollQty>();
            li.Add(obj1);




            gvRoll.DataSource = li;
        }
        public void chkStatus_OnCheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkStatus = (CheckBox)sender;
            GridViewRow row = (GridViewRow)chkStatus.NamingContainer;

            if (chkStatus.Checked == true)
            {

                txtRollQty.Text = (ConvertToDoucble(txtRollQty.Text) + ConvertToDoucble(row.Cells[2].Text)).ToString();

            }
            else
            {
                txtRollQty.Text = (ConvertToDoucble(txtRollQty.Text) - ConvertToDoucble(row.Cells[2].Text)).ToString();
            }

            calculateall();
            upRollQty.Update();
            upNet.Update();
        }
        private void CalculateROll()
        {
            //double qty;
            //for (int i = 0; i < gvRoll.Rows.Count; i++)
            //{
                

            //}
        }

        protected void txtNetQty_TextChanged(object sender, EventArgs e)
        {
           
        }

        protected void txtWithoutPackQty_TextChanged(object sender, EventArgs e)
        {
            calculateall();
            txtNotes.Focus();
            upRollQty.Update();
            upNet.Update();

        }
        void calculateall()
        {
            double val = ConvertToDoucble(txtWithoutPackQty.Text);

            txtNetQty.Text = (ConvertToDoucble(txtRollQty.Text) + val).ToString();

            if (ConvertToDoucble(txtNetQty.Text)>ConvertToDoucble(txtIssuedQty.Text))
            {
                txtWithoutPackQty.Text="0";
                txtNetQty.Text = (ConvertToDoucble(txtRollQty.Text) ).ToString();
            }
           
        }

        protected void gvRoll_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[3].Visible = false;
                //e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
    }
}
