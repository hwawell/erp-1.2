﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;


namespace ERP
{
    public partial class YarnReceiveFromLC : System.Web.UI.Page
    {
         
         protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              

                //if (new Utilities_DL().CheckSecurity("503", USession.SUserID) == true)
                //{


                LoadData();
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
         private void LoadData()
         {
             ddlUnit.DataSource = new Yarn_DL().GetAllUnits();
             ddlUnit.DataValueField = "UNIT1";
             ddlUnit.DataTextField = "UNIT1";
             ddlUnit.DataBind();
         }
       
        private void loadGridData()
        {


           
            gvProducts.DataSource = new Yarn_DL().GetYarnLCBooking(Common.GetNumber(txtFrom.Text), Common.GetNumber(txtTo.Text),txtSearchLC.Text.Trim());
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            lblMsg.Text = string.Empty;
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnValue.Value = row.Cells[0].Text;
            if (row.Cells[2].Text != null && row.Cells[2].Text != "&nbsp;")
            {
                hdnYarnID.Value = row.Cells[1].Text;

                txtYarn.Text = row.Cells[3].Text;

            }
            txtSupplier.Text = row.Cells[4].Text;
            txtCountry.Text = row.Cells[5].Text;
            txtLCNO.Text = row.Cells[6].Text;
            txtTotQty.Text = row.Cells[7].Text;
            txtRcvQty.Text = row.Cells[8].Text;
            txtPDate.Text = row.Cells[9].Text;
            
            //txtYarn.Text = (row.Cells[2].Text);
            //txtYarnDesc.Text = (row.Cells[3].Text.ToString().Trim());
            loadrcvGRID(Convert.ToInt64(row.Cells[0].Text));

            UpEntry.Update();

            mpeProduct.Show();
        }
        protected void gvProducts1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
           // loadGridData();
        }

        protected void gvProducts1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                //e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }

        private void loadrcvGRID(Int64 ID)
        {

            gvProducts1.DataSource = new Yarn_DL().GetActivePurchaseRecordBYLCNO(ID);
            gvProducts1.DataBind();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            if (Convert.ToDecimal(row.Cells[8].Text) == 0)
            {
                new Yarn_DL().DeleteYarnLCBooking(Convert.ToInt64(row.Cells[0].Text), USession.SUserID);

                loadGridData();
            }
        }

        protected void btnDelete1_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            if (Convert.ToDecimal(row.Cells[0].Text) > 0)
            {
                //new Yarn_DL().DeleteYarnLCBooking(Convert.ToInt64(row.Cells[0].Text), USession.SUserID);
                new Yarn_DL().PurchaseDeleteActive(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
                loadGridData();
                updPanel.Update();
                mpeProduct.Hide();
            }
        }


        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
      
        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (CheckEntry() == true)
            {

                try
                {
                    YarnPurchase c = new YarnPurchase();
                    c.SL = 0;
                    c.YID = Convert.ToInt64(hdnYarnID.Value);

                    c.Status = "RECEIVE";// ddlType.Text.ToString();
                    c.CTNQty = Convert.ToDecimal(txtQty.Text);
                    c.TotalQty = Convert.ToDecimal(txtReceiveQty.Text);
                    c.PDate = Convert.ToDateTime(txtPDate.Text);
                    c.Supplier = txtSupplier.Text.ToString();
                    c.Country = txtCountry.Text.ToString();
                    c.PurchaseType = "RECEIVE";
                    c.InvoiceNo = txtInvoiceNo.Text.ToString();
                    c.LotNo = txtLOTNO.Text.ToString();
                    c.LCNo = txtLCNO.Text.ToString();
                    c.UNIT = ddlUnit.Text;
                    c.Notes = txtNotes.Text;
                    c.YarnLCBookingID =Convert.ToInt64( hdnValue.Value);

                    c.FromBarcodeID = Convert.ToInt32(txtBarcodeFrom.Text);
                    c.ToBarcodeID = Convert.ToInt32(txtBarcodeTo.Text);


                    c.EntryID = USession.SUserID;


                    if (new Yarn_DL().SavePurchase(c) == true)
                    {

                        loadrcvGRID(Convert.ToInt64(hdnValue.Value));
                        lblMsg.Text = "Saved Successfully";
                    }


                   // txtPDate.Text = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year;
                    Clear();



                }
                catch
                {

                }
                
            }
            UpEntry.Update();
        }


        private bool CheckEntry()
        {


            if (Common.GetDecimal(hdnYarnID.Value) == 0)
            {
                lblMsg.Text = "Select Yarn";
                return false;
            }
            else if (Common.GetNumber(txtBarcodeFrom.Text) == 0)
            {
                lblMsg.Text = "Starting Barcode No is needed";
                return false;
            }
            else if (Common.GetNumber(txtBarcodeTo.Text) == 0)
            {
                lblMsg.Text = "Ending Barcode No is needed";
                return false;
            }
            else if (new Yarn_DL().IsDuplicateBarcode(Common.GetNumber(txtBarcodeFrom.Text),Common.GetNumber(txtBarcodeTo.Text)) == true)
            {
                lblMsg.Text = "Duplicate Barcode,This Barcode Range is duplicated, Please try new ID";
                return false;
            }
            else if (Common.GetNumber(txtBarcodeTo.Text) == 0)
            {
                int tot = (Common.GetNumber(txtBarcodeTo.Text) - Common.GetNumber(txtBarcodeFrom.Text)) + 1;
                if (Common.GetNumber(txtQty.Text) > 0)
                {
                    int tot1 = Common.GetNumber(txtQty.Text);
                    if (tot != tot1)
                    {
                        lblMsg.Text = "Wrong Barcode serial";
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    lblMsg.Text = "Wrong Barcode serial";
                    return false;
                }
             
            }
            else
            {
                return true;
            }
        }
     

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
            lblMsg.Text = "";
            loadGridData();
            updPanel.Update();
            mpeProduct.Hide();
        }

        protected void gvProducts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnload_Click(object sender, EventArgs e)
        {
            loadGridData();
        }
        private void Clear()
        {

            txtPDate.Text = string.Empty;
            txtTotQty.Text = string.Empty;
            txtNotes.Text = string.Empty;
            hdnValue.Value = "0";
            txtTotQty.Text = "";
            txtLCNO.Text = "";
           // txtInvoiceNo.Text = "";
            txtNotes.Text="";
            
            //ddlYarn.Text = "--Select--";
            //ddlYarn.Focus();
            UpEntry.Update();
        }

        protected void ddlYarnGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            
           // hdnCode.Value = "0";
           // txtYarn.Text = string.Empty;
            //txtYarnDesc.Text = string.Empty;
            Clear();
            lblMsg.Text = "";
            updPanel.Update();
            
            mpeProduct.Show();
            //txtYarn.Focus();
        }
    
    }
}
