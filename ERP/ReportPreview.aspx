﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportPreview.aspx.cs" Inherits="ERP.ReportPreview" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
  
    <form id="form1" runat="server">
    <div align="center">
      
         <CR:CrystalReportViewer ID="Report" runat="server" 
             AutoDataBind="True" EnableDatabaseLogonPrompt="False" 
             EnableParameterPrompt="False" Height="1039px" 
             ReportSourceID="CrystalReportSource1" ReuseParameterValuesOnRefresh="True" 
             HasCrystalLogo="False" DisplayGroupTree="False" 
             />
        <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
            <Report FileName="CrystalReport1.rpt">
             </Report>
        </CR:CrystalReportSource>
    </div>
    </form>
</body>
</html>
