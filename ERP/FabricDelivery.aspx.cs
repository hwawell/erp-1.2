﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class WebForm10 : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //if (new Utilities_DL().CheckSecurity("507", USession.SUserID) == true)
                //{
                this.rblNormal.Items.FindByText("Active").Selected = true;
                loadFabricGroup();
                LoadMCGrid();

                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
                
            }
        }
        private void LoadMCGrid()
        {




            ddlMCNo.DataSource = new KNIT_all_operation().GetAlMCNo();
            ddlMCNo.DataValueField = "MCNo";
            ddlMCNo.DataTextField = "MCNo";
            ddlMCNo.DataBind();


        }
        private void loadFabricGroup()
        {


            ddFGroup.DataSource = new Fabric_CL().GetAllFabricGroup();
            ddFGroup.DataValueField = "SL";
            ddFGroup.DataTextField = "FGroupName";
            ddFGroup.DataBind();

        }
        private void loadFabric()
        {
            ddlFabric.DataSource = new Fabric_CL().GetAlFabric();
            ddlFabric.DataValueField = "Fid";
            ddlFabric.DataTextField = "fab";
            ddlFabric.DataBind();
        }
        private void loadGridData()
        {


            gvProducts.Columns[8].Visible = true;
            gvProducts.Columns[9].Visible = true;
            gvProducts.Columns[10].Visible = false;
            gvProducts.DataSource = new KNIT_all_operation().GetKNITGreyIssue();
            gvProducts.DataBind();

        }
        private void loadDeletedData()
        {

            gvProducts.Columns[8].Visible = false;
            gvProducts.Columns[9].Visible = false;
            gvProducts.Columns[10].Visible = true;
            gvProducts.DataSource = new Fabric_CL().GetDeletedDelivery();
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            ddlFabric.Text = (row.Cells[1].Text);
            txtQty.Text = (row.Cells[4].Text);
            txtPCardId.Text = (row.Cells[5].Text);
            txtNoOfRoll.Text = (row.Cells[6].Text);
            txtPDate.Text = row.Cells[7].Text.Replace("12:00:00 AM", "");
            
            

            //if (txtPDate.Text.Length > 3)
            //{
            //    DateTime d = Convert.ToDateTime(txtPDate.Text);
            //    txtPDate.Text = d.Date.ToString();
            //}



            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Fabric_CL().DeliveryDeleteActive(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Fabric_CL().DeliveryDeleteActive(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            EGreyFabricIssueToPCard c = new EGreyFabricIssueToPCard();

            if (checkDecimal(txtQty.Text) == false)
            {
                txtQty.Text = "0";
            }
            c.ID = Convert.ToInt64(hdnCode.Value);
            c.FabricID = Convert.ToInt64(ddlFabric.Text);
            c.GM = txtGM.Text.Trim();
            c.GSM = txtGSM.Text.Trim();
            c.PCardNO = txtPCardId.Text;
            c.MCNo = ddlMCNo.Text;
            c.IssueQty = Convert.ToDouble(txtQty.Text);
            c.IssueDate = Common.GetDate(txtPDate.Text);
            if (checkDecimal(txtNoOfRoll.Text) == false)
            {
                c.RollNo = 0;
            }
            else
            {
                c.RollNo = Convert.ToInt16(txtNoOfRoll.Text);
            }

            c.EntryUserID = USession.SUserID;
            if (c.IssueQty > 0)
            {
                lblStatus.Text = new KNIT_all_operation().SaveKnitIssueToPCard(c);
            }

            loadGridData();
            //loadGridData();
            updPanel.Update();
            mpeProduct.Hide();

        }
        private bool checkDecimal(string s)
        {

            try
            {
                decimal v;
                v = Convert.ToDecimal(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            EGreyFabricIssueToPCard c = new EGreyFabricIssueToPCard();

            if (checkDecimal(txtQty.Text) == false)
            {
                txtQty.Text = "0";
            }
            c.ID = Convert.ToInt64(hdnCode.Value);
            c.FabricID = Convert.ToInt64(ddlFabric.Text);
            c.GM = txtGM.Text.Trim();
            c.GSM = txtGSM.Text.Trim();
            c.PCardNO = txtPCardId.Text;
            c.MCNo = ddlMCNo.Text;
            c.IssueQty = Convert.ToDouble(txtQty.Text);
            c.IssueDate = Common.GetDate(txtPDate.Text);
            if (checkDecimal(txtNoOfRoll.Text) == false)
            {
                c.RollNo = 0;
            }
            else
            {
                c.RollNo = Convert.ToInt16(txtNoOfRoll.Text);
            }

            c.EntryUserID = USession.SUserID;
            if (c.IssueQty > 0)
            {
              lblStatus.Text= new KNIT_all_operation().SaveKnitIssueToPCard(c);
            }

            
            txtQty.Text = "";
            txtPCardId.Text = "";
            updPanel.Update();
            mpeProduct.Show();
            ddlFabric.Focus();

        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            // txtYarn.Text = string.Empty;
            // txtYarnDesc.Text = string.Empty;

            txtQty.Text = "";
            txtPCardId.Text = "";


            mpeProduct.Show();
            ddlFabric.Focus();
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            loadGridData();
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            if (rblNormal.Items.FindByText("Active").Selected == true)
            {
                loadGridData();

            }
            else
            {
                loadDeletedData();
            }
            
        }

        protected void ddFGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                ddlFabric.DataSource = new Fabric_CL().GetAlFabricbyGRoup(Convert.ToInt16(ddFGroup.SelectedValue));
                ddlFabric.DataValueField = "Fid";
                ddlFabric.DataTextField = "fab";
                ddlFabric.DataBind();
                upFa.Update();
                mpeProduct.Show();
            }
        }
       
    }
}
