<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="INV_STORE_RPT.aspx.cs" Inherits="ERP.INV_STORE_RPT" Title="Untitled Page" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>
  

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();
            List<EINV_StoreReport> liProd = new List<EINV_StoreReport>();
            Int64 gid = 0;

            if (ddlYarnGroup.SelectedItem.Text == "All Group")
            {
                gid = 0;
            }
            else
            {
                gid = Convert.ToInt64(ddlYarnGroup.SelectedValue.ToString());
            }




            liProd = new Inventory_DL().GetInventoryStockReport(Convert.ToInt32(Request.QueryString["INVTypeID"]), Convert.ToInt32(gid), txtFrom0.Text, txtFrom1.Text, txtItem.Text);
            if (txtParent.Text.Length > 2)
            {
                liProd = liProd.FindAll(o => (o.ParentName.Trim().ToUpper() == txtParent.Text.Trim().ToUpper()));

            }
            liProd = liProd.FindAll(o => (txtItem.Text.Length < 1 || o.ItemName.Trim().ToUpper() == txtItem.Text.Trim().ToUpper()));

            this.Store1.DataSource = GetStoreReport(liProd);
            this.Store1.DataBind();
           
         
           
            //GridPanel1.ColumnModel.Columns[0].EditorOptions.
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
     private List<EINV_StoreReportDetails> GetStoreReport(List<EINV_StoreReport> li)
     {
         List<EINV_StoreReportDetails> liD = new List<EINV_StoreReportDetails>();

         foreach (EINV_StoreReport emp in li)
         {

             EINV_StoreReportDetails objE = new EINV_StoreReportDetails();
             objE.ItemID = emp.ItemID;
             objE.ParentName = emp.ParentName;
             objE.ItemName = emp.ItemName;

             objE.GroupName = emp.GroupName;
             objE.OpenBal = emp.OpenBal;

             string[] str1 = emp.RcvTotStr.Split(';');
             string[] str2 = emp.IssTotStr.Split(';');
             string[] str3 = emp.StockStr.Split(';');

             if (str1.Count() >= 1)
             {
                 objE.MainStoreRCV = Convert.ToDouble(str1[0].Substring(str1[0].IndexOf('=') + 1));
                 objE.MainStoreISS = Convert.ToDouble(str2[0].Substring(str2[0].IndexOf('=') + 1));
                 this.GridPanel1.ColumnModel.SetHidden(5, false);
                 this.GridPanel1.ColumnModel.SetHidden(9, false);
                 this.GridPanel1.ColumnModel.SetHidden(13, false);

                
             }
             if (str1.Count() >= 2)
             {
                 objE.SubStore1RCV = Convert.ToDouble(str1[1].Substring(str1[1].IndexOf('=') + 1));
                 objE.SubStore1ISS = Convert.ToDouble(str2[1].Substring(str2[1].IndexOf('=') + 1));

                 this.GridPanel1.ColumnModel.SetHidden(6, false);
                 this.GridPanel1.ColumnModel.SetHidden(10, false);
                 this.GridPanel1.ColumnModel.SetHidden(14, false);
                 
               
             }

             if (str1.Count() >= 3)
             {
                 objE.SubStore2RCV = Convert.ToDouble(str1[2].Substring(str1[2].IndexOf('=') + 1));
                 objE.SubStore2ISS = Convert.ToDouble(str2[2].Substring(str2[2].IndexOf('=') + 1));
                 this.GridPanel1.ColumnModel.SetHidden(7, false);
                 this.GridPanel1.ColumnModel.SetHidden(11, false);
                 this.GridPanel1.ColumnModel.SetHidden(15, false);
                 
               
             }

             if (str1.Count() >= 4)
             {
                 objE.SubStore3RCV = Convert.ToDouble(str1[3].Substring(str1[3].IndexOf('=') + 1));
                 objE.SubStore3ISS = Convert.ToDouble(str2[3].Substring(str2[3].IndexOf('=') + 1));
                 GridPanel1.ColumnModel.Columns[8].Hidden = false;
                 GridPanel1.ColumnModel.Columns[12].Hidden = false;
                 GridPanel1.ColumnModel.Columns[16].Hidden = false;
             }

             objE.MainStoreBAL = Convert.ToDouble(str3[0].Substring(str3[0].IndexOf('=') + 1));
             objE.SubStore1BAL = Convert.ToDouble(str3[1].Substring(str3[1].IndexOf('=') + 1));
             if (str3.Count() >= 3)
             {
                 objE.SubStore2BAL = Convert.ToDouble(str3[2].Substring(str3[2].IndexOf('=') + 1));
             }
             if (str3.Count() >= 4)
             {
                 objE.SubStore3BAL = Convert.ToDouble(str3[3].Substring(str3[3].IndexOf('=') + 1));

             }


             objE.Balance = objE.MainStoreBAL + objE.SubStore1BAL + objE.SubStore2BAL + objE.SubStore3BAL;

             liD.Add(objE);


         }

         return liD;
     }
    
   
   
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

      <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
     <link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>

    <script type="text/javascript">

        

        // if you use jQuery, you can load them when dom is read.
        $(document).ready(function() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            // Place here the first init of the autocomplete
            InitAutoCompl();
            InitParentAutoCompl();
        });
        function InitializeRequest(sender, args) {
        }

        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the Autocomplete
            InitAutoCompl();
            InitParentAutoCompl();
        }
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.search);
            if (results == null)
                return "";
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function InitAutoCompl() {
            $('input[name$="txtItem"]').autocomplete({
                source: function(request, response) {
                    $.ajax({
                    url: "DataLoad.asmx/GetItemList",
                        data: "{'TI':'" + getParameterByName("INVTypeID") + "','GI':'" + document.getElementById('<%= ddlYarnGroup.ClientID %>').value + "' , 'item1': '" + request.term + "','StoreID':'1' }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function(data) { return data; },
                        success: function(data) {
                            response(data.d);
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            //alert(getParameterByName("INVTypeID"));
                        }
                    });
                },
                minLength: 2,
                focus: function(event, ui) {
                    $('input[name$="txtItem"]').val(ui.item.ItemName);


                    return false;
                },
                select: function(event, ui) {
                    $('input[name$="txtItem"]').val(ui.item.ItemName);


                    return false;
                }
            }).data('autocomplete')._renderItem = function(ul, item) {
                return $('<li>').data('item.autocomplete', item).append('<a>' + item.ItemName + '</a>').appendTo(ul);

            };

        }

        function InitParentAutoCompl() {
            $('input[name$="txtParent"]').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "DataLoad.asmx/GetItemList",
                        data: "{'TI':'" + getParameterByName("INVTypeID") + "','GI':'" + document.getElementById('<%= ddlYarnGroup.ClientID %>').value + "' , 'item1': '" + request.term + "','StoreID':'1' }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function(data) { return data; },
                        success: function(data) {
                            response(data.d);
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            //alert(getParameterByName("INVTypeID"));
                        }
                    });
                },
                minLength: 2,
                focus: function(event, ui) {
                $('input[name$="txtParent"]').val(ui.item.ItemName);


                    return false;
                },
                select: function(event, ui) {
                $('input[name$="txtParent"]').val(ui.item.ItemName);


                    return false;
                }
            }).data('autocomplete')._renderItem = function(ul, item) {
                return $('<li>').data('item.autocomplete', item).append('<a>' + item.ItemName + '</a>').appendTo(ul);

            };

        }    
  </script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> 
    <asp:Label ID="lblHead" runat="server" Text="Label"></asp:Label>
    </h2></div>
<div align="left" style="padding: 10px; ">
                      Item Group
                  
                       
                                    <asp:DropDownList ID="ddlYarnGroup" runat="server" Height="24px" Width="157px" 
                                        TabIndex="1" >
                                    </asp:DropDownList>
                                     <asp:Label ID="lblParent" runat="server" Text="Parent Name" ></asp:Label>
                     <asp:TextBox ID="txtParent" runat="server" style="margin-left: 0px" TabIndex="1"   ></asp:TextBox>
                                Item Name
                     <asp:TextBox ID="txtItem" runat="server" style="margin-left: 0px" TabIndex="1" 
                                        Width="248px"></asp:TextBox>
                                        <br />
                      Date
                    <asp:TextBox ID="txtFrom0" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom0_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom0">
                    </cc1:CalendarExtender>
                       
                      To
                    <asp:TextBox ID="txtFrom1" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom1_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom1">
                    </cc1:CalendarExtender>
                       
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="80" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                       
                </div>
               
                 <div style="float: left">
                    </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
           
            >
            <Reader>
            <ext:JsonReader IDProperty="ItemID">
                    <Fields>
                     

                      
                           <ext:RecordField Name="ItemID" />
                          <ext:RecordField Name="GroupName" />
                          <ext:RecordField Name="ParentName" />
                          <ext:RecordField Name="ItemName" />
                          <ext:RecordField Name="OpenBal" />
                          <ext:RecordField Name="MainStoreRCV" />
                          <ext:RecordField Name="SubStore1RCV" />
                          <ext:RecordField Name="SubStore2RCV" />
                           <ext:RecordField Name="SubStore3RCV" />
                          
                           <ext:RecordField Name="MainStoreISS" />
                          <ext:RecordField Name="SubStore1ISS" />
                          <ext:RecordField Name="SubStore2ISS" />
                          <ext:RecordField Name="SubStore3ISS" />
                          
                           <ext:RecordField Name="MainStoreBAL" />
                          <ext:RecordField Name="SubStore1BAL" />
                          <ext:RecordField Name="SubStore2BAL" />
                          <ext:RecordField Name="SubStore3BAL" />
                          
                          <ext:RecordField Name="Balance" />
                          
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Stock Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                         
                          
                          
                            <ext:Column ColumnID="ItemID" Header="ItemID"  DataIndex="ItemID" Sortable="true" />
                            <ext:Column ColumnID="GroupName" Header="GroupName"  DataIndex="GroupName" Sortable="true" />
                            <ext:Column ColumnID="ParentName" Header="ParentName"  DataIndex="ParentName" Sortable="true" />
                            <ext:Column ColumnID="ItemName" Header="ItemName"  DataIndex="ItemName" Sortable="true" />
                            
                            <ext:Column ColumnID="OpenBal"  Header="Opeining Balance"  DataIndex="OpenBal" Sortable="true" />
                            
                            <ext:Column ColumnID="MainStoreRCV" Header="Main Store Receive"  DataIndex="MainStoreRCV" Sortable="true" Hidden="true"/><%--5--%>
                            <ext:Column ColumnID="SubStore1RCV" Header="Sub Store  Receive"  DataIndex="SubStore1RCV" Sortable="true" Hidden="true"/>
                            <ext:Column ColumnID="SubStore2RCV" Header="Sub Store 2 Receive"  DataIndex="SubStore2RCV" Sortable="true" Hidden="true"/>
                            <ext:Column ColumnID="SubStore3RCV" Header="Sub Store 3 Receive"  DataIndex="SubStore3RCV" Sortable="true" Hidden="true"/>
                            
                             <ext:Column ColumnID="MainStoreISS" Header="Main Store Issue"  DataIndex="MainStoreISS" Sortable="true" Hidden="true"/><%--9--%>
                            <ext:Column ColumnID="SubStore1ISS" Header="Sub Store  Issue"  DataIndex="SubStore1ISS" Sortable="true" Hidden="true"/>
                            <ext:Column ColumnID="SubStore2ISS" Header="Sub Store 2 Issue"  DataIndex="SubStore2ISS" Sortable="true" Hidden="true"/>
                            <ext:Column ColumnID="SubStore3ISS" Header="Sub Store 3 v"  DataIndex="SubStore3ISS" Sortable="true" Hidden="true"/>
                            
                             <ext:Column ColumnID="MainStoreBAL" Header="Main Store Stock"  DataIndex="MainStoreBAL" Sortable="true" Hidden="true"/><%--13--%>
                            <ext:Column ColumnID="SubStore1BAL" Header="Sub Store  Stock"  DataIndex="SubStore1BAL" Sortable="true" Hidden="true"/>
                            <ext:Column ColumnID="SubStore2BAL" Header="Sub Store 2 Stock"  DataIndex="SubStore2BAL" Sortable="true" Hidden="true"/>
                            <ext:Column ColumnID="SubStore3BAL" Header="Sub Store 3 Stock"  DataIndex="SubStore3BAL" Sortable="true" Hidden="true"/>
                            
                           
                            
                            <ext:Column ColumnID="Balance" Header="Actual Balance"  DataIndex="Balance" Sortable="true" /><%--17--%>
                           
                       
                       </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="GroupName" />
                                <ext:StringFilter DataIndex="ParentName" />
                                <ext:StringFilter DataIndex="ItemName" />
                               
                               
                                      
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>

