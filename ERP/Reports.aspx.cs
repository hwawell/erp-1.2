﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
namespace ERP
{
    public partial class WebForm19 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                //if (new Utilities_DL().CheckSecurity("505", USession.SUserID) == true)
                //{


                //List<EOrderList> d = new Order_DL().GetActiveOrderList();
                //    ddlOrderList.DataSource =d;

                //    ddlOrderList.DataTextField = "OrderNo";
                //    ddlOrderList.DataValueField = "OrderNo";
                //    ddlOrderList.DataBind();
                //    ddlOrderList.Items.Insert(0,"ALL");
                //    ddlOrderList.Text = "ALL";

                //    ddlOrderList0.DataSource = d;

                //    ddlOrderList0.DataTextField = "OrderNo";
                //    ddlOrderList0.DataValueField = "OrderNo";
                //    ddlOrderList0.DataBind();
                //    ddlOrderList0.Text = "Select";
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
                
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            //string jscript = "";
            //string ReportPar = rdoReport.SelectedValue.ToString();
            //string ReportName="";
            //string pr = "";
            //if (rdoReport.SelectedIndex == 0)
            //{
            //    ReportName = "rptYarnStock";
            //}
            //else if (rdoReport.SelectedIndex == 1)
            //{
            //    ReportName = "rptKnittSock";
            //}
            //else if (rdoReport.SelectedIndex == 2)
            //{
            //    ReportName = "rptGreayStock";
            //}
            //else if (rdoReport.SelectedIndex == 3)
            //{
            //    ReportName = "rptProcessSummary";
            //    pr = "DEYING";
            //}
            //else if (rdoReport.SelectedIndex == 4)
            //{
            //    ReportName = "rptProcessSummary";
            //    pr = "DRY";
            //}
            //else if (rdoReport.SelectedIndex == 5)
            //{
            //    ReportName = "rptProcessSummary";
            //    pr = "BRUSHING";
            //}
            //else if (rdoReport.SelectedIndex == 6)
            //{
            //    ReportName = "rptProcessSummary";
            //    pr = "SHEARING";
            //}
            //else if (rdoReport.SelectedIndex == 7)
            //{
            //    ReportName = "rptProcessSummary";
            //    pr = "SETTING";
            //}
            //else if (rdoReport.SelectedIndex == 8)
            //{
            //    ReportName = "rptProcessSummary";
            //    pr = "ANTIPILLING";
            //}
            //else if (rdoReport.SelectedIndex == 9)
            //{
            //    ReportName = "rptProcessSummary";
            //    pr = "PACKING";
            //}

            //string Fdate = txtFrom.Text;
            //string ToDate = txtTo.Text;
      

            //string RTYpe = rdoReport.SelectedValue.ToString(); //rdoStatement.SelectedItem.Text.ToString();
            //jscript += "<script language=javascript>";
            //jscript += "window.open('./ReportPreview.aspx?Type="+ RTYpe +"&ReportName=" + ReportName + "&Fdate="+ Fdate +"&TDate="+ ToDate +"&Process="+ pr +"'";
            //jscript += ",''";
            ////jscript += ",'scrollbars=2'";
            ////jscript += ",'resizable=1'";
            //jscript += ",'status,resizable,scrollbars'";
            //jscript += "); ";
            //jscript += "</script>";
            ////string script = "var windowObject = window.self; windowObject.opener = window.self; windowObject.close();";
            ////Page.ClientScript.RegisterStartupScript(this.GetType(), "Close Window", script, true);
            //this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm4), "ScriptFunction", jscript);
        }

        protected void btnAll_Click(object sender, EventArgs e)
        {

            //DateTime fm =Convert.ToDateTime("1/1/2009");
            //DateTime to = Convert.ToDateTime(txtTo.Text);
            //new Knit_Process_CL().AllSummaryReport(fm,to);
        }

        protected void cv1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //DateTime dt;

            //try
            //{
            //    dt = Convert.ToDateTime(txtFrom.Text);
            //    args.IsValid = true;

            //}
            //catch (Exception)
            //{
            //    args.IsValid = false;
            //}
        }

        protected void CV2_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime dt;

            //try
            //{
            //    dt = Convert.ToDateTime(txtTo.Text);
            //    args.IsValid = true;

            //}
            //catch (Exception)
            //{
            //    args.IsValid = false;
            //}
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            string jscript = "";
            string ReportPar = "";// rdoReport.SelectedValue.ToString();
            string ReportName = "";
            string pr = "";
            string RTYpe = "";
            bool IsPack = false;
            bool IsDel = false;
            if (rdoSum.SelectedIndex == 0)
            {
                RTYpe = "Deying Production Report By Order";
                pr = "DEYING_PROD_REPORT";
                ReportName = "rpt_Deying_Production_Report";
                IsPack = false;
                IsDel = false;
            }
            else if (rdoSum.SelectedIndex == 1)
            {
                RTYpe = "Packing Report By Order";
                pr = "PACKING_PROD_REPORT";
                ReportName = "rpt_Packing_Production_Report";
                IsPack = true;
                IsDel = false;
            }
            else if (rdoSum.SelectedIndex == 2)
            {
                RTYpe = "Delivery Report By Order";
                pr = "DELIVERY_PROD_REPORT";
                IsPack = true;
                IsDel = true;
                ReportName = "rpt_Paking_Delivery_Report";
            }
            //else if (rdoSum.SelectedIndex == 3)
            //{
            //    RTYpe = "All Product Report By Order";
            //    pr = "ALL_PROD_REPORT";
            //    IsPack = true;
            //    IsDel = true;
            //    ReportName = "rpt_Production_Report";
            //}
            //else if (rdoSum.SelectedIndex == 4)
            //{
            //    RTYpe = "All Product Delivery Report By Order";
            //    pr = "ALL_PROD_DELV_REPORT";
            //    IsPack = true;
            //    IsDel = true;
            //    ReportName = "rpt_production_delivery_report";
            //}




            // string RTYpe = rdoReport.SelectedValue.ToString(); //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&OrderNo=" + tbAuto0.Text.Trim() + "&FromDate=" + txtCurrentDate.Text + "&IsPack=" + IsPack + "&IsDel=" + IsDel + "&Process=" + pr + "'";
            jscript += ",''";
            jscript += ",'scrollbars=2'";
            jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";
            //string script = "var windowObject = window.self; windowObject.opener = window.self; windowObject.close();";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Close Window", script, true);
            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm4), "ScriptFunction", jscript);
        }

        protected void btnDaily_Click(object sender, EventArgs e)
        {
            string jscript = "";
            string ReportPar = "";// rdoReport.SelectedValue.ToString();
            string ReportName = "";
            string pr = "";
            string RTYpe = "";
            bool IsPack = false;
            bool IsDel = false;
            if (rdoDaily.SelectedIndex == 0)
            {
                RTYpe = "Daily Deying Production Report";
                pr = "DAILY_DEYING_PROD_REPORT";
                ReportName = "rpt_Deying_Production_Daily_Report";
                IsPack = false;
                IsDel = false;
            }
            else if (rdoDaily.SelectedIndex == 1)
            {
                RTYpe = "Daily Packing Report";
                pr = "DAILY_PACKING_PROD_REPORT";
                ReportName = "rpt_Packing_Production_Daily_Report";
                IsPack = true;
                IsDel = false;
            }

            else if (rdoDaily.SelectedIndex == 2)
            {
                RTYpe = "Daily Delivery Report";
                pr = "DAILY_DELIVERY_PROD_REPORT";
                ReportName = "rpt_Delivery_Production_Daily_Reports";
                IsPack = true;
                IsDel = true;
            }





            // string RTYpe = rdoReport.SelectedValue.ToString(); //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&Date=" + txtCurrentDate.Text.Trim() + "&IsPack=" + IsPack + "&IsDel=" + IsDel + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";
            //string script = "var windowObject = window.self; windowObject.opener = window.self; windowObject.close();";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Close Window", script, true);
            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm4), "ScriptFunction", jscript);
        }

        protected void btnDaily0_Click(object sender, EventArgs e)
        {
            string jscript = "";
            string ReportPar = "";// rdoReport.SelectedValue.ToString();
            string ReportName = "";
            string pr = "";
            string RTYpe = "";
            int IsDeying = 0;
            int IsDel = 0;
            if (rdoSummary.SelectedIndex == 0)
            {
                RTYpe = "Deying & Packing Summary Report";
                ReportName = "rpt_Production_PackDeyingSumm";
                pr = "DEYING_PACKING_SUMM_REPORT";

               
            }
            else if (rdoSummary.SelectedIndex == 1)
            {
                RTYpe = "Delivery Summary Report";
                pr = "DELIVERY_SUMM_REPORT";
                ReportName = "rpt_Production_DeliverySumm";
               
            }

          
           
            

            
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&PINO=" + tbAuto.Text + "&Process=" + pr + "'";
            jscript += ",''";
            jscript += ",'scrollbars=2'";
            jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";
            //string script = "var windowObject = window.self; windowObject.opener = window.self; windowObject.close();";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Close Window", script, true);
            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm4), "ScriptFunction", jscript);
        }
    }
}
