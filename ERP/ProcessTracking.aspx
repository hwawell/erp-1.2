﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ProcessTracking.aspx.cs" Inherits="ERP.WebForm25" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style3
        {
            height: 465px;
        }
    .style4
    {
        width: 2px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager> 
    <table style="width:100%;">
        <tr>
            <td align="center" colspan="3" style="font-size: large; font-weight: bold">
                Finishing Product Tracking</td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                    AssociatedUpdatePanelID="UpGrid">
                    <ProgressTemplate>
                        <asp:Label ID="Label1" runat="server" Text="Please Wait....." 
                            BackColor="#CC3300" ForeColor="White"></asp:Label>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td align="center">
                <asp:Button ID="Button1" runat="server" Text="Current Packing Status" 
                    onclick="Button1_Click" />
                <asp:Button ID="Button2" runat="server" Text="Finished Product Status" 
                    onclick="Button2_Click" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3" colspan="3" valign="top">
               <asp:UpdatePanel ID="UpGrid" runat="server" UpdateMode="Conditional">
               <ContentTemplate>
               
   
                <div align="center" style="height: 464px; overflow: scroll; width: 747px;">
                    <asp:GridView ID="GridView1" runat="server" 
                        onrowdatabound="GridView1_RowDataBound">
                        <RowStyle BackColor="WhiteSmoke" />
                        <AlternatingRowStyle BackColor="#FFFFE8" />
                    </asp:GridView>
                </div>
                
                            </ContentTemplate>
                   <Triggers>
                       <asp:AsyncPostBackTrigger ControlID="Button1" EventName="Click" />
                       <asp:AsyncPostBackTrigger ControlID="Button2" EventName="Click" />
                   </Triggers>
               </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
