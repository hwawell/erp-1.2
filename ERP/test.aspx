﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="ERP.test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
	<script type="text/javascript">
	    $(function() {
	        $('input[name$="tbAuto"]').autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                    url: "DataLoad.asmx/GetItemList",
	                    data: "{'TI':'" + document.getElementById('<%= tbAuto0.ClientID %>').value + "','GI':'" + document.getElementById('<%= ddlGroup.ClientID %>').value + "' , 'item1': '" + request.term + "' }",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    dataFilter: function(data) { return data; },
	                    success: function(data) {
	                        response(data.d);
	                    },
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                       
	                    }
	                });
	            },
	            minLength: 2,
	            focus: function(event, ui) {
	            $('input[name$="tbAuto"]').val(ui.item.ItemName);
	            $('input[name$="tbAuto1"]').val(ui.item.ItemID + " " + ui.item.GroupName);
	                return false;
	            },
	            select: function(event, ui) {
	            $('input[name$="tbAuto"]').val(ui.item.ItemName);
	            $('input[name$="tbAuto1"]').val(ui.item.ItemID + " " + ui.item.GroupName);
	                return false;
	            }
	            }).data('autocomplete')._renderItem = function(ul, item) {
        return $('<li>').data('item.autocomplete', item).append('<a>' + item.ItemName + '</a>').appendTo(ul);
    
	        };
	    });
	</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:TextBox ID="tbAuto" runat="server" class="tb">
             </asp:TextBox>
     <asp:TextBox ID="tbAuto0" runat="server" Text="1">
             </asp:TextBox>
        <asp:DropDownList ID="ddlGroup" runat="server" 
                        Height="22px" Width="320px">
                    </asp:DropDownList>
    &nbsp;<asp:TextBox ID="tbAuto1" runat="server" Text="1">
             </asp:TextBox></div>
    </form>
</body>
</html>
