﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
using System.Reflection;
namespace ERP
{
    public partial class PrductionReportAll : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                loadOrder();
            }
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            string Title = "";

            if (ddlFor.SelectedIndex < 2)
            {
                if (ddlType.SelectedIndex == 1)
                {
                    tbAuto.Text = "";
                    Title = ddlType.SelectedItem.Text + " Report From " + txtFrom.Text + " To " + txtTo.Text + " For " + ddlFor.SelectedItem.Text;
                    ExportProcessDetails(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().RPT_GETPROCESS_DETAILS( txtFrom.Text, txtTo.Text, ddlFor.SelectedItem.Text));
                }
                else if (ddlType.SelectedIndex == 2)
                {
                    txtFrom.Text = "";
                    txtTo.Text = "";
                    Title = ddlType.SelectedItem.Text + " Report for Order  " + tbAuto.Text + " For " + ddlFor.SelectedItem.Text;
                    ExportProcessDetailsByOrder(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().RPT_GETPROCESS_DETAILS_ByOrder(tbAuto.Text, ddlFor.SelectedItem.Text));
                }
                else
                {
                    txtFrom.Text = "";
                    txtTo.Text = "";
                    Title = ddlType.SelectedItem.Text + " Summary Report for Order  " + tbAuto.Text + " For " + ddlFor.SelectedItem.Text;
                    ExportProcessSummary(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().RPT_Process_Summary(tbAuto.Text, ddlFor.SelectedItem.Text));

                }

            }
            else //Packing Repot 
            {
                if (ddlType.SelectedIndex == 1)
                {
                    tbAuto.Text = "";
                    Title = ddlType.SelectedItem.Text + " Report From " + txtFrom.Text + " To " + txtTo.Text + " For " + ddlFor.SelectedItem.Text;
                    ExportPackingDetails(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().RPT_PACKING_DETAILS(tbAuto.Text, txtFrom.Text, txtTo.Text));
                }
                else if (ddlType.SelectedIndex == 2)
                {
                    txtFrom.Text = "";
                    txtTo.Text = "";
                    Title = ddlType.SelectedItem.Text + " Report for Order  " + tbAuto.Text + " For " + ddlFor.SelectedItem.Text;
                    ExportPackingDetailsBYOrder(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().RPT_PACKING_DETAILSByOrder(tbAuto.Text));
                }
                else
                {
                    txtFrom.Text = "";
                    txtTo.Text = "";
                    Title = ddlType.SelectedItem.Text + " Summary Report for Order  " + tbAuto.Text + " For " + ddlFor.SelectedItem.Text;
                    ExportPackingSummary(ddlFor.SelectedItem.Text + "_DetailsReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, new GreyFabric_DL().RPT_PACKING_Summary(tbAuto.Text));

                }

            }


         

            
        }
        private void loadOrder()
        {
           

        }
        public void ExportProcessDetails(string fileName, List<RPT_KNIT_PROCESS_DETAILSResult> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2>" + ddlType.SelectedItem.Text + " Report- " + txtFrom.Text + "</h2>  </div>");
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            foreach (PropertyInfo proinfo in new RPT_KNIT_PROCESS_DETAILSResult().GetType().GetProperties())
            {

                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(proinfo.Name);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");


            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (RPT_KNIT_PROCESS_DETAILSResult emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");
                foreach (PropertyInfo proinfo in new RPT_KNIT_PROCESS_DETAILSResult().GetType().GetProperties())
                {
                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    if (proinfo.GetValue(emp, null) != null)
                    {
                        HttpContext.Current.Response.Write(proinfo.GetValue(emp, null).ToString());
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("");
                    }
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");

                }

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }

        public void ExportProcessDetailsByOrder(string fileName, List<RPT_KNIT_PROCESS_DETAILS_BY_ORDERResult> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2>" + ddlType.SelectedItem.Text + " No " + tbAuto.Text + " Report</h2>  </div>");
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            foreach (PropertyInfo proinfo in new RPT_KNIT_PROCESS_DETAILS_BY_ORDERResult().GetType().GetProperties())
            {

                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(proinfo.Name);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");


            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (RPT_KNIT_PROCESS_DETAILS_BY_ORDERResult emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");
                foreach (PropertyInfo proinfo in new RPT_KNIT_PROCESS_DETAILS_BY_ORDERResult().GetType().GetProperties())
                {
                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    if (proinfo.GetValue(emp, null) != null)
                    {
                        HttpContext.Current.Response.Write(proinfo.GetValue(emp, null).ToString());
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("");
                    }
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");

                }

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }


        public void ExportPackingDetails(string fileName, List<RPT_KNIT_PACKING_DETAILSResult> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2>Packing Details Report From " + txtFrom.Text + " to " + txtTo.Text +"</h2>  </div>");
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            foreach (PropertyInfo proinfo in new RPT_KNIT_PACKING_DETAILSResult().GetType().GetProperties())
            {

                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(proinfo.Name);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");


            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (RPT_KNIT_PACKING_DETAILSResult emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");
                foreach (PropertyInfo proinfo in new RPT_KNIT_PACKING_DETAILSResult().GetType().GetProperties())
                {
                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    if (proinfo.GetValue(emp, null) != null)
                    {
                        HttpContext.Current.Response.Write(proinfo.GetValue(emp, null).ToString());
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("");
                    }
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");

                }

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }
        public void ExportPackingDetailsBYOrder(string fileName, List<RPT_KNIT_PACKING_DETAILS_BY_ORDERResult> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2>Packing Details Report From " + txtFrom.Text + " to " + txtTo.Text + "</h2>  </div>");
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            foreach (PropertyInfo proinfo in new RPT_KNIT_PACKING_DETAILS_BY_ORDERResult().GetType().GetProperties())
            {

                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(proinfo.Name);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");


            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (RPT_KNIT_PACKING_DETAILS_BY_ORDERResult emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");
                foreach (PropertyInfo proinfo in new RPT_KNIT_PACKING_DETAILS_BY_ORDERResult().GetType().GetProperties())
                {
                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    if (proinfo.GetValue(emp, null) != null)
                    {
                        HttpContext.Current.Response.Write(proinfo.GetValue(emp, null).ToString());
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("");
                    }
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");

                }

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }


        public void ExportProcessSummary(string fileName, List<RPT_KNIT_PROCESS_SUMMARYResult> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2> "+ ddlFor.SelectedItem.Text +" Summary Report Order Wise</h2>  </div>");
            HttpContext.Current.Response.Write("<div> Total Booking Qty:<b> " + empList.Sum(o => o.BookingQty).Value.ToString() + "</b> ,Total " + ddlFor.SelectedItem.Text + " qty : <b>" + empList.Sum(o => o.PQty).ToString() + " </b>  ,Balance Qty :<b>" + empList.Sum(o => o.BookingQty - o.PQty).Value.ToString() + " </b></div>");

            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            foreach (PropertyInfo proinfo in new RPT_KNIT_PROCESS_SUMMARYResult().GetType().GetProperties())
            {
                if (proinfo.Name != "Lotno" || proinfo.Name != "PcardNo")
                {
                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    HttpContext.Current.Response.Write(proinfo.Name);
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");
                }

            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (RPT_KNIT_PROCESS_SUMMARYResult emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");
                foreach (PropertyInfo proinfo in new RPT_KNIT_PROCESS_SUMMARYResult().GetType().GetProperties())
                {
                    if (proinfo.Name != "Lotno" || proinfo.Name != "PcardNo")
                    {
                        HttpContext.Current.Response.Write("<Td>");
                        //Get column headers  and make it as bold in excel columns
                        HttpContext.Current.Response.Write("<B>");
                        if (proinfo.GetValue(emp, null) != null)
                        {
                            HttpContext.Current.Response.Write(proinfo.GetValue(emp, null).ToString());
                        }
                        else
                        {
                            HttpContext.Current.Response.Write("");
                        }
                        HttpContext.Current.Response.Write("</B>");
                        HttpContext.Current.Response.Write("</Td>");
                    }
                }

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }

        public void ExportPackingSummary(string fileName, List<RPT_KNIT_PACKING_SUMMARYResult> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2> " + ddlFor.SelectedItem.Text + " Summary Report For Order " + ddlType.SelectedItem.Text + "</h2>  </div>");
            HttpContext.Current.Response.Write("<div> Total Booking Qty:<b> " + empList.Sum(o => o.BookingQty).Value.ToString() + "</b> ,Total " + ddlFor.SelectedItem.Text + " qty : <b>" + empList.Sum(o => o.PQty).ToString() + " </b>  ,Balance Qty :<b>" + empList.Sum(o => o.BookingQty - o.PQty).Value.ToString() + " </b></div>");

            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            foreach (PropertyInfo proinfo in new RPT_KNIT_PACKING_SUMMARYResult().GetType().GetProperties())
            {

                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(proinfo.Name);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");


            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (RPT_KNIT_PACKING_SUMMARYResult emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");
                foreach (PropertyInfo proinfo in new RPT_KNIT_PACKING_SUMMARYResult().GetType().GetProperties())
                {
                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    if (proinfo.GetValue(emp, null) != null)
                    {
                        HttpContext.Current.Response.Write(proinfo.GetValue(emp, null).ToString());
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("");
                    }
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");

                }

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }




        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedIndex > 0)
            {

                if (ddlType.SelectedIndex == 1)
                {

                    txtFrom.Visible = true;
                    txtTo.Visible = true;
                    tbAuto.Visible = false;
                    lblFrom.Visible = true;
                    lblTo.Visible = true;
                    lblOrder.Visible = false;
                }
                else
                {
                    txtFrom.Visible = false;
                    txtTo.Visible = false;
                    tbAuto.Visible = true;

                    lblFrom.Visible = false;
                    lblTo.Visible = false;
                    lblOrder.Visible = true;

                    
                }
            }
            upSelect.Update();
        }
    }
}
