﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Warehouse_Stock_Report.aspx.cs" Inherits="ERP.Warehouse_Stock_Report" Title="Stock Report" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();
            List<EWareHousePacking> liProd = new List<EWareHousePacking>();
            this.Window1.Title = "Stock Report";
            liProd = new KNIT_all_operation().GetWareHouseStock(DropDownList1.SelectedValue, OpenFunction.ConvertDate(txtFrom.Text), tbAuto.Text);
                    
            
            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
    
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
   <script type="text/javascript">
       $(function() {
           $(".tb").autocomplete({
               source: function(request, response) {
                   $.ajax({
                       url: "DataLoad.asmx/GetOrderList",
                       data: "{ 'PINO': '" + request.term + "' }",
                       dataType: "json",
                       type: "POST",
                       contentType: "application/json; charset=utf-8",
                       dataFilter: function(data) { return data; },
                       success: function(data) {
                           response($.map(data.d, function(item) {
                               return {
                                   value: item.OrderNo
                               }
                           }))
                       },
                       error: function(XMLHttpRequest, textStatus, errorThrown) {
                         
                       }
                   });
               },
               minLength: 2
           });
       });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> Stock Report</h2></div>
 <div align="left" style="padding: 10px; ">
                      Date                     <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                    &nbsp;Stock Type
                    <cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom">
                    </cc1:CalendarExtender>
                   <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                    </div>
                       
                      <asp:DropDownList ID="DropDownList1" runat="server" Height="25px" Width="192px">
                          <asp:ListItem>FINISH_FABRIC</asp:ListItem>
                          <asp:ListItem>NO_GOOD_FABRIC</asp:ListItem>
                          <asp:ListItem>REJECTED_FABRIC</asp:ListItem>
                          <asp:ListItem>LEFT_OVER_STOCK</asp:ListItem>
                      </asp:DropDownList>
&nbsp;Order No
                            
                                    <asp:TextBox ID="tbAuto" runat="server" class="tb">
             </asp:TextBox></div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
           
            >
            <Reader>
         
            <ext:JsonReader IDProperty="SL">
                    <Fields>
                      <ext:RecordField Name="SL" />
                     
                         <ext:RecordField Name="Merchandiser" />
                          <ext:RecordField Name="OrderNo" />
                           <ext:RecordField Name="Customer" />
                           
                        <ext:RecordField Name="ItemDescription" />
                         <ext:RecordField Name="GSM" />
                         <ext:RecordField Name="Width" />
                       
                         
                         <ext:RecordField Name="Color" />
                         <ext:RecordField Name="Lotno" />
                          <ext:RecordField Name="ProcessCard" />
                          <ext:RecordField Name="RollQty" />
                          <ext:RecordField Name="StockQty" />
                       
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Production Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            
                            
                             <ext:Column ColumnID="SL" Header="SL"  DataIndex="SL" Sortable="true" />
                            <ext:Column ColumnID="Merchandiser" Header="Merchandiser"  DataIndex="Merchandiser" Sortable="true" />
                            <ext:Column ColumnID="OrderNo" Header="OrderNo"  DataIndex="OrderNo" Sortable="true" />
                            <ext:Column ColumnID="Customer" Header="Customer"  DataIndex="Customer" Sortable="true" />
                            <ext:Column ColumnID="ItemDescription" Header="ItemDescription"  DataIndex="ItemDescription" Sortable="true" />
                            <ext:Column ColumnID="GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                            <ext:Column ColumnID="Width" Header="Width"  DataIndex="Width" Sortable="true" />
                            
                            <ext:Column ColumnID="Color" Header="Color"  DataIndex="Color" Sortable="true" />
                            <ext:Column ColumnID="Lotno" Header="Lotno"  DataIndex="Lotno" Sortable="true" />
                            <ext:Column ColumnID="ProcessCard" Header="ProcessCard"  DataIndex="ProcessCard" Sortable="true" />
                            <ext:Column ColumnID="RollQty" Header="RollQty"  DataIndex="RollQty" Sortable="true" />
                            <ext:Column ColumnID="StockQty" Header="StockQty"  DataIndex="StockQty" Sortable="true" />
                             
                               
                               
                                                       
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                             
                                <ext:StringFilter DataIndex="Merchandiser" />
                                <ext:StringFilter DataIndex="OrderNo" />
                                <ext:StringFilter DataIndex="Customer" />
                                <ext:StringFilter DataIndex="ItemDescription" />
                                <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="Width" />
                                <ext:StringFilter DataIndex="Color" />
                                
                                <ext:NumericFilter DataIndex="Lotno" />
                                <ext:NumericFilter DataIndex="ProcessCard" />
                                 <ext:NumericFilter DataIndex="RollQty" />
                                  <ext:NumericFilter DataIndex="StockQty" />
                                  
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>

