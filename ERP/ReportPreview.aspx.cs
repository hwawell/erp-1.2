﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
namespace ERP
{
    public partial class ReportPreview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CrystalReportSource1.Report.FileName = "Reports/" + Request.QueryString["ReportName"] + ".rpt";
            //CrystalReportSource1.Report.FileName =  Request.QueryString["ReportName"];
            loadReportInfo();
            this.Page.Title = Request.QueryString["Type"]; 
       
        }
        private void loadReportInfo()
        {
            string db;
            string server;
            string uid;
            string pass;
            string con = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
            char[] con1 = { ';', '=' };
            string[] con2 = con.Split(con1);
            string pr = Request.QueryString["Process"];
            db = con2[3];
            server = con2[1];
            uid = con2[5];
            pass = con2[7];
            ConnectionInfo connectionInfo = new ConnectionInfo();

            connectionInfo.ServerName = server;//"SGH8470KGZ\\DE822000";
            connectionInfo.DatabaseName = db;// "Northwind";
            connectionInfo.UserID = uid;// "sa";
            connectionInfo.Password = pass;// "adminsql";
            // RPT_SCHEDULE_A1_SP

            if (pr == "ORDER_STATUS")
            {
                CrystalReportSource1.ReportDocument.SetParameterValue("@OrderNo", Request.QueryString["Fdate"]);

            }
            else if (pr == "PI")
            {
                string g = Request.QueryString["PI"];
                //string gn = Request.QueryString["GName"];
                CrystalReportSource1.ReportDocument.SetParameterValue("@OrderNo", g);
            
                //  CrystalReportSource1.ReportDocument.SetParameterValue("@FromDate", Convert.ToDateTime(Request.QueryString["FromDate"]));
                // CrystalReportSource1.ReportDocument.SetParameterValue("@ToDate", Convert.ToDateTime(Request.QueryString["ToDate"]));


            }
            else if (pr == "DEYING_PROD_REPORT")
            {
                string g = Request.QueryString["OrderNo"];
                //string gn = Request.QueryString["GName"];
                CrystalReportSource1.ReportDocument.SetParameterValue("@PINO", g);
              //  CrystalReportSource1.ReportDocument.SetParameterValue("@FromDate", Convert.ToDateTime(Request.QueryString["FromDate"]));
               // CrystalReportSource1.ReportDocument.SetParameterValue("@ToDate", Convert.ToDateTime(Request.QueryString["ToDate"]));


            }
            else if (pr == "ALL_PROD_REPORT")
            {
                string g = Request.QueryString["OrderNo"];
                //string gn = Request.QueryString["GName"];
                CrystalReportSource1.ReportDocument.SetParameterValue("@PINO", g);

              //  CrystalReportSource1.ReportDocument.SetParameterValue("@FromDate", Convert.ToDateTime(Request.QueryString["FromDate"]));
//CrystalReportSource1.ReportDocument.SetParameterValue("@ToDate", Convert.ToDateTime(Request.QueryString["ToDate"]));

            }
            else if (pr == "ALL_PROD_DELV_REPORT")
            {
                string g = Request.QueryString["OrderNo"];
                //string gn = Request.QueryString["GName"];
                CrystalReportSource1.ReportDocument.SetParameterValue("@PINO", g);

                //  CrystalReportSource1.ReportDocument.SetParameterValue("@FromDate", Convert.ToDateTime(Request.QueryString["FromDate"]));
                //CrystalReportSource1.ReportDocument.SetParameterValue("@ToDate", Convert.ToDateTime(Request.QueryString["ToDate"]));

            }

            else if (pr == "DAILY_DEYING_PROD_REPORT")
            {
                string g = Request.QueryString["Date"];
                //string gn = Request.QueryString["GName"];
                CrystalReportSource1.ReportDocument.SetParameterValue("@Date",Convert.ToDateTime(g));


            }


            else if (pr == "DAILY_PACKING_PROD_REPORT")
            {
                string g = Request.QueryString["Date"];
                //string gn = Request.QueryString["GName"];
                CrystalReportSource1.ReportDocument.SetParameterValue("@date",Convert.ToDateTime(g));
                CrystalReportSource1.ReportDocument.SetParameterValue("@IsDelivery",Convert.ToBoolean(Request.QueryString["IsDel"]));


            }

            else if (pr == "DAILY_DELIVERY_PROD_REPORT")
            {

                string g = Request.QueryString["Date"];
                //string gn = Request.QueryString["GName"];
                CrystalReportSource1.ReportDocument.SetParameterValue("@date", Convert.ToDateTime(g));
                


            }
            else if (pr == "RANGE_PROD_REPORT")
            {
               
                CrystalReportSource1.ReportDocument.SetParameterValue("@PINNO", Request.QueryString["PINO"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@CustomrID", Request.QueryString["cusid"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@FromDate", Request.QueryString["Fdate"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@ToDate",Request.QueryString["TDate"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@IsDelivery", Request.QueryString["IsDel"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@IsDeying", Request.QueryString["IsDeying"]);

            }
            else if (pr == "DEYING_PACKING_SUMM_REPORT")
            {

                CrystalReportSource1.ReportDocument.SetParameterValue("@PINO", Request.QueryString["PINO"]);
              

            }
            else if (pr == "DELIVERY_SUMM_REPORT")
            {

                CrystalReportSource1.ReportDocument.SetParameterValue("@PINO", Request.QueryString["PINO"]);


            }
                
            else if (pr == "PACKING_PROD_REPORT")
            {
                string g = Request.QueryString["OrderNo"];
                bool pack = Convert.ToBoolean(Request.QueryString["IsPack"]);
                bool del = Convert.ToBoolean(Request.QueryString["IsDel"]);
                //string gn = Request.QueryString["GName"];
                CrystalReportSource1.ReportDocument.SetParameterValue("@PINO", g);
                ////if (pack == true)
                ////{
                ////    CrystalReportSource1.ReportDocument.SetParameterValue("@ISPacking", 1);
                ////}
                ////else
                ////{
                ////    CrystalReportSource1.ReportDocument.SetParameterValue("@ISPacking", 0);
                ////}
                ////if (del == true)
                ////{
                ////    CrystalReportSource1.ReportDocument.SetParameterValue("@IsDelivery", 1);
                ////}
                ////else
                ////{
                ////    CrystalReportSource1.ReportDocument.SetParameterValue("@IsDelivery", 0);
                ////}


               // CrystalReportSource1.ReportDocument.SetParameterValue("@FromDate", Convert.ToDateTime(Request.QueryString["FromDate"]));
               // CrystalReportSource1.ReportDocument.SetParameterValue("@ToDate", Convert.ToDateTime(Request.QueryString["ToDate"]));


            }

            else if (pr == "DELIVERY_PROD_REPORT")
            {
                string g = Request.QueryString["OrderNo"];
                bool pack = Convert.ToBoolean(Request.QueryString["IsPack"]);
                bool del = Convert.ToBoolean(Request.QueryString["IsDel"]);
                //string gn = Request.QueryString["GName"];
                CrystalReportSource1.ReportDocument.SetParameterValue("@PINO", g);
               

              


            }
            else if (pr == "PROCESS_CARD")
            {
                string g = Request.QueryString["PCARD"];
                //string gn = Request.QueryString["GName"];
                CrystalReportSource1.ReportDocument.SetParameterValue("@PCARD", g);



            }
            else if (pr == "ChemicalInventory")
            {
                //string g = Request.QueryString["GID"];
                //string gn = Request.QueryString["GName"];
                CrystalReportSource1.ReportDocument.SetParameterValue("@StockDate", Request.QueryString["CDate"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@Gid", Request.QueryString["GID"]);
                //CrystalReportSource1.ReportDocument.PrintToPrinter(1, false, 0, 0);
               
            }
            else if (pr == "YarnBalance")
            {

                CrystalReportSource1.ReportDocument.SetParameterValue("@StockDate", Request.QueryString["Fdate"]);
                
            }
            else if (pr == "YarnISSUE")
            {

               
                CrystalReportSource1.ReportDocument.SetParameterValue("@FRomDate", Request.QueryString["Fdate"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@ToDateDate", Request.QueryString["Tdate"]);
                //CrystalReportSource1.ReportDocument.SetParameterValue("@type", Request.QueryString["Type"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@type", Request.QueryString["SType"].Trim());

            }
            else if (pr == "YarnRECEIVE")
            {

                CrystalReportSource1.ReportDocument.SetParameterValue("@FRomDate", Request.QueryString["Fdate"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@ToDateDate", Request.QueryString["Tdate"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@type", Request.QueryString["SType"].Trim());

            }
            else if (pr == "FABRIC_PRODUCTION")
            {

                CrystalReportSource1.ReportDocument.SetParameterValue("@FID", Request.QueryString["FID"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@GID", Request.QueryString["GID"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@FromDate", Request.QueryString["FromDate"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@ToDate", Request.QueryString["ToDate"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@Order", Request.QueryString["Order"]);

            }
            else if (pr == "FABRIC_DELIVERY")
            {

                CrystalReportSource1.ReportDocument.SetParameterValue("@FID", Request.QueryString["FID"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@GID", Request.QueryString["GID"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@FromDate", Request.QueryString["FromDate"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@ToDate", Request.QueryString["ToDate"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@Order", Request.QueryString["Order"]);

            }
            else if (pr == "FABRIC_ORDER_STATUS")
            {

                CrystalReportSource1.ReportDocument.SetParameterValue("@FID", Request.QueryString["FID"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@GID", Request.QueryString["GID"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@ToDate", Request.QueryString["ToDate"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@Order", Request.QueryString["Order"]);

            }
            else
            {
                CrystalReportSource1.ReportDocument.SetParameterValue("@FrmDate", Request.QueryString["Fdate"]);
                CrystalReportSource1.ReportDocument.SetParameterValue("@ToDate", Request.QueryString["TDate"]);

                if (pr != "")
                {
                    CrystalReportSource1.ReportDocument.SetParameterValue("@ProcessType", Request.QueryString["Process"]);
                }
            }
            //string r = Request.QueryString["ReportPar"];
            //string ads = Request.QueryString["ADSCode"];
            //CrystalReportSource1.ReportDocument.RecordSelectionFormula = "{" + r + ";1.AdsCode}='" + ads + "'"; //ameterValue("CollectorID", Request.QueryString["ADSCode"]);

            Tables tables = CrystalReportSource1.ReportDocument.Database.Tables;

            foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                tableLogonInfo.ConnectionInfo = connectionInfo;
                table.ApplyLogOnInfo(tableLogonInfo);
            }

        }

    }
}
