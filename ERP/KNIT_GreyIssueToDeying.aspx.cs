﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class WebForm10 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //if (new Utilities_DL().CheckSecurity("507", USession.SUserID) == true)
                //{
                this.rblNormal.Items.FindByText("Active").Selected = true;
                loadFabricGroup();
                LoadMCGrid();

                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}

            }
        }
        private void LoadMCGrid()
        {




            ddlMCNo.DataSource = new KNIT_all_operation().GetAlMCNo();
            ddlMCNo.DataValueField = "MCNo";
            ddlMCNo.DataTextField = "MCNo";
            ddlMCNo.DataBind();


        }
        private void loadFabricGroup()
        {




        }
        private void loadFabric()
        {

            //ddlFabric.DataSource = new KNIT_all_operation().GetFabricListByMCno(ddlMCNo.Text);
            //ddlFabric.DataTextField = "Fabric";
            //ddlFabric.DataValueField = "FID";
            //ddlFabric.DataBind();
        }
        private void loadGridData()
        {


            gvProducts.Columns[8].Visible = true;
            gvProducts.Columns[9].Visible = true;
            gvProducts.Columns[10].Visible = false;
            gvProducts.DataSource = new KNIT_all_operation().GetKNITGreyIssue(txtPCard.Text);
            gvProducts.DataBind();

        }
        private void loadDeletedData()
        {

            gvProducts.Columns[8].Visible = false;
            gvProducts.Columns[9].Visible = false;
            gvProducts.Columns[10].Visible = true;
            gvProducts.DataSource = new Fabric_CL().GetDeletedDelivery();
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            ddlMCNo.SelectedValue = (row.Cells[6].Text);
            loadFabric();

            string FID = (row.Cells[1].Text) + "_" + (row.Cells[4].Text);
           

            string[] str = hdnFID.Value.Split('_');

            //if ((str[1].Replace("GM:", "") == row.Cells[4].Text) )
            //{

            //    ddlFabric.Text = "";
            //}


           
            txtQty.Text = (row.Cells[8].Text);
            txtPCardId.Text = (row.Cells[7].Text);
            txtNoOfRoll.Text = (row.Cells[9].Text);

            if (row.Cells[11].Text.Length > 3)
            {
                ddlSection.SelectedValue = (row.Cells[11].Text);
            }
            //if (txtPDate.Text.Length > 3)
            //{
            //    DateTime d = Convert.ToDateTime(txtPDate.Text);
            //    txtPDate.Text = d.Date.ToString();
            //}



            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Fabric_CL().IssueToDeyingDeleteActive(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Fabric_CL().DeliveryDeleteActive(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            EGreyFabricIssueToPCard c = new EGreyFabricIssueToPCard();

            string[] str = hdnFID.Value.ToString().Split('_');
            if (checkDecimal(txtQty.Text) == false)
            {
                txtQty.Text = "0";
            }
            c.ID = Convert.ToInt64(hdnCode.Value);
            c.FabricID = Convert.ToInt64(str[0]);
            c.GM = str[1];

            c.GSM = str[2];
            c.PCardNO = txtPCardId.Text;
            c.MCNo = ddlMCNo.Text;
            c.IssueQty = Convert.ToDouble(txtQty.Text);
            c.IssueDate = System.DateTime.Now;
            if (checkDecimal(txtNoOfRoll.Text) == false)
            {
                c.RollNo = 0;
            }
            else
            {
                c.RollNo = Convert.ToInt16(txtNoOfRoll.Text);
            }

            c.EntryUserID = USession.SUserID;
            c.Section = ddlSection.SelectedItem.Text;
            if (c.IssueQty > 0)
            {
                lblStatus.Text = new KNIT_all_operation().SaveKnitIssueToPCard(c);
            }

            loadGridData();
            //loadGridData();
            updPanel.Update();
            mpeProduct.Hide();

        }
        private bool checkDecimal(string s)
        {

            try
            {
                decimal v;
                v = Convert.ToDecimal(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            EGreyFabricIssueToPCard c = new EGreyFabricIssueToPCard();

            string[] str = hdnFID.Value.ToString().Split('_');
            if (checkDecimal(txtQty.Text) == false)
            {
                txtQty.Text = "0";
            }
            c.ID = Convert.ToInt64(hdnCode.Value);
            c.FabricID = Convert.ToInt64(str[0]);
            c.GM = str[1];

            c.GSM = str[2];
            c.ReqNo = txtReq.Text;
            c.PCardNO = txtPCardId.Text;
            c.MCNo = ddlMCNo.Text;
            c.IssueQty = Convert.ToDouble(txtQty.Text);
            c.IssueDate = System.DateTime.Now;
            if (checkDecimal(txtNoOfRoll.Text) == false)
            {
                c.RollNo = 0;
            }
            else
            {
                c.RollNo = Convert.ToInt16(txtNoOfRoll.Text);
            }

            c.EntryUserID = USession.SUserID;
            c.Section = ddlSection.SelectedItem.Text;
            string message = "";
            if (c.IssueQty > 0)
            {
                 message = new KNIT_all_operation().SaveKnitIssueToPCard(c);
            }

            lblStatus.Text = message.Substring(2);
            if (message.Substring(0, 1) == "0")
            {
                updPanel.Update();
                mpeProduct.Show();
            }
            else
            {
                txtQty.Text = "";
                txtPCardId.Text = "";
                updPanel.Update();
                mpeProduct.Show();
               
            }
            upStatus.Update();
        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            // txtYarn.Text = string.Empty;
            // txtYarnDesc.Text = string.Empty;

            txtQty.Text = "";
            txtPCardId.Text = "";


            mpeProduct.Show();
            
        }
        protected void ddlMCNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadFabric();
            mpeProduct.Show();
        }


        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            hdnCode.Value = "0";
            loadGridData();
            mpeProduct.Hide();
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            if (rblNormal.Items.FindByText("Active").Selected == true)
            {
                loadGridData();

            }
            else
            {
                loadDeletedData();
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var k = new KNIT_all_operation().GetSinglePCardInfo(txtPCardId.Text);
          if (k != null)
          {

              var val = new KNIT_all_operation().GetFabircDetailsByReqNo(k.ReqNo);
              if (val != null)
              {
                  hdnFID.Value = val.FID.ToString() + "_" + val.GM + "_" + val.GSM;
                  txtFabric.Text = val.Fabric1 + "  GM:" + val.GM + " GSM:" + val.GSM;
              }
              txtReq.Text = k.ReqNo;


              ddlMCNo.DataSource = new KNIT_all_operation().GetListOfMCByRefNo(k.ReqNo);
              ddlMCNo.DataValueField = "MCNo";
              ddlMCNo.DataTextField = "MCNo";
              ddlMCNo.DataBind();

             
          }
        }



    }
}
