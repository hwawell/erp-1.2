<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_SubContact_ReceivebyReqNo.aspx.cs" Inherits="ERP.KNIT_SubContact_ReceivebyReqNo" Title="KNIT_SubContact_ReceivebyReqNo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
                              
              .mGrid { 
    width: 100%; 
    background-color: #fff; 
    margin: 5px 0 10px 0; 
    border: solid 1px #525252; 
    border-collapse:collapse; 
}
.mGrid td { 
    padding: 2px; 
    border: solid 1px #c1c1c1; 
    color: #717171; 
}
.mGrid th { 
    padding: 4px 2px; 
    color: #fff; 
    background: #424242 url(grd_head.png) repeat-x top; 
    border-left: solid 1px #525252; 
    font-size: 0.7em; 
}
.mGrid .alt { background: #fcfcfc url(grd_alt.png) repeat-x top; }
.mGrid .pgr { background: #424242 url(grd_pgr.png) repeat-x top; }
.mGrid .pgr table { margin: 5px 0; }
.mGrid .pgr td { 
    border-width: 0; 
    padding: 0 6px; 
    border-left: solid 1px #666; 
    font-weight: bold; 
    color: #fff; 
    line-height: 12px; 
 }   
.mGrid .pgr a { color: #666; text-decoration: none; }
.mGrid .pgr a:hover { color: #000; text-decoration: none; }
 
    </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
    <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Script/jquery-1.8.2.js") %>"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Script/jquery.number.js") %>"></script>

 <script type="text/javascript">

     $(function() {
         $('#<%=btnSave.ClientID %>').click(function(evt) {
             // Validate the form and retain the result.
         var valuePer = 0.0;
         if (!isNaN(parseFloat($('#<%=txtKG.ClientID %>').val().replace(',', '')))) {
                  valuePer = valuePer + parseFloat($('#<%=txtKG.ClientID %>').val().replace(',', ''));
               }
               if (!isNaN(parseFloat($('#<%=txtKG0.ClientID %>').val().replace(',', '')))) {
                   valuePer = valuePer + parseFloat($('#<%=txtKG0.ClientID %>').val().replace(',', ''));
               }
               if (!isNaN(parseFloat($('#<%=txtKG1.ClientID %>').val().replace(',', '')))) {
                   valuePer = valuePer + parseFloat($('#<%=txtKG1.ClientID %>').val().replace(',', ''));
               }
               if (!isNaN(parseFloat($('#<%=txtKG2.ClientID %>').val().replace(',', '')))) {
                   valuePer = valuePer + parseFloat($('#<%=txtKG2.ClientID %>').val().replace(',', ''));
               }
               if (!isNaN(parseFloat($('#<%=txtKG3.ClientID %>').val().replace(',', '')))) {
                   valuePer = valuePer + parseFloat($('#<%=txtKG3.ClientID %>').val().replace(',', ''));
               }


               if (valuePer != $('#<%=txtTot.ClientID %>').val().replace(',','')) {
                   alert('Yarn Consumption (=' + valuePer + ' ) must be equal to Production Qty (' + $('#<%=txtTot.ClientID %>').val() + ')');
                 evt.preventDefault();
             }

         });

     });
   
     $(function() {
         $('#<%=txtUserPercent.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtTot.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtTot.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;


             totValue = $.number(totValue, 2);
             $('#<%=txtKG.ClientID %>').val(totValue);
            

         });
     });

     $(function() {
         $('#<%=txtUserPercent0.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtTot.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtTot.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;
             totValue = $.number(totValue, 2);
             $('#<%=txtKG0.ClientID %>').val(totValue);

         });
     });

     $(function() {
         $('#<%=txtUserPercent1.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtTot.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtTot.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;
             totValue = $.number(totValue, 2);
             $('#<%=txtKG1.ClientID %>').val(totValue);

         });
     });

     $(function() {
         $('#<%=txtUserPercent2.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtTot.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtTot.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;
             totValue = $.number(totValue, 2);
             $('#<%=txtKG2.ClientID %>').val(totValue);

         });
     });

     $(function() {
         $('#<%=txtUserPercent3.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtTot.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtTot.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;
             totValue = $.number(totValue, 2);
             $('#<%=txtKG3.ClientID %>').val(totValue);

         });
     });
    </script>
  
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<div>
   <div style="height:30px; " align="center">
     <h3> Sub Contact Grey Receive Information</h3>
   </div>
   <fieldset>
   <legend>Product Receive</legend>
   
 
    
      
       
    <div style="width: 20%; float: left" align="right">
        Req No :</div>
   <div style="width: 80%; float: left" align="left">
    
       <asp:TextBox ID="txtReqNo" runat="server"></asp:TextBox>
   
         <asp:Button ID="btnSearch" runat="server" onclick="btnSearch_Click" 
             Text="Search" />
          
   </div>
    <div style="width: 20%; float: left" align="right">
        Fabric :</div>
   <div style="width: 80%; float: left" align="left">
   <asp:UpdatePanel ID="upFabric" runat="server" UpdateMode="Conditional">
       <ContentTemplate>
           <asp:TextBox ID="txtFabric" runat="server" Width="307px" Enabled="False"></asp:TextBox>
           <asp:HiddenField ID="hdnFID" runat="server" />
       </ContentTemplate>
       <Triggers>
           <asp:AsyncPostBackTrigger ControlID="ddlFabricType" 
               EventName="SelectedIndexChanged" />
       </Triggers>
   </asp:UpdatePanel>
  
   </div>
    <div style="width: 20%; float: left" align="right">
        GM :</div>
   <div style="width: 15%; float: left" align="left">
   
       <asp:TextBox ID="txtGM" runat="server"></asp:TextBox>
   
   </div>
    <div style="width: 10%; float: left" align="right">
        GSM :</div>
   <div style="width: 55%; float: left" align="left">
   
       <asp:TextBox ID="txtGSM" runat="server"></asp:TextBox>
   
       Receive Date
       <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
   <cc1:CalendarExtender ID="txtTo_CalendarExtender" runat="server" Enabled="True" 
                        TargetControlID="txtDate">
                    </cc1:CalendarExtender>
   </div>
   
       
          <div style="width: 20%; float: left" align="right">
              Receive Qty (KG) :</div>
   <div style="width: 80%; float: left" align="left">
   
       <asp:TextBox ID="txtTot"   runat="server" MaxLength="20" TabIndex="102" 
                 Height="23px" Width="131px" Font-Bold="True"></asp:TextBox>
   
   </div>
    <div style="width: 20%; float: left" align="right">
        Sub Contact Company :</div>
   <div style="width: 80%; float: left" align="left">
    
       <asp:TextBox ID="txtSubContact" runat="server" Height="22px"  
           Width="547px"></asp:TextBox>
      
   </div>
    <div style="width: 20%; float: left" align="right">
        Notes :</div>
   <div style="width: 70%; float: left" align="left">
    
       <asp:TextBox ID="txtNotes" runat="server" Height="22px" TextMode="SingleLine" 
           Width="500px"></asp:TextBox>
      
   </div>
     <div style="width: 20%; float: left" align="right">
         Challan No :</div>
   <div style="width: 80%; float: left" align="left">
    
       <asp:TextBox ID="txtChallan" runat="server" Height="22px"  
           Width="547px"></asp:TextBox>
      
   </div>
     <div style="width: 20%; float: left" align="right">
         </div>
   <div style="width: 80%; float: left" align="left">
      
      
   </div>
   
   </fieldset>
  
  
   
   
   <div>
       <asp:Button ID="btnSave" runat="server" Text="Save" 
           Height="40px" Width="117px" onclick="btnSave_Click" />
       <asp:Button ID="btnCancel" runat="server" Text="Clear" Width="92px" 
           Height="40px" onclick="btnCancel_Click" />
           
            <asp:ImageButton ID="ibtnNew0" runat="server" Height="30px" 
           ImageUrl="~/Images/Rich Text Format.ico" onclick="ibtnNew_Click" 
           ToolTip="View Product Setup Entry" Width="39px" />
           
            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
   </div>
    <div>
   
                                    <asp:GridView 
                                ID="GridView1" runat="server" 
                                 AutoGenerateColumns="false"
                                GridLines="None"
                                 CssClass="mGrid"
                                PagerStyle-CssClass="pgr"
                                AlternatingRowStyle-CssClass="alt"
                                 Height="16px" 
                                ShowFooter="false"
                                Width="400px" PageSize="20" 
                          >
                             
                                 <RowStyle  />
                                <Columns>
                                    <asp:BoundField  HeaderText="ID" DataField="ID" SortExpression="ID" >
                                       <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                     <asp:BoundField  HeaderText="Receive Date" DataField="PDate" SortExpression="PDate" >
                                       <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                      <asp:BoundField  HeaderText="RollQty" DataField="RollQty" SortExpression="RollQty" >
                                       <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                      <asp:BoundField  HeaderText="SubContactName" DataField="SubContactName" SortExpression="SubContactName" >
                                       <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField  HeaderText="Challan" DataField="Challan" SortExpression="Challan" >
                                       <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                  <asp:TemplateField HeaderText="Delete"  HeaderStyle-HorizontalAlign="Left" >
                                    <ItemTemplate>
                                        <asp:Button ID="btnClose" runat="server" Text="Delete" 
                                            onclick="btnClose_Click1"
                                        />
                           
                                    </ItemTemplate>
                                        
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                             
<PagerStyle CssClass="pgr"></PagerStyle>
                             
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                                        <AlternatingRowStyle CssClass="alt" />
                            </asp:GridView>   
                            
                                </div>
   
   
   
</div>
</asp:Content>

