<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="DyedFab_Receive.aspx.cs" Inherits="ERP.DyedFab_Receive" Title="Dyed Fabric Receive Entry" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style4
        {
        width: 143px; text-align:right;
    }
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
        .style6
        {
            width: 22px;
            height: 2px;
        }
        .style7
        {
            width: 143px;
            height: 2px;
        }
        .style8
        {
            height: 2px;
        }
               
        .style9
        {
            width: 22px;
        }
               
    </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
      <link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>


    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <div class="grid" >
            <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>
                       <asp:Label ID="lblHeader" runat="server" Text=""></asp:Label></h2></center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid" 
                        style="overflow:auto; width: 850px; height: 2000px; background-color: "whitesmoke";">     
                <div style="height:25px;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                        AssociatedUpdatePanelID="updPanel">
                        <ProgressTemplate>
                            <img alt="" src="Images/loading.gif" style="height: 15px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    </div>
                      <asp:UpdatePanel ID="uprbl" runat="server" UpdateMode="Conditional">
                         <ContentTemplate>
                <div style="height: 40px; border-bottom-style: none; border-bottom-color: #003399;">
                    <div style="float: left">
                    <asp:ImageButton ID="ibtnNew0" runat="server" Height="30px" 
                        ImageUrl="~/Images/Rich Text Format.ico" onclick="ibtnNew_Click" 
                        ToolTip="New Unit Entry" Width="39px" />
                </div>
                <div align="center" style="height: 30px">
                    Order No: 
                    <asp:TextBox ID="txtOrderNo" runat="server"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" Text="Search" 
                        onclick="btnSearch_Click" />
               
               
                    </div>
                
                </div>
                <div style="height: 8px; border-bottom-style: groove; border-bottom-color: #003399;">
                
                </div>
                    <!-- Content Goes Here! -->
                    <asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <div style="overflow: auto; width: 850px">
                            <asp:GridView 
                                ID="gvProducts" runat="server" 
                                 AutoGenerateColumns="False" 
                                AllowPaging="True" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                onpageindexchanging="gvProducts_PageIndexChanging" PageSize="15">
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="�" LastPageText="�" />      
                                <Columns>
                               
               
                                    <asp:BoundField  HeaderText="ID" DataField="ID" /> 
                                    <asp:BoundField  HeaderText="OPID" DataField="OPID" /> 
                                 
                                    <asp:BoundField  HeaderText="Product" DataField="Product" /> 
                                    
                                    <asp:BoundField HeaderText="RcvQty" DataField="RcvQty" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                       <HeaderStyle CssClass="first" /><ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField HeaderText="Roll" DataField="Roll" HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                       <HeaderStyle CssClass="first" /><ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="InvoiceNo" DataField="InvoiceNo" HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                       <HeaderStyle CssClass="first" /><ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="TypeOfProduction" DataField="TypeOfProduction" HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                       <HeaderStyle CssClass="first" /><ItemStyle CssClass="first" />
                                    </asp:BoundField>
                               
                                     <asp:BoundField HeaderText="Notes" DataField="Notes" HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                       <HeaderStyle CssClass="first" /><ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField HeaderText="RcvDate" DataField="RcvDate" HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                       <HeaderStyle CssClass="first" /><ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                  
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/btn_edit.gif" 
                                                onclick="btnEdit_Click" Height="16px" />
                                            
                                           
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" 
                                                ImageUrl="~/Images/btn_delete.gif" onclick="btnDelete_Click" 
                                                Height="16px" 
                                            onclientclick="javascript:return confirm('Are you sure you want to delete this row?');" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                 
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>
                         </div>   
                            <asp:Button ID="btnHiddenProduct" runat="Server" Style="display:none"  />
                                 
                    <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlDataEntry" runat="server"  CssClass="modalBox"  
                        BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                        Width="550px" BorderWidth="2px">
                        <center>
                            <asp:Panel ID="ProductCaption" runat="server"   
                                Style="margin-bottom: 10px; cursor: hand;display:block;" 
                                BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                Receive</asp:Panel></center>
                            <cc1:CollapsiblePanelExtender ID="ProductCaption_CollapsiblePanelExtender" 
                                runat="server" Enabled="True" TargetControlID="ProductCaption">
                            </cc1:CollapsiblePanelExtender>
                        
                         
                         
                         
                        
                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                             
                             <tr>
                                <td class="style9">
                                    &nbsp;</td>
                                <td class="style4">
                                    Order Product</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlProduct" runat="server" Height="22px" Width="248px" 
                                        TabIndex="4">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                           
                            
                            
                            
                          
                            <tr>
                                <td class="style9">
                                    &nbsp;</td>
                                <td class="style4">
                                    Receive Qty :</td>
                                <td align="left">
                                 
                                   <asp:TextBox ID="txtReceiveQty" runat="server" style="margin-left: 0px" 
                                        Width="140px" TabIndex="7"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="style9">
                                    &nbsp;</td>
                                <td class="style4">
                                    No of Roll :</td>
                                <td align="left">
                                    <asp:TextBox ID="txtRoll" runat="server" style="margin-left: 0px" 
                                        Width="180px" TabIndex="7"></asp:TextBox>
                                    </td>
                            </tr>
                           
                            <tr>
                                <td class="style9">
                                    &nbsp;</td>
                                <td class="style4">
                                    Invoice No :</td>
                                <td align="left">
                                    <asp:TextBox ID="txtInvoiceNo" runat="server" style="margin-left: 0px" 
                                        Width="242px" TabIndex="9"></asp:TextBox></td>
                            </tr>
                        
                            <tr>
                                <td class="style9">
                                    &nbsp;</td>
                                <td class="style4">
                                    Receive Date :</td>
                                <td align="left">
                                    <asp:TextBox ID="txtReceiveDate" runat="server" style="margin-left: 0px" 
                                        Width="129px" TabIndex="11"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtReceiveDate_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtReceiveDate">
                    </cc1:CalendarExtender>
                                        </td>
                            </tr>
                       
                           
                            <tr>
                                <td class="style9">
                                    &nbsp;</td>
                                <td class="style4">
                                    Notes :</td>
                                <td align="left">
                                    <asp:TextBox ID="txtNotes" runat="server" style="margin-left: 0px" 
                                        Width="242px" TabIndex="14"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="style9">
                                    &nbsp;</td>
                                <td class="style4">
                                    Receive For :</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlSection" runat="server" Height="22px" TabIndex="4" 
                                        Width="248px">
                                        <asp:ListItem>DEYING</asp:ListItem>
                                         <asp:ListItem>PRINTING</asp:ListItem>
                                        <asp:ListItem>RAISING</asp:ListItem>
                                       
                                        <asp:ListItem>PACKING</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="style6">
                                    </td>
                                <td class="style7">
                                    <asp:HiddenField ID="hdnCode" runat="server" Value="-1" />
                                </td>
                                <td class="style8">
                                    <asp:Button ID="btnSave" OnClientClick=" return validate()" runat="server" 
                                        Height="24px"  Text="Save" 
                                        Width="83px" onclick="btnSave_Click" Font-Names="Georgia" TabIndex="15" />
                                   
                                    
                                   
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="60px" 
                                        Height="24px" Font-Names="Georgia" onclick="btnCancel_Click" 
                                        TabIndex="17" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style6">
                                    &nbsp;</td>
                                <td class="style7">
                                    &nbsp;</td>
                                <td class="style8">
                                    <asp:Label ID="lblResult" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style9">
                                    &nbsp;</td>
                                <td class="style4">
                                    &nbsp;</td>
                                <td>
                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                        AssociatedUpdatePanelID="updPanel" DisplayAfter="10">
                                    <ProgressTemplate>
                                      <img id="dd" alt="Loading.." src="Images/loading.gif" />
                                    </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                        </table>
                        
                        
                        
                    </asp:Panel>
                        </ContentTemplate>
                      
                    </asp:UpdatePanel>      
                                 </ContentTemplate>   
                      </asp:UpdatePanel>
                </div></div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>      
    </div>
</asp:Content>
