<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ProcessCard.aspx.cs" Inherits="ERP.WebForm11" Title="Process Card Entry Form" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="AjaxControls" namespace="AjaxControls" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style5
        {
            width: 20px;
        }
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
        .style6
        {
            width: 20px;
            height: 2px;
        }
        .style7
        {
            width: 128px;
            height: 2px;
        }
        .style8
        {
            height: 2px;
        }
               
        .style9
        {
            width: 128px;
        }
               
    </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
     <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
	<script type="text/javascript">
	    $(function() {
	        $(".tb").autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                url: "DataLoad.asmx/GetOrderList",
	                    data: "{ 'PINO': '" + request.term + "' }",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    dataFilter: function(data) { return data; },
	                    success: function(data) {
	                        response($.map(data.d, function(item) {
	                            return {
	                            value: item.OrderNo
	                            }
	                        }))
	                    },
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                        
	                    }
	                });
	            },
	            minLength: 2
	        });
	    });

	    $(document).ready(function() {
	        $('#<%=txtGSM.ClientID %>').bind('keyup', function(e) {

	            var wid = 0.0;
	            var GSM = 0.0;
	            var GM = 0.0;
	            if (isNaN(parseFloat($('#<%=txtwidth.ClientID %>').val()))) {
	                wid = 0;
	            } else {
	            wid = parseFloat($('#<%=txtwidth.ClientID %>').val().replace(',', ''));
	            }

	            if (isNaN(parseFloat($(this).val()))) {
	                GSM = 0;
	            } else {
	            GSM = $(this).val();
	            }
	            GM = (GSM * 1.4 * wid) / 60;

	            $('#<%=txtWeight.ClientID %>').val(GM | 0);
	            return false;
	        });
	        $('#<%=txtwidth.ClientID %>').bind('keyup', function(e) {

	            var wid = 0.0;
	            var GSM = 0.0;
	            var GM = 0.0;
	            if (isNaN(parseFloat($('#<%=txtGSM.ClientID %>').val()))) {
	                wid = 0;
	            } else {
	            wid = parseFloat($('#<%=txtGSM.ClientID %>').val().replace(',', ''));
	            }

	            if (isNaN(parseFloat($(this).val()))) {
	                GSM = 0;
	            } else {
	                GSM = $(this).val();
	            }
	            GM = (GSM * 1.4 * wid) / 60;

	            $('#<%=txtWeight.ClientID %>').val(GM | 0);
	            return false;

	        });
	    });
	    


	
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <div class="grid" >
            <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>List of Process Cards</h2></center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid" 
                        style="overflow:auto; width: 739px; height: auto; background-color:"
                        whitesmoke";">     
                <div style="height:25px;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                        AssociatedUpdatePanelID="updPanel">
                        <ProgressTemplate>
                            <img alt="" src="Images/loading.gif" style="height: 15px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    </div>
                    
                 <div align="center">
                    <asp:Panel ID="pnlDataEntry" runat="server"    CssClass="modalBox"  
                        BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                        Width="693px" BorderWidth="2px">
                        <center>
                            <asp:Panel ID="ProductCaption" runat="server"   
                                Style="margin-bottom: 10px; cursor: hand;" 
                                BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                Process Card Entry Form</asp:Panel></center>
                            
                           <asp:UpdatePanel ID="UpEntry" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" >
                        <ContentTemplate>
                          <table cellpadding="0" cellspacing="2" style="width:98%;">
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style9" align="right">
                                    Processcard</td>
                                <td align="left">
                            
                                    <asp:TextBox ID="txtProcessCard" runat="server" Font-Bold="True" 
                                        Font-Names="Courier New" Font-Size="Large" MaxLength="20" 
                                        TabIndex="1" Width="150px"></asp:TextBox>
                                    
                                </td>
                            </tr>
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Type</td>
                                  <td align="left">
                                      <asp:DropDownList ID="ddlPType" runat="server" AutoPostBack="True" 
                                          Height="25px" onselectedindexchanged="ddlOrder_SelectedIndexChanged" 
                                          TabIndex="2" Width="235px">
                                          <asp:ListItem Selected="True">POLYESTER</asp:ListItem>
                                          <asp:ListItem>COT/CVC</asp:ListItem>
                                      </asp:DropDownList>
                                      <cc1:ListSearchExtender ID="ddlPType_ListSearchExtender" runat="server" 
                                          Enabled="True" IsSorted="true" PromptText="  --" TargetControlID="ddlPType">
                                      </cc1:ListSearchExtender>
                                  </td>
                              </tr>
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      <asp:Label ID="Label3" runat="server" Font-Names="Georgia" Text="Order No:"></asp:Label>
                                  </td>
                                  <td align="left">
                                      <asp:TextBox ID="tbAuto" runat="server" class="tb" TabIndex="3"></asp:TextBox>
                                      <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                          onclick="btnSearch_Click" TabIndex="4" />
                                  </td>
                              </tr>
                            <tr>
                                <td class="style5">
                                    <div style="height: 8px; border-bottom-style: groove; border-bottom-color: #003399;">
                                    </div>
                                </td>
                                <td class="style9" align="right">
                                    <asp:Label ID="Label1" runat="server" Font-Names="Georgia" Text="Order Product"></asp:Label>
                                </td>
                                <td align="left">
                                  <asp:UpdatePanel ID="upDes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlDesc" runat="server" Height="22px" Width="399px" 
                                            AutoPostBack="True" onselectedindexchanged="ddlDesc_SelectedIndexChanged" 
                                            TabIndex="5">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtDescNotes" Width="220px" runat="server"></asp:TextBox>
                                    </ContentTemplate>
                                      <Triggers>
                                          <asp:AsyncPostBackTrigger ControlID="btnSearch" 
                                              EventName="Click" />
                                      </Triggers>
                                   </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style9" align="right">
                                    <asp:Label ID="Label8" runat="server" Font-Names="Georgia" Text="Color"></asp:Label>
                                </td>
                                <td align="left">
                                   <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                    <asp:DropDownList ID="ddlColor" runat="server" Height="20px" Width="235px" 
                                        AutoPostBack="True" TabIndex="6" 
                                            onselectedindexchanged="ddlColor_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    </ContentTemplate>
                                       <Triggers>
                                           <asp:AsyncPostBackTrigger ControlID="ddlDesc" 
                                               EventName="SelectedIndexChanged" />
                                       </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td align="right" class="style9">
                                    Req No</td>
                                <td align="left">
                                    <asp:TextBox ID="txtReqNo" runat="server" 
                                        ontextchanged="txtRequired_TextChanged" TabIndex="7" Width="127px"></asp:TextBox>
                                    <asp:Button ID="btnSearch0" runat="server" onclick="btnSearch0_Click" 
                                        TabIndex="4" Text="View Fabric" Width="83px" />
                                    <asp:TextBox ID="txtFabric" runat="server" ReadOnly="True" TabIndex="13" 
                                        Width="288px"></asp:TextBox>
                            </tr>
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Booking Qty</td>
                                  <td align="left">
                                    <asp:UpdatePanel ID="upBalance" UpdateMode="Conditional" runat="server">
                                    <ContentTemplate>
                                    
                                   
                                      <asp:TextBox ID="txtBookingQty" runat="server" BackColor="#DBDBDB" 
                                          MaxLength="20" TabIndex="100" Height="23px" Width="101px"></asp:TextBox>
                                        Available Qty<asp:TextBox ID="txtAvailableQty" runat="server" 
                                          BackColor="#DBDBDB" MaxLength="20" TabIndex="101" Font-Bold="True" 
                                            Height="23px" Width="107px"></asp:TextBox>
                                          
                                       </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlColor" 
                                                EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                  </td>
                              </tr>
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Required Qty</td>
                                  <td align="left">
                                      <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                      <ContentTemplate>
                                      
                                     
                                       <asp:TextBox ID="txtRequired" runat="server" AutoPostBack="True" 
                                              ontextchanged="txtRequired_TextChanged" TabIndex="10" Width="127px" 
                                           ></asp:TextBox>
                                          Extra Prod.(%) <asp:TextBox ID="txtExtraPer" runat="server" AutoPostBack="True" Height="22px" 
                                              MaxLength="2" ontextchanged="txtExtraPer_TextChanged" TabIndex="8" 
                                              Width="45px"></asp:TextBox>
                                        </ContentTemplate>
                                      </asp:UpdatePanel>
                                           
                                  </td>
                              </tr>
                               <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Production Qty</td>
                                  <td align="left">
                                      <asp:UpdatePanel ID="upProduction" runat="server" UpdateMode="Conditional">
                                      <ContentTemplate>
                                      
                                     
                                       <asp:TextBox ID="txtProductionQty" runat="server" TabIndex="105" Width="127px" 
                                           ReadOnly="True"></asp:TextBox>
                                          Lotno<asp:TextBox ID="txtLotNo" runat="server" Height="22px" TabIndex="11" 
                                           Width="79px"></asp:TextBox>
                                        </ContentTemplate>
                                      </asp:UpdatePanel>
                                           
                                  </td>
                              </tr>
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9"> M/C Capacity 
                                      
                                  </td>
                                  <td align="left">
                                  <asp:UpdatePanel ID="upBalQty" runat="server" UpdateMode="Conditional">
                                      <ContentTemplate>
                                         
                                         
                                          <asp:TextBox ID="txtMCCapacity" runat="server" 
                                              AutoPostBack="True" ontextchanged="txtMCCapacity_TextChanged" TabIndex="12" 
                                              Width="87px"></asp:TextBox>
                                          Balance Qty
                                      
                                         <asp:TextBox ID="txtBalanceQty" runat="server" 
                                          BackColor="#DBDBDB" Font-Bold="True" Height="25px" MaxLength="20" TabIndex="102" 
                                          Width="98px"></asp:TextBox>
                                      </ContentTemplate>
                                      </asp:UpdatePanel>
                                      
                                  </td>
                              </tr>
                             
                            <tr>
                                <td class="style5">
                                    </td>
                                <td class="style9" align="right">
                                    Width</td>
                                <td align="left">
                                  <asp:UpdatePanel ID="upWid" runat="server" UpdateMode="Conditional" >
                                  <ContentTemplate>
                                  
                                
                                    <asp:TextBox ID="txtwidth" runat="server" Width="79px" TabIndex="11"></asp:TextBox>
                                        &nbsp; GSM
                                        <asp:TextBox ID="txtGSM" runat="server" 
                                             TabIndex="12" Width="87px"></asp:TextBox>
                                        Weight(GM)<asp:TextBox ID="txtWeight" runat="server" ReadOnly="true" TabIndex="140" Width="87px"></asp:TextBox>
                                       
                                       </ContentTemplate>
                                  </asp:UpdatePanel> 
                                </td>
                            </tr>
                            
                            
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Descriptionion</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtDescription" runat="server" Width="480px" TabIndex="13"></asp:TextBox>
                                  </td>
                              </tr>
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Notes</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtNotes" runat="server" Width="479px" TabIndex="14" 
                                          ontextchanged="txtNotes_TextChanged"></asp:TextBox>
                                  </td>
                              </tr>
                            
                            
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      YDS Formula </td>
                                  <td align="left">
                                      <asp:TextBox ID="txtCalculation" runat="server" TabIndex="15" Width="100px"></asp:TextBox>
                                      (value for calculation )</td>
                              </tr>
                            
                            
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Date</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtDate" runat="server" TabIndex="16"></asp:TextBox>
                                      <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                                          Enabled="True" Format="MM/dd/yyyy" TargetControlID="txtDate">
                                      </cc1:CalendarExtender>
                                  </td>
                              </tr>
                            
                            
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Packing Style</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtPackingStyle" runat="server" TabIndex="17" Width="272px"></asp:TextBox>
                                  </td>
                              </tr>
                              
                              <tr>
                                  <td class="style5">
                                      &nbsp;</td>
                                  <td align="right" class="style9">
                                      Yard Cal Text</td>
                                  <td align="left">
                                      <asp:TextBox ID="txtYardCal" runat="server" TabIndex="18" Width="272px" 
                                          style="margin-left: 2px"></asp:TextBox>
                                  </td>
                              </tr>
                              
                            
                            
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style9">
                                    &nbsp;</td>
                                <td>
                                    <asp:Label ID="lblMsg" runat="server" Font-Bold="False" Font-Names="Arial" 
                                        Font-Size="Smaller" ForeColor="#CC3300"></asp:Label>
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td class="style6">
                                    </td>
                                <td class="style7">
                                    <asp:HiddenField ID="hdnCode" runat="server" Value="-1" />
                                </td>
                                <td class="style8">
                                    <asp:HiddenField ID="hdnFID" runat="server" />
                                    <asp:Button ID="btnSave" runat="server" Height="24px"  Text="Save" 
                                        Width="60px" onclick="btnSave_Click" Font-Names="Georgia" TabIndex="19" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="60px" 
                                        Height="24px" Font-Names="Georgia" onclick="btnCancel_Click" 
                                        TabIndex="23" />
                                </td>
                            </tr>
                        </table>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        
                    </asp:Panel>
                 </div>
                <div style="height: 40px; border-bottom-style: none; border-bottom-color: #003399;">
                    <div style="float: left">
                                      <div align="center" style="height: 30px; width: 684px;" 
                            __designer:mapid="3f2">
                                         
                                          <asp:RadioButtonList ID="rblNormal" runat="server" AutoPostBack="True" 
                                              Font-Bold="True" Font-Names="Georgia" Font-Size="Small" Height="12px" 
                                              onselectedindexchanged="rblNormal_SelectedIndexChanged" 
                                              RepeatDirection="Horizontal" Visible="False" Width="228px">
                                              <asp:ListItem>Active</asp:ListItem>
                                              <asp:ListItem>Deleted</asp:ListItem>
                                          </asp:RadioButtonList>
                                          <asp:Button ID="btnload" runat="server" onclick="btnload_Click" 
                                              Text="Load PCard" />
                                          &nbsp;&nbsp;&nbsp;&nbsp; Pcard:
                                        
                                          <asp:TextBox ID="txtLastPCard" 
                                              runat="server" Height="21px" 
                                              Width="96px"></asp:TextBox>
                                           
                                              <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                                              Text="Print PCard (Data)" />                                         
                                          <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                                              Text="Print PCard" />
                                         
                                          <asp:Button ID="btnEditPCard" runat="server" Text="Edit PCard" 
                                              onclick="btnEditPCard_Click" />
                                      </div>
                </div>
                
                </div>
                    <!-- Content Goes Here! -->
                    <asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView 
                                ID="gvProducts" runat="server" 
                                 AutoGenerateColumns="False" 
                                AllowPaging="True" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="23px" 
                                onpageindexchanging="gvProducts_PageIndexChanging" PageSize="5" 
                                onselectedindexchanged="gvProducts_SelectedIndexChanged">
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="�" LastPageText="�" />      
                                <Columns>
                                    
                                    <asp:BoundField 
                                        HeaderText="SL" DataField="SL" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PINO" HeaderText="Order No" />
                                    <asp:BoundField 
                                        HeaderText="Process Card" DataField="PCardNo" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                     <asp:BoundField DataField="width" HeaderText="width" >
                                     <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>   
                                      
                                    <asp:BoundField DataField="Weight" HeaderText="Weight" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>  
                                      
                                    <asp:BoundField DataField="ProductionQty" HeaderText="ProductionQty" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField>   
                                      
                                    <asp:BoundField DataField="LotNo" HeaderText="LotNo" >
                                    <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                     </asp:BoundField> 
                                    <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" 
                                                ImageUrl="~/Images/btn_delete.gif" onclick="btnDelete_Click" 
                                                Height="16px" 
                                            onclientclick="javascript:return confirm('Are you sure you want to delete this row?');" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                      <ItemTemplate>
                                     <asp:ImageButton ID="btnActive" runat="server" 
                                                ImageUrl="~/Images/HandleHand.png" onclick="btnActive_Click" 
                                                style="width: 15px" Height="16px" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                            <asp:Button ID="btnHiddenProduct" runat="Server" Style="display:none"  />
                                 
              <%--      <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>--%>
                    
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="rblNormal" 
                                EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnload" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnEditPCard" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>                     
    
                </div></div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>    
    </div></div>
</asp:Content>
