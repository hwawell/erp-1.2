<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="ERP.WebForm19" Title="Summary Reports" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
	<script type="text/javascript">
	    $(function() {
	        $(".tb").autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                url: "DataLoad.asmx/GetOrderList",
	                    data: "{ 'PINO': '" + request.term + "' }",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    dataFilter: function(data) { return data; },
	                    success: function(data) {
	                        response($.map(data.d, function(item) {
	                            return {
	                            value: item.OrderNo
	                            }
	                        }))
	                    },
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                        
	                    }
	                });
	            },
	            minLength: 2
	        });
	    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
  
    <div align="left">
     <div style="float: left; width: 30%; height:191px; display: inline;">
      <h3>Order Details Report</h3>
            <div style="float: left; width: 30%; height:auto">
             Order No
            </div>
            
            <div style="float: left; width: 70%; height:auto">
             
              <asp:TextBox ID="tbAuto0" class="tb" runat="server">
             </asp:TextBox>
            </div>
             <div style="float: left; width: 30%; height:auto">
               .
            </div>
            <div style="float: left; width: 70%; height:96px">
            
               <asp:RadioButtonList ID="rdoSum" runat="server" 
               RepeatDirection="Vertical" Width="200px">
               <asp:ListItem Selected="True">Deying Details Report</asp:ListItem>
               <asp:ListItem>Packing Details Report</asp:ListItem>
               <asp:ListItem>Delivery Details Report</asp:ListItem>
           </asp:RadioButtonList>
            </div>
             <div style="float: left; width: 30%; height:auto"> .
            </div>
             <div style="float: left; width: 70%; height:auto">
                 <asp:Button ID="btnReport" runat="server" Text="Show Details Report" 
               onclick="btnReport_Click" />
            </div>
           
      </div>
     <div style="float: left; width: 27%; height:188px; display: inline;">
        <center> <h3>Daily Report</h3></center>
         <div style="float: left; width: 30%; height:auto" align="right">
             Date : 
         </div>
         <div style="float: left; width: 70%; height:auto">
             <asp:TextBox ID="txtCurrentDate" runat="server"></asp:TextBox>
                  <cc1:CalendarExtender ID="CalendarExtender1dd" runat="server" 
                  Enabled="True" TargetControlID="txtCurrentDate">
              </cc1:CalendarExtender>
          </div>
          <div style="float: left; width: 30%; height:auto">
             .
         </div>
         
         <div style="float: left; width: 70%; height:95px">
              <asp:RadioButtonList ID="rdoDaily" runat="server" 
                   RepeatDirection="Vertical" Width="200px">
                   <asp:ListItem Selected="True">Daily Deying Report</asp:ListItem>
                   <asp:ListItem>Daily Packing Report</asp:ListItem>
                   
                   <asp:ListItem>Daily Delivery Report</asp:ListItem>
                   
               </asp:RadioButtonList>
         </div>
         <div style="float: left; width: 30%; height:auto">
             .
         </div>
         
         <div style="float: left; width: 70%; height:auto">
             <asp:Button ID="btnDaily" runat="server" Text="Show Daily Report" 
               onclick="btnDaily_Click" />
         </div>
     </div>
      <div style="float: left; width: 40%; height:183px;">
         <h3>Summary Report</h3>
         
         <div style="float: left; width: 20%; height:auto">
             Order
         </div>
         
         <div style="float: left; width: 80%; height:auto">
             
              <asp:TextBox ID="tbAuto" class="tb" runat="server">
             </asp:TextBox></div>
         
          <div style="float: left; width: 20%; height:auto">
             .
         </div>
          <div style="float: left; width: 80%; height:auto">
              <asp:RadioButtonList ID="rdoSummary" runat="server" 
               RepeatDirection="Vertical" Width="300px">
               <asp:ListItem Selected="True">Deying &amp; Packing Summary</asp:ListItem>
               <asp:ListItem>Delivery Summary</asp:ListItem>
               
                </asp:RadioButtonList>
         </div>
          <div style="float: left; width: 20%; height:auto">
             .
         </div>
         
          <div style="float: left; width: 80%; height:auto">
             <asp:Button ID="btnDaily0" runat="server" Text="Show Summary Report" 
               onclick="btnDaily0_Click" />
         </div>
         
        
       
         
     </div>
        <div style="clear:both;">
        
        </div>
        </div>
   
  
   
  
</asp:Content>
