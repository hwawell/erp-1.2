﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;

namespace ERP
{
    public partial class YarnLCBookingUI : System.Web.UI.Page
    {
         protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              

                //if (new Utilities_DL().CheckSecurity("503", USession.SUserID) == true)
                //{
                   
                    
                    loadYarn();
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
        private void  loadYarn()
        {
            ddlCountry.DataSource = new Yarn_DL().GetYarnCountry();
            ddlCountry.DataValueField = "CountryOrSupplier";
            ddlCountry.DataTextField = "CountryOrSupplier";
            ddlCountry.DataBind();

            ddlSupplier.DataSource = new Yarn_DL().GetYarnSupplier();
            ddlSupplier.DataValueField = "CountryOrSupplier";
            ddlSupplier.DataTextField = "CountryOrSupplier";
            ddlSupplier.DataBind();


          
            LoadYarnGroup();
        }
        private void LoadYarn(Int64 ygID)
        {
            ddlYarn.DataSource = new Yarn_DL().GetYarnByYarnGroup(ygID);
            ddlYarn.DataValueField = "YID";
            ddlYarn.DataTextField = "YarnName";
            ddlYarn.DataBind();
            
        }
        private void LoadYarnGroup()
        {


            ddlYarnGroup.DataSource = new Yarn_DL().GetActiveYarnGroup();
            ddlYarnGroup.DataValueField = "ID";
            ddlYarnGroup.DataTextField = "YarnGroupName";
            ddlYarnGroup.DataBind();
            ddlYarnGroup.Items.Add("--Select--");
            ddlYarnGroup.Text = "--Select--";


        }

        private void loadGridData(string LCNo)
        {


            List<EYarnLCBooking> li = new Yarn_DL().GetYarnLCBooking(Common.GetNumber(txtFrom.Text), Common.GetNumber(txtTo.Text), LCNo.Trim());


            if (li != null && li.Count > 0)
            {
                lblTotal.Text = li.Sum(l => l.YarnTotalLCValue).ToString();
                ddlCountry.Text = li[0].Country;
                ddlSupplier.Text = li[0].Supplier;
                txtNotes.Text = li[0].Notes;
                txtPDate.Text = li[0].Date;
                txtLCNO.Text = li[0].LCNo;
                txtImportPINo.Text = li[0].ImportPINo;

                gvProducts.DataSource = li;
                gvProducts.DataBind();
            }
            else
            {
                gvProducts.DataSource = null;
                gvProducts.DataBind();
            }


        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            lblMsg.Text = string.Empty;
            btnAdd.Text = "Edit";
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnValue.Value = row.Cells[0].Text;
            if (row.Cells[2].Text != null && row.Cells[2].Text != "&nbsp;")
            {
                ddlYarnGroup.SelectedValue = row.Cells[2].Text;

                LoadYarn(Convert.ToInt32(row.Cells[2].Text));
                ddlYarn.SelectedValue = row.Cells[1].Text;

            }
           // ddlSupplier.Text = row.Cells[4].Text;
           // ddlCountry.Text = row.Cells[5].Text;
            txtLCNO.Text = row.Cells[6].Text;
            txtTotQty.Text = row.Cells[7].Text;
            txtTotPrice.Text = row.Cells[8].Text;
            
           
            //txtYarn.Text = (row.Cells[2].Text);
            //txtYarnDesc.Text = (row.Cells[3].Text.ToString().Trim());


            UpEntry.Update();

           
        }

       

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            if (Convert.ToDecimal(row.Cells[8].Text) == 0)
            {
                new Yarn_DL().DeleteYarnLCBooking(Convert.ToInt64(row.Cells[0].Text), USession.SUserID);

                loadGridData(txtSearchLC.Text);
            }
        }

     

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData(txtSearchLC.Text);
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
      
        protected void btnSave_Click(object sender, EventArgs e)
        {

            
            
        }


        private bool CheckEntry()
        {
            
             if (ddlYarn.Text == "--Select--")
            {
                lblMsg.Text = "Select Yarn";
                return false;
            }
            else if (Common.GetDecimal(ddlYarn.SelectedValue) == 0)
            {
                lblMsg.Text = "Select Yarn";
                return false;
            }
             else if (Common.GetDecimal(txtTotPrice.Text) == 0)
             {
                 lblMsg.Text = "Give LC Price of the Yarn";
                 return false;
             }
             else if (Common.GetDecimal(txtTotQty.Text) == 0)
             {
                 lblMsg.Text = "Give Yarn Qty";
                 return false;
             }
            else
            {
                return true;
            }
        }
     

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
            lblMsg.Text = "";
         
        }

        protected void gvProducts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnload_Click(object sender, EventArgs e)
        {
            loadGridData(txtSearchLC.Text);
        }
        private void Clear()
        {

            btnAdd.Text = "Add";
            hdnValue.Value = "0";
            txtTotQty.Text = "";
            txtTotPrice.Text = "";
           // txtInvoiceNo.Text = "";
           
            
            //ddlYarn.Text = "--Select--";
            //ddlYarn.Focus();
            UpEntry.Update();
        }

        protected void ddlYarnGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
            else
            {
                Int64 GID = Convert.ToInt64(ddlYarnGroup.SelectedValue);
                LoadYarn(GID);
                UpEntry.Update();
            }
        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            
           // hdnCode.Value = "0";
           // txtYarn.Text = string.Empty;
            //txtYarnDesc.Text = string.Empty;
            Clear();
            lblMsg.Text = "";
            updPanel.Update();
            ddlYarnGroup.Focus();
            
            //txtYarn.Focus();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (CheckEntry() == true)
            {

                try
                {
                    YarnLCBooking c = new YarnLCBooking();
                    c.ID = Common.GetNumber(hdnValue.Value);
                    c.YID = Convert.ToInt32(ddlYarn.SelectedValue);



                    c.LCQty = Convert.ToDecimal(txtTotQty.Text);
                    c.ODate = Convert.ToDateTime(txtPDate.Text);
                    c.Supplier = ddlSupplier.Text.ToString();
                    c.CountryOfOrigin = ddlCountry.Text.ToString();
                    c.ImportPINO = txtImportPINo.Text;
                    c.TotalLCValue = Convert.ToDecimal(txtTotPrice.Text);

                    //c.LotNo = txtLOTNO.Text.ToString();
                    c.LCNo = txtLCNO.Text.ToString();

                    c.Notes = txtNotes.Text;




                    c.IsAllReceived = false;
                    c.EMode = "E";
                    c.EntryID = USession.SUserID;


                    lblMsg.Text = new Yarn_DL().SaveYarnLCBooking(c);


                    //txtPDate.Text = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year;
                    Clear();



                }
                catch (Exception ex)
                {
                    lblMsg.Text = ex.Message.ToString();
                }

                loadGridData(txtLCNO.Text);
                upALl.Update();
                UpEntry.Update();
            }
        }

        private void loadListData(string LCNo)
        {



            

        }
        protected void btnOperation_Click(object sender, EventArgs e)
        {
           
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
              
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

           

            Clear();
            lblMsg.Text = "";
            updPanel.Update();
            ddlYarnGroup.Focus();
        }
    
    }
}
