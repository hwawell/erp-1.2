﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;

namespace ERP
{
    public partial class INV_StoreTransfer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (new Utilities_DL().CheckSecurity("502", USession.SUserID) == true)
                //{

                loadGridData();
                HideControl();
                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
        private void HideControl()
        {

            if (Request.QueryString["INVTypeID"] == "2")
            {
              
            }
        }
        private void LoadBasicData()
        {
            Inventory_DL obj = new Inventory_DL();

            IQueryable li = obj.GetUnit(Convert.ToInt32(Request.QueryString["INVTypeID"]));

           

            ddlUnit.DataSource = li;
            ddlUnit.DataTextField = "UnitName";
            ddlUnit.DataValueField = "ID";
            ddlUnit.DataBind();

            IQueryable liStore = obj.GetStore(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlStore.DataSource = liStore;
            ddlStore.DataTextField = "StoreName";
            ddlStore.DataValueField = "ID";
            ddlStore.DataBind();


            ddlStore0.DataSource = liStore;
            ddlStore0.DataTextField = "StoreName";
            ddlStore0.DataValueField = "ID";
            ddlStore0.DataBind();

          

            // ddlGroup0.Items.Insert(0, "NONE");



        }
        private void loadGridData()
        {


            string Head = new Inventory_DL().GetInventoryType(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            this.Title = Head + " - Inventory Transfer";
            lblHeader.Text = "Inventory Transfer Entry for " + Head;

            LoadGroup();

            LoadBasicData();

            loadGrid();

        }
        private void loadGrid()
        {
            gvProducts.DataSource = new Inventory_DL().GetInventoryTransfer(Convert.ToInt32(Request.QueryString["INVTypeID"]), Convert.ToInt32(ddlGroup.SelectedValue), Convert.ToInt32(ddlStore.SelectedValue));
            gvProducts.DataBind();
        }


        private void LoadGroup()
        {
            Inventory_DL objDL = new Inventory_DL();
            ddlGroup.DataSource = objDL.GetItemGroup(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlGroup.DataTextField = "GroupName";
            ddlGroup.DataValueField = "SL";
            ddlGroup.DataBind();

        }


        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {

            
        //<asp:BoundField  HeaderText="ID" DataField="ID" /> 
        //<asp:BoundField  HeaderText="ItemID" DataField="ItemID" /> 
        //<asp:BoundField  HeaderText="FromStoreID" DataField="FromStoreID" /> 
        //<asp:BoundField  HeaderText="ToStoreID" DataField="ToStoreID" /> 
        //<asp:BoundField  HeaderText="IsssueID" DataField="IsssueID" /> 
        //<asp:BoundField  HeaderText="ReceiveID" DataField="ReceiveID" /> 
        
            lblResult.Text = string.Empty;
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            hdnIssueID.Value = row.Cells[4].Text;
            hdnReceiveID.Value = row.Cells[5].Text;

            txtItemID.Text = row.Cells[1].Text;
            txtItem.Text = row.Cells[6].Text;
            txtItem1.Text = row.Cells[6].Text;
           
            ddlUnit.SelectedItem.Text = row.Cells[9].Text;
            txtTotalQty.Text = row.Cells[8].Text;


           
            txtReceiveDate.Text = row.Cells[10].Text;
            txtReq.Text = row.Cells[11].Text;
            txtNotes.Text = row.Cells[12].Text;


            ddlStore.SelectedValue = row.Cells[2].Text;
            ddlStore0.SelectedValue = row.Cells[3].Text;
            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            txtItemID.Text = row.Cells[1].Text;
            txtItem.Text = row.Cells[4].Text;
            txtItem1.Text = row.Cells[4].Text;
            
            ddlUnit.Text = row.Cells[7].Text;
            txtTotalQty.Text = row.Cells[8].Text;


           

            txtNotes.Text = row.Cells[15].Text;

            
        }

       

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGrid();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                if (e.Row.Cells.Count > 2)
                {
                    e.Row.Cells[0].Visible = false;
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[2].Visible = false;
                    e.Row.Cells[3].Visible = false;
                    e.Row.Cells[4].Visible = false;
                    e.Row.Cells[5].Visible = false;
                   
                }


            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            SaveData();
            mpeProduct.Hide();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
            mpeProduct.Show();

        }

        private bool SaveData()
        {
            try
            {

                lblResult.Text = string.Empty;
                EINV_Transfer c = new EINV_Transfer();
                c.ID = Convert.ToInt32(hdnCode.Value);
                c.ItemID = Convert.ToInt32(txtItemID.Text);
               

                c.FromStoreID = Convert.ToInt32(ddlStore.SelectedValue);
                c.ToStoreID = Convert.ToInt32(ddlStore0.SelectedValue);
              
                c.Unit = ddlUnit.SelectedItem.Text;
                c.TransferQty = Convert.ToDouble(txtTotalQty.Text);
                c.Tdate = Convert.ToDateTime(txtReceiveDate.Text);
                c.Notes = txtNotes.Text;
                c.RefCode = txtReq.Text;
                c.EntryID = USession.SUserID;

                c.INVTypeID = Convert.ToInt32(Request.QueryString["INVTypeID"]);

                string res = "0";
                if (c.ItemID > 0)
                {
                    res = new Inventory_DL().SaveInventoryTransfer(c);
                    lblResult.Text = res;
                }
                if (res.Substring(0, 1) == "1")
                {
                    Clear();

                    updPanel.Update();
                }
                return true;
                // mpeProduct.Show();
            }

            catch (Exception ex)
            {
                lblResult.Text = ex.Message.ToString();
                return false;
                // lblResult.Text = ex.Message.ToString();
            }
        }
        private void Clear()
        {
            //lblResult.Text = string.Empty;
            txtNotes.Text = string.Empty;
            txtItem.Text = string.Empty;
            txtItem1.Text = string.Empty;
            txtItemID.Text = string.Empty;

            hdnIssueID.Value = "0";
            hdnReceiveID.Value = "0";
            txtTotalQty.Text = string.Empty;
            
            hdnCode.Value = "0";
            txtItem.Text = string.Empty;
        }
        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {


            Clear();
            lblResult.Text = "";
            updPanel.Update();




            mpeProduct.Show();
            txtItem.Focus();
        }



        protected void btnCancel_Click(object sender, EventArgs e)
        {
            // loadGridData();
            txtItem1.Text = string.Empty;

            mpeProduct.Hide();

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            loadGrid();
        }
    }
}

