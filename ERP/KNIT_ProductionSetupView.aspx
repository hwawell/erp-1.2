﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_ProductionSetupView.aspx.cs" Inherits="ERP.KNIT_ProductionSetupView" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
   <div>
   
                <asp:GridView 
                                ID="gvProducts" runat="server" 
                                  AllowSorting="True"
                                CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                ShowFooter="true"
                                Width="400px" 
                          AutoGenerateColumns="False">
                            
                                 <RowStyle CssClass="row" />
                              
                                <Columns>
                                   
                                    <asp:BoundField 
                                        HeaderText="ID" DataField="ID" 
                                        SortExpression="ID" >
                                    
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    
                                    <asp:BoundField 
                                        HeaderText="OrderNo" DataField="OrderNo" 
                                        SortExpression="OrderNo" >
                                    
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                 
                                    <asp:BoundField 
                                        HeaderText="Product" DataField="OrderProduct" 
                                        SortExpression="OrderProduct">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                   <asp:BoundField 
                                        HeaderText="Fabric" DataField="Fabric" 
                                        SortExpression="Fabric">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="GM" DataField="GM" 
                                        SortExpression="GM">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="GSM" DataField="GSM" 
                                        SortExpression="GSM">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    
                                    <asp:BoundField 
                                        HeaderText="Loss(%)" DataField="LossPer" 
                                        SortExpression="LossPer">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                     <asp:BoundField 
                                        HeaderText="Prod. Qty" DataField="ProductionQty" 
                                        SortExpression="ProductionQty">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                     <asp:BoundField 
                                        HeaderText="Date" DataField="Date" 
                                        SortExpression="Date">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    
                                  
                                    
                                    
                                    <asp:TemplateField HeaderText="Operation"  HeaderStyle-HorizontalAlign="Left" >
                                    <ItemTemplate>
                                        <asp:Button ID="btnOperation" runat="server" Text="Delete" 
                                            onclick="btnOperation_Click"
                                        />
                           
                                    </ItemTemplate>
                                        
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                   
                                 
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                            
   </div>
   <div>
       <asp:Button ID="btnCancel" runat="server" Text="Close" Width="60px" 
                                        Height="24px" Font-Names="Georgia" 
           onclick="btnCancel_Click" />
   </div>
</div>
</asp:Content>
