<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="INV_TRANSFER_RPT.aspx.cs" Inherits="ERP.INV_TRANSFER_RPT" Title="Inventory Transfer Report" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();
            List<EINV_TransferReport> liProd = new List<EINV_TransferReport>();
            Int32 gid = 0;

            if (ddlYarnGroup.SelectedItem.Text == "All Group")
            {
                gid = 0;
            }
            else
            {
                gid = Convert.ToInt32(ddlYarnGroup.SelectedValue.ToString());
            }
            liProd = new Inventory_DL().GetInventoryTransferReport(Convert.ToInt32(Request.QueryString["INVTypeID"]),  gid, txtFrom0.Text, txtFrom1.Text);


            
            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
    
   
   
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> 
    <asp:Label ID="lblHead" runat="server" Text="Label"></asp:Label>
    </h2></div>
 <div align="left" style="padding: 10px; ">
                      Item Group
                   <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                    </div>
                       
                                    <asp:DropDownList ID="ddlYarnGroup" runat="server" Height="24px" Width="157px" 
                                        TabIndex="1" >
                                    </asp:DropDownList>
                              
                                &nbsp;Date
                    <asp:TextBox ID="txtFrom0" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom0_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom0">
                    </cc1:CalendarExtender>
                       
                      To
                    <asp:TextBox ID="txtFrom1" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom1_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom1">
                    </cc1:CalendarExtender>
                       
                </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
           
            >
            <Reader>
            <ext:JsonReader IDProperty="ID">
                    <Fields>
                     
               
                           <ext:RecordField Name="ID" />
                          <ext:RecordField Name="ItemName" />
                          <ext:RecordField Name="GroupName" />
                          <ext:RecordField Name="TransferDate" />
                          <ext:RecordField Name="Unit" />
                        
                          <ext:RecordField Name="Notes" />
                          <ext:RecordField Name="ReqNo" />
                          <ext:RecordField Name="ToStore" />
                          <ext:RecordField Name="FromStore" />
                    
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Issue Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                         
                         
                            <ext:Column ColumnID="ID" Header="ID"  DataIndex="ID" Sortable="true" />
                             <ext:Column ColumnID="ItemName" Header="ItemName"  DataIndex="ItemName" Sortable="true" />
                            <ext:Column ColumnID="GroupName" Header="GroupName"  DataIndex="GroupName" Sortable="true" />
                            <ext:Column ColumnID="TransferDate" Header="TransferDate"  DataIndex="TransferDate" Sortable="true" />
                            <ext:Column ColumnID="Unit" Header="Unit"  DataIndex="Unit" Sortable="true" />
                          
                            <ext:Column ColumnID="Notes" Header="Notes"  DataIndex="Notes" Sortable="true" />
                            <ext:Column ColumnID="ReqNo" Header="ReqNo"  DataIndex="ReqNo" Sortable="true" />
                            <ext:Column ColumnID="FromStore" Header="FromStore"  DataIndex="FromStore" Sortable="true" />
                            <ext:Column ColumnID="ToStore" Header="ToStore"  DataIndex="ToStore" Sortable="true" />
                        
                       
                       </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="IDate" />
                                <ext:StringFilter DataIndex="Country" />
                                <ext:StringFilter DataIndex="Supplier" />
                                <ext:StringFilter DataIndex="ItemName" />
                                <ext:StringFilter DataIndex="InvoiceNo" />
                                 <ext:NumericFilter DataIndex="GatePassNo" />
                               
                                      
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>


