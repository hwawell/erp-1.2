﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
namespace ERP
{
    public partial class DyedFabricReProcess : System.Web.UI.Page
    {
         List<EOrderProductDetails> liColorobj = new List<EOrderProductDetails>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //if (new Utilities_DL().CheckSecurity("502", USession.SUserID) == true)
                //{
                    
                  //  loadGridData();
                   
                   // GenerateProcessCardNo();
                //}
                //else
                //{
                //    this.rblNormal.Items.FindByText("Active").Selected = true;
                //    //  loadGridData();
                //    loadOrder();
                //    loadFabricGroup();

                //    //Response.Redirect("Home.aspx");
                //}
            }
        }
      
       
       
        private void loadGridData()
        {


            gvProducts.Columns[7].Visible = true;
            gvProducts.Columns[8].Visible = false;

            if (tbAuto.Text.Length > 2)
            {
                gvProducts.DataSource = new KNIT_all_operation().GetKnitDyedFabricProcesss(tbAuto.Text);
            }
            else
            {
                gvProducts.DataSource = new KNIT_all_operation().GetKnitDyedFabricProcesss(tbAuto.Text);
            }
           
            gvProducts.DataBind();

        }
        private void loadDeletedData()
        {

            gvProducts.Columns[7].Visible = false;
            gvProducts.Columns[8].Visible = true;
            gvProducts.DataSource = new ProcessCard_CL().GetDeletedPCard();
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            tbAuto.Text = (row.Cells[1].Text);
          
            ddlColor.SelectedValue = (row.Cells[3].Text);
            //txtTotRoll.Text = (row.Cells[4].Text);
          
            //txtWeight.Text = (row.Cells[5].Text);
            
            //txtPerKG.Text = (row.Cells[8].Text);
            //txtUnit.Text = (row.Cells[9].Text);

           
          

            updPanel.Update();

            /////mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new KNIT_all_operation().DeleteKnitDyedFabricProcesssSetup(Convert.ToInt32(row.Cells[0].Text), row.Cells[3].Text);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[1].Visible = false;
                //e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            ProcessCard c = new ProcessCard();

            if (new ProcessCard_CL().CheckProcessCard(txtProcessCard.Text.Trim()) == false && (CheckEntryData() == true))
            {
                ////if (checkDecimal(txtTotRoll.Text) == false)
                ////{
                ////  //  txtTotRoll.Text = "0";
                ////}
                c.SL = Convert.ToInt64(hdnCode.Value);
                c.OPDID = Convert.ToInt32(ddlColor.SelectedValue.ToString());
               

                //c.TotalRoll = Convert.ToInt16(txtTotRoll.Text);
               

               


                c.EntryID = USession.SUserID;
                if (c.PCardNo.Length > 2)
                {
                    new ProcessCard_CL().Save(c);
                }
                loadGridData();
            }
            else
                lblMsg.Text = "Process Card is already Exist";
               // ScriptManager.RegisterClientScriptBlock(Page,Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Process Card is already Exists');</script>",false);

            //loadGridData();
           // updPanel.Update();
           //// mpeProduct.Hide();

        }
        private bool CheckEntryData()
        {
            string str="";
            bool check = true;
            if (new Order_DL().HasOrderNoExist(tbAuto.Text)==false)
            {
                str = "Order No Not Valid";
                check = false;
            }
            else if (ddlDesc.Text == "--Select--")
            {
                str = "Select Order Item";
                check = false;
            }
            else if (ddlColor.Text == "--Select--")
            {
                str = "Select Color";
                check = false;
            }
            //if (new ProcessCard_CL().CheckProcessCardWithProduct(txtProcessCard.Text.Trim(),0,Convert.ToInt32( ddlColor.SelectedItem.Value)) == false)
            //{
            //    str = "Process Card is not Valid for this Order";
            //    check = false;
            //}

            else if (ddlColor.Text == "--Select--")
            {
                str = "Select Color";
                check = false;
            }
            if (check == false)
            {
                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('"+ str +"');</script>", false);
                lblMsg.Text = str;

            }
            return check;
        }
        private bool checkDecimal(string s)
        {

            try
            {
                decimal v;
                v = Convert.ToDecimal(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private void SaveData()
        {
          

            try
            {
                lblMsg.ForeColor = System.Drawing.Color.Red;
                if (CheckEntryData() == true)
                {
                    if (Convert.ToInt32(hdnCode.Value) > 0 || new ProcessCard_CL().CheckProcessCard(txtProcessCard.Text.Trim()) == true)
                    {
                        //if (checkDecimal(txtTotRoll.Text) == false)
                        //{
                        //    txtTotRoll.Text = "0";
                        //}
                        EDyedFabricProcessSetup c = new EDyedFabricProcessSetup();
                        c.ProcessCardNo = txtProcessCard.Text;
                        c.DDate = Convert.ToDateTime(txtDate.Text);
                        c.RollNo = Convert.ToInt32(txtRoll.Text);
                        c.RollQty = Convert.ToDecimal(txtQty.Text);

                        c.ReprocessSection = ddlSection.SelectedItem.Text;
                        c.ReprocessDescription = txtDescription.Text;
                       
                       
                        c.EntryID = USession.SUserID;
                        c.PCardReason = ddlReason.SelectedValue.ToString();

                        string mes=new KNIT_all_operation().SaveKnitDyedFabricProcesssSetup(c, GetReturnList());
                        lblMsg.Text = mes;
                        if (mes.Substring(0,1) == "1")
                        {
                           
                            ClearALl();
                            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Data Saved successfully !!!');</script>", false);
                        }
                        
                          

                      

                    }
                    else
                        lblMsg.Text = "Process Card is not valid";

                }

                //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('Process Card is already Exists');</script>", false);
                //GenerateProcessCardNo();
                UpEntry.Update();

            }
            catch (Exception ex )
            {
                lblMsg.Text = "Process Card Not saved" + ex.Message.ToString();
                UpEntry.Update();
            }
        }
        private List<EDyedFabricProcess> GetReturnList()
        {
            EDyedFabricProcess obj;
            List<EDyedFabricProcess> li = new List<EDyedFabricProcess>();


            foreach (GridViewRow ob in GridView1.Rows)
            {
                CheckBox s = (CheckBox)ob.Cells[0].FindControl("chkStatus");

                obj = new EDyedFabricProcess();
                if (s.Checked == true)
                {
                    obj.SL = Convert.ToInt32(ob.Cells[1].Text);


                    li.Add(obj);

                }
            }


          
            return li;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {




            SaveData();



            /////mpeProduct.Show();
           

        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            // txtYarn.Text = string.Empty;
            // txtYarnDesc.Text = string.Empty;

            txtProcessCard.Text = "";
        
           
        
        
           
         


           //// mpeProduct.Show();
            tbAuto.Focus();
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

            ClearALl();
           
        }

        private void ClearALl()
        {


            try
            {


                txtRoll.Text = string.Empty;
                txtQty.Text = string.Empty;
                tbAuto.Text = string.Empty;
                txtProcessCard.Text = string.Empty;

                ddlColor.Text = "--Select--";
                ddlDesc.Text = "--Select--";
                
                hdnCode.Value = "0";


                
                UpEntry.Update();
            }
            catch
            {

            }
        }
        protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        private void LoadOrderProductDesc()
        {
            if (tbAuto.Text.Length > 5)
            {
                ddlDesc.DataSource = new Order_DL().GetOrderProductDesc(tbAuto.Text);
                ddlDesc.DataValueField = "OPID";
                ddlDesc.DataTextField = "DESC";

                ddlDesc.DataBind();
                ddlDesc.Items.Add("--Select--");
                ddlDesc.Text = "--Select--";
            }
        }
        protected void ddlDesc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDesc.SelectedItem.Text != "--Select--")
            {
                

                LoadOrderproductDetails();
            }
        }
        private void LoadOrderproductDetails()
        {
            try
            {
                liColorobj = new Order_DL().GetOrderProductColorList(Convert.ToInt32(ddlDesc.SelectedValue.ToString()));
                ddlColor.DataSource = liColorobj;
            ddlColor.DataValueField = "OPDID";
            ddlColor.DataTextField = "Color";
            ddlColor.DataBind();
            ddlColor.Items.Add("--Select--");
            ddlColor.Text = "--Select--";

           
                string s = ddlDesc.SelectedItem.Text.Replace(',', ' ');
                string[] desc = s.Split('>');
                if (desc.Length > 1)
                {
                   
                    UpEntry.Update();
                }
            }
            catch
            {
            }
        }
        protected void btnload_Click(object sender, EventArgs e)
        {
           
                loadGridData();

            
        }

        protected void txtCalculation1_TextChanged(object sender, EventArgs e)
        {

        }

       
        protected void gvProducts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void gvProducts_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
            //    Button btn = (Button)e.Row.Cells[3].Controls[1];
            //    if (e.Row.Cells[2].Text == "Running")
            //    {
            //        btn.Text = "Stop";
            //        btn.BackColor = System.Drawing.Color.Red;
            //    }
            //    else
            //    {
            //        btn.Text = "Start";
            //        btn.BackColor = System.Drawing.Color.Green;
            //    }

            }

        }

        public void chkStatus_OnCheckedChanged(object sender, EventArgs e)
        {

            int Roll = 0;
            double qty = 0;

            foreach(GridViewRow row in GridView1.Rows)
            {
                CheckBox chkStatus = (CheckBox)row.Cells[0].FindControl("chkStatus");
                if (chkStatus.Checked == true)
                {
                    Roll = Roll + 1;
                    qty = qty + Convert.ToDouble(row.Cells[4].Text);
                }
                //GridViewRow row = (GridViewRow)chkStatus.NamingContainer;

            }

            txtRoll.Text = Roll.ToString();
            txtQty.Text = qty.ToString();

            upSumm.Update();
            
        }
        protected void btnOperation_Click(object sender, EventArgs e)
        {
            Button btnEdit = sender as Button;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            if (btnEdit.Text == "Select")
            {
                row.ForeColor = System.Drawing.Color.Green;
                row.Font.Bold = true;
                row.Cells[4].Text = "Selected";
                btnEdit.Text = "UnSelect";
                btnEdit.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                row.Font.Bold = false;
                row.Cells[4].Text = "Available";
                row.ForeColor = System.Drawing.Color.Black;
                btnEdit.Text = "Select";
                btnEdit.BackColor = System.Drawing.Color.Green;
            }
            CalculateQty();

        }
       
        protected void txtGSM_TextChanged(object sender, EventArgs e)
        {

        }

       
        protected void ddlColor_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlColor.SelectedItem.Text != "--Select--")
            {
                LoadReturnList();
            }

          

        }
        private void LoadReturnList()
        {
            List<EDyedFabricProcess> liobj = new KNIT_all_operation().GetReturnFabricList(Convert.ToInt32(ddlColor.SelectedValue));
            GridView1.DataSource = liobj;
            GridView1.DataBind();
            
            
           
           // txtBalanceQty.Text = (obj.Booking - obj.ProductionQty).ToString();
          
            
        }
        private void CalculateQty()
        {
            int TotalRoll = 0;
            double totalQty = 0;
            foreach (GridViewRow ob in GridView1.Rows)
            {
                if (ob.Cells[4].Text == "Selected")
                {
                    TotalRoll = TotalRoll + 1;
                    totalQty = totalQty + Convert.ToDouble(ob.Cells[3].Text);

                }
            }
            txtRoll.Text = TotalRoll.ToString();
            txtQty.Text = totalQty.ToString();
            UpEntry.Update();
        }
        private double ConvertToDoucble(string val)
        {
            double res = 0;
            try
            {
                res = Convert.ToDouble(val);
            }
            catch
            {
                res = 0;
            }
            return res;
        }
        private void GenerateProcessCardNo()
        {
            txtProcessCard.Text = new ProcessCard_CL().GenearteProcessCardNo();

        }
       

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            
        }

        protected void btnPrintNew_Click(object sender, EventArgs e)
        {

          
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
           
        }

        protected void txtNotes_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadOrderProductDesc();
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
  
    }
}
