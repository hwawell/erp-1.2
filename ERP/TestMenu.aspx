﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestMenu.aspx.cs" Inherits="ERP.TestMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Online Collection Monitoring</title>
    <link href ="StyleSheet.css" type="text/css" rel="Stylesheet" />
         <script language="javascript" src="../choosedate.js" type="text/javascript"></script>

    <link href="../calendar.css" rel="stylesheet" type="text/css" />
    <link href="../Style.css" rel="stylesheet" type="text/css" />  
    <%--<asp:ContentPlaceHolder id="head" runat="server">
    </asp:ContentPlaceHolder>--%>
    <style>
body {
	font-size: 1em;
	background-color: #333333;
}
/* CSS for All the Menus START */
ul.ws_css_cb_menu {

	background-repeat:repeat-x;
	background-position:top left;
	width:700px;
	font:bold 13px Arial, Helvetica, sans-serif;
	display:block;
	float: left;
	height:33px;
	-moz-border-radius-topright: 0px;
	-moz-border-radius-bottomright: 10px;
	-moz-border-radius-bottomleft: 10px;
	-moz-border-radius-topleft: 0px;
	-webkit-border-top-right-radius: 0px;
	-webkit-border-bottom-right-radius: 10px;
	-webkit-border-bottom-left-radius: 10px;
	-webkit-border-top-left-radius: 0px;
	border-top-right-radius: 0px;
	border-bottom-right-radius: 10px;
	border-bottom-left-radius: 10px;
	border-top-left-radius: 0px;
}
ul.ws_css_cb_menu li {
	display:block;
	margin:2px 0px 0px 2px;
	float:left;
}
ul.ws_css_cb_menu a:hover ul, ul.ws_css_cb_menu a:hover a:hover ul, ul.ws_css_cb_menu a:hover a:hover a:hover ul {
	display:block;
}
ul.ws_css_cb_menu a {
	display:block;
	vertical-align:middle;
	border-width:0px;
	border-color:#6655ff;
	border-style:solid;
	padding:4px;
	_padding-left:0;
	color: #444444;
	text-decoration:none;
	text-align:left;
}
ul.ws_css_cb_menu span {
	overflow:hidden;
}
ul.ws_css_cb_menu li a:hover, ul.ws_css_cb_menu li a {
	padding:15px;
	color:#763319
}
ul.ws_css_cb_menum li a:hover, ul.ws_css_cb_menum li a {
	padding:2px;
	font-weight: normal;
	color: #000
}
ul.ws_css_cb_menu ul {
	position: absolute;
	left:-1px;
	top:98%;
	width:160.65px;
	border-left:1px solid #d5a000;
	border-bottom:3px solid #d5a000;
	border-right:3px solid #d5a000;
	border-top:0px;
	background-color:#fftfff;
}
ul.ws_css_cb_menu ul ul {
	position: absolute;
	left:98%;
	top:-2px;
}
ul.ws_css_cb_menu, ul.ws_css_cb_menu ul {
	margin:0px;
	list-style:none;
	padding:0px 2px 2px 0px;
}
ul.ws_css_cb_menu a:active, ul.ws_css_cb_menu a:focus {
	outline-style:none;
}
ul.ws_css_cb_menu ul li {
	float: left;
	width: 200px;
}
ul.ws_css_cb_menu ul a {
	white-space:nowrap;
	text-align:left;
}
ul.ws_css_cb_menu li:hover {
	position:relative;
}
ul.ws_css_cb_menu li:hover>a {
	background-color:#fff;
	color: #000;
	border-color:#665500;
	border-style:solid;
	text-decoration:none;
}
ul.ws_css_cb_menu li a:hover {
	position:relative;
	background-color:#fff;
	color: #000;
	text-decoration:none;
	border-color:#665500;
	border-style:solid;
}
ul.ws_css_cb_menum li a:hover {
	background-color: #ffc000
}
ul.ws_css_cb_menu img {
	border: none;
	float:left;
	margin-right:4px;
	width:16px;
	height:16px;
}
ul.ws_css_cb_menu ul img {
	width:16px;
	height:16px;
}
ul.ws_css_cb_menu ul, ul.ws_css_cb_menu a:hover ul ul {
	display:none;
	z-index:99999;
}
ul.ws_css_cb_menu li:hover>ul {
	display:block
}
ul.ws_css_cb_menu span {
	display:block;
	padding-right:11px;
	background-image:url('http://www.wittysparks.com/wp-content/themes/wp-max/images/arrow_down.gif');
	background-position:right center;
	background-repeat: no-repeat;
}
/* CSS for TABLE Tags for IE 6 and Lower START */
ul.ws_css_cb_menu li a table, ul.ws_css_cb_menu li a:hover table {
	border-collapse:collapse;
	margin:-4px 0px 0px -9px;
	border:0px;
	padding:0px;
}
ul.ws_css_cb_menu li a table tr td, ul.ws_css_cb_menu li a:hover table tr td {
	padding:0px;
	border:0px;
}
ul.ws_css_cb_menu li a table ul, ul.ws_css_cb_menu li a:hover table ul {
	border-collapse:collapse;
	padding:0px;
	margin:-4px 0px 0px -9px;
}
ul.ws_css_cb_menu ul span, ul.ws_css_cb_menu a:hover table span {
	background-image:url('http://www.wittysparks.com/wp-content/themes/wp-max/images/arrow_down.gif');
}
ul.ws_css_cb_menu table a:hover span, ul.ws_css_cb_menu table a:hover a:hover span, ul.ws_css_cb_menu table a:hover a:hover a:hover span {
	background-image:url('http://www.wittysparks.com/wp-content/themes/wp-max/images/arrow_down.gif');
}
ul.ws_css_cb_menu table a:hover table span, ul.ws_css_cb_menu table a:hover a:hover table span {
	background-image:url('http://www.wittysparks.com/wp-content/themes/wp-max/images/arrow_down.gif');
}
/* CSS for TABLE Tags for IE 6 and Lower END */
/* CSS for All the Menus END */

/* Video Menu Starts - Our Second Menu, by using the above class - this is just to change the colors and the height, width of the menu */
ul.videoCssMenu {
	font-size: 13px;
	background-image:url('http://www.wittysparks.com/wp-content/themes/wp-max/images/video_menu_bg.gif');
	-moz-border-radius: 0px;
	-webkit-border:0px;
	border:0px;
}
ul.videoCssMenu .videoMenuTitleImage {
	margin: -2px 0px 0px -2px;
	width:100px;
	height:30px;
}
ul.videoCssMenu ul {
	left:0px;
	background-color: #c8703c;
	width:400px;
	border-left:0px;
	border-bottom:1px solid #6e2a02;
	border-right:3px solid #6e2a02;
}
ul.videoCssMenu li a {
	padding:5px !important;
	color:#fff;
}
ul.videoCssMenu ul li a {
	color: #fff;
}
ul.videoCssMenu ul li {
	width:200px;
	float: left;
	height: 27px;
}
ul.videoCssMenu li:hover>a, ul.videoCssMenu li a:hover {
	background-color:#c8703c;
	border-color:#fff;
	padding:5px !important;
	color:#000;
}
ul.videoCssMenu li ul li:hover>a, ul.videoCssMenu li ul li a:hover {
	background-color:#ecb899;
	border-color:#336699;
	padding:5px;
	color:#000;
}
</style>
   
    <style type="text/css">
        .style1
        {
            width: 218px;
        }
        .style2
        {
            width: 4px;
        }
        </style>
         <script language="javascript" type="text/javascript">
        var oLastBtn=0;
        bIsMenu = false;
    //No RIGHT CLICK************************
        if (window.Event)
            document.captureEvents(Event.mouseup);
        
        function nocontextmenu()
        {
            event.cancelBubble = true
            event.returnValue = false;
            return false;
        }
        
        function norightclick(e)
        {
        if (window.Event)
            {
            if (e.which !=1)
            return false;
            }
        else if (event.button !=1)
            {
            event.cancelBubble = true
            event.returnValue = false;
            return false;
            }
        }
       document.oncontextmenu = nocontextmenu;
       document.onmousedown = norightclick;
        function onKeyDown() 
        {
        //78=N, 8=^H, 82=R, 116=t, 122=z
            if ((event.altKey) || ((event.keyCode == 8) ||((event.ctrlKey) && ((event.keyCode == 78) || (event.keyCode == 82))) ||(event.keyCode == 116)||(event.keyCode == 122)))
                {
                event.keyCode = 0;
                event.returnValue = false;
                }          
        }
    </script>

    <script language="JavaScript" type="text/jscript">
        function disableselect(e)
        {
            return false
        }
        function reEnable()
        {
            return true
        }
        //if IE4+
      //  document.onselectstart=new Function ("return false")

        //if NS6
        if (window.sidebar)
        {
            document.onmousedown=disableselect
            document.onclick=reEnable
        }
        
        function maximizeWindow( ) 
        {
        var offset = (navigator.userAgent.indexOf("Mac") != -1 || 
                  navigator.userAgent.indexOf("Gecko") != -1 || 
                  navigator.appName.indexOf("Netscape") != -1) ? 0 : 4;
        window.moveTo(-offset, -offset);
        window.resizeTo(screen.availWidth + (2 * offset), 
                   screen.availHeight + (2 * offset));
        }
    </script>
</head>
<body bgcolor="#D0D0D0" style="margin: 0px; background-color: #D4D0C8;">
    <form id="form1" runat="server">
    <center>
    <table style="width: 82%; height: 708px;" cellspacing="0px">
        <tr>
            <td valign="top" align="left">
                                   <div style="position: absolute; width: 103px; height: 14px; top: 92px; left: 738px;">
                                       <asp:LinkButton ID="lnkLogOut0" runat="server" Font-Names="Verdana" 
                                           Font-Size="10px" PostBackUrl="~/Login.aspx" ForeColor="White">Change Password</asp:LinkButton>
                                      </div>
                             
        
                <img alt="" src="images/Bannerd.jpg" style="width: 992px; height: 109px;" />
                <table style="width: 103%; height: 540px;" cellspacing="0px">
                    <tr><td colspan="3">
                    </td></tr>
                    <tr>
                        <td class="style1" style="background-color: #336699" valign="top">
                            <div style="top: 213px; left: 6px; width: 215px; height: 350px; background-color: #336699;"  >
                            </div>
                        </td>
                        <td class="style2" style="background-image: url('images/botton_bg.gif')">
                            &nbsp;</td>
                        <td valign="top" style="background-color: #D4D0C8">
                          
        
                                
                                   <div style="position: absolute; width: 39px; height: 11px; top: 93px; left: 883px;">
                                       <asp:LinkButton ID="lnkLogOut" runat="server" Font-Names="Verdana" 
                                           Font-Size="10px" PostBackUrl="~/Login.aspx" ForeColor="White">Logout</asp:LinkButton>
                                      </div>
                             
        
        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <img alt="" src="images/bottom.jpg" 
                    style="width: 994px; height: 52px; margin-top: 0px;" /></td>
        </tr>
    </table>
    </center>
    </form>
</body>
</html>

