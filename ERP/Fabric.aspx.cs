﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;

namespace ERP
{
    public partial class WebForm8 : System.Web.UI.Page
    {
       
       protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                if (new Utilities_DL().CheckSecurity("500", USession.SUserID) == true)
                {
                    this.rblNormal.Items.FindByText("Active").Selected = true;
                    loadGridData();
                    loadFabricGroup();
                }
                else
                {
                    Response.Redirect("Home.aspx");
                }
            }
        }

       private void loadFabricGroup()
       {


           ddFGroup.DataSource = new Fabric_CL().GetAllFabricGroup();
           ddFGroup.DataValueField = "SL";
           ddFGroup.DataTextField = "FGroupName";
           ddFGroup.DataBind();

       }
        private void loadGridData()
        {


            gvProducts.Columns[7].Visible = true;
            gvProducts.Columns[8].Visible = true;
            gvProducts.Columns[9].Visible = false;
            gvProducts.DataSource = new Fabric_CL().GetActiveFabric();
            gvProducts.DataBind();

        }
        private void loadDeletedData()
        {

            gvProducts.Columns[7].Visible = false;
            gvProducts.Columns[8].Visible = false;
            gvProducts.Columns[9].Visible = true;
            gvProducts.DataSource = new Fabric_CL().GetDeletedFabrics();
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            ddFGroup.Text = row.Cells[1].Text;
            txtFabric.Text = (row.Cells[3].Text);
            txtGM.Text = (row.Cells[4].Text.ToString().Trim());
            txtGSM.Text = (row.Cells[5].Text.ToString().Trim());
           

            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Fabric_CL().DeleteActiveFabric(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Fabric_CL().DeleteActiveFabric(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            try
            {
                Fabric c = new Fabric();
                c.FID = Convert.ToInt64(hdnCode.Value);
                c.Fabric1 = txtFabric.Text.ToString();
                c.GM = txtGM.Text.ToString();
                c.GSM = txtGSM.Text.ToString();
                c.GID = Convert.ToInt16(ddFGroup.SelectedValue);
                c.EntryID = USession.SUserID;
                //TBL_Product t = new TBL_Product();
                //t.ProductCode = txtPCode.Text.ToString();
                //t.ProductName = txtProduct.Text.ToString();
                //t.ProductDescription = txtDescription.Text.ToString();
                if (txtFabric.Text.Length > 0)
                {
                    new Fabric_CL().Save(c);
                }
                loadGridData();
                //loadGridData();
                updPanel.Update();
                mpeProduct.Hide();
            }
            catch (Exception ex)
            {

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Fabric c = new Fabric();
                c.FID = Convert.ToInt64(hdnCode.Value);
                c.Fabric1 = txtFabric.Text.ToString();
                c.GM = txtGM.Text.ToString();
                c.GSM = txtGSM.Text.ToString();
                c.GID = Convert.ToInt16(ddFGroup.SelectedValue);
                c.EntryID = USession.SUserID;
                //TBL_Product t = new TBL_Product();
                //t.ProductCode = txtPCode.Text.ToString();
                //t.ProductName = txtProduct.Text.ToString();
                //t.ProductDescription = txtDescription.Text.ToString();
                if (txtFabric.Text.Length > 0)
                {
                    new Fabric_CL().Save(c);
                }
                //loadGridData();
                //loadGridData();
                txtFabric.Text = "";
                txtGM.Text = "";
                txtGSM.Text = "";
                updPanel.Update();
                mpeProduct.Show();
                txtFabric.Focus();
            }
            catch (Exception ex)
            {

            }

        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            txtFabric.Text = string.Empty;
            txtGSM.Text = string.Empty;
            txtGM.Text = string.Empty;
            


            mpeProduct.Show();
            txtFabric.Focus();
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblNormal.Items.FindByText("Active").Selected == true)
            {
                loadGridData();

            }
            else
            {
                loadDeletedData();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            loadGridData();
        }
       

    }
}
