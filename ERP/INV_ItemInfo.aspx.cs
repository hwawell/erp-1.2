﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class INV_ItemInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (new Utilities_DL().CheckSecurity("502", USession.SUserID) == true)
                //{

                loadGridData();

                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
        private void loadGridData()
        {

            string Head = new Inventory_DL().GetInventoryType(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            this.Title = Head + " - Inventory Item";
            lblHeader.Text = "Inventory Item Entry for " + Head;

            LoadGroup();



            loadGrid();

        }
        private void loadGrid()
        {
            gvProducts.DataSource = new Inventory_DL().GetInventoryItem(Convert.ToInt32(Request.QueryString["INVTypeID"]),0,Convert.ToInt32(ddlGroup.SelectedValue));
            gvProducts.DataBind();
        }

        private void loadParent()
        {
            
            ddlGroup0.DataSource = new Inventory_DL().GetInventoryItem(Convert.ToInt32(Request.QueryString["INVTypeID"]), 0, Convert.ToInt32(ddlGroup.SelectedValue));
            ddlGroup0.DataTextField = "ItemName";
            ddlGroup0.DataValueField = "itemID";
            ddlGroup0.DataBind();

            ddlGroup0.Items.Insert(0, "NONE");
        }
        private void LoadGroup()
        {
            Inventory_DL objDL = new Inventory_DL();
            ddlGroup.DataSource = objDL.GetItemGroup(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlGroup.DataTextField = "GroupName";
            ddlGroup.DataValueField = "SL";
            ddlGroup.DataBind();
            
        }


        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            loadParent();
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;

            txtUnit.Text = (row.Cells[3].Text);
            ddlGroup.SelectedValue = (row.Cells[1].Text);
            
            if (row.Cells[2].Text == "0")
            {
                ddlGroup0.Text = "NONE";

            }
            else
            {
                ddlGroup0.SelectedValue = row.Cells[2].Text;
            }
           // ddlGroup0.SelectedValue=(row.
            txtItem.Text = (row.Cells[5].Text);



            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;

            //INV_Unit c = new INV_Unit();
            //c.ID = Convert.ToInt32(row.Cells[0].Text);
            //c.UnitName = row.Cells[1].Text;
            //c.INVTypeID = Convert.ToInt32(Request.QueryString["INVTypeID"]);
            //if (c.UnitName.Length > 0)
            //{
            //    new Inventory_DL().SaveUnit(c);
            //}

            //loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ////ImageButton btnEdit = sender as ImageButton;
            ////GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            ////new Yarn_DL().DeleteActive(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            ////loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                if (e.Row.Cells.Count > 2)
                {
                    e.Row.Cells[0].Visible = false;
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[2].Visible = false;
                }
               

            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            try
            {
                lblResult.Text = string.Empty;
                EINV_Item c = new EINV_Item();
                c.ItemID = Convert.ToInt32(hdnCode.Value);
                c.ItemName = txtItem.Text.ToString();
                c.GID = Convert.ToInt32(ddlGroup.SelectedValue.ToString());
                if (ddlGroup0.Text == "NONE")
                {
                    c.ParentItemID = 0;
                }
                else
                {
                    c.ParentItemID = Convert.ToInt32(ddlGroup0.SelectedValue.ToString());

                }
                c.INVTypeID = Convert.ToInt32(Request.QueryString["INVTypeID"]);
                c.EntryMode = "E";
                c.EntryID = USession.SUserID;

                if (c.ItemName.Length > 0)
                {
                    string res = new Inventory_DL().SaveInventoryItem(c);
                }

                txtItem.Text = "";
                mpeProduct.Hide();
                // updPanel.Update();
                loadGridData();

                txtUnit.Focus();
            }
            catch(Exception ex)
            {
                lblResult.Text = ex.Message.ToString();
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                lblResult.Text = string.Empty;
                EINV_Item c = new EINV_Item();
                c.ItemID = Convert.ToInt32(hdnCode.Value);
                c.ItemName = txtItem.Text.ToString();
                c.GID = Convert.ToInt32(ddlGroup.SelectedValue.ToString());
                if (ddlGroup0.Text == "NONE")
                {
                    c.ParentItemID = 0;
                }
                else
                {
                    c.ParentItemID = Convert.ToInt32(ddlGroup0.SelectedValue.ToString());

                }
                c.INVTypeID = Convert.ToInt32(Request.QueryString["INVTypeID"]);
                c.EntryMode = "E";
                c.EntryID = USession.SUserID;

                string res = "0";
                if (c.ItemName.Length > 0)
                {
                    res = new Inventory_DL().SaveInventoryItem(c);
                    lblResult.Text = res;
                }
                if (res.Substring(0, 1) == "1")
                {
                    txtItem.Text = "";

                    LoadGroup();
                    updPanel.Update();
                }
                mpeProduct.Show();
            }

            catch (Exception ex)
            {
                lblResult.Text = ex.Message.ToString();
            }

        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            txtItem.Text = string.Empty;
            txtUnit.Text = ddlGroup.SelectedItem.Text;
            lblResult.Text = string.Empty;


            loadParent();

            mpeProduct.Show();
            txtUnit.Focus();
        }

        

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            loadGridData();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            loadGrid();
        }
    }
}

