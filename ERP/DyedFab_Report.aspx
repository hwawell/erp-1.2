﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="DyedFab_Report.aspx.cs" Inherits="ERP.DyedFab_Report" Title="Report : Dyed Fabric Report" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();

            this.Window1.Title = "Report";

            string Title = "";

            
       
            if (tbAuto.Text.Length < 5)
            {
                tbAuto.Text = "";
                Title = ddlFor.SelectedItem.Text + " Report From " + txtFrom.Text + " To " + txtTo.Text + " ";
                
            }
            else
            {
                txtFrom.Text = "";
                txtTo.Text = "";
                Title = ddlFor.SelectedItem.Text + " Report For Order No: " + tbAuto.Text + " ";
               
            }

            if (ddlFor.SelectedIndex == 0)
            {
                this.Store1.DataSource = new GreyFabric_DL().rpt_GreyFabric_Issue(tbAuto.Text, txtFrom.Text, txtTo.Text);
                this.Store1.DataBind();
            }
            else
            {
                
                this.Store1.DataSource = new GreyFabric_DL().rpt_GreyFabric_Receive(tbAuto.Text, txtFrom.Text, txtTo.Text);
                this.Store1.DataBind();
            }

                  
               
         
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
    
   
   
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
	<script type="text/javascript">
	    $(function() {
	        $(".tb").autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                url: "DataLoad.asmx/GetOrderList",
	                    data: "{ 'PINO': '" + request.term + "' }",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    dataFilter: function(data) { return data; },
	                    success: function(data) {
	                        response($.map(data.d, function(item) {
	                            return {
	                            value: item.OrderNo
	                            }
	                        }))
	                    },
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                       
	                    }
	                });
	            },
	            minLength: 2
	        });
	    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> &nbsp;Report</h2></div>


             
              <div align="left" style="padding: 10px; ">
                  Dyed Fabric Report For
                  <asp:DropDownList ID="ddlFor" runat="server">
                      <asp:ListItem>DYED_FABRIC_ISSUE</asp:ListItem>
                      <asp:ListItem>DYED_FABRIC_RECEIVE</asp:ListItem>
                     
                  </asp:DropDownList>
                  
                   Type of Report
                  <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true"
                      onselectedindexchanged="ddlType_SelectedIndexChanged">
                       <asp:ListItem>--Select Type--</asp:ListItem>
                      <asp:ListItem>Date Wise</asp:ListItem>
                      <asp:ListItem>By Order</asp:ListItem>
                   
                  </asp:DropDownList>
                  
              </div>
     
             <asp:UpdatePanel ID="upSelect" UpdateMode="Conditional" runat="server">
 <ContentTemplate>

 <div align="left" style="padding: 10px; ">
                    <asp:Label ID="lblFrom" runat="server" Text="From Date :" Visible="false"></asp:Label>
                      <asp:TextBox ID="txtFrom" runat="server" Visible="false"></asp:TextBox><cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom"  >
                    </cc1:CalendarExtender>
                
                       
                      <asp:Label ID="lblTo" runat="server" Text="To" Visible="false"></asp:Label><asp:TextBox ID="txtTo" runat="server" Visible="false"></asp:TextBox><cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                        Enabled="True" TargetControlID="txtTo">
                    </cc1:CalendarExtender> 
     <asp:Label ID="lblOrder" runat="server" Text="Order No" Visible="false"></asp:Label><asp:TextBox ID="tbAuto" class="tb" runat="server" Visible="false">
             </asp:TextBox></div>
     </ContentTemplate>
     <Triggers>
         <asp:AsyncPostBackTrigger ControlID="ddlType" 
             EventName="SelectedIndexChanged" />
     </Triggers>
 </asp:UpdatePanel>

                <div align="left" style="padding: 10px; ">
                      <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                    </div>
              </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server" >  
           
            
            <Reader>
            <ext:JsonReader IDProperty="SL">
                    <Fields>

                    
                    
                      <ext:RecordField Name="SL" />
                     
                        <ext:RecordField Name="PDate" />
                         <ext:RecordField Name="InvoiceNo" />
                         <ext:RecordField Name="OrderNo" />
                            <ext:RecordField Name="PCardNo" />
                         <ext:RecordField Name="Merchandiser" />
                           <ext:RecordField Name="Customer" />
                         <ext:RecordField Name="ItemDescription" />
                          <ext:RecordField Name="LotNo" />
                          <ext:RecordField Name="GSM" />
                          <ext:RecordField Name="Width" />
                            <ext:RecordField Name="Color" />
                              <ext:RecordField Name="LD_DesignNo" />
                                <ext:RecordField Name="Roll" />
                                  <ext:RecordField Name="Qty" />
                                    <ext:RecordField Name="TypeOfProduction" />
                            <ext:RecordField Name="notes" />
                                                      

                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Production Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            
                       
                       
                       
                        
                           
                          
                            <ext:Column ColumnID="SL" Header="SL"  DataIndex="SL" Sortable="true" />
                            <ext:Column ColumnID="PDate" Header="Date"  DataIndex="PDate" Sortable="true" />
                            
                            <ext:Column ColumnID="OrderNo" Header="OrderNo"  DataIndex="OrderNo" Sortable="true" />
                            <ext:Column ColumnID="InvoiceNo" Header="InvoiceNo"  DataIndex="InvoiceNo" Sortable="true" />
                            <ext:Column ColumnID="PCardNo" Header="ProcessCard"  DataIndex="PCardNo" Sortable="true" />
                             <ext:Column ColumnID="Merchandiser" Header="Merchandiser"  DataIndex="Merchandiser" Sortable="true" />
                            
                            <ext:Column ColumnID="Customer" Header="Customer"  DataIndex="Customer" Sortable="true" />
                            <ext:Column ColumnID="ItemDescription" Header="ItemDescription"  DataIndex="ItemDescription" Sortable="true" />
                            <ext:Column ColumnID="LotNo" Header="LotNo"  DataIndex="LotNo" Sortable="true" />
                            <ext:Column ColumnID="GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                            <ext:Column ColumnID="Width" Header="Width"  DataIndex="Width" Sortable="true" />
                            <ext:Column ColumnID="Color" Header="Color"  DataIndex="Color" Sortable="true" />
                            <ext:Column ColumnID="LD_DesignNo" Header="LD_DesignNo"  DataIndex="LD_DesignNo" Sortable="true" />
                             <ext:Column ColumnID="Roll" Header="Roll"  DataIndex="Roll" Sortable="true" />
                            
                             <ext:Column ColumnID="Qty" Header="Qty"  DataIndex="Qty" Sortable="true" />
                              <ext:Column ColumnID="TypeOfProduction" Header="TypeOfProduction"  DataIndex="TypeOfProduction" Sortable="true" />
                              <ext:Column ColumnID="notes" Header="notes"  DataIndex="notes" Sortable="true" />
                             
                            
                           </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                    
                                <ext:DateFilter DataIndex="PDate" />
                                <ext:StringFilter DataIndex="OrderNo" />
                                <ext:StringFilter DataIndex="Customer" />
                                <ext:StringFilter DataIndex="PCardNo" />
                                 <ext:StringFilter DataIndex="ItemDescription" />
                                <ext:StringFilter DataIndex="Color" />
                           
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
                
                 
            </Items>
        </ext:Window>

</div>
</asp:Content>
