﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using BDLayer;
using System.Collections.Generic;

namespace ERP
{
    public partial class Warehouse_Stock_Report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Window1.Hide();
            }
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            List<EWareHousePacking> liProd = new List<EWareHousePacking>();
            this.Window1.Title = "Stock Report";
            liProd = new KNIT_all_operation().GetWareHouseStock(DropDownList1.SelectedValue, OpenFunction.ConvertDate(txtFrom.Text), tbAuto.Text);
                    
            
         
            Export("StockReport_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, liProd);
        }
        public void Export(string fileName, List<EWareHousePacking> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h3>Stock Report on " + txtFrom.Text + "</h3>  </div>");
            HttpContext.Current.Response.Write("<div> <h3>Stock Type : " + DropDownList1.SelectedValue + "</h3>  </div>");
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            foreach (PropertyInfo proinfo in new EWareHousePacking().GetType().GetProperties())
            {

                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(proinfo.Name);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");


            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (EWareHousePacking emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");
                foreach (PropertyInfo proinfo in new EWareHousePacking().GetType().GetProperties())
                {
                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    if (proinfo.GetValue(emp, null) != null)
                    {
                        HttpContext.Current.Response.Write(proinfo.GetValue(emp, null).ToString());
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("");
                    }
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");

                }

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }
    }
}
