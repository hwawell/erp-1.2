﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_Production_Report_DateWise.aspx.cs" Inherits="ERP.KNIT_Production_Report_DateWise" Title="Knitting Production Report" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();
            List<EKNITDailyReport> liProd = new List<EKNITDailyReport>();
            this.Window1.Title = "Knitting Production Report";
            
            liProd = new KNIT_all_operation().KnitDailyProductionReport(DropDownList1.Text, OpenFunction.ConvertDate(txtFrom.Text), OpenFunction.ConvertDate(txtFrom0.Text),DropDownList2.SelectedValue.ToString().Trim());

            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
     [DirectMethod(Namespace = "NETBEE")]
     public void ShowSummaryReport()
     {
         //System.Threading.Thread.Sleep(3000);

         try
         {
             Window1.Show();
             List<EKNITDailyReport> liProd = new List<EKNITDailyReport>();
             this.Window1.Title = "Knitting Summary Production Report";

             liProd = new KNIT_all_operation().KnitDailyProductionSummaryReport(DropDownList1.Text, OpenFunction.ConvertDate(txtFrom.Text), OpenFunction.ConvertDate(txtFrom0.Text), DropDownList2.SelectedValue.ToString().Trim());

             this.Store1.DataSource = liProd;
             this.Store1.DataBind();

             X.Mask.Hide();
         }
         catch
         {
             X.Mask.Hide();
         }

     }
    
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
   <script type="text/javascript">
       $(function() {
           $(".tb").autocomplete({
               source: function(request, response) {
                   $.ajax({
                       url: "DataLoad.asmx/GetOrderList",
                       data: "{ 'PINO': '" + request.term + "' }",
                       dataType: "json",
                       type: "POST",
                       contentType: "application/json; charset=utf-8",
                       dataFilter: function(data) { return data; },
                       success: function(data) {
                           response($.map(data.d, function(item) {
                               return {
                                   value: item.OrderNo
                               }
                           }))
                       },
                       error: function(XMLHttpRequest, textStatus, errorThrown) {
                         
                       }
                   });
               },
               minLength: 2
           });
       });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> Knitting Production Report</h2></div>
 <div align="left" style="padding: 10px; ">
                      Date                     <asp:TextBox ID="txtFrom" runat="server" Height="22px" 
                          Width="99px"></asp:TextBox>
                      &nbsp;To<asp:TextBox ID="txtFrom0" runat="server" Height="22px" Width="100px"></asp:TextBox><cc1:CalendarExtender 
                          ID="txtFrom0_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom0">
                    </cc1:CalendarExtender>
                      &nbsp;MCNo
                    <cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom">
                    </cc1:CalendarExtender>
                   <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Details Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                         <ext:Button ID="Button2" runat="server" Height="30" Width ="100" 
                             Text="Summary Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowSummaryReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Details-Export To XL" 
                             onclick="btnExport_Click" />
                         <br />
                         
                       <asp:Button ID="btnExport0" runat="server" Text="Summary-Export To XL" 
                             onclick="btnExport0_Click" />
                    </div>
                       
                      <asp:DropDownList ID="DropDownList1" runat="server" Height="25px" Width="192px">
                      </asp:DropDownList>
                      Shift<asp:DropDownList ID="DropDownList2" runat="server" Height="24px" 
                          Width="77px">
                          <asp:ListItem>All Shift</asp:ListItem>
                          <asp:ListItem>DAY</asp:ListItem>
                          <asp:ListItem>NIGHT</asp:ListItem>
                      </asp:DropDownList>
    </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
           
            >
            <Reader>
         
            <ext:JsonReader IDProperty="SL">
                    <Fields>
                   
                     
                         <ext:RecordField Name="MCNo" />
                          <ext:RecordField Name="FabricGroup" />
                          <ext:RecordField Name="Fabric" />
                           <ext:RecordField Name="GM" />
                            <ext:RecordField Name="Shift" />
                           
                        <ext:RecordField Name="GSM" />
                         <ext:RecordField Name="Roll_No" />
                         <ext:RecordField Name="RollQty" />
                       
                         
                         <ext:RecordField Name="KnitDateTime" />
                        
                       
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Production Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            
                            
                           
                            <ext:Column ColumnID="MCNo" Header="MCNo"  DataIndex="MCNo" Sortable="true" />
                            <ext:Column ColumnID="FabricGroup" Header="FabricGroup"  DataIndex="FabricGroup" Sortable="true" />
                            <ext:Column ColumnID="Fabric" Header="Fabric"  DataIndex="Fabric" Sortable="true" />
                            <ext:Column ColumnID="GM" Header="GM"  DataIndex="GM" Sortable="true" />
                           
                            <ext:Column ColumnID="GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                            <ext:Column ColumnID="Roll_No" Header="Roll No"  DataIndex="Roll_No" Sortable="true" />
                            
                            <ext:Column ColumnID="RollQty" Header="Roll Qty"  DataIndex="RollQty" Sortable="true" />
                            <ext:Column ColumnID="KnitDateTime" Header="Knit DateTime"  DataIndex="KnitDateTime" Sortable="true" />
                            <ext:Column ColumnID="Shift" Header="Shift"  DataIndex="Shift" Sortable="true" />
                           
                             
                               
                               
                                                       
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                             
                                <ext:StringFilter DataIndex="MCNo" />
                                 <ext:StringFilter DataIndex="FabricGroup" />
                                <ext:StringFilter DataIndex="Fabric" />
                                <ext:StringFilter DataIndex="GM" />
                                <ext:StringFilter DataIndex="Roll_No" />
                                <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="RollQty" />
                                <ext:DateFilter DataIndex="KnitDateTime" />
                                <ext:DateFilter DataIndex="Shift" />
                                
                            
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>



