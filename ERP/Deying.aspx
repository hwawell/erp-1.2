﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Deying.aspx.cs" Inherits="ERP.WebForm12" Title="Deying Section" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <style type="text/css">
        .style4
        {
        width: 143px;
    }
        .style5
        {
        width: 47px;
    }
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
        .style6
        {
            width: 47px;
            height: 2px;
        }
        .style7
        {
            width: 143px;
            height: 2px;
        }
        .style8
        {
            height: 2px;
        }
               
    </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <div class="grid" >
            <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>List of Deying Entry</h2></center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid" 
                        style="overflow:auto; width: 739px; height: 600px; background-color: "whitesmoke";">     
                <div style="height:25px;">
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                        AssociatedUpdatePanelID="updPanel">
                        <ProgressTemplate>
                            <img alt="" src="Images/loading.gif" style="height: 15px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    </div>
                <div style="height: 40px; border-bottom-style: none; border-bottom-color: #003399;">
                    <div style="float: left">
                    <asp:ImageButton ID="ibtnNew0" runat="server" Height="30px" 
                        ImageUrl="~/Images/Rich Text Format.ico" onclick="ibtnNew_Click" 
                        ToolTip="New Customer Entry" Width="39px" />
                         Shift :
                        <asp:DropDownList ID="ddlShift" runat="server">
                            <asp:ListItem Selected="True">DAY</asp:ListItem>
                            <asp:ListItem>NIGHT</asp:ListItem>
                        </asp:DropDownList>
                </div>
                <div align="center" style="height: 30px">
                 <asp:UpdatePanel ID="uprbl" runat="server" UpdateMode="Conditional">
                     <ContentTemplate>
                         <asp:TextBox ID="txtPCard" runat="server"></asp:TextBox>
                         <asp:Button ID="btnSearch" runat="server" onclick="btnSearch_Click" 
                             Text="Search PCard" />
                         <asp:Label ID="Label5" runat="server" Text="Type * to search all"></asp:Label>
                     </ContentTemplate>
                    </asp:UpdatePanel>
                    </div>
                
                </div>
                <div style="height: 8px; border-bottom-style: groove; border-bottom-color: #003399;">
                
                </div>
                    <!-- Content Goes Here! -->
                    <asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView 
                                ID="gvProducts" runat="server" 
                                 AutoGenerateColumns="False" 
                                AllowPaging="True" AllowSorting="True"
                                CssClass="datatable" CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                onpageindexchanging="gvProducts_PageIndexChanging" PageSize="2">
                            
                                <PagerStyle CssClass="pager-row" />
                                <RowStyle CssClass="row" />
                                <PagerSettings 
                                    Mode="NumericFirstLast" PageButtonCount="3"
                                    FirstPageText="«" LastPageText="»" />      
                                <Columns>
                                    <asp:BoundField 
                                        HeaderText="sl" DataField="sl" 
                                        HeaderStyle-CssClass="first" ItemStyle-CssClass="first">
                                    
                                        <HeaderStyle CssClass="first" />
                                        <ItemStyle CssClass="first" />
                                    </asp:BoundField>
                                    <asp:BoundField 
                                        HeaderText="Process Card" DataField="PALL" 
                                        SortExpression="PCardNo" 
                                    >
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField 
                                        HeaderText="Qty" DataField="Pqty" 
                                        SortExpression="Pqty">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField 
                                        HeaderText="M/C No" DataField="MCNo" 
                                        SortExpression="MCNo">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="Notes" DataField="NOtes" 
                                        SortExpression="NOtes">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    
                                    
                                    <asp:BoundField DataField="EntryDate" HeaderText="EntryDate">
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:TemplateField HeaderText="Finished">
                                        <ItemTemplate>
                                            
                                           <asp:Button ID="btnFinish" runat="server" Text="Deying Finished" onclick="Unnamed1_Click" 
                                                Width="114px" onclientclick="javascript:return confirm('Dyeing Process finished! Do you want to sent to Dry Section');"></asp:Button>
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="~/Images/btn_edit.gif" 
                                                onclick="btnEdit_Click" Height="16px" 
                                                />
                                            
                                           
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                    <asp:ImageButton ID="btnDelete" runat="server" 
                                                ImageUrl="~/Images/btn_delete.gif" onclick="btnDelete_Click" 
                                                Height="16px"
                                            onclientclick="javascript:return confirm('Are you sure you want to delete this row?');" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active">
                                      <ItemTemplate>
                                     <asp:ImageButton ID="btnActive" runat="server" 
                                                ImageUrl="~/Images/HandleHand.png" onclick="btnActive_Click" 
                                                style="width: 15px" Height="16px" />
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:BoundField 
                                        HeaderText="PCard" DataField="PCardNo" 
                                        SortExpression="Pqty">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                     <asp:BoundField 
                                        HeaderText="CCArd" DataField="CCardNo" 
                                        SortExpression="Pqty">
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                            <asp:Button ID="btnHiddenProduct" runat="Server" Style="display:none"  />
                                 
                    <cc1:ModalPopupExtender ID="mpeProduct" runat="server" 
                        TargetControlID="btnHiddenProduct" PopupControlID="pnlDataEntry" 
                         PopupDragHandleControlID="ProductCaption" 
                        Drag="True" BackgroundCssClass="modalBackground" >
                    </cc1:ModalPopupExtender>
                   <%-- Style="display:none"--%>
                    <asp:Panel ID="pnlDataEntry" runat="server"   CssClass="modalBox"  
                        BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                        Width="448px" BorderWidth="2px">
                        <center>
                            <asp:Panel ID="ProductCaption" runat="server"   
                                Style="margin-bottom: 10px; cursor: hand;" 
                                BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                Deying Processing</asp:Panel></center>
                            <cc1:CollapsiblePanelExtender ID="ProductCaption_CollapsiblePanelExtender" 
                                runat="server" Enabled="True" TargetControlID="ProductCaption">
                            </cc1:CollapsiblePanelExtender>
                         
                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label3" runat="server" Text="Process Card No" 
                                        Font-Names="Georgia"></asp:Label>
                                </td>
                                <td>
                            
                                    <asp:Label ID="lblProcessCard" runat="server"></asp:Label>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    <asp:HiddenField ID="hdnCode" runat="server" Value="-1" />
                                </td>
                                <td class="style4">
                                    <asp:Label ID="Label1" runat="server" Text="Production Qty" 
                                        Font-Names="Georgia"></asp:Label>
                                </td>
                                <td style="margin-left: 40px">
                                    <asp:TextBox ID="txtQty" runat="server" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                           <%-- <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label5" runat="server" Font-Names="Georgia" 
                                        Text="Back To Dyeing Qty"></asp:Label>
                                </td>
                                <td style="margin-left: 40px">
                                    <asp:TextBox ID="txtReturn" runat="server"></asp:TextBox>
                                </td>
                            </tr>--%>
                            <%--<tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    <asp:Label ID="Label7" runat="server" Font-Names="Georgia" 
                                        Text="Sub Process Card No"></asp:Label>
                                </td>
                                <td style="margin-left: 40px">
                                    <asp:TextBox ID="txtCPCard" runat="server"></asp:TextBox>
                                </td>
                            </tr>--%>
                            <tr>
                                <td class="style5">
                                    <asp:HiddenField ID="hdnQty" runat="server" Value="0" />
                                </td>
                                <td class="style4">
                                    <asp:Label ID="Label2" runat="server" Text="M/C No" Font-Names="Georgia"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMCNo" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td class="style5">
                                    <asp:HiddenField ID="hdnPcard" runat="server" Value="" />
                                </td>
                                <td class="style4">
                                    <asp:Label ID="Label6" runat="server" Font-Names="Georgia" 
                                        Text="Production Date"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                                        Enabled="True" TargetControlID="txtDate">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td class="style5">
                                    <asp:HiddenField ID="hdnCCard" runat="server" Value="" />
                                </td>
                                <td class="style4">
                                    <asp:Label ID="Label4" runat="server" Font-Names="Georgia" Text="Comments"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNotes" runat="server" MaxLength="200" Width="230px"></asp:TextBox>
                                </td>
                            </tr>
                          
                            <tr>
                                <td class="style6">
                                    </td>
                                <td class="style7">
                                    &nbsp;</td>
                                <td class="style8">
                                   
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    &nbsp;</td>
                                <td>
                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                        AssociatedUpdatePanelID="updPanel" DisplayAfter="10">
                                    <ProgressTemplate>
                                      <img id="dd" alt="Loading.." src="Images/loading.gif" />
                                    </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                            <tr>
                            <div> 
                            </div>
                            </tr>
                            <caption>
                                this is grid
                            </caption>
                        </table>
                        <div>
                             <asp:Button ID="btnSave" runat="server" Height="24px"  Text="Save" 
                                        Width="60px" onclick="btnSave_Click" Font-Names="Georgia" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="60px" 
                                        Height="24px" Font-Names="Georgia" onclick="btnCancel_Click" />
                        </div>
                    </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>                     
    
                </div></div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>      
    </div>
</asp:Content>
