<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_ProductionSetup.aspx.cs" Inherits="ERP.KNIT_ProductionSetup" Title="Grey Machine Setup" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style4
        {
        width: 143px;
    }
        .style5
        {
        width: 47px;
    }
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
        .style6
        {
            width: 47px;
            height: 2px;
        }
        .style7
        {
            width: 143px;
            height: 2px;
        }
                      
              .mGrid { 
    width: 100%; 
    background-color: #fff; 
    margin: 5px 0 10px 0; 
    border: solid 1px #525252; 
    border-collapse:collapse; 
}
.mGrid td { 
    padding: 2px; 
    border: solid 1px #c1c1c1; 
    color: #717171; 
}
.mGrid th { 
    padding: 4px 2px; 
    color: #fff; 
    background: #424242 url(grd_head.png) repeat-x top; 
    border-left: solid 1px #525252; 
    font-size: 0.7em; 
}
.mGrid .alt { background: #fcfcfc url(grd_alt.png) repeat-x top; }
.mGrid .pgr { background: #424242 url(grd_pgr.png) repeat-x top; }
.mGrid .pgr table { margin: 5px 0; }
.mGrid .pgr td { 
    border-width: 0; 
    padding: 0 6px; 
    border-left: solid 1px #666; 
    font-weight: bold; 
    color: #fff; 
    line-height: 12px; 
 }   
.mGrid .pgr a { color: #666; text-decoration: none; }
.mGrid .pgr a:hover { color: #000; text-decoration: none; }
 
    </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
    
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Script/jquery-1.8.2.js") %>"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/Script/jquery.number.js") %>"></script>

 <script type="text/javascript">

     $(function() {
         $('#<%=btnSave.ClientID %>').click(function(evt) {
             // Validate the form and retain the result.
         var valuePer = 0.0;
         if (!isNaN(parseFloat($('#<%=txtKG.ClientID %>').val().replace(',', '')))) {
                  valuePer = valuePer + parseFloat($('#<%=txtKG.ClientID %>').val().replace(',', ''));
               }
               if (!isNaN(parseFloat($('#<%=txtKG0.ClientID %>').val().replace(',', '')))) {
                   valuePer = valuePer + parseFloat($('#<%=txtKG0.ClientID %>').val().replace(',', ''));
               }
               if (!isNaN(parseFloat($('#<%=txtKG1.ClientID %>').val().replace(',', '')))) {
                   valuePer = valuePer + parseFloat($('#<%=txtKG1.ClientID %>').val().replace(',', ''));
               }
               if (!isNaN(parseFloat($('#<%=txtKG2.ClientID %>').val().replace(',', '')))) {
                   valuePer = valuePer + parseFloat($('#<%=txtKG2.ClientID %>').val().replace(',', ''));
               }
               if (!isNaN(parseFloat($('#<%=txtKG3.ClientID %>').val().replace(',', '')))) {
                   valuePer = valuePer + parseFloat($('#<%=txtKG3.ClientID %>').val().replace(',', ''));
               }


               if (valuePer != $('#<%=txtNet.ClientID %>').val().replace(',', '')) {
                   alert('Yarn Consumption (=' + valuePer + ' ) must be equal to Production Qty (' + $('#<%=txtNet.ClientID %>').val() + ')');
                 evt.preventDefault();
             }

         });

     });
   
     $(function() {
         $('#<%=txtUserPercent.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtNet.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtNet.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;


             totValue = $.number(totValue, 2);
             $('#<%=txtKG.ClientID %>').val(totValue);
            

         });
     });

     $(function() {
         $('#<%=txtUserPercent0.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtNet.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtNet.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;
             totValue = $.number(totValue, 2);
             $('#<%=txtKG0.ClientID %>').val(totValue);

         });
     });

     $(function() {
         $('#<%=txtUserPercent1.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtNet.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtNet.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;
             totValue = $.number(totValue, 2);
             $('#<%=txtKG1.ClientID %>').val(totValue);

         });
     });

     $(function() {
         $('#<%=txtUserPercent2.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtNet.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtNet.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;
             totValue = $.number(totValue, 2);
             $('#<%=txtKG2.ClientID %>').val(totValue);

         });
     });

     $(function() {
         $('#<%=txtUserPercent3.ClientID %>').bind('keyup', function(e) {

             var valuePer = 0.0;
             var valueBook = 0.0;
             var totValue = 0.0;
             if (isNaN(parseFloat($('#<%=txtNet.ClientID %>').val()))) {
                 valueBook = 0;
             } else {
             valueBook = parseFloat($('#<%=txtNet.ClientID %>').val().replace(',', ''));
             }

             if (isNaN(parseFloat($(this).val()))) {
                 valuePer = 0;
             } else {
                 valuePer = $(this).val();
             }
             totValue = (valueBook * valuePer) / 100;
             totValue = $.number(totValue, 2);
             $('#<%=txtKG3.ClientID %>').val(totValue);

         });
     });

    
     
    </script>
  
    <style type="text/css">
        .style3
        {
            width: 191px;
        }
        .style4
        {
            width: 142px;
        }
        .style5
        {
            width: 54px;
        }
        .style6
        {
            width: 193px;
        }
        .style7
        {
            width: 269px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<div>
   <div style="height:30px; " align="center">
     <h3> Production Machine Setup </h3>
   </div>
  
   <fieldset>
   <legend>Search</legend>
       <div>
     <h3>  Month : <asp:TextBox ID="txtReqNo" runat="server"></asp:TextBox>
       <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" 
                        TargetControlID="txtReqNo" Format="MMM-yyyy">
                    </cc1:CalendarExtender>
         <asp:Button ID="Button5" runat="server" Text="Search Req" 
             onclick="Button5_Click" />
         Requisition No :<asp:DropDownList ID="ddlReq" runat="server">
         </asp:DropDownList>
         <asp:Button ID="btnSearch" runat="server" onclick="btnSearch_Click" 
             Text="Search" />
          
       </div>
   </fieldset>
   <fieldset>
   <legend>Product Information For Machine Setup</legend>
   
 
     
   
      
   
       
    <div style="width: 20%; float: left" align="right">
        Fabric Type :</div>
        <asp:UpdatePanel ID="upFabric" runat="server" UpdateMode="Conditional">
       <ContentTemplate>
   <div style="width: 80%; float: left" align="left">
    
        <asp:CheckBox ID="chkSubContact" runat="server" Text="Sub Contact" />
    
        <asp:DropDownList ID="ddlFabricType" runat="server" AutoPostBack="True" 
                          Height="25px" 
                          TabIndex="3" Width="235px" 
            onselectedindexchanged="ddlFabricType_SelectedIndexChanged">
          </asp:DropDownList>
      
        Fabric
   
  
        <asp:DropDownList ID="ddlFabric" runat="server" AutoPostBack="True" 
                          Height="25px" 
                          TabIndex="3" Width="400px">
          </asp:DropDownList>
              <cc1:ListSearchExtender ID="ListSearchExtender3" runat="server" Enabled="True" 
          IsSorted="true" PromptText="  --" TargetControlID="ddlFabric">
      </cc1:ListSearchExtender>
     
  
   </div>
     </ContentTemplate>
       <Triggers>
           <asp:AsyncPostBackTrigger ControlID="ddlFabricType" 
               EventName="SelectedIndexChanged" />
       </Triggers>
   </asp:UpdatePanel>
    <div style="width: 20%; float: left" align="right">
        GM :</div>
   <div style="width: 15%; float: left" align="left">
   
       <asp:TextBox ID="txtGM" runat="server"></asp:TextBox>
   
   </div>
    <div style="width: 10%; float: left" align="right">
        GSM :</div>
   <div style="width: 55%; float: left" align="left">
   
       <asp:TextBox ID="txtGSM" runat="server"></asp:TextBox>
   
       &nbsp; Date
       <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
   <cc1:CalendarExtender ID="txtTo_CalendarExtender" runat="server" Enabled="True" 
                        TargetControlID="txtDate">
                    </cc1:CalendarExtender>
                  
                        
                <asp:GridView 
                                ID="gvProducts" runat="server" 
                                  CssClass="mGrid"
                                PagerStyle-CssClass="pgr"
                                AlternatingRowStyle-CssClass="alt"
                                  AllowSorting="True"
                                CellPadding="0" 
                                BorderWidth="0px" GridLines="None" 
                                onrowdatabound="gvProducts_RowDataBound" Height="16px" 
                                ShowFooter="true"
                                Width="400px" onrowcreated="gvProducts_RowCreated" 
                          >
                            
                               
                                <RowStyle CssClass="row" />
                              
                                <Columns>
                                   
                                    <asp:BoundField 
                                        HeaderText="ID" DataField="ID" 
                                        SortExpression="ID" >
                                    
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    
                                    <asp:BoundField 
                                        HeaderText="Machine" DataField="Machine" 
                                        SortExpression="Machine" >
                                    
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                 
                                    <asp:BoundField 
                                        HeaderText="AssignQty" DataField="AssignQty" 
                                        SortExpression="AssignQty" >
                                    
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    
                                    
                                   
                                    
                                  
                                       <asp:BoundField 
                                        HeaderText="Status" DataField="Status" 
                                        SortExpression="Status">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:TemplateField HeaderText="Operation"  HeaderStyle-HorizontalAlign="Left" >
                                    <ItemTemplate>
                                        <asp:Button ID="btnOperation" runat="server" Text="Delete" 
                                            onclick="btnOperation_Click"
                                        />
                           
                                    </ItemTemplate>
                                 
                                        
                                        
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                   
                                 
                                </Columns>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                            </asp:GridView>   
                            
                  
   </div>
   
       
          <div style="width: 20%; float: left;Top:10px;" align="right">
              Location :</div>
   <div style="width: 80%; float: left;Top:10px;" align="left">
   
       
   
   
   
       <asp:TextBox ID="txtLocation"   runat="server" MaxLength="20" TabIndex="102" 
                 Height="20px" Width="170px" Font-Bold="True"></asp:TextBox>
   
   </div>

    <div style="width: 20%; float: left" align="right">
        Notes :</div>
   <div style="width: 80%; float: left" align="left">
    
       <asp:TextBox ID="txtNotes" runat="server" Height="20px" TextMode="MultiLine" 
           Width="680px"></asp:TextBox>
      
   </div>
   </fieldset>
   
   <asp:UpdatePanel ID="upOrder" runat="server" UpdateMode="Conditional">
   <ContentTemplate>
   
 
   <fieldset>
   <legend> Order Info</legend>
   <div style="width: 100%; float: left" align="right">
       <div style="width: 15%; float: left" align="right">
            Order No :</div>
       <div style="width: 85%; float: left" align="left">
        
             <asp:TextBox ID="txtOrderNo" runat="server" Width="110px"></asp:TextBox>
          
             <asp:Button ID="Button2" runat="server" Text="Search" onclick="Button2_Click" />
           
             Description :
            <asp:DropDownList ID="ddlOrderDesc" runat="server" Height="22px" Width="550px" 
                 onselectedindexchanged="ddlOrderDesc_SelectedIndexChanged" 
                 AutoPostBack="True" >
           </asp:DropDownList>
       </div>
       
         <div style="clear:both" />
        <div style="width: 15%; float: left" align="right">
            Booking Qty :</div>
       <div style="width: 85%; float: left" align="left">
        
              
             <asp:TextBox ID="txtBookingQty" runat="server" style="margin-top: 1px"></asp:TextBox>
             Knit Grey Qty :
             <asp:TextBox ID="txtQtyUsed" runat="server" style="margin-top: 1px"></asp:TextBox>
             <asp:Button  ID="Button3" runat="server" Text="Add Order" 
                 onclick="Button3_Click" />
          
       </div>
   </div>
   <div style="clear:both" />
   <div style="padding-left:15%; "  align="left">
                        
                <asp:GridView ID="gvProducts0" runat="server" 
                                  onrowdatabound="gvProducts0_RowDataBound" Height="30px" 
                                ShowFooter="False"
                                CellPadding="0" 
                                CssClass="mGrid" 
                                 PagerStyle-CssClass="pgr"
                                AlternatingRowStyle-CssClass="alt"
                               
                                BorderWidth="0px" GridLines="None" 
                                
                          AutoGenerateColumns="False"  Width="749px">
                          
                      
                               
                                 <RowStyle CssClass="row" Font-Size="10pt" />
                              
                                <Columns>
                                   
                                      <asp:BoundField 
                                        HeaderText="OPID" DataField="OPID" 
                                        SortExpression="OPID" >
                                    
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>                                
                                    
                                    <asp:BoundField 
                                        HeaderText="OrderNo" DataField="OrderNo" 
                                        SortExpression="OrderNo" >
                                    
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                 
                                    <asp:BoundField 
                                        HeaderText="FabricDescription" DataField="Description" 
                                        SortExpression="Description">
                                         <HeaderStyle Wrap="False" HorizontalAlign ="Left"/>
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField 
                                        HeaderText="Setup Qty" DataField="Qty" 
                                        SortExpression="Qty">
                                        <HeaderStyle Wrap="False" HorizontalAlign ="Left" />
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    
                                    
                                  
                                  
                                    
                                    <asp:TemplateField HeaderText="Delete"  HeaderStyle-HorizontalAlign="Left" >
                                    <ItemTemplate>
                                        <asp:Button ID="btnOperation0" runat="server" Text="Delete" 
                                            onclick="btnOperation0_Click"
                                        />
                           
                                    </ItemTemplate>
                                        
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                   
                                 
                                </Columns>
                                 <PagerStyle CssClass="pgr" />
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                 <HeaderStyle Font-Size="11pt" />
                                <EditRowStyle Height="10px" />
                                 <AlternatingRowStyle CssClass="alt" />
                            </asp:GridView>   
                            
     
   </div>
   
      
   <hr />
    
   <div>
        Total Production Qty 
       <asp:TextBox ID="txtTotQty" runat="server" ReadOnly="True"></asp:TextBox>
        Extra Prod(%): <asp:TextBox ID="txtExtra" runat="server" Width="66px"></asp:TextBox>
       
        <asp:Button ID="Button4" runat="server" Text="Calculate" 
            onclick="Button4_Click1" />
       
      <b> Net Production Qty :<asp:TextBox ID="txtNet" runat="server" ReadOnly="True"></asp:TextBox></b></div>
   </fieldset> 
     </ContentTemplate>
   
   </asp:UpdatePanel>
   <div>
      <fieldset>
   <legend> Yarn Composition</legend>
   
        <div>
        
       <table style="width: 100%;">
           <tr>
               <td align="right" class="style3">
                   
                   Yarn Type</td>
               <td align="left" class="style4">
                   
                   <asp:DropDownList ID="ddlYarnType" runat="server" 
                       onselectedindexchanged="ddlYarnType_SelectedIndexChanged" 
                       AutoPostBack="True">
                   </asp:DropDownList>
               </td>
               <td align="left" class="style5">
                   
                   Yarn 1</td>
               <td align="left" class="style6">
                
                    
                     
                     
                          <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                 <ContentTemplate>     
                               <asp:DropDownList ID="ddlYarn" runat="server" Width="200px">
                               </asp:DropDownList>
                           </ContentTemplate>
                                 <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="ddlYarnType" 
                                         EventName="SelectedIndexChanged" />
                                 </Triggers>
                           </asp:UpdatePanel> 
                     
               </td>
               <td align="left" class="style7">
                   Use %<asp:TextBox ID="txtUserPercent" runat="server" Width="57px"></asp:TextBox>KG
                   <asp:TextBox ID="txtKG" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>
               </td>
               <td>
                   &nbsp;</td>
           </tr>
           <tr>
               <td align="right" class="style3">
                   
                   Yarn Type</td>
               <td align="left" class="style4">
                   
                   <asp:DropDownList ID="ddlYarnType0" runat="server" 
                       onselectedindexchanged="ddlYarnType0_SelectedIndexChanged" 
                       AutoPostBack="True">
                   </asp:DropDownList>
               </td>
               <td align="left" class="style5">
                   
                   Yarn 2</td>
               <td align="left" class="style6">
                
                     
                   <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                 <ContentTemplate>               
                   <asp:DropDownList ID="ddlYarn0" runat="server" Width="200px">
                   </asp:DropDownList>
                   </ContentTemplate>
                                 <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="ddlYarnType0" 
                                         EventName="SelectedIndexChanged" />
                                 </Triggers>
                    </asp:UpdatePanel> 
               </td>
               <td align="left" class="style7">
                   Use %<asp:TextBox ID="txtUserPercent0" runat="server" Width="57px"></asp:TextBox>KG
                   <asp:TextBox ID="txtKG0" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>
               </td>
               <td>
                   &nbsp;</td>
           </tr>
           <tr>
               <td align="right" class="style3">
                   
                   Yarn Type</td>
               <td align="left" class="style4">
                   
                   <asp:DropDownList ID="ddlYarnType1" runat="server" 
                       onselectedindexchanged="ddlYarnType1_SelectedIndexChanged" 
                       AutoPostBack="True">
                   </asp:DropDownList>
               </td>
               <td align="left" class="style5">
                   
                   Yarn 3</td>
               <td align="left" class="style6">
                
                     
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                 <ContentTemplate>              
                   <asp:DropDownList ID="ddlYarn1" runat="server" Width="200px">
                   </asp:DropDownList>
                   </ContentTemplate>
                                 <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="ddlYarnType1" 
                                         EventName="SelectedIndexChanged" />
                                 </Triggers>
                           </asp:UpdatePanel> 
               </td>
               <td align="left" class="style7">
                   Use %<asp:TextBox ID="txtUserPercent1" runat="server" Width="57px"></asp:TextBox>KG
                   <asp:TextBox ID="txtKG1" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>
               </td>
               <td>
                   &nbsp;</td>
           </tr>
           <tr>
               <td align="right" class="style3">
                   
                   Yarn Type</td>
               <td align="left" class="style4">
                   
                   <asp:DropDownList ID="ddlYarnType2" runat="server" 
                       onselectedindexchanged="ddlYarnType2_SelectedIndexChanged" 
                       AutoPostBack="True">
                   </asp:DropDownList>
               </td>
               <td align="left" class="style5">
                   
                   Yarn 4</td>
               <td align="left" class="style6">
                
                     
                   <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                 <ContentTemplate>               
                   <asp:DropDownList ID="ddlYarn2" runat="server" Width="200px">
                   </asp:DropDownList>
                   </ContentTemplate>
                                 <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="ddlYarnType2" 
                                         EventName="SelectedIndexChanged" />
                                 </Triggers>
                           </asp:UpdatePanel> 
               </td>
               <td align="left" class="style7">
                   Use %<asp:TextBox ID="txtUserPercent2" runat="server" Width="57px"></asp:TextBox>KG
                   <asp:TextBox ID="txtKG2" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>
               </td>
               <td>
                   &nbsp;</td>
           </tr>
           <tr>
               <td align="right" class="style3">
                   
                   Yarn Type</td>
               <td align="left" class="style4">
                   
                   <asp:DropDownList ID="ddlYarnType3" runat="server" 
                       onselectedindexchanged="ddlYarnType3_SelectedIndexChanged" 
                       AutoPostBack="True">
                   </asp:DropDownList>
               </td>
               <td align="left" class="style5">
                   
                   Yarn 5</td>
               <td align="left" class="style6">
                
                     
                   <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                 <ContentTemplate>               
                   <asp:DropDownList ID="ddlYarn3" runat="server" Width="200px">
                   </asp:DropDownList>
                   </ContentTemplate>
                                 <Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="ddlYarnType3" 
                                         EventName="SelectedIndexChanged" />
                                 </Triggers>
                           </asp:UpdatePanel> 
               </td>
               <td align="left" class="style7">
                   Use %<asp:TextBox ID="txtUserPercent3" runat="server" Width="57px"></asp:TextBox>KG
                   <asp:TextBox ID="txtKG3" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>
                   <asp:HiddenField ID="hdnValue" runat="server" />
               </td>
               <td>
                   &nbsp;</td>
           </tr>
           </table>
           
        </div>
        <div>
        </div>
        </fieldset>
   </div>
 
<fieldset>
 <asp:UpdatePanel ID="upGrid" runat="server" UpdateMode="Conditional">
   <ContentTemplate>
   <legend> Assign MC</legend>
   <div>
       MC No<asp:DropDownList ID="ddlMCNo0" runat="server">
       </asp:DropDownList>
          &nbsp;Assign Qty
   <asp:TextBox ID="txtAssignQty" runat="server" Width="88px"></asp:TextBox>
       
       <asp:Button ID="btnLoad" runat="server" Text="Add/Edit MC" 
           onclick="btnLoad_Click" />
      
       Remaining Qty<asp:TextBox ID="txtRemaining" runat="server" Width="81px"></asp:TextBox>Finish 
       Qty:
       <asp:TextBox ID="txtRemaining0" runat="server" Width="81px"></asp:TextBox>
       </div>
    <div style="overflow:auto; height:200px;" >
                  
                        
   </div>
   
    </ContentTemplate>
                   
   </asp:UpdatePanel> 
       </fieldset>
   
 
   
   
   <div>
       <asp:Button ID="btnSave" runat="server" Text="Save" 
           Height="40px" Width="117px" onclick="btnSave_Click" />
       <asp:Button ID="btnCancel" runat="server" Text="Clear" Width="92px" 
           Height="40px" onclick="btnCancel_Click" />
           
            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
   </div>
     
   
   
</div>
</asp:Content>
