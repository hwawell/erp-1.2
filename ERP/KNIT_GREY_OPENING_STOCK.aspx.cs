﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;

namespace ERP
{
    public partial class KNIT_GREY_OPENING_STOCK : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                loadFabricGroup();
              
            }
        }
        private void loadFabricGroup()
        {


            ddGroup.DataSource = new Fabric_CL().GetAllFabricGroup();
            ddGroup.DataValueField = "SL";
            ddGroup.DataTextField = "FGroupName";
            ddGroup.DataBind();
            ddGroup.Items.Add("All");
            ddGroup.Text = "All";
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            Int64 ID = Common.GetNumber(ddGroup.SelectedValue.ToString());
            gvData.DataSource = new KNIT_all_operation().GetGreyOpeningBalance(ID);
            gvData.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<EGreyOpeningBalance> li=new List<EGreyOpeningBalance>();
            EGreyOpeningBalance obj;
            foreach (GridViewRow ob in gvData.Rows)
            {
                obj=new EGreyOpeningBalance();
                obj.ID = Convert.ToInt64(ob.Cells[0].Text);
                TextBox tb = (TextBox)ob.FindControl("txtQty");
                if (tb != null && tb.Text.Length>0)
                {
                    obj.Qty =Common.GetDouble(tb.Text);
                    li.Add(obj);
                }

            }
            bool res = new KNIT_all_operation().SaveGreyOpeningStock(li);
            if (res == true)
            {
                lblStatus.Text = "Data Saved Successfully";
            }
            else
            {
                lblStatus.Text = "Operation Failed";
            }
            mpeProduct.Show();
        }
        protected void btnOK_Click(object sender, EventArgs e)
        {

        }
    }
}
