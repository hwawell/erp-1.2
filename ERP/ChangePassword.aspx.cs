﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BDLayer;
namespace ERP
{
    public partial class WebForm33 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool res = new Utilities_DL().HasChangePassword(USession.SUserID, txtOldPassword.Text, txtNew.Text);
            if (res == true)
            {
                txtNew.Text = "";
                txtOldPassword.Text = "";
                txtConfirm.Text = "";
                lblStatus.Text = "Password has Changed";
            }
            else
            {
                lblStatus.Text = "Password has not Changed";
            }
        }
    }
}
