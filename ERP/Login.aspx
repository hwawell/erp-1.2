﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="OCM.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" type="text/css" href="css/bootstrap/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="css/Stylesheet.css"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap/css/bootstrap-theme.min.css"/>
<script src="css/bootstrap/js/bootstrap.min.js"></script>

<!-- Latest compiled and minified JavaScript -->

    <title>Login..</title>
  
</head>
<body >
    <form id="form1" runat="server" 
    >
    
    <div class="container1">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title"><b>ERP Solution </b></h1>
            <h1 class="text-center login-title">Hwawell Textiles (BD) Limited </h1>
            <div class="account-wall">
                <img class="profile-img" src="Images/logo.png"
                    alt="">
                <form class="form-signin">
                <input type="text"  runat="server" ID="txtUser" class="form-control" placeholder="User ID" required autofocus>
                
                <input type="password" runat="server" ID="txtPass" class="form-control" placeholder="Password" required>
                
                    <asp:Button ID="Button1" class="btn btn-lg btn-primary btn-block" 
                    runat="server" Text="Sign in" onclick="Button1_Click2" />
                  
                <label class="checkbox pull-left">
                   
                </label>
                
                </form>
            </div>
            
        </div>
    </div>
</div>
    

    </form>
</body>
</html>
