﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="OrderStatus.aspx.cs" Inherits="ERP.WebForm20" Title="Order Status" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style3
        {
            width: 300px;
        }
        .style4
        {
            width: 300px;
            height: 28px;
        }
        .style5
        {
            height: 28px;
        }
        .style6
        {
            height: 28px;
            width: 327px;
            font-weight: bold;
        }
        .style7
        {
            width: 327px;
        }
        .style8
        {
            width: 300px;
            height: 14px;
        }
        .style9
        {
            width: 327px;
            height: 14px;
        }
        .style10
        {
            height: 14px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 95%;">
        <tr>
            <td align="center" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
            </td>
            <td class="style6">
                Find Order Status</td>
            <td class="style5">
            </td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style6">
                &nbsp;</td>
            <td class="style5">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                <asp:Label ID="Label1" runat="server" Text="Choose Order No"></asp:Label>
            </td>
            <td class="style7">
                <asp:TextBox ID="txtOrderNo" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style8">
            </td>
            <td class="style9">
            </td>
            <td class="style10">
            </td>
        </tr>
        <tr>
            <td align="right" class="style3">
                &nbsp;</td>
            <td class="style7">
                <asp:Button ID="btnReport" runat="server" Text="Show Report" 
                    onclick="btnReport_Click" style="height: 26px" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
