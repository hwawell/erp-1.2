<%@ Page Title="Fabric Report" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="FebricReport.aspx.cs" Inherits="ERP.FebricReport" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style3
        {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width:100%;">
        <tr>
            <td class="style3">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td style="font-size: medium; font-weight: bold">
                Fabric Report</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                Fabric Group</td>
            <td>
                <asp:DropDownList ID="ddGroup" runat="server" AutoPostBack="True" Height="21px" 
                    onselectedindexchanged="ddGroup_SelectedIndexChanged" Width="170px">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                Fabric
            </td>
            <td>
                <asp:DropDownList ID="ddFabric" runat="server" Height="21px" 
                    onselectedindexchanged="DropDownList2_SelectedIndexChanged" Width="170px">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                Order</td>
            <td>
                <asp:DropDownList ID="ddOrder" runat="server" Height="21px" Width="169px">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                From Date</td>
            <td>
                <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtDate">
                </cc1:CalendarExtender>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" class="style3">
                To Date</td>
            <td>
                <asp:TextBox ID="txtTodate" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="txtTodate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtTodate">
                </cc1:CalendarExtender>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="style3" colspan="2">
                <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                    Text="Fabric Production" />
                <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                    Text="Fabric Delivery" />
                <asp:Button ID="Button3" runat="server" Text="Fabric Order Status" 
                    Width="148px" onclick="Button3_Click" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
