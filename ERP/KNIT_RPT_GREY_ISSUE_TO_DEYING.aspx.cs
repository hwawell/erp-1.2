﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
using System.Reflection;
namespace ERP
{
    public partial class KNIT_RPT_GREY_ISSUE_TO_DEYING : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                loadOrder();
            }
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            List<ERptGreyIssueToDeying> liProd = new List<ERptGreyIssueToDeying>();
            this.Window1.Title = "Grey Issue To Deying";
            string Dt = "";// = "1";
            if (chkDate.Checked == true)
            {
                Dt = txtFrom.Text;
            }
            else
            {
                Dt = "1/1/2000";
            }
            string OrderNo = "";
            if (tbAuto.Text.Length == 0)
            {
                OrderNo = "--All Order--";
            }
            else
            {
                OrderNo = tbAuto.Text;
            }


            liProd = new KNIT_all_operation().GetRPT_GreyIssueToDeying(Dt, OrderNo);
            Export("GREY_ISSUE_TO_Deying_" + txtFrom.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, liProd);
        }
        private void loadOrder()
        {
           

        }
        public void Export(string fileName, List<ERptGreyIssueToDeying> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2>Grey Issue To Deying - " + txtFrom.Text + "</h2>  </div>");
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            foreach (PropertyInfo proinfo in new ERptGreyIssueToDeying().GetType().GetProperties())
            {

                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(proinfo.Name);
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");


            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (ERptGreyIssueToDeying emp in empList)
            {
                TableRow row1 = new TableRow();
                HttpContext.Current.Response.Write("</TR>");
                foreach (PropertyInfo proinfo in new ERptGreyIssueToDeying().GetType().GetProperties())
                {
                    HttpContext.Current.Response.Write("<Td>");
                    //Get column headers  and make it as bold in excel columns
                    HttpContext.Current.Response.Write("<B>");
                    if (proinfo.GetValue(emp, null) != null)
                    {
                        HttpContext.Current.Response.Write(proinfo.GetValue(emp, null).ToString());
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("");
                    }
                    HttpContext.Current.Response.Write("</B>");
                    HttpContext.Current.Response.Write("</Td>");

                }

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }
    }
}
