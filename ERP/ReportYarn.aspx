﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ReportYarn.aspx.cs" Inherits="ERP.WebForm24" Title="Untitled Page" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style3
        {
            color: #003366;
            font-weight: bold;
            height: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
 <div align="center" class="style3">
 Yarn Balance Report
 </div>
 <div align="center" style="height: 40px">
 
     Date From
     <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
     <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
         Enabled="True" TargetControlID="txtDate">
     </cc1:CalendarExtender>
&nbsp;To
     <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
     <cc1:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" 
         Enabled="True" TargetControlID="txtToDate">
     </cc1:CalendarExtender>
 
 </div>
 <div style="text-align: center; height: 43px;">
     <asp:Button ID="btnShow" runat="server" Height="25px" Text="Stock Summary" 
         Width="148px" onclick="btnShow_Click" />
    
 </div>
  <div style="text-align: center">
     
                                    <asp:DropDownList ID="ddlIssueTo" runat="server" 
          Height="26px" Width="149px" 
                                        AutoPostBack="True" TabIndex="2">
                                      
                                         <asp:ListItem Value="ISSUE">ISSUE</asp:ListItem>
                                        <asp:ListItem Value="RETURN">RETURN</asp:ListItem>
                                        <asp:ListItem Value="LOAN">LOAN</asp:ListItem>
                                        <asp:ListItem Value="SC">SC</asp:ListItem>
                                        
                                         <asp:ListItem>ALL</asp:ListItem>
                                        
                                    </asp:DropDownList>
                                        
                                   
                                   
     <asp:Button ID="Button2" runat="server" Height="25px" Text="Issue Report" 
         Width="148px" onclick="btnShow0_Click" />

 </div>
  <div style="text-align: center">

                                    <asp:DropDownList ID="ddlType" runat="server" Height="26px" 
                                        Width="150px" TabIndex="2">
                                        <asp:ListItem Value="RECEIVE">RECEIVE</asp:ListItem>
                                        <asp:ListItem Value="RETURN">RETURN</asp:ListItem>
                                        <asp:ListItem Value="LOAN">LOAN</asp:ListItem>
                                        <asp:ListItem Value="SC">SC</asp:ListItem>
                                        
                                        
                                        <asp:ListItem>ALL</asp:ListItem>
                                        
                                        
                                    </asp:DropDownList>

     <asp:Button ID="Button6" runat="server" Height="25px" Text="Receive Report" 
         Width="148px" onclick="btnShow1_Click" />
 </div>
</asp:Content>
