﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_BalanceSheet.aspx.cs" Inherits="ERP.KNIT_BalanceSheet" Title="Untitled Page" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            List<EKnitingBalanceSheet> liProd = new List<EKnitingBalanceSheet>();
            this.Window1.Title = "Knit Production Balance Sheet";
            liProd = new KNIT_all_operation().GetKNITBalanceSheet(Convert.ToDateTime(txtFrom.Text));
                    
            
            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
     [DirectMethod(Namespace = "NETBEE")]
     public void XLExport()
     {
         List<EKnitingBalanceSheet> liProd = new List<EKnitingBalanceSheet>();
         
         liProd = new KNIT_all_operation().GetKNITBalanceSheet(Convert.ToDateTime(txtFrom.Text));
         string json = JSON.Serialize(liProd);
         StoreSubmitDataEventArgs eSubmit = new StoreSubmitDataEventArgs(json, null);
         XmlNode xml = eSubmit.Xml;

         this.Response.Clear();
         this.Response.ContentType = "application/octet-stream";
         this.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xlsx");
         XslCompiledTransform xtCsv = new XslCompiledTransform();
         xtCsv.Load(Server.MapPath("Excel.xslx"));
         xtCsv.Transform(xml, null, this.Response.OutputStream);
         this.Response.End();

         //string json = GridData.Value.ToString();
         //StoreSubmitDataEventArgs eSubmit = new StoreSubmitDataEventArgs(json, null);
         //XmlNode xml = eSubmit.Xml;

         //this.Response.Clear();
         //this.Response.ContentType = "application/vnd.ms-excel";
         //this.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls");
         //XslCompiledTransform xtExcel = new XslCompiledTransform();
         //xtExcel.Load(Server.MapPath("Excel.xsl"));
         //xtExcel.Transform(xml, null, this.Response.OutputStream);
         //this.Response.End();

     }
     protected void SubmitGrids(object sender, DirectEventArgs e)
     {
         //////JSON representation
         ////string grid1Json = e.ExtraParams["Grid1"];

         ////StoreSubmitDataEventArgs eSubmit = new StoreSubmitDataEventArgs(grid1Json, null);
         ////XmlNode xml = eSubmit.Xml;

         ////this.Response.Clear();
         ////this.Response.ContentType = "application/octet-stream";
         ////this.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls");
         ////XslCompiledTransform xtCsv = new XslCompiledTransform();
         ////xtCsv.Load(Server.MapPath("Excel.xsl"));
         ////xtCsv.Transform(xml, null, this.Response.OutputStream);
         ////this.Response.End();

         string json = e.ExtraParams["Grid1"];
         StoreSubmitDataEventArgs eSubmit = new StoreSubmitDataEventArgs(json, null);
         XmlNode xml = eSubmit.Xml;

         this.Response.Clear();
         this.Response.ContentType = "application/vnd.ms-excel";
         this.Response.AddHeader("Content-Disposition", "attachment; filename=submittedData.xls");
         XslCompiledTransform xtExcel = new XslCompiledTransform();
         xtExcel.Load(Server.MapPath("Excel.xls"));
         xtExcel.Transform(xml, null, this.Response.OutputStream);
         this.Response.End();
     }
    
     protected void Store1_RefreshData(object sender, StoreRefreshDataEventArgs e)
     {
         List<EKnitingBalanceSheet> liProd = new List<EKnitingBalanceSheet>();
         this.Window1.Title = "Knit Production Balance Sheet";
         liProd = new KNIT_all_operation().GetKNITBalanceSheet(Convert.ToDateTime(txtFrom.Text));


         this.Store1.DataSource = liProd;
         this.Store1.DataBind();
     }
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> Production Summary Report</h2></div>
 <div align="left" style="padding: 10px; ">
                      From
                    <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom">
                    </cc1:CalendarExtender>
                   <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                    </div>
                       
                </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
            OnRefreshData="Store1_RefreshData"
            >
            <Reader>
            <ext:JsonReader IDProperty="ID">
                    <Fields>
                      <ext:RecordField Name="ID" />
                      <ext:RecordField Name="OrderNo" />
                        <ext:RecordField Name="Fabric" />
                         <ext:RecordField Name="GM" />
                         <ext:RecordField Name="GSM" />
                         <ext:RecordField Name="MCNo" />
                         <ext:RecordField Name="Customer" />
                         <ext:RecordField Name="RunningTime" />
                         <ext:RecordField Name="ProductionQty" />
                          <ext:RecordField Name="PrevQty" />
                          <ext:RecordField Name="RcvQty" />
                          <ext:RecordField Name="IssueQty" />
                          <ext:RecordField Name="GreyIssueQty" />
                    </Fields>
                </ext:JsonReader>
              
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Production Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            
                            <ext:Column ColumnID="OrderNo" Header="OrderNo"  DataIndex="OrderNo" Sortable="true" />
                            <ext:Column ColumnID="Fabric" Header="Fabric"  DataIndex="Fabric" Sortable="true" />
                            <ext:Column ColumnID="GM" Header="GM"  DataIndex="GM" Sortable="true" />
                            <ext:Column ColumnID="GSM" Header="GSM"  DataIndex="GSM" Sortable="true" />
                            <ext:Column ColumnID="MCNo" Header="MCNo"  DataIndex="MCNo" Sortable="true" />
                            <ext:Column ColumnID="Customer" Header="Customer"  DataIndex="Customer" Sortable="true" />
                            <ext:Column ColumnID="RunningTime" Header="RunningTime"  DataIndex="RunningTime" Sortable="true" />
                            <ext:Column ColumnID="ProductionQty" Header="ProductionQty"  DataIndex="ProductionQty" Sortable="true" />
                            <ext:Column ColumnID="PrevQty" Header="PrevQty"  DataIndex="PrevQty" Sortable="true" />
                            <ext:Column ColumnID="RcvQty" Header="RcvQty"  DataIndex="RcvQty" Sortable="true" />
                            <ext:Column ColumnID="IssueQty" Header="IssueQty"  DataIndex="IssueQty" Sortable="true" />
                            <ext:Column ColumnID="GreyIssueQty" Header="GreyIssueQty"  DataIndex="GreyIssueQty" Sortable="true" />
                                                             
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="OrderNo" />
                                <ext:StringFilter DataIndex="Fabric" />
                                <ext:StringFilter DataIndex="GM" />
                                <ext:StringFilter DataIndex="GSM" />
                                <ext:StringFilter DataIndex="MCNo" />
                                <ext:StringFilter DataIndex="Customer" />
                                <ext:DateFilter DataIndex="RunningTime" />
                                <ext:NumericFilter DataIndex="ProductionQty" />
                                <ext:NumericFilter DataIndex="PrevQty" />
                                 <ext:NumericFilter DataIndex="RcvQty" />
                                  <ext:NumericFilter DataIndex="IssueQty" />
                                   <ext:NumericFilter DataIndex="GreyIssueQty" />
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>
