﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ReportChemical.aspx.cs" Inherits="ERP.WebForm23" Title="Untitled Page" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style3
        {
            color: #003366;
            font-weight: bold;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
  <div align="center" style="height: 86px" class="style3">
   Chemical Inventory Report
  </div>
   <div align="center" style="height: 42px">
  
      <asp:Label ID="Label2" runat="server" Text="From"></asp:Label>
      &nbsp;<asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
      <cc1:CalendarExtender ID="txtFrom_CalendarExtender" runat="server" 
          Enabled="True" TargetControlID="txtFrom">
      </cc1:CalendarExtender>
&nbsp;&nbsp; To <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
      <cc1:CalendarExtender ID="txtTo_CalendarExtender" runat="server" Enabled="True" 
          TargetControlID="txtTo">
      </cc1:CalendarExtender>
  
  </div>
  <div align="center" style="height: 33px">
  
      <asp:Label ID="Label1" runat="server" Text="Select Chemical Group :"></asp:Label>
&nbsp;<asp:DropDownList ID="ddlGroup" runat="server" Height="22px" Width="161px">
      </asp:DropDownList>
  
  </div>
  <div align="center">
      <asp:Button ID="Button1" runat="server" Text="Show Report" 
          onclick="Button1_Click" />
  </div>
</asp:Content>
