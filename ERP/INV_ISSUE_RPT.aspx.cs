﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
using System.Collections.Generic;
using System.Reflection;

namespace ERP
{
    public partial class INV_ISSUE_RPT : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadBasicData();
            }
        }
        private void LoadBasicData()
        {

            string Head = new Inventory_DL().GetInventoryType(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            this.Title = Head + " - Inventory Issue";
            lblHead.Text = "Issue Report For " + Head;


            Inventory_DL objDL = new Inventory_DL();
            ddlYarnGroup.DataSource = objDL.GetItemGroup(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlYarnGroup.DataTextField = "GroupName";
            ddlYarnGroup.DataValueField = "SL";
            ddlYarnGroup.DataBind();
            ddlYarnGroup.Items.Insert(0, "All Group");



            ddlType.DataSource = objDL.GetIssueType(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlType.DataTextField = "TypeName";
            ddlType.DataValueField = "ID";
            ddlType.DataBind();

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            List<EINV_Issue> liProd = new List<EINV_Issue>();
            Int32 gid = 0;


            if (ddlYarnGroup.SelectedItem.Text == "All Group")
            {
                gid = 0;
            }
            else
            {
                gid = Convert.ToInt32(ddlYarnGroup.SelectedValue.ToString());
            }
            liProd = new Inventory_DL().GetInventoryIssueReport(Convert.ToInt32(Request.QueryString["INVTypeID"]), Convert.ToInt32(ddlType.SelectedValue), gid, txtFrom0.Text, txtFrom1.Text);



            Export("Issue_Report_" + txtFrom0.Text + "_" + System.DateTime.Now.TimeOfDay.Hours + "_" + System.DateTime.Now.TimeOfDay.Minutes, liProd);
        }
        public void Export(string fileName, List<EINV_Issue> empList)
        {

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName + ".xls"));
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<div> <h2>" + lblHead.Text + " From" + txtFrom0.Text + " To" + txtFrom1.Text + " </h2>  </div>");
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> " +
              "  <TR>");

            HttpContext.Current.Response.Write("<Td><B>Sl</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Issue Date</B></Td>");
           
            if (Request.QueryString["INVTypeID"] != "2")
            {
                HttpContext.Current.Response.Write("<Td><B>Country</B></Td>");
                HttpContext.Current.Response.Write("<Td><B>Supplier</B></Td>");
            }
           
            HttpContext.Current.Response.Write("<Td><B>InvoiceNo</B></Td>");
            //HttpContext.Current.Response.Write("<Td><B>LCNo</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>GatePassNo</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>LoanCompany</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Group Name</B></Td>");
            //HttpContext.Current.Response.Write("<Td><B>Sub Group</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>ItemName</B></Td>");
            //if (Request.QueryString["INVTypeID"] != "2")
            //{
                HttpContext.Current.Response.Write("<Td><B>BoxUNIT</B></Td>");
                HttpContext.Current.Response.Write("<Td><B>BoxQty</B></Td>");
            //}
         
            HttpContext.Current.Response.Write("<Td><B>TotalQty</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Unit</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>Notes</B></Td>");
            HttpContext.Current.Response.Write("<Td><B>ReceiveType</B></Td>");


            HttpContext.Current.Response.Write("</TR>");
            int i = 0;
            foreach (EINV_Issue emp in empList)
            {

                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<Td>" + i++ + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.Idate.Value.ToString("dd-MMM-yyyy") + "</Td>");
                if (Request.QueryString["INVTypeID"] != "2")
                {
                    HttpContext.Current.Response.Write("<Td>" + emp.Country + "</Td>");
                    HttpContext.Current.Response.Write("<Td>" + emp.Supplier + "</Td>");
                }
                HttpContext.Current.Response.Write("<Td>" + emp.InvoiceNo + "</Td>");
                //HttpContext.Current.Response.Write("<Td>" + emp.LCNo + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.GatePassNo + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.LoanCompany + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.GroupName + "</Td>");
                //HttpContext.Current.Response.Write("<Td>" + emp.ParentName + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.ItemName + "</Td>");
                //if (Request.QueryString["INVTypeID"] != "2")
                //{
                    HttpContext.Current.Response.Write("<Td>" + emp.BoxUnit + "</Td>");
                    HttpContext.Current.Response.Write("<Td>" + emp.BoxQty + "</Td>");
                //}
                HttpContext.Current.Response.Write("<Td>" + emp.TotalQty + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.Unit + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.Notes + "</Td>");
                HttpContext.Current.Response.Write("<Td>" + emp.IssueType + "</Td>");

            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.End();

        }
    }
}
