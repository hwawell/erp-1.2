﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;

namespace ERP
{
    public partial class WebForm20 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ////ddlOrder.DataSource =new Order_DL().GetActiveRecord();
                ////ddlOrder.DataTextField = "PINO";
                ////ddlOrder.DataValueField = "PINO";
                ////ddlOrder.DataBind();
                ////ddlOrder.Items.Insert(0, "--Select--");
            }
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            string jscript = "";
           
            string ReportName = "";
            string pr ="ORDER_STATUS" ;

            ReportName = "rptOrderStatus";
                    

            string Fdate = txtOrderNo.Text.ToString();
            string ToDate = "None";


            string RTYpe = "Order Status Report"; //rdoStatement.SelectedItem.Text.ToString();
            jscript += "<script language=javascript>";
            jscript += "window.open('./ReportPreview.aspx?Type=" + RTYpe + "&ReportName=" + ReportName + "&Fdate=" + Fdate + "&TDate=" + ToDate + "&Process=" + pr + "'";
            jscript += ",''";
            //jscript += ",'scrollbars=2'";
            //jscript += ",'resizable=1'";
            jscript += ",'status,resizable,scrollbars'";
            jscript += "); ";
            jscript += "</script>";
            //string script = "var windowObject = window.self; windowObject.opener = window.self; windowObject.close();";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Close Window", script, true);
            this.Page.ClientScript.RegisterClientScriptBlock(typeof(WebForm4), "ScriptFunction", jscript);
        }
    }
}
