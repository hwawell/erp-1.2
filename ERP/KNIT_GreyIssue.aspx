﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_GreyIssue.aspx.cs" Inherits="ERP.KNIT_GreyIssue" Title="Grey Issue To Lab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<div>

</div>
<div>
<fieldset>
<legend><h4> Order Product Info</h4></legend>
  
    <div style="float: left; width: 20%" align="right">
        Order No :
    </div>
    <div style="float: left; width: 80%" align="left">
        <asp:DropDownList ID="ddlOrder" runat="server" Height="21px" Width="201px" 
            AutoPostBack="True" onselectedindexchanged="ddlOrder_SelectedIndexChanged">
        </asp:DropDownList>
    </div>
    <div style="float: left; width: 20%" align="right">
        Order Product :     </div>
    <div style="float: left; width: 80%" align="left">
            <asp:UpdatePanel id="upOP" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
    
    
                <asp:DropDownList ID="ddlOP" runat="server" Height="21px" Width="700px" 
                        AutoPostBack="True" onselectedindexchanged="ddlOP_SelectedIndexChanged">
                </asp:DropDownList>
            </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlOrder" 
                        EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
    </div>
    <div style="float: left; width: 20%" align="right">
        Color :
    </div>
    <div style="float: left; width: 80%" align="left">
     <asp:UpdatePanel id="upColor" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
        <asp:DropDownList ID="ddlColor" runat="server" Height="21px" Width="300px">
        </asp:DropDownList>
        </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlOP" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
    </div>
   
    
 </fieldset>   
    <fieldset>
    <legend> <h4> Grey Issue To Lab Entry</h4></legend>
        <asp:UpdatePanel ID="upEntry" runat="server" UpdateMode="Conditional">
     <ContentTemplate>
     <div>
 
     
     
     
            <div style="float: left; width: 20%" align="right">
                Fabric Group :
            </div>
            <div style="float: left; width: 80%" align="left">
                <asp:DropDownList ID="ddlFG" runat="server" Height="22px" Width="249px" 
                    AutoPostBack="True" onselectedindexchanged="ddlFG_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            
            <div style="float: left; width: 20%" align="right">
                Knitt (Fabric):
            </div>
            <div style="float: left; width: 80%" align="left">
            <%--<asp:UpdatePanel id="UpdatePanel1" runat="server" UpdateMode="Conditional">
            
                <ContentTemplate>--%>
                <asp:DropDownList ID="ddlFabric" runat="server" Height="22px" Width="388px">
                </asp:DropDownList>
              <%-- </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlFG" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>--%>
                
            </div>
            
            <div style="float: left; width: 20%" align="right">
                GM :
            </div>
            <div style="float: left; width: 15%" align="left">
                 <asp:TextBox ID="txtGM" runat="server"></asp:TextBox>
            </div>
            <div style="float: left; width: 10%" align="right">
                GSM :
            </div>
            <div style="float: left; width: 55%" align="left">
                 <asp:TextBox ID="txtGSM" runat="server"></asp:TextBox>
            </div>
            
            <div style="float: left; width: 20%" align="right">
                MC No :
            </div>
            <div style="float: left; width: 15%" align="left">
                <asp:DropDownList ID="ddlMCNo" runat="server" Height="21px" Width="128px" >
                </asp:DropDownList>
            </div>
            <div style="float: left; width: 15%" align="right">
                Issue Qty (KG):
            </div>
            <div style="float: left; width: 50%" align="left">
                 <asp:TextBox ID="txtIssueQty" runat="server"></asp:TextBox>           
            </div>
            
            <div style="float: left; width: 20%" align="right">
                Issue Date:
            </div>
            <div style="float: left; width: 80%" align="left">
                <asp:TextBox ID="txtIssueDate" runat="server"></asp:TextBox> 
                 <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                                          Enabled="True" Format="MM/dd/yyyy" TargetControlID="txtIssueDate">
                 </cc1:CalendarExtender> 
            </div>
     
           <%-- <asp:UpdatePanel ID="upSave" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
            --%>
            
                <div>
                <asp:Button ID="btnSave" runat="server" Text="Save" Width="85px" 
                    onclick="btnSave_Click" />
                <asp:Button ID="btnClear" runat="server" Text="Clear" Width="74px" 
                        onclick="btnClear_Click" />
                <asp:Label ID="lblStatus" runat="server" ></asp:Label>
            </div>
            
           <%-- </ContentTemplate>
            </asp:UpdatePanel>--%>
     </div>
       </ContentTemplate>
     </asp:UpdatePanel>
    </fieldset>
    
    <fieldset>
    <legend> <h4>View Grey Issued For the Order</h4></legend>
    <div>
        <div> 
            <asp:Button ID="btnView" runat="server" Text="View Grey Issue" 
                onclick="btnView_Click" /></div>
            
         <div>
             <asp:GridView ID="gvData" runat="server" onrowdatabound="gvData_RowDataBound">
             <Columns>
              <asp:TemplateField HeaderText="Operation"  HeaderStyle-HorizontalAlign="Left" >
                                    <ItemTemplate>
                                        <asp:Button ID="btnOperation" runat="server" Text="Delete" onclick="btnOperation_Click"
                                        onclientclick="javascript:return confirm('Do you want to Stop this Machine?');"
                                        />
                           
                                    </ItemTemplate>
                                        
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                   
                                 
                                
             </Columns>
             </asp:GridView>
         </div>
    </div>
    </fieldset>
</div>
</asp:Content>
