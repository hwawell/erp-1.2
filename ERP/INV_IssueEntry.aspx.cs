﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;
namespace ERP
{
    public partial class INV_IssueEntry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (new Utilities_DL().CheckSecurity("502", USession.SUserID) == true)
                //{

                loadGridData();

                //}
                //else
                //{
                //    Response.Redirect("Home.aspx");
                //}
            }
        }
        private void LoadBasicData()
        {
            Inventory_DL obj = new Inventory_DL();

            IQueryable li = obj.GetUnit(Convert.ToInt32(Request.QueryString["INVTypeID"]));



            ddlUnit.DataSource = li;
            ddlUnit.DataTextField = "UnitName";
            ddlUnit.DataValueField = "ID";
            ddlUnit.DataBind();


            ddlType.DataSource = obj.GetIssueType(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlType.DataTextField = "TypeName";
            ddlType.DataValueField = "ID";
            ddlType.DataBind();

            ddlStore.DataSource = obj.GetStore(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlStore.DataTextField = "StoreName";
            ddlStore.DataValueField = "ID";
            ddlStore.DataBind();

            ddlLander.DataSource = obj.GetLander(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlLander.DataTextField = "LName";
            ddlLander.DataValueField = "ID";
            ddlLander.DataBind();

            ddlLander.Items.Insert(0, "NONE");

           
            // ddlGroup0.Items.Insert(0, "NONE");



        }
        private void loadGridData()
        {


            string Head = new Inventory_DL().GetInventoryType(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            this.Title = Head + " - Inventory Issue";
            lblHeader.Text = "Inventory Issue Entry for " + Head;

            LoadGroup();

            LoadBasicData();

            loadGrid();

        }
        private void loadGrid()
        {
            gvProducts.DataSource = new Inventory_DL().GetInventoryIssue(Convert.ToInt32(Request.QueryString["INVTypeID"]), Convert.ToInt32(ddlGroup.SelectedValue));
            gvProducts.DataBind();
        }


        private void LoadGroup()
        {
            Inventory_DL objDL = new Inventory_DL();
            ddlGroup.DataSource = objDL.GetItemGroup(Convert.ToInt32(Request.QueryString["INVTypeID"]));
            ddlGroup.DataTextField = "GroupName";
            ddlGroup.DataValueField = "SL";
            ddlGroup.DataBind();

        }


        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {

            lblResult.Text = string.Empty;
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;

            try
            {
                txtItemID.Text = row.Cells[1].Text;
                if (row.Cells[13].Text.Trim() != "0")
                {
                    ddlStore.SelectedValue = row.Cells[13].Text;
                }
                lblStore.Text = ddlStore.SelectedItem.Text;
                EINV_Item objL = new Inventory_DL().GetInventoryItemBasicByItem(Convert.ToInt32(Request.QueryString["INVTypeID"]), Convert.ToInt32(ddlGroup.SelectedValue), Convert.ToInt64(row.Cells[1].Text), Convert.ToInt32(ddlStore.SelectedValue));
                if (objL != null)
                {
                    txtBalance.Text = (objL.StockBalance + Convert.ToDouble(row.Cells[6].Text)).ToString();
                }
                txtItem.Text = row.Cells[4].Text;
                txtItem1.Text = row.Cells[4].Text;

                ddlType.Text = row.Cells[2].Text;

                if (row.Cells[3].Text == "0")
                {
                    ddlLander.Text = "NONE";
                }
                else
                    ddlLander.SelectedValue = row.Cells[3].Text;



                ddlUnit.SelectedItem.Text = row.Cells[5].Text;
                txtTotalQty.Text = row.Cells[6].Text;



                txtInvoiceNo.Text = row.Cells[7].Text;
                txtGatePass.Text = row.Cells[8].Text;

                txtReceiveDate.Text = row.Cells[9].Text;// OpenFunction.ConvertDate(row.Cells[9].Text).Value.ToString("mm/dd/yyyy");
                txtNotes.Text = row.Cells[10].Text;
            }

            catch
            {

            }
            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            txtItemID.Text = row.Cells[1].Text;
            txtItem.Text = row.Cells[4].Text;
            txtItem1.Text = row.Cells[4].Text;
            ddlType.Text = row.Cells[2].Text;

            if (row.Cells[3].Text == "0")
            {
                ddlLander.Text = "NONE";
            }
            else
                ddlLander.SelectedValue = row.Cells[3].Text;


           
            ddlUnit.Text = row.Cells[5].Text;
            txtTotalQty.Text = row.Cells[6].Text;


           
            txtInvoiceNo.Text = row.Cells[7].Text;
            txtGatePass.Text = row.Cells[8].Text;

            txtReceiveDate.Text = OpenFunction.ConvertDate(row.Cells[9].Text).Value.ToString("mm/dd/yyyy");
            txtNotes.Text = row.Cells[10].Text;

        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ////ImageButton btnEdit = sender as ImageButton;
            ////GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            ////new Yarn_DL().DeleteActive(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            ////loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGrid();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                if (e.Row.Cells.Count > 2)
                {
                    e.Row.Cells[0].Visible = false;
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[2].Visible = false;
                    e.Row.Cells[3].Visible = false;
                   
                }


            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            SaveData();
            mpeProduct.Hide();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
            mpeProduct.Show();

        }

        private bool SaveData()
        {
            try
            {

                lblResult.Text = string.Empty;
                EINV_Issue c = new EINV_Issue();
                c.ID = Convert.ToInt32(hdnCode.Value);
                c.ItemID = Convert.ToInt32(txtItemID.Text);
                c.IssueTypeID = Convert.ToInt32(ddlType.SelectedValue);
                if (ddlLander.SelectedItem.Text == "NONE")
                    c.LanderID = 0;
                else
                    c.LanderID = Convert.ToInt32(ddlLander.SelectedValue);

               
             
                c.Unit = ddlUnit.SelectedItem.Text;
                c.TotalQty = Convert.ToDouble(txtTotalQty.Text);
                c.StoreID = Convert.ToInt32(ddlStore.SelectedValue);
                c.InvoiceNo = txtInvoiceNo.Text;
                c.GatePassNo = txtGatePass.Text;
                c.Idate = OpenFunction.ConvertDate(txtReceiveDate.Text);
               
                c.Notes = txtNotes.Text;
                c.EMode = "E";
                c.EntryID = USession.SUserID;

                c.INVTypeID = Convert.ToInt32(Request.QueryString["INVTypeID"]);

                string res = "0";
                if (c.ItemID > 0)
                {
                    res = new Inventory_DL().SaveInventoryIssue(c);
                    lblResult.Text = res;
                }
                if (res.Substring(0, 1) == "1")
                {
                    Clear();

                    updPanel.Update();
                }
                return true;
                // mpeProduct.Show();
            }

            catch (Exception ex)
            {
                lblResult.Text = ex.Message.ToString();
                return false;
                // lblResult.Text = ex.Message.ToString();
            }
        }
        private void Clear()
        {
            //lblResult.Text = string.Empty;
            txtNotes.Text = string.Empty;
            txtItem.Text = string.Empty;
            txtItem1.Text = string.Empty;
            txtItemID.Text = string.Empty;
         
            txtReceiveDate.Text = string.Empty;
            txtGatePass.Text = string.Empty;
            txtInvoiceNo.Text = string.Empty;
            
            txtTotalQty.Text = string.Empty;
            ddlLander.Text = "NONE";
            hdnCode.Value = "0";
            txtItem.Text = string.Empty;
        }
        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {


            Clear();
            lblResult.Text = "";
            lblStore.Text = ddlStore.SelectedItem.Text;
            updPanel.Update();




            mpeProduct.Show();
            txtItem.Focus();
        }



        protected void btnCancel_Click(object sender, EventArgs e)
        {
            // loadGridData();
            txtItem1.Text = string.Empty;
            loadGrid();
            mpeProduct.Hide();

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            loadGrid();
        }
    }
}

