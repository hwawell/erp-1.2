﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ChemicalInventory.aspx.cs" Inherits="ERP.WebForm22" Title="Chemical Inventory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style4
        {
        width: 240px;
    }
        .style5
        {
        width: 47px;
    }
        .modalBackground 
        {
	        background-color: Gray;
	        filter: alpha(opacity=50);
	        opacity: 0.5;
        }
        .modalBox 
        {
	        background-color : #f5f5f5;
	        border-width: 3px;
	        border-style: solid;
	        border-color: Blue;
	        padding: 3px;
        }
        .modalBox caption 
        {
	        background-image: url(images/window_titlebg.gif);
	        background-repeat:repeat-x;
        }
        .style6
        {
            width: 47px;
            height: 2px;
        }
        .style7
        {
            width: 240px;
            height: 2px;
        }
        .style8
        {
            height: 2px;
        }
               
    </style>
   <link type="text/css" href="css/grid.css" rel="stylesheet" />
    <link type="text/css" href="css/round.css" rel="stylesheet" />
    <link type="text/css" href="css/core.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <div class="grid" >
            <div class="rounded">
                <div class="top-outer"><div class="top-inner"><div  class="top">
                   <center> <h2>List of Chemical Inventory</h2></center>
                </div></div></div>
                <div class="mid-outer"><div class="mid-inner">
                <div class="mid" 
                        style="overflow:auto; width: 739px; height: 1600px; background-color: "whitesmoke";">     
                <div style="height:25px;">
                   
                    </div>
                <div style="height: 40px; border-bottom-style: none; border-bottom-color: #003399;">
                    <div style="float: left">
                </div>
                <div align="center" style="height: 30px">
                
                    </div>
                
                </div>
                <div style="height: 8px; border-bottom-style: groove; border-bottom-color: #003399;">
                
                </div>
                    <!-- Content Goes Here! -->
                  
                                 
                   
                    <asp:Panel ID="pnlDataEntry" runat="server"  CssClass="modalBox"  
                        BorderColor="#00009B" BorderStyle="Groove" BackColor="WhiteSmoke" 
                        Width="727px" BorderWidth="2px" Height="171px">
                        <center>
                            <asp:Panel ID="ProductCaption" runat="server"   
                                Style="margin-bottom: 10px; cursor: hand;" 
                                BorderStyle="None" BackImageUrl="~/Images/tm.gif" Font-Bold="True" 
                                Font-Names="Georgia" Font-Size="Medium" ForeColor="White">
                                Chemical Inventory Info</asp:Panel></center>
                            <cc1:CollapsiblePanelExtender ID="ProductCaption_CollapsiblePanelExtender" 
                                runat="server" Enabled="True" TargetControlID="ProductCaption">
                            </cc1:CollapsiblePanelExtender>
                         
                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4" align="right">
                                    Chemical Group</td>
                                <td>
                                    <asp:DropDownList ID="ddlChemicalGroup" runat="server" Height="16px" 
                                        Width="160px">
                                    </asp:DropDownList>
                                    
                                    
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4" align="right">
                                    Date Date</td>
                                <td>
                                    <asp:TextBox ID="txtStockDate" runat="server"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txtStockDate_CalendarExtender" runat="server" 
                                        Enabled="True" TargetControlID="txtStockDate">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td align="right" class="style4">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td align="right" class="style4">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            
                            
                            <tr>
                                <td class="style6">
                                    </td>
                                <td class="style7">
                                    &nbsp;</td>
                                <td class="style8">
                                    <asp:Button ID="btnSave" runat="server" Height="24px"  Text="Show Inventory" 
                                        Width="147px" onclick="btnSave_Click" Font-Names="Georgia" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style4">
                                    &nbsp;</td>
                                <td>
                                   
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                       
                </div></div></div>
            <div class="bottom-outer"><div class="bottom-inner">
            <div class="bottom"></div></div></div>                   
        </div>      
    </div>
</asp:Content>
