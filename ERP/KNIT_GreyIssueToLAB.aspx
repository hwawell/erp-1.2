﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="KNIT_GreyIssueToLAB.aspx.cs" Inherits="ERP.KNIT_GreyIssue" Title="Grey Issue To Lab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>  
	<script type="text/javascript">
	    $(function() {
	        $(".tb").autocomplete({
	            source: function(request, response) {
	                $.ajax({
	                url: "DataLoad.asmx/GetOrderList",
	                    data: "{ 'PINO': '" + request.term + "' }",
	                    dataType: "json",
	                    type: "POST",
	                    contentType: "application/json; charset=utf-8",
	                    dataFilter: function(data) { return data; },
	                    success: function(data) {
	                        response($.map(data.d, function(item) {
	                            return {
	                            value: item.OrderNo
	                            }
	                        }))
	                    },
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                       
	                    }
	                });
	            },
	            minLength: 2
	        });
	    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<div>

</div>
<div>
<fieldset>
<legend><h4> Grey Boooking For Lab</h4></legend>
  
    <div style="float: left; width: 20%" align="right">
        Order No :     </div>
    <div style="float: left; width: 80%" align="left">
         <asp:TextBox ID="tbAuto" class="tb" runat="server">
             </asp:TextBox>
                                      <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                          onclick="btnSearch_Click" />
         
    </div>
    <div style="float: left; width: 20%" align="right">
        Order Product :     </div>
    <div style="float: left; width: 80%" align="left">
            <asp:UpdatePanel id="upOP" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
    
    
                <asp:DropDownList ID="ddlOP" runat="server" Height="21px" Width="700px" 
                        AutoPostBack="True" onselectedindexchanged="ddlOP_SelectedIndexChanged">
                </asp:DropDownList>
            </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" 
                        EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
    </div>
    <div style="float: left; width: 20%" align="right">
        Color :     </div>
    <div style="float: left; width: 80%" align="left">
     <asp:UpdatePanel id="upColor" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
        <asp:DropDownList ID="ddlColor" runat="server" Height="21px" Width="300px" 
                        AutoPostBack="True" onselectedindexchanged="ddlColor_SelectedIndexChanged">
        </asp:DropDownList>
        
                    Req No:  <asp:DropDownList ID="ddlReq" runat="server" Height="21px" Width="128px" 
                        onselectedindexchanged="ddlReq_SelectedIndexChanged" AutoPostBack="True"> 
               </asp:DropDownList>
                    MC NO: <asp:DropDownList ID="ddlMCNo" runat="server" Height="21px" Width="128px" >
                   
               
                </asp:DropDownList>
                <br />
                <asp:HiddenField ID="hdnFID" runat="server" />
                    Fabric : 
                    <asp:Label ID="lblFabric" runat="server" Text=""></asp:Label>
        </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlOP" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
          
       
   
    </div>
    
      <div style="float: left; width: 20%" align="right">
                Issue Qty (KG) :
            </div>
            <div style="float: left; width: 80%" align="left">
                 <asp:TextBox ID="txtIssueQty" runat="server"></asp:TextBox>           
            </div>
   
     <div>
                <asp:Button ID="btnSave" runat="server" Text="Save" Width="85px" 
                    onclick="btnSave_Click" />
                <asp:Button ID="btnClear" runat="server" Text="Clear" Width="74px" 
                        onclick="btnClear_Click" />
                <asp:Label ID="lblStatus" runat="server" ></asp:Label>
                    <asp:HiddenField ID="hdnValue" runat="server" />
            </div>
 </fieldset>   
   
    
    <fieldset>
    <legend> <h4>View Grey Issued For the Order</legend>
    <div>
        <div> 
          <asp:TextBox  Issue Dateasp:TextBox ID="txtFrom0" runat="server" Height="22px" Width="100px"></asp:TextBox>
            <cc1:CalendarExtender 
                          ID="txtFrom0_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom0">
                    </cc1:CalendarExtender>
            <asp:Button ID="btnView" runat="server" Text="View Grey Issue" 
                onclick="btnView_Click" /></div>
            
         <div>
             <asp:GridView ID="gvData" runat="server" onrowdatabound="gvData_RowDataBound">
             <Columns>
              <asp:TemplateField HeaderText="Operation"  HeaderStyle-HorizontalAlign="Left" >
              
                                    <ItemTemplate>
                                        <asp:Button ID="btnOperation" runat="server" Text="Delete" onclick="btnOperation_Click"
                                        onclientclick="javascript:return confirm('Do you want to Stop this Machine?');"
                                        />
                           
                                    </ItemTemplate>
                                        
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                             
                                  
             </Columns>
             </asp:GridView>
         </div>
    </div>
    </fieldset>
</div>
</asp:Content>
