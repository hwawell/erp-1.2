﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BDLayer;

namespace ERP
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (new Utilities_DL().CheckSecurity("500", USession.SUserID) == true)
                {

                    this.rblNormal.Items.FindByText("Active").Selected = true;
                    loadGridData();
                }
                
                else
                {
                    Response.Redirect("Home.aspx");
                }
            }
        }
        private void loadGridData()
        {
           
           
            gvProducts.Columns[7].Visible = true;
            gvProducts.Columns[8].Visible = true;
            gvProducts.Columns[9].Visible = false;
            gvProducts.DataSource = new Customer_DL().GetActiveCustomers();
            gvProducts.DataBind();

        }
        private void loadDeletedData()
        {
            
            gvProducts.Columns[7].Visible = false;
            gvProducts.Columns[8].Visible = false;
            gvProducts.Columns[9].Visible = true;
            gvProducts.DataSource = new Customer_DL().GetDeletedCustomers();
            gvProducts.DataBind();

        }

        protected void btnEdit_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            hdnCode.Value = row.Cells[0].Text;
            txtCustomer.Text = (row.Cells[1].Text);
            txtAddress.Text = (row.Cells[2].Text);
            txtTelephone.Text = (row.Cells[3].Text);
            txtShort.Text = (row.Cells[4].Text);
            
            updPanel.Update();

            mpeProduct.Show();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Customer_DL().DeleteActive(Convert.ToInt64(row.Cells[0].Text), "D", USession.SUserID);
            loadGridData();
        }

        protected void btnActive_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btnEdit = sender as ImageButton;
            GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
            new Customer_DL().DeleteActive(Convert.ToInt64(row.Cells[0].Text), "E", USession.SUserID);
            loadDeletedData();
        }

        protected void gvProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProducts.PageIndex = e.NewPageIndex;
            loadGridData();
        }

        protected void gvProducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager)
            {
                e.Row.Cells[0].Visible = false;
            }
            if ((e.Row.RowType != DataControlRowType.Header) && (e.Row.RowType != DataControlRowType.Pager))
            {
                e.Row.Attributes["onmouseover"] = string.Format("javascript: this.t =this.style.backgroundColor; this.style.backgroundColor = 'LightSteelBlue';");
                e.Row.Attributes["onmouseout"] = string.Format("javascript:this.style.backgroundColor = this.t;");
            }
        }
        protected void btnSave_Click1(object sender, EventArgs e)
        {
            Customer c = new Customer();
            c.CustID = Convert.ToInt64(hdnCode.Value);
            c.CName = txtCustomer.Text.ToString();
            c.Address = txtAddress.Text.ToString();
            c.Tel = txtTelephone.Text.ToString();
            c.ConPerson = txtShort.Text.ToString();
            c.EntryID = USession.SUserID;
            //TBL_Product t = new TBL_Product();
            //t.ProductCode = txtPCode.Text.ToString();
            //t.ProductName = txtProduct.Text.ToString();
            //t.ProductDescription = txtDescription.Text.ToString();
            new Customer_DL().Save(c);
            loadGridData();
            //loadGridData();
            updPanel.Update();
            mpeProduct.Hide();

        }

        protected void ibtnNew_Click(object sender, ImageClickEventArgs e)
        {
            updPanel.Update();
            hdnCode.Value = "0";
            txtCustomer.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtTelephone.Text = string.Empty;
            txtShort.Text = string.Empty;
          
     
            mpeProduct.Show();
        }

        protected void rblNormal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblNormal.Items.FindByText("Active").Selected == true)
            {
                loadGridData();

            }
            else
            {
                loadDeletedData();
            }
        }
    }
}
