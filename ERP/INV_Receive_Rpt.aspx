﻿<%@ Page Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="INV_Receive_Rpt.aspx.cs" Inherits="ERP.INV_Receive_Rpt" Title="Untitled Page" %>
<%@ Import Namespace="System.Collections.Generic" %> 
<%@ Import Namespace="BDLayer" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Xml.Xsl" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Linq" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<script runat="server">
     [DirectMethod(Namespace = "NETBEE")]
    public void ShowReport()
    {
        //System.Threading.Thread.Sleep(3000);
       
        try
        {
            Window1.Show();
            List<EINV_Receive> liProd = new List<EINV_Receive>();
            Int64 gid = 0;

            if (ddlYarnGroup.SelectedItem.Text == "All Group")
            {
                gid = 0;
            }
            else
            {
                gid = Convert.ToInt64(ddlYarnGroup.SelectedValue.ToString());
            }
            liProd = new Inventory_DL().GetInventoryReceiveReport(Convert.ToInt32(Request.QueryString["INVTypeID"]), Convert.ToInt32(gid), Convert.ToInt32(ddlType.SelectedValue), txtFrom0.Text, txtFrom1.Text);

            
            this.Store1.DataSource = liProd;
            this.Store1.DataBind();
            
            X.Mask.Hide();
        }
        catch
        {
            X.Mask.Hide();
        }
       
    }
    
   
   
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
<div style="bottom: 20px"><h2> 
    <asp:Label ID="lblHead" runat="server" Text="Label"></asp:Label>
    </h2></div>
 <div align="left" style="padding: 10px; ">
                      Item Group
                   <div style="float: left">
                         <ext:Button ID="Button1" runat="server" Height="30" Width ="100" Text="Show Report">
                        <Listeners>
                            <Click Handler="Ext.net.Mask.show({ msg : 'Loading Production Data...' }); NETBEE.ShowReport();" />
                        </Listeners>
                        </ext:Button>
                         
                       <asp:Button ID="btnExport" runat="server" Text="Export To XL" 
                             onclick="btnExport_Click" />
                    </div>
                       
                                    <asp:DropDownList ID="ddlYarnGroup" runat="server" Height="24px" Width="157px" 
                                        TabIndex="1" AutoPostBack="false">
                                    </asp:DropDownList>
                                Receive Type  <asp:DropDownList ID="ddlType" runat="server" Height="22px" Width="248px" 
                                        TabIndex="4">
                                    </asp:DropDownList> Date
                    <asp:TextBox ID="txtFrom0" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom0_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom0">
                    </cc1:CalendarExtender>
                       
                      To
                    <asp:TextBox ID="txtFrom1" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="txtFrom1_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtFrom1">
                    </cc1:CalendarExtender>
                       
                </div>
                
<div>
        <ext:Hidden ID="GridData" runat="server" />
        <ext:Store ID="Store1" runat="server"   
           
            >
            <Reader>
            <ext:JsonReader IDProperty="ID">
                    <Fields>
                      <ext:RecordField Name="ID" />
                     
                           <ext:RecordField Name="SL" />
                          <ext:RecordField Name="RDate" />
                          <ext:RecordField Name="Country" />
                          <ext:RecordField Name="Supplier" />
                          <ext:RecordField Name="InvoiceNo" />
                          <ext:RecordField Name="LCNo" />
                          <ext:RecordField Name="GatePassNo" />
                          <ext:RecordField Name="ReceiveType" />
                          <ext:RecordField Name="LoanCompany" />
                          <ext:RecordField Name="ItemName" />
                          <ext:RecordField Name="GroupName" />
                        <%--  <ext:RecordField Name="BoxUNIT" />
                          <ext:RecordField Name="BoxQty" />--%>
                          <ext:RecordField Name="Unit" />
                          <ext:RecordField Name="TotalQty" />
                           <ext:RecordField Name="TotalAmount" />
                            <ext:RecordField Name="CashMemo" />
                             <ext:RecordField Name="PurchaserName" />
                          <ext:RecordField Name="Notes" />
                    </Fields>
                </ext:JsonReader>
                 
            </Reader>
           
        </ext:Store>
        
        <ext:Window 
            ID="Window1" 
            runat="server"
            Collapsible="true"
            Maximizable="true"
            Icon="Camera"
            Title="Receive Report"
            Width="1000px"
            Height="400"
             Y="380"
             X="200"
           
            Layout="FitLayout" >
            <Items>
                <ext:GridPanel
                    ID="GridPanel1" 
                    runat="server" 
                    StoreID="Store1"
                    StripeRows="true"
                    Header="false"
                    Border="false"
                    >
                   <LoadMask ShowMask="false" />
                    <SelectionModel>
                        <ext:RowSelectionModel 
                            ID="SelectedRowModel1" 
                            runat="server" 
                            SingleSelect="true" 
                            />
                    </SelectionModel>       
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                         
                            <ext:Column ColumnID="SL" Header="SL"  DataIndex="SL" Sortable="true" />
                             <ext:Column ColumnID="RDate" Header="Receive Date"  DataIndex="RDate" Sortable="true" />
                            <ext:Column ColumnID="Country" Header="Country"  DataIndex="Country" Sortable="true" />
                            <ext:Column ColumnID="Supplier" Header="Supplier"  DataIndex="Supplier" Sortable="true" />
                            <ext:Column ColumnID="InvoiceNo" Header="Invoice No"  DataIndex="InvoiceNo" Sortable="true" />
                            <ext:Column ColumnID="LCNo" Header="LCNo"  DataIndex="LCNo" Sortable="true" />
                            <ext:Column ColumnID="GatePassNo" Header="GatePassNo"  DataIndex="GatePassNo" Sortable="true" />
                            <ext:Column ColumnID="LoanCompany" Header="LoanCompany"  DataIndex="LoanCompany" Sortable="true" />
                             <ext:Column ColumnID="GroupName" Header="Group Name"  DataIndex="GroupName" Sortable="true" />
                            <ext:Column ColumnID="ItemName" Header="ItemName"  DataIndex="ItemName" Sortable="true" />
                            <%--
                            <ext:Column ColumnID="BoxUNIT" Header="BoxUNIT"  DataIndex="BoxUNIT" Sortable="true" />
                            <ext:Column ColumnID="BoxQty" Header="BoxQty"  DataIndex="BoxQty" Sortable="true" />--%>
                            <ext:Column ColumnID="TotalQty" Header="TotalQty"  DataIndex="TotalQty" Sortable="true" />
                            <ext:Column ColumnID="Unit" Header="Unit"  DataIndex="Unit" Sortable="true" />
                            <ext:Column ColumnID="CashMemo" Header="Cash Memo No"  DataIndex="CashMemo" Sortable="true" />
                             <ext:Column ColumnID="UnitPrice" Header="Unit Price"  DataIndex="UnitPrice" Sortable="true" />
                            <ext:Column ColumnID="TotalAmount" Header="Total Price"  DataIndex="TotalAmount" Sortable="true" />
                           
                            <ext:Column ColumnID="PurchaserName" Header="Purchaser"  DataIndex="PurchaserName" Sortable="true" />
                            
                            <ext:Column ColumnID="Notes" Header="Notes"  DataIndex="Notes" Sortable="true" />
                            <ext:Column ColumnID="ReceiveType" Header="ReceiveType"  DataIndex="ReceiveType" Sortable="true" />
                            
                       
                       </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                            <Filters>
                                <ext:StringFilter DataIndex="RDate" />
                                <ext:StringFilter DataIndex="Country" />
                                <ext:StringFilter DataIndex="Supplier" />
                                <ext:StringFilter DataIndex="ItemName" />
                                <ext:StringFilter DataIndex="InvoiceNo" />
                                 <ext:NumericFilter DataIndex="GatePassNo" />
                                <ext:NumericFilter DataIndex="TotalAmount" />
                               
                                      
                           
                           
                           </Filters>
                            
                        </ext:GridFilters>
                    </Plugins>
                   
                    <BottomBar>
                        <ext:PagingToolbar 
                            ID="PagingToolBar1" 
                            runat="server" 
                            StoreID="Store1"
                            PageSize="30" 
                            DisplayInfo="true"
                            DisplayMsg="Displaying Records {0} - {1} of {2}"
                            />
                    </BottomBar>
                   
                </ext:GridPanel>
            </Items>
        </ext:Window>

</div>
</asp:Content>

